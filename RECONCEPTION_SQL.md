# Reconception pour 4F5 avec SQL

## Principes

1. uniquement JavaFx et Jdk headless (ne pas tenter version JSweet)

1. «mode librairie» plutôt que Framework

1. mode client/serveur par défaut (pas besoin d'un module pour le créer)

1. graphe des tâches encore utile, mais on va tenter de ne PAS avoir à l'expliquer autant

1. support pour des tâches backend qui charge le modèle à partir de SQL

## Wish list

1. App compagnon
    * visualiser les messages, tâches, etc.

1. tests unitaires

1. gestion des sessions/usagers

1. utiliser vertx pour gérer les threads

1. améliorer les messages d'erreur partout (continuer avec le Fatal, mais améliorer)

1. se débarasser des déclarations obligatoires

1. améliorer le côté client/serveur pour le jeu vidéo (?)
    * messages via UDP
    * messages en binaire (skipper le temps sérialisation Json mode texte)


1. Mode inspection sur les interfaces graphiques

## Outils

1. VS Code
1. DBeaver pour gérer la DB SQlite
1. Bruno comme postman (?)


