/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.app.frontend;


import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import ca.ntro.app.Ntro;
import ca.ntro.core.NtroCore;
import ca.ntro.ntro_core_impl.NtroCoreImpl;
import ca.ntro.ntro_core_impl.path.Path;
import ca.ntro.ntro_core_impl.services.StackAnalyzerImpl;
import javafx.fxml.FXMLLoader;
import javafx.fxml.LoadException;
import javafx.scene.Parent;
import javafx.scene.layout.Pane;

public class ViewLoaderFx<V extends View<?>> implements ViewLoader<V> {
	
	private String fxmlPath;
	private String cssPath;
	private String resourcesPath;
	private Class<? extends View<?>> viewClass;

	public Class<? extends View<?>> getViewClass() {
		return viewClass;
	}

	public void setViewClass(Class<? extends View<?>> viewClass) {
		this.viewClass = viewClass;
	}

	public String getFxmlPath() {
		return fxmlPath;
	}

	public void setFxmlPath(String fxmlPath) {
		this.fxmlPath = fxmlPath;
	}

	public String getCssPath() {
		return cssPath;
	}

	public void setCssPath(String cssPath) {
		this.cssPath = cssPath;
	}

	public String getResourcesPath() {
		return resourcesPath;
	}

	public void setResourcesPath(String resourcesPath) {
		this.resourcesPath = resourcesPath;
	}

	private URL urlFromPath(String path) {
		URL url = null;
		
		if(Ntro.options().useJarResources()) {

			url = viewClass.getResource(path);

		}else {

			try {

				url = Paths.get(path).toUri().toURL();

			} catch (MalformedURLException e) {

				e.printStackTrace();
			}
			
			if(url == null) {
				throw new RuntimeException("Not found " + path);
			}
		}

		return url;
	}

	private String resourceNameFromPath(String path) {
		String resourceName = path.replace("/", ".");

		if(resourceName.startsWith(".")) {
			resourceName = resourceName.substring(1);
		}

		if(resourceName.endsWith(".properties")) {
			int index = resourceName.lastIndexOf(".properties");
			resourceName = resourceName.substring(0,index);
		}

		return resourceName;
	}
	

	private FXMLLoader createFxmlLoader() {
		
		FXMLLoader loader = null;
		
		URL fxmlUrl = urlFromPath(getFxmlPath());
		ResourceBundle resources = loadResourceBundle();
		
		if(resources != null) {

			loader = new FXMLLoader(fxmlUrl, resources);
			
		}else {

			loader = new FXMLLoader(fxmlUrl);
		}
		
		return loader;
	}
	
	public ResourceBundle loadResourceBundle() {
		ResourceBundle resources = null;
		
		if(getResourcesPath() != null) {
			
			try {
				
				resources = ResourceBundle.getBundle(resourceNameFromPath(getResourcesPath()));
				
				if(resources == null) {
					throw new RuntimeException("Resources not found: " + getResourcesPath());
				}

			}catch(MissingResourceException e) {
				
				throw new RuntimeException("Resources not found: " + getResourcesPath());
			}
		}
		
		return resources;
	}


	@SuppressWarnings("unchecked")
	@Override
	public V createView() {
		FXMLLoader loader = createFxmlLoader();

		Parent parent = null;
		try {

			parent = loader.load();

		} catch (Exception e) {
			
			NtroCore.logger().fatal(e);

		}

		if(getCssPath() != null) {
			URL cssUrl = urlFromPath(getCssPath());
			
			if(cssUrl == null) {
				throw new RuntimeException(".css file not found: " + getCssPath());
			}

			parent.getStylesheets().add(cssUrl.toExternalForm());
		}
		
		V view = null;
		try {
			
			view = (V) loader.getController();

			if(viewClass != null
					&& !viewClass.isInstance(view)) {

				NtroCore.logger().fatal("View class mismatch in " + fxmlPath
				                     + "\n\n\tFXML -> fx:controller = \"" + view.getClass().getName() + "\""
						             + "\n\tJava -> registrar.registerView(" 
				                     + NtroCoreImpl.reflection().simpleName(viewClass)
				                     + ".class, \"" + fxmlPath + "\");");
			}

		} catch(Exception e) {

			NtroCore.logger().fatal("Error with fx:controller", e);
		}
		
		if(view == null) {
			NtroCore.logger().fatal("Error with fx:controller (view == null)");
		}

		if(!(view instanceof ViewFx)) {
			NtroCore.logger().fatal("Error with fx:controller (view !instanceof ViewFx)");
		}

		((ViewFx) view).setPane((Pane) parent);

		try {
		
			view.initialize();

		}catch(Exception e) {
			
			NtroCore.logger().fatal("Error loading " + fxmlPath + " (" + NtroCoreImpl.reflection().simpleName(view.getClass()) + ")\n" , e);

		}
		

		return view;
	}

}
