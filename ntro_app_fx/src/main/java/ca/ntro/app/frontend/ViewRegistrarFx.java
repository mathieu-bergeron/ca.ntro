package ca.ntro.app.frontend;

import ca.ntro.ntro_app_fx_impl.frontend.ViewRegistrar;

public interface ViewRegistrarFx extends ViewRegistrar {

	<V extends View<?>> void registerView(Class<V> viewClass, String fxmlPath);
	<V extends View<?>> void registerFragment(Class<V> viewClass, String fxmlPath);

	void registerStylesheet(String cssPath);

}
