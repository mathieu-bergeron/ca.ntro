package ca.ntro.app.session;


import java.util.List;

import ca.ntro.app.Ntro;
import ca.ntro.app.messages.Message;
import ca.ntro.app.models.Model;
import ca.ntro.core.locale.Locale;
import ca.ntro.ntro_app_fx_abstr.models.ModelSelectionsWriter;
import ca.ntro.ntro_app_fx_abstr.session.SessionAbstr;
import ca.ntro.ntro_app_fx_impl.NtroImpl;
import ca.ntro.ntro_app_fx_impl.models.SelectionsByModel;
import ca.ntro.ntro_app_fx_impl.modified.Observable;

public class Session<THIS extends Session<?>> implements SessionAbstr<THIS>, Observable {
	
	private String sessionId = Ntro.random().nextId(4);
	
	private SelectionsByModel modelSelections = new SelectionsByModel();
	
	private transient boolean isNamedSession = false;
	
	void registerSessionId(String sessionId) {
		this.sessionId = sessionId;
		this.isNamedSession = true;
	}

	boolean isNamedSession() {
		return isNamedSession;
	}


	@Override
	public String sessionId() {
		return sessionId;
	}

	@Override
	public Locale locale() {
		return Ntro.currentLocale();
	}

	@SuppressWarnings("unchecked")
	@Override
	public THIS setModelSelection(Class<? extends Model> modelClass, String modelId) {

		if(!modelSelections.isModelSelected(modelClass, modelId)) {

			modelSelections.setModelSelection(modelClass, modelId);

			NtroImpl.messageService().observe(modelClass, modelId);
		}
		
		return (THIS) this;
	}

	@SuppressWarnings("unchecked")
	@Override
	public THIS addModelSelection(Class<? extends Model> modelClass, String modelId) {

		if(!modelSelections.isModelSelected(modelClass, modelId)) {

			modelSelections.addModelSelection(modelClass, modelId);

			NtroImpl.messageService().observe(modelClass, modelId);
		}

		return (THIS) this;
	}

	@SuppressWarnings("unchecked")
	@Override
	public THIS clearModelSelections(Class<? extends Model> modelClass) {
		modelSelections.clearModelSelections(modelClass);
		
		return (THIS) this;
	}

	public <MSG extends Message<?>> void copyModelSelectionsInto(ModelSelectionsWriter<?> otherSelections) {

		modelSelections.modelClasses().forEach(modelClass -> {

			otherSelections.clearModelSelections(modelClass);
			
			modelSelections.selections(modelClass).forEach(modelId -> {

				otherSelections.addModelSelection(modelClass, modelId);

			});
		});

	}

	public boolean isModelSelected(Class<? extends Model> modelClass, String modelId) {
		return modelSelections.isModelSelected(modelClass, modelId);
	}

	@Override
	public boolean hasModelSelections(Class<? extends Model> modelClass) {
		return modelSelections.hasModelSelections(modelClass);
	}

	@Override
	public String getModelSelection(Class<? extends Model> modelClass) {
		return modelSelections.getModelSelection(modelClass);
	}

	@Override
	public List<String> getModelSelections(Class<? extends Model> modelClass) {
		return getModelSelections(modelClass);
	}

	void reObserveModelSelections() {
		modelSelections.modelClasses().forEach(modelClass -> {
			
			modelSelections.selections(modelClass).forEach(modelId -> {

				NtroImpl.messageService().observe(modelClass, modelId);

			});
		});
	}

}
