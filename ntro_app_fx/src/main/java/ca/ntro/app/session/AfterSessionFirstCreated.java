package ca.ntro.app.session;

public interface AfterSessionFirstCreated {
	
	void afterSessionFirstCreated();

}
