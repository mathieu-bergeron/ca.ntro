package ca.ntro.app.session;

public interface SessionRegistrar {
	
	<S extends Session<?>> void registerSessionClass(Class<S> sessionClass);

}
