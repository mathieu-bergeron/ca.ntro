package ca.ntro.app.session;

public interface SessionAccessor {
	
	static void registerSessionId(Session<?> session, String sessionId) {
		session.registerSessionId(sessionId);
	}

	static void reObserveModelSelections(Session<?> session) {
		session.reObserveModelSelections();
		
	}

	static boolean isNamedSession(Session<?> session) {
		return session.isNamedSession();
	}

}
