/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.app.tasks.frontend;

import ca.ntro.app.events.Event;
import ca.ntro.app.frontend.View;
import ca.ntro.app.frontend.ViewLoader;
import ca.ntro.app.modified.Modified;
import ca.ntro.app.modified.Snapshot;
import ca.ntro.app.services.Window;
import ca.ntro.app.tasks.SimpleTaskCreator;
import ca.ntro.app.tasks.TaskGroupCreator;
import ca.ntro.core.NtroCore;
import ca.ntro.ntro_app_fx_abstr.messages.MessageAbstr;
import ca.ntro.ntro_app_fx_abstr.session.SessionAbstr;
import ca.ntro.ntro_core_impl.NtroCoreImpl;
import ca.ntro.ntro_app_fx_impl.modified.Observable;
import ca.ntro.ntro_app_fx_impl.tasks.Tasks;
import ca.ntro.ntro_app_fx_impl.tasks.frontend.ClockDescriptorNtro;
import ca.ntro.ntro_app_fx_impl.tasks.frontend.FrontendEventTaskDescriptorNtro;
import ca.ntro.ntro_app_fx_impl.tasks.frontend.FrontendMessageTaskDescriptorNtro;
import ca.ntro.ntro_app_fx_impl.tasks.frontend.FrontendModifiedTaskDescriptorNtro;
import ca.ntro.ntro_app_fx_impl.tasks.frontend.FrontendSimpleTaskDescriptor;
import ca.ntro.ntro_app_fx_impl.tasks.frontend.FrontendSimpleTaskDescriptorNtro;
import ca.ntro.ntro_app_fx_impl.tasks.frontend.FrontendSnapshotTaskDescriptorNtro;
import ca.ntro.ntro_app_fx_impl.tasks.frontend.FrontendTaskGroupDescriptor;
import ca.ntro.ntro_app_fx_impl.tasks.frontend.FrontendTaskGroupDescriptorNtro;

public interface FrontendTasks extends Tasks {

	SimpleTaskCreator<?> task(String taskId);
	<O> SimpleTaskCreator<O> task(FrontendSimpleTaskDescriptor<O> descriptor);

	TaskGroupCreator<?, FrontendTasks> taskGroup(String taskGroupId);
	<O> TaskGroupCreator<O, FrontendTasks> taskGroup(FrontendTaskGroupDescriptor<O> descriptor);
	

	public static FrontendSimpleTaskDescriptor<Window> window() {
		return new FrontendSimpleTaskDescriptorNtro<>("window");
	}


	public static <O> FrontendSimpleTaskDescriptor<O> viewLoaders() {
		return new FrontendSimpleTaskDescriptorNtro<O>("ViewLoaders");
	}

	public static <O> FrontendSimpleTaskDescriptor<O> create(Class<O> _class) {
		return new FrontendSimpleTaskDescriptorNtro<>(NtroCoreImpl.reflection().simpleName(_class));
	}

	public static <O> FrontendSimpleTaskDescriptor<O> created(Class<O> _class) {
		return new FrontendSimpleTaskDescriptorNtro<O> (NtroCoreImpl.reflection().simpleName(_class));
	}

	public static <V extends View<?>> FrontendSimpleTaskDescriptor<ViewLoader<V>> viewLoader(Class<V> viewClass) {
		return new FrontendSimpleTaskDescriptorNtro<>("viewLoader["+ NtroCoreImpl.reflection().simpleName(viewClass) + "]");
	}

	public static <EVT extends Event> FrontendSimpleTaskDescriptor<EVT> event(Class<EVT> eventClass) {
		return new FrontendSimpleTaskDescriptorNtro<>("event["+ NtroCoreImpl.reflection().simpleName(eventClass) + "]");
	}

	public static <S extends SessionAbstr> FrontendSimpleTaskDescriptor<S> session(Class<S> sessionClass) {
		return new FrontendSimpleTaskDescriptorNtro<>("session["+ NtroCoreImpl.reflection().simpleName(sessionClass) + "]");
	}

	// FIXME: change for Snapshot<R>
	public static <R extends Observable> FrontendSimpleTaskDescriptor<Snapshot<R>> snapshot(Class<R> revisableClass) {
		return new FrontendSnapshotTaskDescriptorNtro<>(revisableClass);
	}

	public static <OBS extends Observable> FrontendSimpleTaskDescriptor<Modified<OBS>> modified(Class<OBS> observableClass) {
		return new FrontendModifiedTaskDescriptorNtro<>(observableClass);
	}

	public static <MSG extends MessageAbstr> FrontendSimpleTaskDescriptor<MSG> message(Class<MSG> messageClass) {
		return new FrontendMessageTaskDescriptorNtro<>(messageClass);
	}

	public static ClockDescriptor clock() {
		return new ClockDescriptorNtro();
	}

}
