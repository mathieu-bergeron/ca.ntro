/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.app.tasks.backend;

import ca.ntro.app.models.Model;
import ca.ntro.app.tasks.SimpleTaskCreator;
import ca.ntro.app.tasks.TaskGroupCreator;
import ca.ntro.core.NtroCore;
import ca.ntro.ntro_app_fx_abstr.messages.MessageAbstr;
import ca.ntro.ntro_core_impl.NtroCoreImpl;
import ca.ntro.ntro_app_fx_impl.modified.Observable;
import ca.ntro.ntro_app_fx_impl.tasks.Tasks;
import ca.ntro.ntro_app_fx_impl.tasks.backend.BackendMessageTaskDescriptorNtro;
import ca.ntro.ntro_app_fx_impl.tasks.backend.BackendModelTaskDescriptorNtro;
import ca.ntro.ntro_app_fx_impl.tasks.backend.BackendSimpleTaskDescriptor;
import ca.ntro.ntro_app_fx_impl.tasks.backend.BackendSimpleTaskDescriptorNtro;
import ca.ntro.ntro_app_fx_impl.tasks.backend.BackendTaskGroupDescriptor;
import ca.ntro.ntro_core_impl.reflection.object_graph.revisions.Revisions;

public interface BackendTasks extends Tasks {

	/**
	 *  Créer une tâche
	 */
	SimpleTaskCreator<?> task(String taskId);

	<O> SimpleTaskCreator<O> task(BackendSimpleTaskDescriptor<O> descriptor);

	TaskGroupCreator<?, BackendTasks> taskGroup(String taskGroupId);

	<O> TaskGroupCreator<O, BackendTasks> taskGroup(BackendTaskGroupDescriptor<O> descriptor);

	public static <O> BackendSimpleTaskDescriptor<O> create(Class<O> _class) {
		return new BackendSimpleTaskDescriptorNtro<>(NtroCoreImpl.reflection().simpleName(_class));
	}

	public static <O> BackendSimpleTaskDescriptor<O> session() {
		return new BackendSimpleTaskDescriptorNtro<>("session");
	}

	public static <O> BackendSimpleTaskDescriptor<O> created(Class<O> _class) {
		return new BackendSimpleTaskDescriptorNtro<O> (NtroCoreImpl.reflection().simpleName(_class));
	}

	public static <MSG extends MessageAbstr> BackendSimpleTaskDescriptor<MSG> message(Class<MSG> messageClass) {
		return new BackendMessageTaskDescriptorNtro<>(messageClass);
	}

	public static <MSG extends MessageAbstr> BackendSimpleTaskDescriptor<MSG> message(Class<MSG> messageClass, String channelId) {
		return new BackendMessageTaskDescriptorNtro<>(messageClass, channelId);
	}

	public static <M extends Model> BackendSimpleTaskDescriptor<M> model(Class<M> modelClass) {
		return new BackendModelTaskDescriptorNtro<>(modelClass);
	}

	public static <M extends Model> BackendSimpleTaskDescriptor<M> model(Class<M> modelClass, String modelId) {
		return new BackendModelTaskDescriptorNtro<>(modelClass, modelId);
	}

	public static <R extends Observable> BackendSimpleTaskDescriptor<Revisions> revisions(Class<R> revisableClass) {
		return new BackendSimpleTaskDescriptorNtro<>("revisions["+ NtroCoreImpl.reflection().simpleName(revisableClass) + "]");
	}

}
