/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.app;


import ca.ntro.app.headless.NtroServerJdk;
import ca.ntro.app.options.SetOptionsLambda;
import ca.ntro.core.NtroCore;
import ca.ntro.ntro_core_impl.NtroCoreImpl;
import ca.ntro.ntro_core_impl.services.StackAnalyzerImpl;
import ca.ntro.ntro_core_impl.values.CodeLocation;
import ca.ntro.ntro_app_fx_impl.InitializerFinalizerFx;
import ca.ntro.ntro_app_fx_impl.NtroAppFxImpl;
import ca.ntro.ntro_app_fx_impl.NtroExecutableImpl;

/**
 * <p>Frontend and Backend in the same program.
 * 
 * <p>The main class must have this form:
 * 
 * <code>
 * <pre>
 * public class MyApp implements NtroAppFx {
 *     
 *     public static void main(String[] args){
 *         NtroAppFx.launch(args);
 *     }
 *     
 *     // ...
 * }
 * </pre>
 * </code>
 * 
 * The two other Ntro configurations are:
 * 
 * <ul>
 * <li>{@link NtroClientFx}: Frontend only (Backend runs in a server we connect to)</li>
 * <li>{@link NtroServerFx}: Backend only (Frontend runs in clients that connect to us)</li>
 * </ul>
 * 
 */

public interface NtroAppFx extends NtroClientFx {
	
	public static void setOptions(SetOptionsLambda lambda) {
		NtroExecutableImpl.registerInitializerFinalizer(InitializerFinalizerFx.class);
		NtroAppFxImpl.instance().setOptions(lambda);
	}

    public static void launch(String[] args) {
    	CodeLocation appLocation = NtroCoreImpl.stackAnalyzer().callerLocation();
    	((StackAnalyzerImpl) NtroCoreImpl.stackAnalyzer()).registerAppLocation(appLocation);

        NtroAppFxImpl.instance().resolveProjectPathIfNeeded(appLocation);

		NtroExecutableImpl.registerInitializerFinalizer(InitializerFinalizerFx.class);

    	NtroAppFxImpl.instance().initializeWithArgs(args);

    	((StackAnalyzerImpl) NtroCoreImpl.stackAnalyzer()).registerAppLocation(appLocation);

        Class<? extends NtroAppFx> appClass = (Class<? extends NtroAppFx>) appLocation._class();

    	NtroAppFxImpl.instance().launch(appClass);
    }

}
