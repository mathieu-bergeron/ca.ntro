package ca.ntro.app.events;

import ca.ntro.ntro_app_fx_impl.services.EventServiceNtro;

public interface EventAccessor {

	static void registerEventService(Event event, EventServiceNtro eventService) {
		event.registerEventService(eventService);
	}

	static void registerTargetId(Event event, String targetId) {
		event.registerTargetId(targetId);
	}

}
