package ca.ntro.app.options;

import ca.ntro.core.options.CoreOptions;

public interface Options {
	
	boolean isProd();
	
	CoreOptions coreOptions();

	boolean graphicalLogger();
	
	boolean useJarResources();
	
	
	

}
