package ca.ntro.app.options;

import ca.ntro.ntro_core_abstr.SetCoreOptionsLambda;

public interface OptionsWriter {
	
	void graphicalLogger(boolean graphicalLogger);
	
	void setCoreOptions(SetCoreOptionsLambda lambda);
	
	void saveJson(String filePath);
	
	void isProd(boolean isProd);

	void useJarResources(boolean useJarResources);

}
