package ca.ntro.app.fx.controls;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class ResizableAvatar extends ResizableImage {
	
	private Color backgroundColor = Color.BLUEVIOLET;
	private double radiusFactor = 1.0;
	
	public ResizableAvatar() {
		super();
	}

	public Color getBackgroundColor() {
		return backgroundColor;
	}

	public void setBackgroundColor(Color backgroundColor) {
		this.backgroundColor = backgroundColor;
	}

	public double getRadiusFactor() {
		return radiusFactor;
	}

	public void setRadiusFactor(double radiusFactor) {
		this.radiusFactor = radiusFactor;
	}

	@Override
	protected void drawImageOnCanvas(GraphicsContext gc) {
		gc.beginPath();
		gc.arc(getWorldWidth()/2, 
			   getWorldHeight()/2, 
			   getWorldWidth()/2 * radiusFactor, 
			   getWorldHeight()/2 * radiusFactor,
			   0,
			   360);
		gc.clip();
		
		gc.setFill(backgroundColor);
		gc.fillRect(0, 0, getWorldWidth(), getWorldHeight());

		gc.drawImage(getImage(), 0, 0);
	}
	
	

}
