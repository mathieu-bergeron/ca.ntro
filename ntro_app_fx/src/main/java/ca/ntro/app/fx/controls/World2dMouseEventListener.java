package ca.ntro.app.fx.controls;

public interface World2dMouseEventListener {
	
	void onMouseEvent(World2dMouseEventFx mouseEvent);

}
