package ca.ntro.app.fx.controls;

public enum ResizeMode {

	NONE,
	RESIZE,
	SCALE;

}
