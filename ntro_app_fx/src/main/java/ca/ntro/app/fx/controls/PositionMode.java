package ca.ntro.app.fx.controls;

public enum PositionMode {
	
	NONE,

	TOP_LEFT,
	TOP_CENTER,
	TOP_RIGHT,
	
	CENTER_LEFT,
	CENTER,
	CENTER_RIGHT,
	
	BOTTOM_LEFT,
	BOTTOM_CENTER,
	BOTTOM_RIGHT;
	

}
