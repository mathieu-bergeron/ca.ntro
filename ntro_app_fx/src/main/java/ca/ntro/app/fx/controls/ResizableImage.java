package ca.ntro.app.fx.controls;

import ca.ntro.core.NtroCore;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

public class ResizableImage extends ResizableWorld2dCanvasFx {
	
	private Image image;

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
		
		if(image != null) {
			setInitialWorldSize(image.getWidth(), image.getHeight());
		}
	}
	
	public ResizableImage() {
	}

	@Override
	protected void initialize() {
		onRedraw(() -> {
			if(image != null) {
				drawOnWorld(gc -> {
					drawImageOnCanvas(gc);
				});
			}
		});
	}

	protected void drawImageOnCanvas(GraphicsContext gc) {
		gc.drawImage(image, 0, 0);
	}
}
