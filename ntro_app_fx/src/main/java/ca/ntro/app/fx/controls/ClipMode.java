package ca.ntro.app.fx.controls;

public enum ClipMode {
	
	CLIP_AT_VIEWPORT,
	CLIP_AT_VIEWSCREEN,
	CLIP_AT_WORLD;

}
