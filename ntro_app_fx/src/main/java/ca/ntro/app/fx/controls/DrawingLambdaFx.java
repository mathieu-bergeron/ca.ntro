package ca.ntro.app.fx.controls;

import javafx.scene.canvas.GraphicsContext;

public interface DrawingLambdaFx {
	
	void draw(GraphicsContext gc);

}
