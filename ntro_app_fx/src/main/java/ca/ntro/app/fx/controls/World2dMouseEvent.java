package ca.ntro.app.fx.controls;

import javafx.scene.input.MouseEvent;

public interface World2dMouseEvent {
	
	MouseEvent mouseEventFx();
	
	double worldX();
	double worldY();

	double viewportX();
	double viewportY();

	double viewscreenX();
	double viewscreenY();

	double canvasX();
	double canvasY();

}
 