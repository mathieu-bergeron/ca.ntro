/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.app.fx.controls;

import java.util.Set;

import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.transform.Affine;

/**
 *  see: https://ntro.ca/4f5/modules/08/theorie/canvas/
 *
 *  world2d:    where the drawing lives
 *  viewport:   the region of the drawing we are displaying
 *  viewscreen: the region of the rawCanvas we are projecting the viewport on
 *  rawCavnas:  the actual JavaFx Canvas
 *
 *  invalidate() when the Canvas needs to be redrawn
 *
 */
public abstract class ResizableWorld2dCanvasFx extends Pane {

	
	private static final double DEFAULT_WIDTH = 640;
	private static final double DEFAULT_HEIGHT = 360;
	
    private Canvas rawCanvas;
    private GraphicsContext rawGc;

    private double epsilon = 3;

    private DrawListener drawListener;
    private EventHandler<MouseEvent> mouseEventHandlerFx;
    
    private Affine drawOnViewscreenTransform = defaultTransform();
    private Affine drawOnViewportTransform   = defaultTransform();
    private Affine drawOnWorldTransform     = defaultTransform();

    private PositionMode   positionMode     = PositionMode.CENTER;
    private ResizeMode     resizeMode       = ResizeMode.SCALE;
    private AspectMode     aspectMode       = AspectMode.PRESERVE_ASPECT_RATIO;
    private Set<ClipMode>  clipModes        = Set.of(ClipMode.CLIP_AT_VIEWPORT, 
    		                                         ClipMode.CLIP_AT_VIEWSCREEN, 
    		                                         ClipMode.CLIP_AT_WORLD);
    
    private double worldWidth = DEFAULT_WIDTH;
    private double worldHeight = DEFAULT_HEIGHT;
;
    private double viewportWidth = DEFAULT_WIDTH;
    private double viewportHeight = DEFAULT_HEIGHT;
    private double viewportTopLeftX = 0;
    private double viewportTopLeftY = 0;

    private double viewscreenWidth = DEFAULT_WIDTH;
    private double viewscreenHeight = DEFAULT_HEIGHT;
    private double viewscreenTopLeftX = 0;
    private double viewscreenTopLeftY = 0;
    
    protected double getEpsilon() {
        return epsilon;
    }

	protected void setEpsilon(int epsilon) {
        this.epsilon = epsilon;
    }
	
	public PositionMode getPositionMode() {
		return positionMode;
	}

	public void setPositionMode(PositionMode positionMode) {
		this.positionMode = positionMode;
	}

	public ResizeMode getResizeMode() {
		return resizeMode;
	}

	public void setResizeMode(ResizeMode resizeMode) {
		this.resizeMode = resizeMode;
	}

	public AspectMode getAspectMode() {
		return aspectMode;
	}

	public void setAspectMode(AspectMode aspectMode) {
		this.aspectMode = aspectMode;
	}

	public Set<ClipMode> getClipModes() {
		return clipModes;
	}

	public void setClipModes(Set<ClipMode> clipModes) {
		this.clipModes = clipModes;
	}

	public double getWorldWidth() {
		return worldWidth;
	}

	public double getCanvasWidth() {
		return getWidth();
	}

	public double getCanvasHeight() {
		return getHeight();
	}

	public void setWorldWidth(double worldWidth) {			
		this.worldWidth = worldWidth;
		recomputeTransforms();
	}

	public double getWorldHeight() {
		return worldHeight;
	}

	public void setWorldHeight(double worldHeight) {
		this.worldHeight = worldHeight;
		recomputeTransforms();
	}

	public double getViewportWidth() {
		return viewportWidth;
	}

	public void setViewportWidth(double viewportWidth) {
		this.viewportWidth = viewportWidth;
		recomputeTransforms();
	}

	public double getViewportHeight() {
		return viewportHeight;
	}

	public void setViewportHeight(double viewportHeight) {
		this.viewportHeight = viewportHeight;
		recomputeTransforms();
		invalidate();
	}

	public double getViewportTopLeftX() {
		return viewportTopLeftX;
	}

	public void setViewportTopLeftX(double viewportTopLeftX) {
		this.viewportTopLeftX = viewportTopLeftX;
		recomputeTransforms();
		invalidate();
	}

	public double getViewportTopLeftY() {
		return viewportTopLeftY;
	}

	public void setViewportTopLeftY(double viewportTopLeftY) {
		this.viewportTopLeftY = viewportTopLeftY;
		recomputeTransforms();
		invalidate();
	}

	public double getViewscreenWidth() {
		return viewscreenWidth;
	}

	public void setViewscreenWidth(double viewscreenWidth) {
		this.viewscreenWidth = viewscreenWidth;
		recomputeTransforms();
		invalidate();
	}

	public double getViewscreenHeight() {
		return viewscreenHeight;
	}

	public void setViewscreenHeight(double viewscreenHeight) {
		this.viewscreenHeight = viewscreenHeight;
		recomputeTransforms();
		invalidate();
	}

	public double getViewscreenTopLeftX() {
		return viewscreenTopLeftX;
	}

	public void setViewscreenTopLeftX(double viewscreenTopLeftX) {
		this.viewscreenTopLeftX = viewscreenTopLeftX;
		recomputeTransforms();
		invalidate();
	}

	public double getViewscreenTopLeftY() {
		return viewscreenTopLeftY;
	}

	public void setViewscreenTopLeftY(double viewscreenTopLeftY) {
		this.viewscreenTopLeftY = viewscreenTopLeftY;
		recomputeTransforms();
		invalidate();
	}

	public void setInitialWorldSize(double worldWidth, double worldHeight) {
		setWorldWidth(worldWidth);
		setWorldHeight(worldHeight);

		setViewportWidth(worldWidth);
		setViewportHeight(worldHeight);
	}

	public ResizableWorld2dCanvasFx() {
		this.setMinWidth(epsilon * 2);
		this.setMinHeight(epsilon * 2);
		initializeCanvas();
		Platform.runLater(() -> {
			initialize();
			invalidate();
		});
    }
	
	protected abstract void initialize();

    public void onRedraw(DrawListener drawListener) {
    	this.drawListener = drawListener;
    }

    public void onMouseEvent(World2dMouseEventListener mouseEventListener) {
    	if(mouseEventHandlerFx != null) {
    		rawCanvas.addEventFilter(MouseEvent.ANY, mouseEventHandlerFx);
    	}
    	
    	mouseEventHandlerFx = new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent rawEvent) {
				mouseEventListener.onMouseEvent(new World2dMouseEventFx(rawEvent, ResizableWorld2dCanvasFx.this));
			}
		};

    	rawCanvas.addEventFilter(MouseEvent.ANY, mouseEventHandlerFx);
    }

    private void initializeCanvas() {
        installRawCanvas();
        
        installWidthObserver();
        installHeightObserver();
    }

    private void installRawCanvas() {
        rawCanvas = new Canvas();
        rawGc = rawCanvas.getGraphicsContext2D();
        this.getChildren().add(rawCanvas);
    }
    
    private void installWidthObserver() {
    	this.widthProperty().addListener((observable, oldValue, newValue) -> {
			resizeRawCanvasIfNeeded();
    	});
    }

    private void installHeightObserver() {
    	this.heightProperty().addListener((observable, oldValue, newValue) -> {
			resizeRawCanvasIfNeeded();
    	});
    }

    private void resizeRawCanvasIfNeeded() {
        if(shouldResizeRawCanvas()) {
            resizeRawCanvas();
        }
    }

	private boolean shouldResizeRawCanvas() {
		return Math.abs(getWidth() - rawCanvas.getWidth()) > epsilon 
				|| Math.abs(getHeight() - rawCanvas.getHeight()) > epsilon;
	}

	private void resizeRawCanvas() {
		rawCanvas.setWidth(this.getWidth());
		rawCanvas.setHeight(this.getHeight());

		if(sizesAreValid()) {
			resizeRelocateRegions();
			recomputeTransforms();
		}

		invalidate();
	}
	
	public void invalidate() {
		if(drawListener != null) {
			clearCanvas();
			drawListener.draw();
		}
	}

	public void clearCanvas() {
		rawGc.clearRect(0, 
						0, 
						rawCanvas.getWidth(), 
						rawCanvas.getHeight());
	}

	private Affine defaultTransform() {
		Affine defaultTransform = new Affine();
		return defaultTransform;
	}

	private void resizeRelocateRegions() {
		if(aspectMode == AspectMode.PRESERVE_ASPECT_RATIO) {
			
			resizeRelocateRegionsPreserveAspectRatio();
			
		}else {
			
			resizeRelocateRegionsIgnoreAspectRatio();

		}
	}

	private void resizeRelocateRegionsIgnoreAspectRatio() {
		viewscreenWidth = rawCanvas.getWidth();
		viewscreenHeight = rawCanvas.getHeight();

		viewscreenTopLeftX = 0;
		viewscreenTopLeftY = 0;
	}

	private void resizeRelocateRegionsPreserveAspectRatio() {
		double viewportAspectRatio = viewportWidth / viewportHeight;
		double rawAspectRatio = rawCanvas.getWidth() / rawCanvas.getHeight();

		if(viewportAspectRatio > rawAspectRatio) {
			
			viewscreenWidth = rawCanvas.getWidth();
			viewscreenHeight = viewscreenWidth / viewportAspectRatio;

			viewscreenTopLeftX = 0;
			
			switch(positionMode) {

				case TOP_CENTER:
				case TOP_LEFT:
				case TOP_RIGHT:
					viewscreenTopLeftY = 0;
				break;

				case BOTTOM_CENTER:
				case BOTTOM_LEFT:
				case BOTTOM_RIGHT:
					viewscreenTopLeftY = rawCanvas.getHeight() - viewscreenHeight;
				break;

				case CENTER:
				case CENTER_LEFT:
				case CENTER_RIGHT:
				default:
					viewscreenTopLeftY = (rawCanvas.getHeight() - viewscreenHeight) / 2;
			}

		}else {
			
			viewscreenHeight = rawCanvas.getHeight();
			viewscreenWidth = viewscreenHeight * viewportAspectRatio;

			viewscreenTopLeftY = 0;

			switch(positionMode) {

				case TOP_LEFT:
				case CENTER_LEFT:
				case BOTTOM_LEFT:
					viewscreenTopLeftX = 0;
				break;

				case TOP_RIGHT:
				case CENTER_RIGHT:
				case BOTTOM_RIGHT:
					viewscreenTopLeftX = rawCanvas.getWidth() - viewscreenWidth;
				break;

				case TOP_CENTER:
				case CENTER:
				case BOTTOM_CENTER:
				default:
					viewscreenTopLeftX = (rawCanvas.getWidth() - viewscreenWidth) / 2;
			}
		}
	}


	void recomputeTransforms() {
		drawOnViewscreenTransform = defaultTransform();
		drawOnViewscreenTransform.setTx(viewscreenTopLeftX);
		drawOnViewscreenTransform.setTy(viewscreenTopLeftY);
		
		drawOnViewportTransform = defaultTransform();
		drawOnViewportTransform.append(drawOnViewscreenTransform);
		drawOnViewportTransform.setMxx(viewscreenWidth / viewportWidth);
		drawOnViewportTransform.setMyy(viewscreenHeight / viewportHeight);
		
		drawOnWorldTransform = defaultTransform();
		drawOnWorldTransform.append(drawOnViewportTransform);
		drawOnWorldTransform.setTx(drawOnWorldTransform.getTx() - viewportTopLeftX * drawOnWorldTransform.getMxx());
		drawOnWorldTransform.setTy(drawOnWorldTransform.getTy() - viewportTopLeftY * drawOnWorldTransform.getMyy());
		
	}

	private boolean sizesAreValid() {
		return rawCanvas.getWidth() > 0 
				&& rawCanvas.getHeight() > 0
				&& worldWidth > 0
				&& worldHeight > 0
				&& viewscreenWidth > 0
				&& viewscreenHeight > 0
				&& viewportWidth > 0
				&& viewportHeight > 0;
	}

	public void drawOnCanvas(DrawingLambdaFx lambda) {
		rawGc.save();

		rawGc.beginPath();
		rawGc.rect(0, 
				   0, 
				   rawCanvas.getWidth(), 
				   rawCanvas.getHeight());
		rawGc.clip();

		lambda.draw(rawGc);
		
		rawGc.restore();
	}

	public void drawOnViewscreen(DrawingLambdaFx lambda) {
		rawGc.save();
		
		rawGc.setTransform(drawOnViewscreenTransform);
		
		if(clipModes.contains(ClipMode.CLIP_AT_VIEWSCREEN)) {
			rawGc.beginPath();
			rawGc.rect(0, 
					   0, 
					   viewscreenWidth, 
					   viewscreenHeight);
			rawGc.clip();
		}

		lambda.draw(rawGc);
		
		rawGc.restore();
	}

	public void drawOnViewport(DrawingLambdaFx lambda) {
		rawGc.save();

		rawGc.setTransform(drawOnViewportTransform);

		if(clipModes.contains(ClipMode.CLIP_AT_VIEWPORT)) {
			rawGc.beginPath();
			rawGc.rect(0, 
					   0, 
					   viewportWidth, 
					   viewportHeight);
			rawGc.clip();
		}

		lambda.draw(rawGc);
		
		rawGc.restore();
	}
	
	public void drawOnWorld(DrawingLambdaFx lambda) {
		rawGc.save();

		rawGc.setTransform(drawOnWorldTransform);

		if(clipModes.contains(ClipMode.CLIP_AT_VIEWPORT)) {
			rawGc.beginPath();
			rawGc.rect(viewportTopLeftX, 
					   viewportTopLeftY, 
					   viewportWidth, 
					   viewportHeight);
			rawGc.clip();
		}
		
		if(clipModes.contains(ClipMode.CLIP_AT_WORLD)) {
			rawGc.beginPath();
			rawGc.rect(0, 
					   0, 
					   worldWidth, 
					   worldHeight);
			rawGc.clip();
		}
		
		lambda.draw(rawGc);
		
		rawGc.restore();
	}

	public double worldWidthFromCanvasWidth(double canvasWidth) {
		return canvasWidth / drawOnWorldTransform.getMxx();
	}

	public double worldHeightFromCanvasHeight(double canvasHeight) {
		return canvasHeight / drawOnWorldTransform.getMyy();
	}

	public double canvasWidthFromWorldWidth(double worldWidth) {
		return worldWidth * drawOnWorldTransform.getMxx();
	}

	public double canvasHeightFromWorldHeight(double worldHeight) {
		return worldHeight * drawOnWorldTransform.getMyy();
	}

	public double worldXFromCanvasX(double canvasX) {
		return (canvasX - drawOnWorldTransform.getTx()) / drawOnWorldTransform.getMxx();
	}

	public double worldYFromCanvasY(double canvasY) {
		return (canvasY - drawOnWorldTransform.getTy()) / drawOnWorldTransform.getMyy();
	}


	public double viewportXFromCanvasX(double canvasX) {
		return (canvasX - drawOnViewportTransform.getTx()) / drawOnViewportTransform.getMxx();
	}

	public double viewportYFromCanvasY(double canvasY) {
		return (canvasY - drawOnViewportTransform.getTy()) / drawOnViewportTransform.getMyy();
	}


	public double viewscreenXFromCanvasX(double canvasX) {
		return (canvasX - drawOnViewscreenTransform.getTx()) / drawOnViewscreenTransform.getMxx();
	}

	public double viewscreenYFromCanvasY(double canvasY) {
		return (canvasY - drawOnViewscreenTransform.getTy()) / drawOnViewscreenTransform.getMyy();
	}

	public void relocateResizeViewport(double topLeftX, 
			                           double topLeftY, 
			                           double width, 
			                           double height) {

		this.viewportTopLeftX = topLeftX;
		this.viewportTopLeftY = topLeftY;
		this.viewportWidth = width;
		this.viewportHeight = height;

		if(sizesAreValid()) {
			resizeRelocateRegions();
			recomputeTransforms();
		}
		
		invalidate();
	}

	public void relocateViewport(double topLeftX, double topLeftY) {
		this.viewportTopLeftX = topLeftX;
		this.viewportTopLeftY = topLeftY;

		if(sizesAreValid()) {
			resizeRelocateRegions();
			recomputeTransforms();
		}

		invalidate();
	}





}
