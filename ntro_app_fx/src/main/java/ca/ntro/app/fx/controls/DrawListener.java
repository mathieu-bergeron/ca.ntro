package ca.ntro.app.fx.controls;

public interface DrawListener {

	void draw();

}
