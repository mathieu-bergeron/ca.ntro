package ca.ntro.app.fx.controls;

import javafx.scene.input.MouseEvent;

public class World2dMouseEventFx implements World2dMouseEvent {
	
	private MouseEvent rawMouseEvent;
	private ResizableWorld2dCanvasFx resizableCanvas;
	
	public World2dMouseEventFx() {
	}
	
	public World2dMouseEventFx(MouseEvent rawMouseEvent, ResizableWorld2dCanvasFx resizableCanvas) {
		this.rawMouseEvent = rawMouseEvent;
		this.resizableCanvas = resizableCanvas;
	}

	@Override
	public MouseEvent mouseEventFx() {
		return rawMouseEvent;
	}

	@Override
	public double worldX() {
		return resizableCanvas.worldXFromCanvasX(canvasX());
	}

	@Override
	public double worldY() {
		return resizableCanvas.worldYFromCanvasY(canvasY());
	}

	@Override
	public double viewportX() {
		return resizableCanvas.viewportXFromCanvasX(canvasX());
	}

	@Override
	public double viewportY() {
		return resizableCanvas.viewportYFromCanvasY(canvasY());
	}

	@Override
	public double viewscreenX() {
		return resizableCanvas.viewscreenXFromCanvasX(canvasX());
	}

	@Override
	public double viewscreenY() {
		return resizableCanvas.viewscreenYFromCanvasY(canvasY());
	}

	@Override
	public double canvasX() {
		return rawMouseEvent.getX();
	}

	@Override
	public double canvasY() {
		return rawMouseEvent.getY();
	}

	public double worldWidthFromCanvasWidth(double d) {
		return resizableCanvas.worldWidthFromCanvasWidth(d);
	}

	public double worldHeightFromCanvasHeight(double d) {
		return resizableCanvas.worldHeightFromCanvasHeight(d);
	}

	public double canvasWidthFromWorldWidth(double worldWidth) {
		return resizableCanvas.canvasWidthFromWorldWidth(worldWidth);
	}

	public double canvasHeightFromWorldHeight(double worldHeight) {
		return resizableCanvas.canvasHeightFromWorldHeight(worldHeight);
	}


}
