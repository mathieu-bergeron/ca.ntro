package ca.ntro.app.fx.controls;

public interface ResizeListener {
	
	void onResize();

}
