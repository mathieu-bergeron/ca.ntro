package ca.ntro.app.fx.controls;

public enum AspectMode {
	
	PRESERVE_ASPECT_RATIO,
	IGNORE_ASPECT_RATIO;

}
