package ca.ntro.app.common;

import java.net.BindException;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

import ca.ntro.app.Ntro;
import ca.ntro.app.messages.Message;
import ca.ntro.app.messages.MessageAccessor;
import ca.ntro.core.NtroCore;
import ca.ntro.ntro_app_fx_abstr.messages.MessageAbstr;
import ca.ntro.ntro_app_fx_impl.NtroImpl;
import ca.ntro.ntro_core_impl.NtroCoreImpl;
import ca.ntro.ntro_app_fx_impl.messages.BroadcastMessage;
import ca.ntro.ntro_app_fx_impl.messages.BroadcastMessageImpl;
import ca.ntro.ntro_app_fx_impl.messages.DeliveryMode;
import ca.ntro.ntro_app_fx_impl.messages.MessageFromServerHandler;
import ca.ntro.ntro_app_fx_impl.messages.MessageServer;
import ca.ntro.ntro_app_fx_impl.messages.MessageServiceImpl;
import ca.ntro.ntro_app_fx_impl.messages.ObservationFromServerHandler;
import ca.ntro.ntro_app_fx_impl.messages.RegisterSession;
import ca.ntro.ntro_app_fx_impl.messages.RegisterSessionImpl;
import ca.ntro.ntro_app_fx_impl.messages.RequestFirstObservationImpl;
import ca.ntro.ntro_app_fx_impl.messages.SendMessageTo;
import ca.ntro.ntro_app_fx_impl.messages.SendMessageToImpl;
import ca.ntro.ntro_app_fx_impl.modified.Observable;
import ca.ntro.ntro_app_fx_impl.modified.Observation;
import ca.ntro.ntro_core_impl.json.JsonObject;
import ca.ntro.ntro_core_impl.reflection.object_graph.ObjectGraph;

public class WebSocketServerNtro extends WebSocketServer implements MessageServer {
	
	private int port;
	private String serverName;
	private Set<WebSocket> connections = new HashSet<>();
	private Map<String, WebSocket> connectionBySessionId = new HashMap<>();

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	public WebSocketServerNtro(String serverName, int port) {
		super(new InetSocketAddress(serverName, port));
		
		setServerName(serverName);
		setPort(port);

        NtroCore.factory().registerNamedClass(BroadcastMessageImpl.class);
        NtroCore.factory().registerNamedClass(RequestFirstObservationImpl.class);
        NtroCore.factory().registerNamedClass(SendMessageToImpl.class);
        NtroCore.factory().registerNamedClass(RegisterSessionImpl.class);
	}

	@Override
	public void onOpen(WebSocket conn, ClientHandshake handshake) {
		connections.add(conn);
	}

	@Override
	public void onClose(WebSocket conn, int code, String reason, boolean remote) {
		connections.remove(conn);
	}

	@Override
	public void onMessage(WebSocket conn, String messageText) {
		NtroCoreImpl.logger().info("onMessage: " + messageText);

		try {
			JsonObject jsonObject = NtroCore.json().fromJsonString(messageText);
			ObjectGraph objectGraph = NtroCoreImpl.reflection().graphFromJsonObject(jsonObject);

			Object messageObject = NtroCoreImpl.reflection().objectFromGraph(objectGraph);

			if(messageObject instanceof RegisterSession) {
				
				RegisterSession registerSession = (RegisterSession) messageObject;
				
				String sessionId = registerSession.sessionId();
				
				Ntro.logger().info("Session connected: " + sessionId);
				
				connectionBySessionId.put(sessionId, conn);

			} else if(messageObject instanceof RequestFirstObservationImpl) {
				
				RequestFirstObservationImpl requestFirstObservationNtro = (RequestFirstObservationImpl) messageObject;
				
				Class<? extends Observable> modelClass = (Class<? extends Observable>) NtroCore.factory().namedClass(requestFirstObservationNtro.getClassName());
				
				NtroImpl.models().watch(modelClass, requestFirstObservationNtro.getModelId());

			} else if(messageObject instanceof BroadcastMessage) {
				
				BroadcastMessage broadcastMessage = (BroadcastMessage) messageObject;
				MessageAbstr message = broadcastMessage.message();
				
				broadcastMessage(conn, message);

			} else if(messageObject instanceof SendMessageTo) {
				
				SendMessageTo sendMessageTo = (SendMessageTo) messageObject;
				MessageAbstr message = sendMessageTo.message();
				String targetSessionId = sendMessageTo.targetSessionId();
				
				sendMessageTo(message, targetSessionId);

			} else if(messageObject instanceof Message) {
				
				Message<?> message = (Message<?>) messageObject;
				
				MessageAccessor.registerMessageService(message, (MessageServiceImpl) NtroImpl.messageService());

				NtroImpl.messageService().receiveMessageFromServer(message);

			}



		}catch(Throwable t) {

			System.out.println("\n\n[ERROR] onMessage: " + messageText + "\n\n");
			t.printStackTrace();

		}
		
	}

	private void sendMessageTo(MessageAbstr message, String targetSessionId) {

		WebSocket connection = connectionBySessionId.get(targetSessionId);
		
		if(connection != null) {
			
			String messageText = NtroCoreImpl.reflection().toJsonObject(message).toJsonString(false);
			connection.send(messageText);
			
		}
	}

	private void broadcastMessage(WebSocket conn, MessageAbstr message) {
		String messageText = NtroCoreImpl.reflection().toJsonObject(message).toJsonString(false);

		for(WebSocket broadcastTo : connections) {
			if(!broadcastTo.equals(conn)) {
				broadcastTo.send(messageText);
			}
		}
	}

	@Override
	public void onError(WebSocket conn, Exception ex) {
		if(ex instanceof BindException) {
			
			NtroCoreImpl.logger().fatal("Cannot listen on ws://" + getServerName() + ":" + getPort() + "\nPort already in use?");

		}else {

			NtroCoreImpl.logger().fatal(ex);
		}
	}

	@Override
	public void onStart() {
		NtroCore.logger().info("Listening on ws://" + getServerName() + ":" + getPort() + "\n\n\n");
		NtroImpl.messageService().registerMessageServer(this, DeliveryMode.SERVER_MODE);
	}

	@Override
	public void sendMessageToServer(MessageAbstr message) {
	}

	@Override
	public void broadcastMessageToOtherClients(MessageAbstr message) {
		// XXX: broadcast to all clients
		String messageText = NtroCoreImpl.reflection().toJsonObject(message).toJsonString(false);
		
		for(WebSocket broadcastTo : connections) {
			broadcastTo.send(messageText);
		}
	}

	@Override
	public <O extends Observable> void pushObservationToClients(Class<O> observableClass, 
			                                                    String observableId, 
			                                                    Observation<?> observation) {

		for(WebSocket client : connections) {
			pushObservationToClient(client, observation);
		}
	}

	@Override
	public <O extends Observable> void pushObservationToClients(Class<O> observableClass, 
			                                                    Observation<?> observation) {

		pushObservationToClients(observableClass, null, observation);
		
	}

	private void pushObservationToClient(WebSocket client, Observation<?> observation) {
		client.send(NtroCoreImpl.reflection().toJsonObject(observation).toJsonString(false));
	}

	@Override
	public void onMessageFromServer(MessageFromServerHandler handler) {
	}

	@Override
	public void onObservationFromServer(ObservationFromServerHandler handler) {
	}

	@Override
	public <O extends Observable> void requestFirstObservationFromServer(Class<O> observableClass, 
			                                                             String observableId) {
		
		// XXX: not supported here
		
	}


}
