/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.app.common;

import ca.ntro.ntro_app_fx_impl.executable_spec.ServerSpec;

public class ServerRegistrarNtro implements ServerRegistrar {
	
	private int port          = ServerSpec.DEFAULT_PORT;
	private String serverName = ServerSpec.DEFAULT_HOSTNAME;

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	@Override
	public void registerPort(int port) {
		setPort(port);
	}

	@Override
	public void registerName(String serverName) {
		setServerName(serverName);
	}

	public void saveSpec(String backendHostname, Integer backendPort) {
		backendHostname = this.serverName;
		backendPort = this.port;
	}

}
