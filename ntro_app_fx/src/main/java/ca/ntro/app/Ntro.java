/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.app;

import ca.ntro.app.events.Event;

import ca.ntro.app.messages.Message;
import ca.ntro.app.models.Model;
import ca.ntro.app.options.Options;
import ca.ntro.app.session.Session;
import ca.ntro.core.NtroCore;
import ca.ntro.core.locale.Locale;
import ca.ntro.core.services.Asserter;
import ca.ntro.core.services.ExitHandler;
import ca.ntro.core.services.LocaleService;
import ca.ntro.core.services.Logger;
import ca.ntro.core.services.RandomService;
import ca.ntro.core.services.Time;
import ca.ntro.ntro_app_fx_impl.NtroImpl;
import ca.ntro.ntro_core_impl.NtroCoreImpl;

/**
 * <p>Main class for calling Ntro
 * 
 * <p>For example:
 * 
 * <ul>
 * <li><code>Ntro.session()</code>: returns the current session object</li>
 * <li><code>Ntro.newMessage(...): creates and returns a fully initialized message</code></li>
 * <li><code>Ntro.logger().info(...)</code>: displays/logs some info string</li>
 * <li><code>Ntro.assertNotNull(...)</code>: fails if some value is null</li>
 * <li>...</li>
 * </ul>
 * 
 */

public class Ntro {
	
	private Ntro() {}
	

	/* <Messages> */

	public static <MSG extends Message> MSG newMessage(Class<MSG> messageClass) {
		return NtroImpl.messageService().newMessage(messageClass);
	}

	/* </Messages> */


	
	/* <Events> */

	public static <E extends Event> E newEvent(Class<E> eventClass) {
		return NtroImpl.eventService().newEvent(eventClass);
	}

	/* </Events> */

	/* <ModelStore> */

	public static <M extends Model> void deleteModel(Class<M> modelClass) {
		NtroImpl.models().delete(modelClass);
	}

	public static <M extends Model> void deleteModel(Class<M> modelClass, String modelId) {
		NtroImpl.models().delete(modelClass, modelId);
	}

	/* </ModelStore> */
	
	
	
	

	
	/* <Options> */
	
	public static Options options(){
		return NtroImpl.options();
	}


	/* </Options> */


	/* <Asserter> */
	
	public static Asserter asserter(){
		return NtroCoreImpl.asserter();
	}

	public static void assertNotNull(Object value) {
		NtroCoreImpl.asserter().assertNotNull(value);
	}

	/* </Asserter> */

	/* <Locale> */


	public static LocaleService locale() {

		return NtroCoreImpl.locale();

	}

	public static Locale currentLocale() {
		return NtroCoreImpl.locale().currentLocale();
	}

	public static Locale buildLocale(String language) {
		return NtroCoreImpl.locale().buildLocale(language);
	}

	public static Locale buildLocale(String language, String country) {
		return NtroCoreImpl.locale().buildLocale(language, country);
	}

	public static void changeLocale(Locale newLocale) {
		NtroCoreImpl.locale().changeLocale(newLocale);
	}

	
	/* </Locale> */

	/* <Logger> */

	public static Logger logger(){
		return NtroCoreImpl.logger();
	}

	/* </Logger> */


	/* <Random> */

	public static RandomService random(){
		return NtroCoreImpl.random();
	}

	/* </Random> */
	
	
	/* <Exit> */
	
	public static void exit(ExitHandler exitHandler) {
		NtroCore.exit(exitHandler);
	}

	public static void exit() {
		NtroCore.exit();
	}

	/* </Exit> */
	
	/* <Session> */
	
	public static <S extends Session<S>> S session() {
		return NtroImpl.session();
	}

	public static <S extends Session<S>> S session(Class<S> sessionClass) {
		return NtroImpl.session();
	}

	/* </Session> */


	/* <Mode> */

	public static boolean isApp() {
		return NtroImpl.isApp();
	}
	
	public static boolean isClient() {
		return NtroImpl.isClient();
	}

	public static boolean isServer() {
		return NtroImpl.isServer();
	}

	/* </Mode> */

	/* <OS> */

	public static boolean isWindows() {
		return NtroImpl.isWindows();
	}
	
	public static boolean isLinux() {
		return NtroImpl.isLinux();
	}

	public static boolean isMac() {
		return NtroImpl.isMac();
	}

	/* </OS> */
	
	
	/* <Time> */

	public static Time time() {
		return NtroCoreImpl.time();
	}

	/* </Time> */
	
}
