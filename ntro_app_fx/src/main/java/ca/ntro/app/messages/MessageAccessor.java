package ca.ntro.app.messages;

import ca.ntro.ntro_app_fx_impl.messages.MessageServiceImpl;
import ca.ntro.ntro_app_fx_impl.models.SelectionsByModel;

public interface MessageAccessor {
	
	static void registerMessageService(Message<?> message, MessageServiceImpl messageServiceImpl) {
		message.registerMessageService(messageServiceImpl);
	}

	static SelectionsByModel getModelSelections(Message<?> message) {
		return message.getModelSelections();
	}

	static void mergeCompatibleModelSelectionsInto(Message<?> message, SelectionsByModel otherModelSelections) {
		message.mergeCompatibleModelSelectionsInto(otherModelSelections);
	}

}
