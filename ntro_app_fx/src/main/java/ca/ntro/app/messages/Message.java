/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.app.messages;

import ca.ntro.app.models.Model;
import ca.ntro.ntro_app_fx_abstr.messages.MessageAbstr;
import ca.ntro.ntro_app_fx_impl.messages.MessageServiceImpl;
import ca.ntro.ntro_app_fx_impl.models.SelectionsByModel;

public class Message<THIS extends Message<?>> implements MessageAbstr<THIS> {
	
	private transient MessageServiceImpl messageService;
	
	private SelectionsByModel modelSelections = new SelectionsByModel();

	void registerMessageService(MessageServiceImpl messageService) {
		this.messageService = messageService;
	}

	public void send() {
		messageService.deliverMessage(this);
	}

	public void sendTo(String sessionId) {
		messageService.deliverMessageTo(this, sessionId);
	}

	public void broadcast() {
		messageService.broadcastMessageToOtherClients(this);
	}

	SelectionsByModel getModelSelections() {
		return modelSelections;
	}

	@Override
	public THIS setModelSelection(Class<? extends Model> modelClass, String modelId) {
		modelSelections.setModelSelection(modelClass, modelId);
		
		return (THIS) this;
	}

	@Override
	public THIS addModelSelection(Class<? extends Model> modelClass, String modelId) {
		modelSelections.addModelSelection(modelClass, modelId);
		
		return (THIS) this;
	}

	@Override
	public THIS clearModelSelections(Class<? extends Model> modelClass) {
		modelSelections.clearModelSelections(modelClass);
		
		return (THIS) this;
	}

	void mergeCompatibleModelSelectionsInto(SelectionsByModel otherModelSelections) {
		otherModelSelections.restricTo(modelSelections);
		
	}

}
