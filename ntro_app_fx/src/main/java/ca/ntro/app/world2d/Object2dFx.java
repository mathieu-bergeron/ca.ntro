package ca.ntro.app.world2d;

import ca.ntro.app.fx.controls.World2dMouseEventFx;
import ca.ntro.app.models.ModelValue;
import ca.ntro.ntro_app_fx_impl.world2d.region_tree.AnonymousRegion2d;
import javafx.scene.canvas.GraphicsContext;

public abstract class Object2dFx<WORLD2D extends World2dFx> implements ModelValue {
	
	private WORLD2D world2d;
	private double topLeftX;
	private double topLeftY;
	private double width; 
	private double height;
	private double speedX;
	private double speedY;

	public WORLD2D getWorld2d() {
		return world2d;
	}

	public void setWorld2d(WORLD2D world2d) {
		this.world2d = world2d;
	}

	public double getTopLeftX() {
		return topLeftX;
	}

	public void setTopLeftX(double topLeftX) {
		this.topLeftX = topLeftX;
	}

	public double getTopLeftY() {
		return topLeftY;
	}

	public void setTopLeftY(double topLeftY) {
		this.topLeftY = topLeftY;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public double getSpeedX() {
		return speedX;
	}

	public void setSpeedX(double speedX) {
		this.speedX = speedX;
	}

	public double getSpeedY() {
		return speedY;
	}

	public void setSpeedY(double speedY) {
		this.speedY = speedY;
	}

	public abstract String id();
	public abstract void initialize();
	/**
	 *  not called in headless mode
	 *  use this to initialize Fx objects
	 */
	public void initializeFx() {}

	public abstract void drawOnWorld(GraphicsContext gc);

	public void onTimePasses(double secondsElapsed) {
		topLeftX += speedX * secondsElapsed;
		topLeftY += speedY * secondsElapsed;
	}
	
	public boolean collidesWith(Object2dFx other) {
		return other.collidesWith(topLeftX, topLeftY, width, height);
	}

	public boolean collidesWith(double topLeftX, double topLeftY, double width, double height) {
		return collidesOneAxis(this.topLeftX, this.width, topLeftX, width)
				&& collidesOneAxis(this.topLeftY, this.height, topLeftY, height);
	}
	
	private boolean collidesOneAxis(double coord1, double size1, double coord2, double size2) {
		return (coord1 < coord2 && coord1 + size1 >= coord2)
				|| (coord2 <= coord1 && coord2 + size2 >= coord1);
	}
	

	protected abstract boolean onMouseEvent(World2dMouseEventFx mouseEvent);

	public void copyDataFrom(Object2dFx object2d) {
		setTopLeftX(object2d.getTopLeftX());
		setTopLeftY(object2d.getTopLeftY());
		setWidth(object2d.getWidth());
		setHeight(object2d.getHeight());
		setSpeedX(object2d.getSpeedX());
		setSpeedY(object2d.getSpeedY());
	}
	
	public boolean isContainedIn(AnonymousRegion2d otherRegion, double epsilon) {
		return isContainedIn(otherRegion.topLeftX(),
				             otherRegion.topLeftY(),
				             otherRegion.width(),
				             otherRegion.height(),
				             epsilon);
	}

	public boolean isContainedIn(double topLeftX, 
			                     double topLeftY, 
			                     double width, 
			                     double height, 
			                     double epsilon) {
		
		return AnonymousRegion2d.isContainedIn(this.topLeftX,
				                               this.topLeftY,
				                               this.width,
				                               this.height,
				                               topLeftX,
				                               topLeftY,
				                               width,
				                               height,
				                               epsilon);
	}


	public boolean intersectsWith(AnonymousRegion2d otherRegion, double epsilon) {
		return intersectsWith(otherRegion.topLeftX(),
				              otherRegion.topLeftY(),
				              otherRegion.width(),
				              otherRegion.height(),
				              epsilon);
	}

	public boolean intersectsWith(double topLeftX, 
			                      double topLeftY, 
			                      double width, 
			                      double height, 
			                      double epsilon) {
		
		return AnonymousRegion2d.intersectsWith(this.topLeftX,
				                                this.topLeftY,
				                                this.width,
				                                this.height,
				                                topLeftX,
				                                topLeftY,
				                                width,
				                                height,
				                                epsilon);
	}

	public boolean isEqualTo(AnonymousRegion2d otherRegion, double epsilon) {
		return isEqualTo(otherRegion.topLeftX(),
				         otherRegion.topLeftY(),
				         otherRegion.width(),
				         otherRegion.height(),
				         epsilon);
	}

	public boolean isEqualTo(double topLeftX, 
			                 double topLeftY, 
			                 double width, 
			                 double height,
			                 double epsilon) {
		
		return AnonymousRegion2d.isEqualTo(this.topLeftX, 
				                           this.topLeftY,
				                           this.width,
				                           this.height,
				                           topLeftX,
				                           topLeftY,
				                           width,
				                           height,
				                           epsilon);
	}





}
