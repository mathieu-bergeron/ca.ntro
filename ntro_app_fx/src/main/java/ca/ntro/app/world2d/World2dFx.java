package ca.ntro.app.world2d;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import ca.ntro.app.fx.controls.ResizableWorld2dCanvasFx;
import ca.ntro.app.fx.controls.World2dMouseEventFx;
import ca.ntro.app.models.ModelValue;
import ca.ntro.ntro_app_fx_impl.world2d.region_tree.AnonymousRegion2dNtro;

public abstract class World2dFx implements ModelValue {

	private final double INTERSECT_EPSILON = 5;

	private double width = 640;
	private double height = 360;

	private List<Object2dFx<?>> objects = new ArrayList<>();

	private List<String> idsOfObjectsToRemove = new ArrayList<>();
	private List<Object2dFx<?>> objectsToAdd = new ArrayList<>();
	
	private boolean clipAtViewport = true;

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public List<Object2dFx<?>> getObjects() {
		return objects;
	}

	public void setObjects(List<Object2dFx<?>> objects) {
		this.objects = objects;
	}
	
	public boolean getClipAtViewport() {
		return clipAtViewport;
	}

	public void setClipAtViewport(boolean clipAtViewport) {
		this.clipAtViewport = clipAtViewport;
	}

	public World2dFx() {
        initialize();
	}
	
	
	protected abstract void initialize();
	protected abstract void onMouseEventNotConsumed(World2dMouseEventFx mouseEvent);
	
	public void initializeFx() {
		synchronized (objects) {
			for(Object2dFx<?> object2d: objects) {
				object2d.initializeFx();
			}
		}
	}
	

	/**
	 * Adds an object to the world2d
	 * Calls initialize of the object
	 * @param object2d
	 */
	@SuppressWarnings("unchecked")
	public <W extends World2dFx> void addObject2d(Object2dFx<W> object2d) {
		object2d.setWorld2d((W) this);
		object2d.initialize();
		objectsToAdd.add(object2d);
	}

	/**
	 * Calls drawOn for each object added to the world2d
	 * @param canvas
	 */
	public void drawOn(ResizableWorld2dCanvasFx canvas) {
		updateObjects();

		AnonymousRegion2dNtro viewport = new AnonymousRegion2dNtro(canvas.getViewportTopLeftX(), 
				                                                   canvas.getViewportTopLeftY(), 
				                                                   canvas.getViewportWidth(), 
				                                                   canvas.getViewportHeight());
		
		// TODO: find objects that intersect with 
		//       the viewport in log(n) using
		//       a RegionTree
		synchronized(objects) {
			canvas.drawOnWorld(gc -> {
				for(Object2dFx<?> object2d: objects) {
					if(!clipAtViewport ||
							object2d.intersectsWith(viewport, INTERSECT_EPSILON)) {

						object2d.drawOnWorld(gc);
					}
				}
			});
		}
	}

	public void onTimePasses(double secondsElapsed) {
		updateObjects();

		synchronized (objects) {
			for(Object2dFx<?> objet2d: objects) {
				objet2d.onTimePasses(secondsElapsed);
			}
		}
	}

	private void updateObjects() {
		removeObjectsImpl();
		addObjectsImpl();
	}

	private void addObjectsImpl() {
		synchronized(objects) {
			for(Object2dFx<?> objectToAdd : objectsToAdd) {
				Integer index = objectIndex(objectToAdd);
				if(index == null) {
					objects.add(objectToAdd);
				}
			}
		}
		
		objectsToAdd.clear();
	}

	public void copyDataFrom(World2dFx world2d) {
		updateObjects();

		synchronized (objects) {
			for(int i = 0; i < objects.size(); i++) {
				world2d.copyDataTo(i, objects.get(i));
			}
		}
	}

	protected void copyDataTo(int i, Object2dFx<?> object2d) {
		updateObjects();

		synchronized (objects) {
			if(i < objects.size()) {
				object2d.copyDataFrom(objects.get(i));
			}
		}
	}

	public void dispatchMouseEvent(World2dMouseEventFx world2dMouseEventFx) {
		updateObjects();

        double worldX = world2dMouseEventFx.worldX();
        double worldY = world2dMouseEventFx.worldY();

        boolean consumed = false;
        synchronized (objects) {
			for(Object2dFx<?> object : objects) {

				if(object.collidesWith(worldX - INTERSECT_EPSILON, 
									   worldY - INTERSECT_EPSILON, 
									   2 * INTERSECT_EPSILON, 
									   2 * INTERSECT_EPSILON)) {

					consumed = consumed || object.onMouseEvent(world2dMouseEventFx);
				}
			}
		}

        if(!consumed) {
            onMouseEventNotConsumed(world2dMouseEventFx);
        }
	}

	public void removeObject(String objectId) {
		if(!idsOfObjectsToRemove.contains(objectId)) {
			idsOfObjectsToRemove.add(objectId);
		}
	}

	private void removeObjectsImpl() {
		synchronized (objects) {
			for(String idOfObjectToRemove : this.idsOfObjectsToRemove) {
				Integer indexOfObjectToRemove = objectIndexById(idOfObjectToRemove);
				
				if(indexOfObjectToRemove != null) {
						objects.remove((int) indexOfObjectToRemove);
				}
			}
		}
		
		this.idsOfObjectsToRemove.clear();
	}

	public void removeObjectsWithIdIn(Set<String> idsOfObjectsToRemove) {
		for(String idOfObjectToRemove : idsOfObjectsToRemove) {
			if(!this.idsOfObjectsToRemove.contains(idOfObjectToRemove)) {
				this.idsOfObjectsToRemove.add(idOfObjectToRemove);
			}
		}
	}

	public void removeObjectsWithIdNotIn(Set<String> idsOfObjectToKeep) {
		synchronized (objects) {
			for(Object2dFx<?> candidate : objects) {
				if(candidate != null
						&& candidate.id() != null
						&& !(idsOfObjectToKeep.contains(candidate.id()))
						&& !(this.idsOfObjectsToRemove.contains(candidate.id()))) {
					
					this.idsOfObjectsToRemove.add(candidate.id());
				}
			}
		}
	}

	private Integer objectIndex(Object2dFx<?> object) {
		if(object == null) return null;

		Integer index = null;
		String id = object.id();
		
		synchronized (objects) {

			for(int i = 0; i < objects.size(); i++) {
				Object2dFx<?> candidate = objects.get(i);
				
				if(object == candidate) {

					index = i;
					break;

				}else if(candidate != null
						&& candidate.id() != null
						&& candidate.id().equals(id)) {

					index = i;
					break;
				}
			}
		}

		return index;
	}

	private Integer objectIndexById(String id) {
		Integer index = null;
		
		synchronized (objects) {

			for(int i = 0; i < objects.size(); i++) {
				Object2dFx<?> candidate = objects.get(i);

				if(candidate != null
						&& candidate.id() != null
						&& candidate.id().equals(id)) {

					index = i;
					break;
				}
			}
		}

		return index;
	}

	public Object2dFx<?> objectById(String id) {
		updateObjects();

		Object2dFx<?> result = null;
		
		synchronized (objects) {
			Integer objectIndex = objectIndexById(id);
			
			if(objectIndex != null) {
				result = objects.get(objectIndex);
			}
		}
		
		

		return result;
	}
}