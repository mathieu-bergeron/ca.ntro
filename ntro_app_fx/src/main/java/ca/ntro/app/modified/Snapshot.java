package ca.ntro.app.modified;

import ca.ntro.ntro_app_fx_impl.modified.Observable;

public interface Snapshot<V extends Observable> {

	String id();
	V currentValue();

}
