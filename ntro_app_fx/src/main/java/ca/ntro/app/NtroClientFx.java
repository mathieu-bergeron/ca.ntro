/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.app;

import java.io.File;
import java.nio.file.Paths;

import ca.ntro.app.frontend.FrontendRegistrarFx;
import ca.ntro.app.headless.NtroServerJdk;
import ca.ntro.app.options.SetOptionsLambda;
import ca.ntro.core.NtroCore;
import ca.ntro.ntro_app_fx_abstr.NtroApp;
import ca.ntro.ntro_app_fx_abstr.NtroClient;
import ca.ntro.ntro_core_impl.NtroCoreImpl;
import ca.ntro.ntro_core_impl.services.StackAnalyzerImpl;
import ca.ntro.ntro_core_impl.values.CodeLocation;
import ca.ntro.ntro_app_fx_impl.AppWrapperFx;
import ca.ntro.ntro_app_fx_impl.InitializerFinalizerFx;
import ca.ntro.ntro_app_fx_impl.InitializerFx;
import ca.ntro.ntro_app_fx_impl.NtroClientFxImpl;
import ca.ntro.ntro_app_fx_impl.NtroExecutableImpl;
import ca.ntro.ntro_app_fx_impl.options.OptionsImpl;
import javafx.application.Application;

/**
 * <p>Frontend only (the Backend runs in a server that we connect to)</li>
 * 
 * <p>The main class must have this form:
 * 
 * <code>
 * <pre>
 * public class MyApp implements NtroClientFx {
 *     
 *     public static void main(String[] args){
 *         NtroClientFx.launch(args);
 *     }
 *     
 *     // ...
 * }
 * </pre>
 * </code>
 * 
 * <p>The two other Ntro configurations are:
 * 
 * <p>
 * <ul>
 * <li>{@link NtroAppFx}: Frontend and backend in the same program</li>
 * <li>{@link NtroServerFx}: Backend only (Frontend runs in clients that connect to us)</li>
 * </ul>
 * 
 * 
 */

public interface NtroClientFx extends NtroClient<FrontendRegistrarFx> {

	public static void setOptions(SetOptionsLambda lambda) {
		NtroExecutableImpl.registerInitializerFinalizer(InitializerFinalizerFx.class);
		NtroClientFxImpl.instance().setOptions(lambda);
	}
    
    public static void launch(String[] args) {
    	CodeLocation appLocation = NtroCoreImpl.stackAnalyzer().callerLocation();

    	((StackAnalyzerImpl) NtroCoreImpl.stackAnalyzer()).registerAppLocation(appLocation);

        NtroClientFxImpl.instance().resolveProjectPathIfNeeded(appLocation);

		NtroExecutableImpl.registerInitializerFinalizer(InitializerFinalizerFx.class);

    	NtroClientFxImpl.instance().initializeWithArgs(args);
    	
    	((StackAnalyzerImpl) NtroCoreImpl.stackAnalyzer()).registerAppLocation(appLocation);

        Class<? extends NtroClientFx> appClass = (Class<? extends NtroClientFx>) appLocation._class();

    	NtroClientFxImpl.instance().launch(appClass);

    }

}
