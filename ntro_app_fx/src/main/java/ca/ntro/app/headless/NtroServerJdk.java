package ca.ntro.app.headless;

import ca.ntro.app.NtroAppFx;
import ca.ntro.app.NtroClientFx;
import ca.ntro.app.NtroServerFx;
import ca.ntro.app.common.ServerRegistrarJdk;
import ca.ntro.app.options.SetOptionsLambda;
import ca.ntro.ntro_app_fx_abstr.NtroServer;
import ca.ntro.ntro_app_fx_impl.InitializerFinalizerJdk;
import ca.ntro.ntro_app_fx_impl.NtroExecutableImpl;
import ca.ntro.ntro_app_fx_impl.NtroServerJdkImpl;
import ca.ntro.ntro_core_impl.NtroCoreImpl;
import ca.ntro.ntro_core_impl.services.StackAnalyzerImpl;
import ca.ntro.ntro_core_impl.values.CodeLocation;


/**
 * <p>Backend only (Frontend runs in clients that connect to us); pure Java version. The pure Java version is a bit harder to use than {@link NtroServerFx}, but is truly headless (can run on a server without a display).</li>
 * 
 * <p>The main class must have this form:
 * 
 * <code>
 * <pre>
 * public class MyApp implements NtroServerJdk {
 *     
 *     public static void main(String[] args){
 *         NtroServerJdk.launch(args);
 *     }
 *     
 *     // ...
 * }
 * </pre>
 * </code>
 * 
 * The two other Ntro configurations are:
 * 
 * <ul>
 * <p>
 * <li>{@link NtroAppFx}: Frontend and backend in the same program</li>
 * <li>{@link NtroClientFx}: Frontend only (Backend runs in a server we connect to)</li>
 * </ul>
 * 
 */


public interface NtroServerJdk extends NtroServer {

	public static void setOptions(SetOptionsLambda lambda) {
		NtroExecutableImpl.registerInitializerFinalizer(InitializerFinalizerJdk.class);
		NtroServerJdkImpl.instance().setOptions(lambda);
	}

	public static void initializeWithoutLaunching(CodeLocation appLocation, String[] args) {
		NtroExecutableImpl.registerInitializerFinalizer(InitializerFinalizerJdk.class);
    	NtroServerJdkImpl.instance().initializeWithArgs(args);
        NtroServerJdkImpl.instance().resolveProjectPathIfNeeded(appLocation);
	}

	public static void resolveProjectPathIfNeeded(CodeLocation appLocation) {
        NtroServerJdkImpl.instance().resolveProjectPathIfNeeded(appLocation);
	}

	public static void registerOptions() {
        NtroServerJdkImpl.instance().registerOptions();
	}

    public static void launch(String[] args) {
    	CodeLocation appLocation = NtroCoreImpl.stackAnalyzer().callerLocation();

    	((StackAnalyzerImpl) NtroCoreImpl.stackAnalyzer()).registerAppLocation(appLocation);

        NtroServerJdkImpl.instance().resolveProjectPathIfNeeded(appLocation);

		NtroExecutableImpl.registerInitializerFinalizer(InitializerFinalizerJdk.class);

    	NtroServerJdkImpl.instance().initializeWithArgs(args);


        Class<? extends NtroServerJdk> callerClass = (Class<? extends NtroServerJdk>) appLocation._class();

    	NtroServerJdkImpl.instance().launch(callerClass);
    }

}
