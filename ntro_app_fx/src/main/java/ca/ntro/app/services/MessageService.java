/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.app.services;

import ca.ntro.app.messages.Message;
import ca.ntro.ntro_app_fx_abstr.messages.MessageAbstr;
import ca.ntro.ntro_app_fx_impl.messages.DeliveryMode;
import ca.ntro.ntro_app_fx_impl.messages.MessageServer;
import ca.ntro.ntro_app_fx_impl.modified.Observable;
import ca.ntro.ntro_app_fx_impl.modified.Observation;
import ca.ntro.ntro_core_impl.task_graphs.task_graph.SimpleTask;

public interface MessageService {
	
	void registerMessageServer(MessageServer server, DeliveryMode deliveryMode);

	<MSG extends Message> MSG newMessage(Class<MSG> messageClass);

	<MSG extends MessageAbstr> void addMessageHandler(Class<MSG> messageClass, SimpleTask messageHandlerTask);
	<MSG extends MessageAbstr> void removeMessageHandler(Class<MSG> messageClass, SimpleTask messageHandlerTask);

	void observe(Class<? extends Observable> observableClass, String observableId);
	void addObserverTask(Class<? extends Observable> observableClass, SimpleTask observationHandlerTask);
	void removeObserverTask(Class<? extends Observable> observableClass, SimpleTask observationHandlerTask);

	void addSnapshotTask(Class<? extends Observable> observableClass, SimpleTask snapshotHandlerTask);
	void removeSnapshotTask(Class<? extends Observable> observableClass, SimpleTask snapshotHandlerTask);

	void sendMessageToServer(MessageAbstr message);
	void broadcastMessageToOtherClients(MessageAbstr message);

	void receiveMessageFromServer(MessageAbstr message);

	<O extends Observable> void pushObservationToClients(Class<O> observationClass, String observationId, Observation<O> observation);
	<O extends Observable> void pushObservationToClients(Class<O> observationClass, Observation<O> observation);

	<O extends Observable> void receiveObservationFromServer(Class<O> observationClass, String observationId, Observation<?> observation);
	<O extends Observable> void receiveObservationFromServer(Class<O> observationClass, Observation<?> observation);

}
