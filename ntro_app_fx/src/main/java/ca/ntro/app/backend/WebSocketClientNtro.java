/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.app.backend;

import java.net.ConnectException;
import java.net.URI;
import java.net.URISyntaxException;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import ca.ntro.app.Ntro;
import ca.ntro.core.NtroCore;
import ca.ntro.ntro_app_fx_abstr.messages.MessageAbstr;
import ca.ntro.ntro_app_fx_impl.NtroImpl;
import ca.ntro.ntro_core_impl.NtroCoreImpl;
import ca.ntro.ntro_app_fx_impl.backend.MessageFromServerHandlerNull;
import ca.ntro.ntro_app_fx_impl.backend.ObservationFromServerHandlerNull;
import ca.ntro.ntro_app_fx_impl.messages.BroadcastMessageImpl;
import ca.ntro.ntro_app_fx_impl.messages.DeliveryMode;
import ca.ntro.ntro_app_fx_impl.messages.MessageFromServerHandler;
import ca.ntro.ntro_app_fx_impl.messages.MessageServer;
import ca.ntro.ntro_app_fx_impl.messages.ObservationFromServerHandler;
import ca.ntro.ntro_app_fx_impl.messages.RegisterSessionImpl;
import ca.ntro.ntro_app_fx_impl.messages.RequestFirstObservationImpl;
import ca.ntro.ntro_app_fx_impl.modified.Observable;
import ca.ntro.ntro_app_fx_impl.modified.Observation;
import ca.ntro.ntro_app_fx_impl.modified.ObservationNtro;
import ca.ntro.ntro_core_impl.json.JsonObject;
import ca.ntro.ntro_core_impl.reflection.object_graph.ObjectGraph;

public class WebSocketClientNtro extends WebSocketClient implements MessageServer {
	
	private MessageFromServerHandler messageHandler = new MessageFromServerHandlerNull();
	private ObservationFromServerHandler observationHandler = new ObservationFromServerHandlerNull();
	
	
	public WebSocketClientNtro(String serverName, int port) {
		super(connectionUri(serverName, port));

        NtroCore.factory().registerNamedClass(ObservationNtro.class);
	}
	
	private static URI connectionUri(String serverName, int port) {
		URI uri = null;
		try {

			uri = new URI(connectionString(serverName, port));

		} catch (URISyntaxException e) {
			NtroCoreImpl.logger().fatal(e);
		}
		
		return uri;
	}
	
	private static String connectionString(String serverName, int port) {
		return String.format("ws://%s:%d", serverName, port);
		
	}

	@Override
	public void onOpen(ServerHandshake handshakedata) {
		Ntro.logger().info("connected to " + getURI());

		NtroImpl.messageService().registerMessageServer(this, DeliveryMode.CLIENT_MODE);
		
		sendRegisterSessionMessage(Ntro.session().sessionId());
		
	}

	private void sendRegisterSessionMessage(String sessionId) {
		RegisterSessionImpl registerSession = new RegisterSessionImpl(sessionId);

		String messageText = NtroCoreImpl.reflection().toJsonObject(registerSession).toJsonString(false);
		
		sendRawMessageToServer(messageText);

	}

	@Override
	public void onMessage(String messageText) {
		NtroCore.logger().info("onMessage: " + messageText);
		
		try {
			JsonObject jsonObject = NtroCore.json().fromJsonString(messageText);
			ObjectGraph objectGraph = NtroCoreImpl.reflection().graphFromJsonObject(jsonObject);

			Object messageObject = NtroCoreImpl.reflection().objectFromGraph(objectGraph);
			
			if(messageObject instanceof MessageAbstr) {
				
				messageHandler.onMessage((MessageAbstr) messageObject);

			}else if(messageObject instanceof Observation) {
				
				Observation<?> observation = (Observation<?>) messageObject;
				Class<? extends Observable> observableClass = (Class<? extends Observable>) observation.currentValue().getClass();
				String observableId = observation.id();
				
				observationHandler.onObservation(observableClass, observableId, observation);
			}

		}catch(Throwable t) {
			NtroCoreImpl.logger().info("onMessage: " + messageText + "\n\n");
			NtroCoreImpl.logger().fatal(t);
		}
	}

	@Override
	public void onClose(int code, String reason, boolean remote) {
		NtroCoreImpl.logger().fatal("lost connection to " + getURI());
	}

	@Override
	public void onError(Exception ex) {
		if(ex instanceof ConnectException) {
			
			
			NtroCoreImpl.logger().fatal("failed to connect to " + getURI(), ex);
			

		}else {

			NtroCoreImpl.logger().fatal(ex);;
		}
	}

	@Override
	public void sendMessageToServer(MessageAbstr message) {
		//NtroCore.logger().info("sendMessage " + NtroCore.reflection().toJsonObject(message).toJsonString(false));
		sendRawMessageToServer(NtroCoreImpl.reflection().toJsonObject(message).toJsonString(false));
	}

	private void sendRawMessageToServer(String messageText) {
		send(messageText);
	}

	@Override
	public void broadcastMessageToOtherClients(MessageAbstr message) {
		BroadcastMessageImpl broadcastMessage = new BroadcastMessageImpl(message);
		send(NtroCoreImpl.reflection().toJsonObject(broadcastMessage).toJsonString(false));
	}


	@Override
	public void onMessageFromServer(MessageFromServerHandler handler) {
		this.messageHandler = handler;
	}

	@Override
	public void onObservationFromServer(ObservationFromServerHandler handler) {
		this.observationHandler = handler;
	}

	@Override
	public <O extends Observable> void pushObservationToClients(Class<O> observableClass, 
			                                                    String observableId, 
			                                                    Observation<?> observation) {
		
		// XXX: not supported here
	}

	@Override
	public <O extends Observable> void pushObservationToClients(Class<O> observableClass, Observation<?> observation) {

		// XXX: not supported here
	}

	@Override
	public <O extends Observable> void requestFirstObservationFromServer(Class<O> observableClass, 
			                                                             String modelId) {
		
		RequestFirstObservationImpl requestFirstObservationMessage = new RequestFirstObservationImpl(NtroCoreImpl.reflection().simpleName(observableClass), 
				                                                                                                  modelId);
		
		sendRawMessageToServer(NtroCoreImpl.reflection().toJsonObject(requestFirstObservationMessage).toJsonString(false));
		
	}

}
