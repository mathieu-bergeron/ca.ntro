package ca.ntro.app;

import ca.ntro.app.common.ServerRegistrarJdk;
import ca.ntro.app.headless.NtroServerJdk;
import ca.ntro.app.options.SetOptionsLambda;
import ca.ntro.ntro_app_fx_abstr.NtroServer;
import ca.ntro.ntro_core_impl.NtroCoreImpl;
import ca.ntro.ntro_core_impl.services.StackAnalyzerImpl;
import ca.ntro.ntro_core_impl.values.CodeLocation;
import ca.ntro.ntro_app_fx_impl.InitializerFinalizerFx;
import ca.ntro.ntro_app_fx_impl.NtroExecutableImpl;
import ca.ntro.ntro_app_fx_impl.NtroServerFxImpl;


/**
 * <p>Backend only (the Frontend runs in clients that connect to us)
 * 
 * 
 * 
 * <p>The main class must have this form:
 * 
 * <code>
 * <pre>
 * public class MyApp implements NtroServerFx {
 *     
 *     public static void main(String[] args){
 *         NtroServerFx.launch(args);
 *     }
 *     
 *     // ...
 * }
 * </pre>
 * </code>
 * 
 * The two other Ntro configurations are:
 * 
 * <ul>
 * <p>
 * <li>{@link NtroAppFx}: Frontend and backend in the same program</li>
 * <li>{@link NtroClientFx}: Frontend only (Backend runs in a server we connect to)</li>
 * </ul>
 * 
 * <br>
 * 
 * <p><strong>NOTE:</strong> This version is not headless (it must run on a server with a display). Use {@link NtroServerJdk} if you require a truly headless server.
 * 
 */

public interface NtroServerFx extends NtroServer {

	public static void setOptions(SetOptionsLambda lambda) {
		NtroExecutableImpl.registerInitializerFinalizer(InitializerFinalizerFx.class);
		NtroServerFxImpl.instance().setOptions(lambda);
	}
    
    public static void launch(String[] args) {
    	CodeLocation appLocation = NtroCoreImpl.stackAnalyzer().callerLocation();

    	((StackAnalyzerImpl) NtroCoreImpl.stackAnalyzer()).registerAppLocation(appLocation);

        NtroServerFxImpl.instance().resolveProjectPathIfNeeded(appLocation);

		NtroExecutableImpl.registerInitializerFinalizer(InitializerFinalizerFx.class);

    	NtroServerFxImpl.instance().initializeWithArgs(args);

    	((StackAnalyzerImpl) NtroCoreImpl.stackAnalyzer()).registerAppLocation(appLocation);

        Class<? extends NtroServerFx> appClass = (Class<? extends NtroServerFx>) appLocation._class();

    	NtroServerFxImpl.instance().launch(appClass);
    }
}
