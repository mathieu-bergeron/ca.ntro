package ca.ntro.ntro_app_fx_impl.session;

import static ca.ntro.app.tasks.frontend.FrontendTasks.*;

import java.nio.file.Path;
import java.nio.file.Paths;

import ca.ntro.app.Ntro;
import ca.ntro.app.session.AfterSessionFirstCreated;
import ca.ntro.app.session.Session;
import ca.ntro.app.session.SessionAccessor;
import ca.ntro.app.session.SessionRegistrar;
import ca.ntro.core.NtroCore;
import ca.ntro.core.services.JsonParseError;
import ca.ntro.ntro_app_fx_impl.NtroImpl;
import ca.ntro.ntro_app_fx_impl.tasks.frontend.FrontendTaskFactory;
import ca.ntro.ntro_core_impl.NtroCoreImpl;
import ca.ntro.ntro_core_impl.json.JsonObject;
import ca.ntro.ntro_core_impl.task_graphs.generic_task_graph.SimpleTaskOptions;
import ca.ntro.ntro_core_impl.task_graphs.task_graph.ExecutableTaskNtro;
import ca.ntro.ntro_core_impl.task_graphs.task_graph_trace.TaskResultsCondition;
import ca.ntro.ntro_core_impl.task_graphs.task_graph_trace.TaskTraceNtro;

public class SessionRegistrarImpl implements SessionRegistrar  {
	
	private Class<? extends Session> sessionClass = Session.class;
	private Session<?> session = null;

	public Class<? extends Session> getSessionClass() {
		return sessionClass;
	}

	public void setSessionClass(Class<? extends Session> sessionClass) {
		this.sessionClass = sessionClass;
	}

	@Override
	public <S extends Session<?>> void registerSessionClass(Class<S> sessionClass) {
		this.sessionClass = sessionClass;
		NtroCoreImpl.factory().registerNamedClass(sessionClass);

	}

	@SuppressWarnings("unchecked")
	public <S extends Session<?>> void createRegisteredSession(String sessionId) {
		if(sessionClass != null) {
		
			this.session = loadOrCreateSession((Class<S>) sessionClass, sessionId);

			NtroImpl.registerSession(session);
		}
	}

	private <S extends Session<?>> S loadOrCreateSession(Class<S> sessionClass, String sessionId) {
		
		S session = null;

		if(sessionId != null) {

			session = loadSessionFromFileIfPossible(sessionClass, sessionId);

		}
		
		if(session == null) {

			session = newSession(sessionClass, sessionId);

		}
		
		return session;
	}

	private <S extends Session<?>> S newSession(Class<S> sessionClass, String sessionId) {
		S session = NtroCoreImpl.factory().newInstance(sessionClass);

		if(sessionId != null) {

			SessionAccessor.registerSessionId(session, sessionId);
		}
		
		if(session instanceof AfterSessionFirstCreated) {
			
			((AfterSessionFirstCreated) session).afterSessionFirstCreated();
			
		}

		return session;
	}

	@SuppressWarnings("unchecked")
	private <S extends Session<?>> S loadSessionFromFileIfPossible(Class<S> sessionClass, String sessionId) {
		String sessionsPath = Ntro.options().coreOptions().sessionsPath();
		Path sessionFilepath = Paths.get(sessionsPath, sessionId + ".json");
		String rawSessionFilepath = sessionFilepath.toAbsolutePath().toString();
		
		S session = null;
		
		if(NtroCore.storage().ifFileExists(rawSessionFilepath)) {
			
			String sessionText = NtroCore.storage().readTextFile(rawSessionFilepath);
			
			try {

			JsonObject sessionJson = NtroCore.json().fromJsonString(sessionText);
			session = (S) NtroCore.reflection().fromJsonObject(sessionJson);
			SessionAccessor.registerSessionId(session, sessionId);

			}catch(JsonParseError e) {
				NtroCoreImpl.logger().warning("JsonParseError for " + rawSessionFilepath);
			}

		}

		return session;
	}

	public void addSessionTask(FrontendTaskFactory taskFactory) {
		Class<? extends Session> sessionClass = this.sessionClass;
		if(sessionClass == null) {
			sessionClass = Session.class;
		}

		taskFactory.orphanTask(session(sessionClass),
					           SimpleTaskOptions.taskClass(ExecutableTaskNtro.class)
					                                 .traceClass(TaskTraceNtro.class)
					                                 .resultsClass(TaskResultsCondition.class))

		                .executesAndReturnsValue(inputs -> {
		                	
		                	return session;

		                });
	}


}
