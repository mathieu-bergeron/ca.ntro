/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.ntro_app_fx_impl.backend;

import ca.ntro.app.backend.BackendRegistrar;
import ca.ntro.ntro_app_fx_impl.messages.MessageRegistrarNtro;
import ca.ntro.ntro_app_fx_impl.models.ModelRegistrarImpl;
import ca.ntro.ntro_app_fx_impl.tasks.backend.BackendTaskFactory;
import ca.ntro.ntro_core_impl.NtroCoreImpl;

public class BackendRegistrarNtro implements BackendRegistrar {

	private BackendTaskFactory taskFactory = new BackendTaskFactory();
	private MessageRegistrarNtro messageRegistrar;
	private ModelRegistrarImpl modelRegistrar;
	private Backend backend = new DefaultBackend();

	public MessageRegistrarNtro getMessageRegistrar() {
		return messageRegistrar;
	}

	public void setMessageRegistrar(MessageRegistrarNtro messageRegistrar) {
		this.messageRegistrar = messageRegistrar;
	}

	public ModelRegistrarImpl getModelRegistrar() {
		return modelRegistrar;
	}

	public void setModelRegistrar(ModelRegistrarImpl modelRegistrar) {
		this.modelRegistrar = modelRegistrar;
	}

	public BackendTaskFactory getTaskFactory() {
		return taskFactory;
	}

	public void setTaskFactory(BackendTaskFactory taskFactory) {
		this.taskFactory = taskFactory;
	}

	public Backend getBackend() {
		return backend;
	}

	public void setBackend(Backend backend) {
		this.backend = backend;
	}
	
	public BackendRegistrarNtro() {
	}

	public BackendRegistrarNtro(ModelRegistrarImpl modelRegistrar, MessageRegistrarNtro messageRegistrarNtro) {
		setModelRegistrar(modelRegistrar);
		setMessageRegistrar(messageRegistrarNtro);
	}

	@Override
	public void registerBackend(Class<? extends Backend> backendClass) {

		backend = NtroCoreImpl.factory().newInstance(backendClass);

		registerBackendObject(backend);
	}

	public void registerBackendObject(Backend backend) {

		if(backend.isLocalBackend()) {

			backend.asLocalBackend().createTasks(getTaskFactory().asTasks());
		}

		setBackend(backend);
	}

	public void prepareToExecuteTasks() {

		getTaskFactory().prepareToExecuteTasks();
	}

	public void executeTasks() {
		
		getTaskFactory().executeTasks();

	}

	public void writeGraph() {
		if(getTaskFactory() != null) {
			getTaskFactory().writeGraph();
		}
	}

	public void openConnection() {
		if(getBackend().isRemoteBackend()) {
			
			getBackend().asRemoteBackend().openConnection();

		}
	}

	public boolean isRemoteBackend() {
		return getBackend().isRemoteBackend();
	}

}
