package ca.ntro.ntro_app_fx_impl;

import ca.ntro.ntro_app_fx_impl.services.ExitServiceFx;
import ca.ntro.ntro_app_fx_impl.services.LoggerFx;
import ca.ntro.ntro_app_fx_impl.services.MessageServiceFx;
import ca.ntro.ntro_app_fx_impl.services.StorageServiceFx;
import ca.ntro.ntro_app_fx_impl.services.ThreadServiceFx;
import ca.ntro.ntro_app_fx_impl.services.TimeFxAnimationTimer;
import ca.ntro.ntro_app_fx_impl.services.TimeFxJdkTimer;
import ca.ntro.ntro_core_impl.NtroCoreImpl;

public class InitializerFx {
	
	public static void initializeCore() {
		NtroCoreImpl.registerExitService(new ExitServiceFx());
		NtroCoreImpl.registerLogger(new LoggerFx());
		NtroCoreImpl.registerThreadService(new ThreadServiceFx());
		//NtroCoreImpl.registerTime(new TimeFxJdkTimer());
		NtroCoreImpl.registerTime(new TimeFxAnimationTimer());
		NtroCoreImpl.registerStorageService(new StorageServiceFx());
	}

	public static void initializeApp() {
        NtroImpl.registerMessageService(new MessageServiceFx());
	}

}
