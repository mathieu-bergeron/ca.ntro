/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.ntro_app_fx_impl;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

import ca.ntro.app.Ntro;
import ca.ntro.app.NtroClientFx;
import ca.ntro.core.NtroCore;
import ca.ntro.core.services.ExitHandler;
import ca.ntro.ntro_app_fx_impl.executable_spec.ClientFxSpec;
import ca.ntro.ntro_app_fx_impl.frontend.window.WindowFx;
import ca.ntro.ntro_core_impl.NtroCoreImpl;
import javafx.application.Application;
import javafx.stage.Stage;

public class AppWrapperFx extends Application implements ExitHandler {
	
    public static Class<? extends NtroClientFx> appClass;
    public static String                        sessionId;
    public static ClientFxSpec                  spec;
    
    @Override
    public void start(Stage primaryStage) throws Exception {
		NtroCoreImpl.registerAppExitHandler(this);

		primaryStage.setOnCloseRequest(event -> {
			NtroCore.exit();
		});
		
		Runtime.getRuntime().addShutdownHook(new Thread(() -> {
			NtroCore.exit();
		}));

		WindowFx window = new WindowFx(primaryStage);
		NtroImpl.registerWindow(window);

		NtroCore.logger().run();

		NtroCore.threads().runInWorkerThread(() -> {

			NtroCore.logger().info("App running. Press Enter or Ctrl+D to close App");

			try {

				System.in.read();
				NtroCore.exit();

			} catch (IOException e) {

				e.printStackTrace();
			}
		});


		try {

			System.out.println("\n\n\n");
			NtroCore.logger().info("Ntro version 0.2");
			NtroCore.logger().info("java version " + System.getProperty("java.version"));

			File javaRootDir      = Paths.get(NtroCore.options().javaPath()).toFile();
			File resourcesRootDir = Paths.get(NtroCore.options().resourcesPath()).toFile();

			spec.analyzeSources(resourcesRootDir, javaRootDir);

			spec.registerAppClass(appClass);
			spec.registerSessionId(sessionId);
			spec.registerWindow(window);
			
			spec.callExecutableCodeAndFillSpec();

			NtroImpl.loadOptionsFile();

			NtroCore.logger().info("locale: '" + Ntro.currentLocale() + "'");
			
			spec.writeSpec();

			spec.createExecutableFromSpec();
			
			spec.runExecutable();

		}catch(Throwable t) {

			NtroCoreImpl.logger().fatal(t);
			
		}
    }

    @Override
    public void onExit() {

    	NtroCore.logger().info("Writing JSON files");
        NtroImpl.models().writeModelFiles();
        NtroImpl.writeSessionFile();
        NtroImpl.writeOptionsFile();

    	NtroCore.logger().info("Generating graphs");
        NtroImpl.models().writeGraphs();
        
        if(!spec.isRemoteBackend()) {
			spec.writeBackendGraph();
        }
        
        spec.writeFrontendGraph();
    }
}
