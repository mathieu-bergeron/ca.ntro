package ca.ntro.ntro_app_fx_impl;

import java.util.Map;

import ca.ntro.app.NtroServerFx;
import ca.ntro.core.NtroCore;
import ca.ntro.ntro_app_fx_impl.executable_spec.ExecutableSpec;
import ca.ntro.ntro_app_fx_impl.executable_spec.ServerFxSpec;
import ca.ntro.ntro_core_impl.NtroCoreImpl;
import ca.ntro.ntro_core_impl.services.StackAnalyzerImpl;
import javafx.application.Application;

public class NtroServerFxImpl extends NtroExecutableImpl<NtroServerFx, ServerFxSpec> {
	
	private static final NtroServerFxImpl instance = new NtroServerFxImpl();
	
	public static NtroServerFxImpl  instance() {
		return instance;
	}
	
	@Override
	protected void launchImpl(Class<? extends NtroServerFx> serverClass, ServerFxSpec spec) {

        ServerWrapperFx.serverClass        = serverClass;
        ServerWrapperFx.spec = spec;

    	((StackAnalyzerImpl) NtroCoreImpl.stackAnalyzer()).registerAppWrapperClass(ServerWrapperFx.class);

        NtroCore.logger().setErrorDialogEnabled(false);

        Application.launch(ServerWrapperFx.class);

	}

	@Override
	protected void initializeNtroMode() {
		NtroImpl.registerNtroMode(NtroMode.SERVER);
	}

	@Override
	protected void initializeWithParams(Map<String, String> params) {
		
	}

	@Override
	protected ExecutableSpec createEmptySpec() {
		return new ServerFxSpec();
	}



}
