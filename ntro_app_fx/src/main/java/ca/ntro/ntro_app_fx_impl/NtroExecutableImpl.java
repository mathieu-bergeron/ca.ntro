package ca.ntro.ntro_app_fx_impl;

import java.io.File;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import ca.ntro.app.options.SetOptionsLambda;
import ca.ntro.core.NtroCore;
import ca.ntro.ntro_app_fx_abstr.NtroExecutable;
import ca.ntro.ntro_app_fx_impl.executable_spec.ExecutableSpec;
import ca.ntro.ntro_app_fx_impl.options.OptionsImpl;
import ca.ntro.ntro_core_impl.InitializerFinalizerImpl;
import ca.ntro.ntro_core_impl.NtroCoreImpl;
import ca.ntro.ntro_core_impl.options.CoreOptionsImpl;
import ca.ntro.ntro_core_impl.values.CodeLocation;


public abstract class NtroExecutableImpl<APP_CLASS extends NtroExecutable, PROJECT_DESC extends ExecutableSpec> {

	private static InitializerFinalizerImpl initializerFinalizer = null;
	public static void registerInitializerFinalizer(Class<? extends InitializerFinalizerImpl> initializerFinalizerClass) {
		if(initializerFinalizer == null) {
			initializerFinalizer = NtroCore.factory().newInstance(initializerFinalizerClass);
		}
	}
	
	protected ExecutableSpec projectDescription = createEmptySpec();
	protected OptionsImpl  options                  = new OptionsImpl();

	private boolean initialized = false;
	private boolean launched = false;
	
	protected abstract ExecutableSpec createEmptySpec();
	
	@SuppressWarnings("unchecked")
	protected PROJECT_DESC projectDescription() {
		return (PROJECT_DESC) projectDescription;
	}
	
	private void initialize() {
		registerOptions();
		
		deleteTmpFiles();

		initializerFinalizer.initialize();
		
		initializeNtroMode();
		
	}
	
	private void deleteTmpFiles() {
    	File tmpDir = Paths.get(options.coreOptions().tmpPath()).toFile();
    	if(tmpDir.exists()) {
			deleteTmpFiles(tmpDir);
    	}
	}

    private void deleteTmpFiles(File tmpDir) {
    	for(File tmpFile : tmpDir.listFiles()) {
    		tmpFile.delete();
    	}
    }

	public void registerOptions() {
		NtroImpl.registerOptions(options);
    	NtroCoreImpl.registerOptions((CoreOptionsImpl) options.coreOptions());
        NtroCore.logger().setErrorDialogEnabled(options.getErrorDialog());
	}

	public void setOptions(SetOptionsLambda lambda) {
		initializeIfNeeded();
		failIfAlreadyLaunched("setOptions");

		lambda.setOptions(options);
	}


	protected void failIfAlreadyLaunched(String methodName) {
		if(launched) {
			NtroCore.logger().fatal("Method " + methodName + " must be called before launch");
		}
	}

	public void initializeIfNeeded() {
		if(!initialized) {
			initialize();
			initialized = true;
		}
	}

	public void loadOptions(String filePath) {
		initializeIfNeeded();
		failIfAlreadyLaunched("loadOptions");
	}


    public void launch(Class<? extends APP_CLASS> appClass) {
    	launched = true;

    	setDefaultExceptionHandler();

    	createDirectoriesIfNeeded();
        
        launchImpl(appClass, projectDescription());

    }
    
	private void createDirectoriesIfNeeded() {
		File storageDir = Paths.get(options.coreOptions().storagePath()).toFile();
		if(!storageDir.exists()) {
			storageDir.mkdirs();
		}

    	File tmpDir = Paths.get(options.coreOptions().tmpPath()).toFile();
    	if(!tmpDir.exists()) {
			tmpDir.mkdirs();
    	}

    	File sessionsDir = Paths.get(options.coreOptions().sessionsPath()).toFile();
		if(!sessionsDir.exists()) {
			sessionsDir.mkdirs();
		}

    	File graphsDir = Paths.get(options.coreOptions().graphsPath()).toFile();
		if(!graphsDir.exists()) {
			graphsDir.mkdirs();
		}

    	File optionsDir = Paths.get(options.coreOptions().optionsPath()).toFile();
		if(!optionsDir.exists()) {
			optionsDir.mkdirs();
		}
	}



	private void setDefaultExceptionHandler() {
		Thread.setDefaultUncaughtExceptionHandler((thread, throwable) -> {
    		NtroCoreImpl.logger().fatal(throwable);
    	});
	}

    protected abstract void launchImpl(Class<? extends APP_CLASS> appClass, PROJECT_DESC projectDescription);

	public void initializeWithArgs(String[] args) {
		initializeIfNeeded();
		
		Map<String,String> params = parseArgs(args);
		
		initializeWithParams(params);

	}
	
	private Map<String,String> parseArgs(String[] args){
		Map<String,String> params = new HashMap<>();
		
		for(String arg : args) {
			String[] segments = arg.split("=");
			if(segments.length == 2) {
				String paramName = segments[0];
				String paramValue = segments[1];
				params.put(paramName, paramValue);
			}
		}

		return params;
	}
	
	protected abstract void initializeWithParams(Map<String, String> params);

	protected abstract void initializeNtroMode();

	public void resolveProjectPathIfNeeded(CodeLocation appLocation) {
		Class<?> appClass     = appLocation._class();
		String appFilename    = appClass.getSimpleName() + ".java";
		String projectDirname = appClass.getPackage().getName();
		
		// TODO: use classPath to identify project folder
		String thisVmClasspath = System.getProperty("java.class.path");
		

		options.resolveProjectPathIfNeeded(appFilename, projectDirname);
	}

}
