package ca.ntro.ntro_app_fx_impl.options;

import ca.ntro.app.options.Options;
import ca.ntro.app.options.OptionsWriter;
import ca.ntro.core.options.CoreOptions;
import ca.ntro.ntro_core_abstr.SetCoreOptionsLambda;
import ca.ntro.ntro_core_impl.options.CoreOptionsImpl;

public class OptionsImpl 

       implements OptionsWriter,
                  Options { 
	
	private CoreOptionsImpl coreOptions = new CoreOptionsImpl();

	private boolean errorDialog = true;
	
	private boolean isProd = false;

	private boolean useJarResources = false;
	
	
	public OptionsImpl() {
		coreOptions.resolvePaths();
	}
	
	

	public boolean getErrorDialog() {
		return errorDialog;
	}

	public void setErrorDialog(boolean errorDialog) {
		this.errorDialog = errorDialog;
	}


	@Override
	public void graphicalLogger(boolean enabled) {
		this.errorDialog = enabled;
	}

	@Override
	public boolean graphicalLogger() {
		return errorDialog;
	}

	@Override
	public CoreOptions coreOptions() {
		return coreOptions;
	}

	@Override
	public void setCoreOptions(SetCoreOptionsLambda lambda) {
		lambda.setCoreOptions(coreOptions);
	}

	@Override
	public void saveJson(String filePath) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isProd() {
		return isProd;
	}

	@Override
	public void isProd(boolean isProd) {
		this.isProd = isProd;
	}

	public void resolvePaths() {
		coreOptions.resolvePaths();
	}



	@Override
	public void useJarResources(boolean useJarResources) {
		this.useJarResources = useJarResources;
	}

	@Override
	public boolean useJarResources() {
		return useJarResources;
	}



	public void resolveProjectPathIfNeeded(String appFilename, String projectDirname) {
		coreOptions.resolveProjectPathIfNeeded(appFilename, projectDirname);
	}

}
