package ca.ntro.ntro_app_fx_impl;

public class InitializerFinalizerFx extends InitializerFinalizerApp {

	@Override
	public void initialize() {
    	InitializerFx.initializeCore();
    	InitializerFx.initializeApp();

		super.initialize();
	}

}
