package ca.ntro.ntro_app_fx_impl;

import ca.ntro.ntro_app_fx_impl.executable_spec.AppFxSpec;
import ca.ntro.ntro_app_fx_impl.executable_spec.ExecutableSpec;

public class NtroAppFxImpl extends NtroClientFxImpl {
	
	private static final NtroAppFxImpl instance = new NtroAppFxImpl();
	
	public static NtroAppFxImpl instance() {
		return instance;
	}

	@Override
	protected void initializeNtroMode() {
		NtroImpl.registerNtroMode(NtroMode.APP);
	}

	@Override
	protected ExecutableSpec createEmptySpec() {
		return new AppFxSpec();
	}




}
