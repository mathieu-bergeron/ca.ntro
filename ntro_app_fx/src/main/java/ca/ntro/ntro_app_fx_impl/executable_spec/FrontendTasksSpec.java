package ca.ntro.ntro_app_fx_impl.executable_spec;


import ca.ntro.app.tasks.SimpleTaskCreator;
import ca.ntro.app.tasks.TaskGroupCreator;
import ca.ntro.app.tasks.frontend.FrontendTasks;
import ca.ntro.ntro_app_fx_impl.tasks.frontend.FrontendSimpleTaskDescriptor;
import ca.ntro.ntro_app_fx_impl.tasks.frontend.FrontendTaskGroupDescriptor;

public class FrontendTasksSpec extends TaskGroupSpec<FrontendTasks> implements FrontendTasks {
	
	@Override
	public SimpleTaskCreator<?> task(String taskId) {
		return createTask(taskId);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <O> SimpleTaskCreator<O> task(FrontendSimpleTaskDescriptor<O> descriptor) {
		return (SimpleTaskCreator<O>) createTask(descriptor.id());
	}

	@Override
	public TaskGroupCreator<?, FrontendTasks> taskGroup(String taskGroupId) {
		return createTaskGroup(taskGroupId);
	}

	@Override
	public <O> TaskGroupCreator<O, FrontendTasks> taskGroup(FrontendTaskGroupDescriptor<O> descriptor) {
		return createTaskGroup(descriptor.id());
	}

	@Override
	protected TaskGroupSpec newTaskGroup() {
		return new FrontendTasksSpec();
	}
}
