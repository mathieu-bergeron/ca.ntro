package ca.ntro.ntro_app_fx_impl.executable_spec;

import ca.ntro.app.backend.LocalBackendNtro;
import ca.ntro.app.common.ServerRegistrar;
import ca.ntro.app.common.WebSocketServerNtro;
import ca.ntro.ntro_app_fx_abstr.NtroServer;
import ca.ntro.ntro_app_fx_impl.backend.Backend;

public class ServerSpec<APP extends NtroServer> extends ExecutableSpec<APP> implements ServerRegistrar {
	
	public static final String DEFAULT_HOSTNAME = "localhost";
	public static final int    DEFAULT_PORT     = 6876;          // NTRO on alphanumeric keypad
	
	private String hostname = DEFAULT_HOSTNAME;
	private int    port     = DEFAULT_PORT;

    private transient WebSocketServerNtro webSocketServer;

    @Override
	protected void callExectuableCodeAndFillSpec(APP app) {
    	super.callExectuableCodeAndFillSpec(app);
    	
    	app.registerServer(this);
	}

	@Override
	public void registerPort(int port) {
		this.port = port;
	}

	@Override
	public void registerName(String string) {
		this.hostname = hostname;
	}

	public void startWebSocketServer() {
		webSocketServer = new WebSocketServerNtro(hostname, port);
		webSocketServer.start();
	}

	public void stopWebSocketServer() {

		if(webSocketServer != null) {

			try {

				webSocketServer.stop();

			} catch (InterruptedException e) {

				e.printStackTrace();

			}
		}

	}

	@Override
	protected Class<? extends Backend> desiredBackendClass() {
		return LocalBackendNtro.class;
	}


}
