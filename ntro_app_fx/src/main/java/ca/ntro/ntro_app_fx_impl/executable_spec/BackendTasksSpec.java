package ca.ntro.ntro_app_fx_impl.executable_spec;

import ca.ntro.app.tasks.SimpleTaskCreator;
import ca.ntro.app.tasks.TaskGroupCreator;
import ca.ntro.app.tasks.backend.BackendTasks;
import ca.ntro.ntro_app_fx_impl.tasks.backend.BackendSimpleTaskDescriptor;
import ca.ntro.ntro_app_fx_impl.tasks.backend.BackendTaskFactory;
import ca.ntro.ntro_app_fx_impl.tasks.backend.BackendTaskGroupDescriptor;

public class BackendTasksSpec extends TaskGroupSpec<BackendTasks> implements BackendTasks {
	
	@Override
	public SimpleTaskCreator<?> task(String taskId) {
		return createTask(taskId);
	}

	@Override
	public <O> SimpleTaskCreator<O> task(BackendSimpleTaskDescriptor<O> descriptor) {
		return createTask(descriptor);
	}


	@Override
	public TaskGroupCreator<?, BackendTasks> taskGroup(String taskGroupId) {
		return createTaskGroup(taskGroupId);
	}

	@Override
	public <O> TaskGroupCreator<O, BackendTasks> taskGroup(BackendTaskGroupDescriptor<O> descriptor) {
		return createTaskGroup(descriptor);
	}


	@Override
	protected TaskGroupSpec newTaskGroup() {
		return new BackendTasksSpec();
	}


}
