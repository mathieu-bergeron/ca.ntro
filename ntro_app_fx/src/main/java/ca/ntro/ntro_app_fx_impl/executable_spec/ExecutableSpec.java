package ca.ntro.ntro_app_fx_impl.executable_spec;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import ca.ntro.app.Ntro;
import ca.ntro.app.backend.BackendRegistrar;
import ca.ntro.app.backend.LocalBackendNtro;
import ca.ntro.app.frontend.Frontend;
import ca.ntro.app.messages.Message;
import ca.ntro.app.messages.MessageRegistrar;
import ca.ntro.app.models.Model;
import ca.ntro.app.models.ModelOrValue;
import ca.ntro.app.models.ModelRegistrar;
import ca.ntro.app.models.ModelValue;
import ca.ntro.app.session.SessionAccessor;
import ca.ntro.core.NtroCore;
import ca.ntro.core.util.Splitter;
import ca.ntro.ntro_app_fx_abstr.NtroExecutable;
import ca.ntro.ntro_app_fx_abstr.messages.MessageAbstr;
import ca.ntro.ntro_app_fx_impl.backend.Backend;
import ca.ntro.ntro_app_fx_impl.backend.DefaultBackend;
import ca.ntro.ntro_app_fx_impl.tasks.backend.BackendTaskFactory;
import ca.ntro.ntro_core_impl.values.Pair;

public abstract class ExecutableSpec<APP extends NtroExecutable> implements MessageRegistrar, ModelRegistrar, BackendRegistrar {
	
	private String                app            = null;
	private transient Class<?>    appClass       = null;

	private String                             backend         = null;
	private transient Class<? extends Backend> backendClass    = DefaultBackend.class;
	private String                             backendHostname = null;
	private Integer                            backendPort     = null;
	private transient Backend                  backendObject   = new DefaultBackend();

	private           BackendTasksSpec      backendTasks       = new BackendTasksSpec();
	private transient BackendTaskFactory    backendTaskFactory = new BackendTaskFactory();
	
	private           Map<String, String>   messages       = new HashMap<>();
	private transient Map<String, Class<?>> messageClasses = new HashMap<>();

	private           Map<String, String>   models       = new HashMap<>();
	private transient Map<String, Class<?>> modelClasses = new HashMap<>();

	private           Map<String, String>   values       = new HashMap<>();
	private transient Map<String, Class<?>> valueClasses = new HashMap<>();
	
	public void registerAppClass(Class<?> appClass) {
		this.appClass = appClass;
		this.app = appClass.getCanonicalName();
	}
	

	public void analyzeSources(File resourcesRootDir, File javaRootDir) {
		if(javaRootDir != null
				&& javaRootDir.exists()
				&& javaRootDir.isDirectory()) {
			
			analyzeProjectJavaSources(javaRootDir, javaRootDir);
		}
		
		if(resourcesRootDir != null
				&& resourcesRootDir.exists()
				&& resourcesRootDir.isDirectory()) {

			analyzeProjectResources(resourcesRootDir, resourcesRootDir);
		}
	}

	private void analyzeProjectResources(File resourcesRootDir, File rootDir) {
		for(File file : rootDir.listFiles()) {
			if(file.isDirectory()) {

				analyzeProjectResources(resourcesRootDir, file);

			}else {
				
				Pair<String,String> baseExtPair = Splitter.analyzeFileName(file.getName());
				
				if(baseExtPair != null) {
					
					String basename = baseExtPair.left();
					String ext      = baseExtPair.right();
					
					analyzeProjectResourceFile(resourcesRootDir, file, basename, ext);
				
					
				}else {

					Ntro.logger().warning("a resource file without extension is ignored: (" + file.getAbsolutePath() + ")");

				}
			}
		}
	}

	protected void analyzeProjectResourceFile(File resourcesRootDir, File file, String basename, String ext) {

	}

	private void analyzeProjectJavaSources(File javaRootDir, File currentDir) {
		for(File file : currentDir.listFiles()) {
			if(file.isDirectory()) {

				analyzeProjectJavaSources(javaRootDir, file);

			}else {

				Pair<String,String> baseExtPair = Splitter.analyzeFileName(file.getName());
				
				if(baseExtPair != null) {
					
					String basename = baseExtPair.left();
					String ext      = baseExtPair.right();
					
					if(ext.equalsIgnoreCase("java")) {
						
						analyzeProjectJavaFile(javaRootDir, file, basename);

					}
					
				}else {

					Ntro.logger().warning("a source file without extension is ignored: (" + file.getAbsolutePath() + ")");

				}

			}
		}
	}

	protected void analyzeProjectJavaFile(File javaRootDir, File file, String basename) {
		Path relativeFilepath = javaRootDir.toPath().relativize(file.toPath());
		
		String fullClassname = "";
		
		if(relativeFilepath.getNameCount() >= 1) {
			fullClassname += relativeFilepath.getName(0);
		}
		
		for(int i = 1; i < relativeFilepath.getNameCount() - 1; i++) {
			fullClassname += "." + relativeFilepath.getName(i);
		}
		
		fullClassname += "." + basename;
		
		try {

			Class<?> _class = ExecutableSpec.class.forName(fullClassname);
			
			analyzeProjectClass(_class);

		} catch (ClassNotFoundException e) {
			
			Ntro.logger().warning("Cannot load class for file " + file.getAbsolutePath());

		}
	}
	
	
	protected abstract Class<? extends Backend> desiredBackendClass();
	
	protected void analyzeProjectClass(Class<?> _class) {
		if(NtroCore.reflection().ifClassIsSubTypeOf(_class, Message.class)) {

			registerMessage((Class<? extends Message>) _class);

		}else if(NtroCore.reflection().ifClassIsSubTypeOf(_class, Model.class)) {

			registerModel((Class<? extends Model>) _class);

		}else if(NtroCore.reflection().ifClassIsSubTypeOf(_class, ModelValue.class)) {

			registerValue((Class<? extends ModelValue>) _class);

		}else if(NtroCore.reflection().ifClassIsSubTypeOf(_class, desiredBackendClass())) {

			registerBackend((Class<? extends Backend>) _class);

		}
		
	}
	
	protected void registerClass(Map<String,String> names, 
			                     Map<String,Class<?>> classes, 
			                     Class<?> _class,
			                     String classType) {

		String className     = _class.getSimpleName();
		String classFullname = _class.getCanonicalName();
		
		String storedFullname  = names.get(className);

		if(storedFullname != null && !storedFullname.equals(classFullname)) {

			Ntro.logger().fatal(String.format("Cannot redeclare %s. %s %s was already declared as %s", classType.toLowerCase(), classType, className, storedFullname));

		}else {
			
			names.put(className, classFullname);
			classes.put(className, _class);

		}
		
	}

	@Override
	public <MSG extends MessageAbstr> void registerMessage(Class<MSG> messageClass) {
		registerClass(messages, messageClasses, messageClass, "Message");
	}

	@Override
	public <M extends Model> void registerModel(Class<M> modelClass) {
		registerClass(models, modelClasses, modelClass, "Model");
		
	}

	@Override
	public <V extends ModelOrValue> void registerValue(Class<V> valueClass) {
		registerClass(values, valueClasses, valueClass, "Value");
	}

	@Override
	public void registerBackend(Class<? extends Backend> backendClass) {
		this.backendClass  = backendClass;
		this.backend       = backendClass.getCanonicalName();
	}

	public void createBackendAndCallItToFillSpec() {

		this.backendObject = NtroCore.factory().newInstance(backendClass);
		
		if(backendObject.isLocalBackend()) {
			
			backendObject.asLocalBackend().createTasks(backendTasks);

		}
	}
	
	public void writeSpec() {
		String jsonString = NtroCore.reflection().toJsonObject(this).toJsonString();
		String basename = this.getClass().getSimpleName();
		
		String filepath = NtroCore.options().storagePath() + "/" + basename + ".json";
		
		String relativeFilepath = Paths.get(NtroCore.options().projectPath()).getParent().relativize(Paths.get(filepath)).toString();
		
		NtroCore.logger().info("writing project specification to " + relativeFilepath);
		NtroCore.storage().writeTextFileBlocking(filepath, jsonString);
	}

	public void createExecutableFromSpec() {
		registerNamedClasses(models, modelClasses);
		registerNamedClasses(values, valueClasses);
		registerNamedClasses(messages, messageClasses);

		backendTasks.createSubTasks(backendTaskFactory);
	}
	
	protected void registerNamedClasses(Map<String, String> names, Map<String, Class<?>> classes) {
		for(String name : names.keySet()) {
			Class<?> _class = classes.get(name);
			NtroCore.factory().registerNamedClass(_class);
		}
	}

	public void prepareToExecuteBackendTasks() {

		backendTaskFactory.prepareToExecuteTasks();
		
	}

	public void executeBackendTasks() {
		
		backendTaskFactory.executeTasks();
		
	}

	public void writeBackendGraph() {
		if(backendTaskFactory != null) {
			backendTaskFactory.writeGraph();
		}
	}

	@SuppressWarnings("unchecked")
	protected APP instantiateAppClass() {
		return (APP) NtroCore.factory().newInstance(appClass);
	}

	public void callExecutableCodeAndFillSpec() {
		APP app = instantiateAppClass();
		
		callExectuableCodeAndFillSpec(app);
	}
	
	protected void callExectuableCodeAndFillSpec(APP app) {
		app.registerMessages(this);
		app.registerModels(this);
		app.registerBackend(this);
		
		createBackendAndCallItToFillSpec();

		if(isRemoteBackend()) {

			backendHostname = backendObject.asRemoteBackend().getHostname();
			backendPort     = backendObject.asRemoteBackend().getPort();

		}
	}

	public void runExecutable() {
		if(isRemoteBackend()) {
			
			backendObject.asRemoteBackend().openConnection();

		}else {
			
			runLocalBackend();
			
		}
	}
	
	protected void runLocalBackend() {
		prepareToExecuteBackendTasks();
		executeBackendTasks();
	}

	public boolean isRemoteBackend() {
		boolean isRemote = false;
		
		if(backendObject != null) {
			isRemote = backendObject.isRemoteBackend();
		}

		return isRemote;
	}
}
