package ca.ntro.ntro_app_fx_impl.executable_spec;

import ca.ntro.app.frontend.View;
import ca.ntro.ntro_app_fx_impl.frontend.ViewRegistrarFxImpl;

public class ViewSpecFx extends ViewSpec {
	
	private String fxmlPath  = null;

	public ViewSpecFx(Class<? extends View<?>> viewClass, String fxmlPath) {
		super(viewClass);
		this.fxmlPath = fxmlPath;
	}

	public void registerView(ViewRegistrarFxImpl viewRegistrarFx) {
		viewRegistrarFx.registerView(viewClass(), fxmlPath);
	}

}
