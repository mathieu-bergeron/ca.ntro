package ca.ntro.ntro_app_fx_impl.executable_spec;

import static ca.ntro.app.tasks.frontend.FrontendTasks.*;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import ca.ntro.app.Ntro;
import ca.ntro.app.backend.RemoteBackendNtro;
import ca.ntro.app.events.Event;
import ca.ntro.app.events.EventRegistrar;
import ca.ntro.app.frontend.Frontend;
import ca.ntro.app.frontend.View;
import ca.ntro.app.frontend.ViewData;
import ca.ntro.app.services.Window;
import ca.ntro.app.session.AfterSessionFirstCreated;
import ca.ntro.app.session.Session;
import ca.ntro.app.session.SessionAccessor;
import ca.ntro.app.session.SessionRegistrar;
import ca.ntro.app.tasks.SimpleTaskCreator;
import ca.ntro.app.tasks.TaskGroupCreator;
import ca.ntro.app.tasks.frontend.FrontendTasks;
import ca.ntro.core.NtroCore;
import ca.ntro.core.locale.Locale;
import ca.ntro.core.services.JsonParseError;
import ca.ntro.core.tasks.Task;
import ca.ntro.ntro_app_fx_abstr.NtroClient;
import ca.ntro.ntro_app_fx_impl.NtroImpl;
import ca.ntro.ntro_app_fx_impl.backend.Backend;
import ca.ntro.ntro_app_fx_impl.frontend.ViewRegistrar;
import ca.ntro.ntro_app_fx_impl.tasks.frontend.FrontendSimpleTaskDescriptor;
import ca.ntro.ntro_app_fx_impl.tasks.frontend.FrontendTaskFactory;
import ca.ntro.ntro_app_fx_impl.tasks.frontend.FrontendTaskGroupDescriptor;
import ca.ntro.ntro_core_impl.NtroCoreImpl;
import ca.ntro.ntro_core_impl.json.JsonObject;
import ca.ntro.ntro_core_impl.task_graphs.generic_task_graph.SimpleTaskOptions;
import ca.ntro.ntro_core_impl.task_graphs.task_graph.ExecutableTaskNtro;
import ca.ntro.ntro_core_impl.task_graphs.task_graph.SimpleTask;
import ca.ntro.ntro_core_impl.task_graphs.task_graph.SimpleTaskNtro;
import ca.ntro.ntro_core_impl.task_graphs.task_graph_trace.TaskResultsCondition;
import ca.ntro.ntro_core_impl.task_graphs.task_graph_trace.TaskResultsEventHandler;
import ca.ntro.ntro_core_impl.task_graphs.task_graph_trace.TaskTraceNtro;
import ca.ntro.ntro_core_impl.tasks.TaskWrapper;

public abstract class ClientSpec<APP extends NtroClient> extends ExecutableSpec<APP> implements EventRegistrar, SessionRegistrar, ViewRegistrar {

	private           String      sessionId      = null;
	private transient Window      window         = null;
	
	private           String                    frontend       = null;
	private transient Class<? extends Frontend> frontendClass  = null;
	private transient Frontend<?>               frontendObject = null;
	
	private           FrontendTasksSpec   frontendTasks       = new FrontendTasksSpec();
	private transient FrontendTaskFactory frontendTaskFactory = new FrontendTaskFactory();

	private           Map<String, String>   events       = new HashMap<>();
	private transient Map<String, Class<?>> eventClasses = new HashMap<>();
	
	private           String                   session       = null;
	private transient Class<? extends Session> sessionClass  = Session.class;
	private transient Session                  sessionObject = null;
	
	private String                defaultLanguage = null;
	private String                defaultCountry = null;
	private Map<String, String>   translations  = new HashMap<>();

	private transient Map<String, Class<? extends View>> viewClasses = new HashMap<>();
			
	private Map<String, ViewSpec>     views        = new HashMap<>();
	
	private Map<String, String>             viewData        = new HashMap<>();
	private transient Map<String, Class<?>> viewDataClasses = new HashMap<>();
	
	protected Locale getDefaultLocale() {
		return Ntro.locale().buildLocale(defaultLanguage, defaultCountry);
	}

	protected Class<? extends View> getViewClass(String viewClassName){
		return viewClasses.get(viewClassName);
	}
	
	protected void forEachView(Consumer<ViewSpec> c) {
		for(ViewSpec view : views.values()) {
			c.accept(view);
		}
	}

	protected void forEachTranslation(BiConsumer<String, String> c) {
		for(Map.Entry<String, String> entry : translations.entrySet()) {
			c.accept(entry.getKey(), entry.getValue());
		}
	}
	
	protected void registerFrontendImpl(Class<? extends Frontend> frontendClass) {
		this.frontendClass = frontendClass;
		this.frontend      = frontendClass.getCanonicalName();
	}

	protected void createFrontendAndCallItToFillSpec() {
		if(frontendClass != null) {

			frontendObject = NtroCore.factory().newInstance(frontendClass);
			
			frontendObject.registerSessionClass(this);
			frontendObject.registerEvents(this);
			frontendObject.createTasks(frontendTasks);
			
			registerViews(frontendObject);
		}
		
		if(defaultLanguage == null) {
			defaultLanguage = Ntro.locale().currentLocale().language();
		}
		
		if(defaultCountry == null) {
			defaultCountry = Ntro.locale().currentLocale().country();
		}
		
	}
	
	public void registerWindow(Window window) {
		this.window = window;
	}

	public void registerSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	
	protected abstract void registerViews(Frontend<?> frontendObject);

	public boolean isFrontendRegistered() {
		return this.frontendObject != null;
	}

	public void prepareToFrontendExecuteTasks() {

		frontendTaskFactory.prepareToExecuteTasks();

		frontendTaskFactory.registerTaskTrace();

	}

	public void executeFrontendTasks() {
		
		frontendTaskFactory.executeTasks();
		
	}

	public void writeFrontendGraph() {
		if(frontendTaskFactory != null) {
			frontendTaskFactory.writeGraph();
		}
	}

	@Override
	public <E extends Event> void registerEvent(Class<E> eventClass) {
		registerClass(events, eventClasses, eventClass, "Event");
	}

	@Override
	public <S extends Session<?>> void registerSessionClass(Class<S> sessionClass) {
		this.sessionClass = sessionClass;
		this.session = sessionClass.getCanonicalName();
	}
	
	@Override
	public void createExecutableFromSpec() {
		super.createExecutableFromSpec();
		
		registerNamedClasses();

		createRegisteredSession(sessionId);
		
		createTasks();
		
	}

	private void createTasks() {
		addWindowTask();

		addViewLoaderTasks(frontendTaskFactory);
		
		addEventHandlerTasks();

		addSessionTask();

		frontendTasks.createSubTasks(frontendTaskFactory);
	}

	private void registerNamedClasses() {
		registerNamedClasses(events, eventClasses);

		for(Class<?> viewDataClass : viewDataClasses.values()) {
			NtroCore.factory().registerNamedClass(viewDataClass);
		}
		
		NtroCore.factory().registerNamedClass(sessionClass);
		
		forEachView(viewSpec -> {
			viewSpec.registerNamedClass();
		});
	}
	
	private void addEventHandlerTasks() {
		for(String eventName : events.keySet()) {

			Class<? extends Event> eventClass = (Class<? extends Event>) eventClasses.get(eventName);

			Task eventHandlerTask = frontendTaskFactory.orphanTask(event(eventClass), 
					                                               SimpleTaskOptions.taskClass(SimpleTaskNtro.class)
					                                                                .traceClass(TaskTraceNtro.class)
					                                                                .resultsClass(TaskResultsEventHandler.class))

					                                    .getTask();
			
			TaskWrapper eventHandlerTaskWrapper = (TaskWrapper) eventHandlerTask;
			
			SimpleTask eventHandler = (SimpleTask) eventHandlerTaskWrapper.getTaskImpl();
			
			NtroImpl.eventService().registerEventHandler(eventClass, eventHandler);
		}
	}

	protected abstract void addViewLoaderTasks(FrontendTaskFactory frontendTaskFactory);

	private void addWindowTask() {
		frontendTaskFactory.orphanTask(window(),
					                SimpleTaskOptions.taskClass(ExecutableTaskNtro.class)
					                                 .traceClass(TaskTraceNtro.class)
					                                 .resultsClass(TaskResultsCondition.class))

		                .executesAndReturnsValue(inputs -> {

		                	return window;
		                });
	}

	public <S extends Session<?>> void createRegisteredSession(String sessionId) {
		if(sessionClass != null) {
		
			this.sessionObject = loadOrCreateSession(sessionClass, sessionId);

			NtroImpl.registerSession(sessionObject);
		}

		Session<?> session = Ntro.session();
		if(SessionAccessor.isNamedSession(session)) {

			NtroCore.logger().info("session: " + session.sessionId());
			
		}else {

			NtroCore.logger().info("anonymous session (" + session.sessionId() + ")");

		}
	}

	private <S extends Session<?>> S loadOrCreateSession(Class<S> sessionClass, String sessionId) {
		
		S session = null;

		if(sessionId != null) {

			session = loadSessionFromFileIfPossible(sessionClass, sessionId);

		}
		
		if(session == null) {

			session = newSession(sessionClass, sessionId);

		}
		
		return session;
	}

	private <S extends Session<?>> S newSession(Class<S> sessionClass, String sessionId) {
		S session = NtroCoreImpl.factory().newInstance(sessionClass);

		if(sessionId != null) {

			SessionAccessor.registerSessionId(session, sessionId);
		}
		
		if(session instanceof AfterSessionFirstCreated) {
			
			((AfterSessionFirstCreated) session).afterSessionFirstCreated();
			
		}

		return session;
	}

	@SuppressWarnings("unchecked")
	private <S extends Session<?>> S loadSessionFromFileIfPossible(Class<S> sessionClass, String sessionId) {
		String sessionsPath = Ntro.options().coreOptions().sessionsPath();
		Path sessionFilepath = Paths.get(sessionsPath, sessionId + ".json");
		String rawSessionFilepath = sessionFilepath.toAbsolutePath().toString();
		
		S session = null;
		
		if(NtroCore.storage().ifFileExists(rawSessionFilepath)) {
			
			String sessionText = NtroCore.storage().readTextFile(rawSessionFilepath);
			
			try {

			JsonObject sessionJson = NtroCore.json().fromJsonString(sessionText);
			session = (S) NtroCore.reflection().fromJsonObject(sessionJson);
			SessionAccessor.registerSessionId(session, sessionId);

			}catch(JsonParseError e) {
				NtroCoreImpl.logger().warning("JsonParseError for " + rawSessionFilepath);
			}

		}

		return session;
	}

	public void addSessionTask() {
		Class<? extends Session> sessionClass = this.sessionClass;
		if(sessionClass == null) {
			sessionClass = Session.class;
		}

		frontendTaskFactory.orphanTask(session(sessionClass),
					                    SimpleTaskOptions.taskClass(ExecutableTaskNtro.class)
					                                     .traceClass(TaskTraceNtro.class)
					                                     .resultsClass(TaskResultsCondition.class))

		                .executesAndReturnsValue(inputs -> {
		                	
		                	return sessionObject;
		                });
	}

	@Override
	public void runExecutable() {
		super.runExecutable();

		SessionAccessor.reObserveModelSelections(Ntro.session());

		if(isFrontendRegistered()) {

			prepareToFrontendExecuteTasks();
			executeFrontendTasks();

		}else {

			NtroCore.logger().fatal("Please register a frontend.");

		}
	}

	@Override
	public void registerDefaultLocale(Locale locale) {
		defaultLanguage = locale.language();
		defaultCountry  = locale.country();
	}

	@Override
	public void registerTranslations(Locale locale, String resourcesPath) {
		translations.put(locale.language(), resourcesPath);
	}

	@Override
	public void registerViewData(Class<? extends ViewData> viewDataClass) {
		String viewDataName = viewDataClass.getSimpleName();
		String viewDataFullname = viewDataClass.getCanonicalName();

		viewData.put(viewDataName, viewDataFullname);
		viewDataClasses.put(viewDataName, viewDataClass);
	}

	protected void addViewSpec(String viewName, ViewSpec viewSpec) {
		views.put(viewName, viewSpec);
	}

	@SuppressWarnings("rawtypes")
	@Override
	protected void analyzeProjectClass(Class<?> _class) {
		super.analyzeProjectClass(_class);

		if(NtroCore.reflection().ifClassIsSubTypeOf(_class, Frontend.class)) {

			registerFrontendImpl((Class<? extends Frontend>) _class);

		} else if(NtroCore.reflection().ifClassIsSubTypeOf(_class, Event.class)) {
			
			registerEvent((Class<? extends Event>) _class);

		} else if(NtroCore.reflection().ifClassIsSubTypeOf(_class, Session.class)) {
			
			registerSessionClass((Class<? extends Session>) _class);

		} else if(NtroCore.reflection().ifClassIsSubTypeOf(_class, ViewData.class)) {
			
			registerViewData((Class<? extends ViewData>) _class);

		} else if(NtroCore.reflection().ifClassIsSubTypeOf(_class, View.class)) {

			viewClasses.put(_class.getCanonicalName(), (Class<? extends View>) _class);

		}
	}

	@Override
	protected void callExectuableCodeAndFillSpec(APP app) {
		super.callExectuableCodeAndFillSpec(app);
		
		createFrontendAndCallItToFillSpec();
	}

	@Override
	protected Class<? extends Backend> desiredBackendClass() {
		return RemoteBackendNtro.class;
	}

}
