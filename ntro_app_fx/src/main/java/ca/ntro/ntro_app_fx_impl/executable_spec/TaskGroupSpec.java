package ca.ntro.ntro_app_fx_impl.executable_spec;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ca.ntro.app.tasks.SimpleTaskCreator;
import ca.ntro.app.tasks.SubTasksLambda;
import ca.ntro.app.tasks.TaskGroupCreator;
import ca.ntro.ntro_app_fx_impl.tasks.SimpleTaskDescriptor;
import ca.ntro.ntro_app_fx_impl.tasks.TaskDescriptor;
import ca.ntro.ntro_app_fx_impl.tasks.TaskFactory;
import ca.ntro.ntro_app_fx_impl.tasks.TaskGroupDescriptor;
import ca.ntro.ntro_core_impl.NtroCoreImpl;
import ca.ntro.ntro_core_impl.exceptions.InvalidCharacterException;
import ca.ntro.ntro_core_impl.identifyers.Key;

public abstract class TaskGroupSpec<TASKS> extends TaskSpec implements TaskGroupCreator {
	
	private List<String>               taskCreationOrder = new ArrayList<>();
	private Map<String, TaskSpec>      simpleTasks       = new HashMap<>();
	private Map<String, TaskGroupSpec> taskGroups        = new HashMap<>();
	
	private transient Map<String, TaskDescriptor> descriptors = new HashMap<>();
	
	protected abstract TaskGroupSpec newTaskGroup();
	
	protected <O> SimpleTaskCreator<O> createTask(String taskId){
		try {

			Key.mustBeValidKey(taskId);

		} catch (InvalidCharacterException e) {
			NtroCoreImpl.logger().fatal("taskId must be a valid Java identifyer\n\n\t\t'" + taskId + "' is not valid");
		}
		
		TaskSpec task = new TaskSpec();

		taskCreationOrder.add(taskId);
		simpleTasks.put(taskId, task);

		return task;
	}

	protected <O> SimpleTaskCreator<O> createTask(SimpleTaskDescriptor<O> descriptor) {
		TaskSpec task = new TaskSpec();
		
		String taskId = descriptor.id();
		descriptors.put(taskId, descriptor);

		taskCreationOrder.add(taskId);
		simpleTasks.put(taskId, task);

		return task;
	}
	
	protected <O> TaskGroupCreator<O, TASKS> createTaskGroup(String taskGroupId){
		try {

			Key.mustBeValidKey(taskGroupId);

		} catch (InvalidCharacterException e) {
			NtroCoreImpl.logger().fatal("taskId must be a valid Java identifyer\n\n\t\t'" + taskGroupId + "' is not valid");
		}

		TaskGroupSpec taskGroup = newTaskGroup();
		
		taskCreationOrder.add(taskGroupId);
		taskGroups.put(taskGroupId, taskGroup);

		return taskGroup;
	}

	protected <O> TaskGroupCreator<O, TASKS> createTaskGroup(TaskGroupDescriptor<O> descriptor) {
		TaskGroupSpec taskGroup = newTaskGroup();
		
		String taskGroupId = descriptor.id();
		descriptors.put(taskGroupId, descriptor);
		
		taskCreationOrder.add(taskGroupId);
		taskGroups.put(taskGroupId, taskGroup);

		return taskGroup;
	}


	@Override
	public TaskGroupCreator contains(SubTasksLambda subTasksLambda) {
		subTasksLambda.createSubTasks((TASKS) this);

		return this;
	}

	public void createSubTasks(TaskFactory taskFactory) {
		for(String taskId : taskCreationOrder) {
			
			TaskSpec task = simpleTasks.get(taskId);
			
			if(task != null) {
				
				TaskDescriptor descriptor = descriptors.get(taskId);

				SimpleTaskCreator taskCreator = null;
				
				if(descriptor != null) {
					taskCreator = taskFactory.task((SimpleTaskDescriptor) descriptor);
				}else {
					taskCreator = taskFactory.task(taskId);
				}

				task.addWaitsFors(taskCreator);
				task.addExecutor(taskCreator);
				
			}else {
				
				task = taskGroups.get(taskId);
				
				if(task != null) {
					
					TaskGroupSpec taskGroup = (TaskGroupSpec) task;

					TaskDescriptor descriptor = descriptors.get(taskId);

					TaskGroupCreator taskGroupCreator = null;
					
					if(descriptor != null) {
						taskGroupCreator = taskFactory.taskGroup((TaskGroupDescriptor) descriptor);
					}else {
						taskGroupCreator = taskFactory.taskGroup(taskId);
					}
					
					taskGroup.addWaitsFors(taskGroupCreator);

					taskGroupCreator.contains(subTasks -> {

						taskGroup.createSubTasks((TaskFactory)subTasks);

					});
				}
			}
		}
		
	}


}
