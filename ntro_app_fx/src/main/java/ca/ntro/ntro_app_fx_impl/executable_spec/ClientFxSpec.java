package ca.ntro.ntro_app_fx_impl.executable_spec;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ca.ntro.app.Ntro;
import ca.ntro.app.NtroClientFx;
import ca.ntro.app.frontend.Frontend;
import ca.ntro.app.frontend.FrontendRegistrarFx;
import ca.ntro.app.frontend.View;
import ca.ntro.app.frontend.ViewRegistrarFx;
import ca.ntro.core.NtroCore;
import ca.ntro.core.util.StringUtils;
import ca.ntro.ntro_app_fx_impl.frontend.ViewRegistrarFxImpl;
import ca.ntro.ntro_app_fx_impl.tasks.frontend.FrontendTaskFactory;

public class ClientFxSpec<APP extends NtroClientFx> extends ClientSpec<APP> implements FrontendRegistrarFx, ViewRegistrarFx {

	private String cssPath = null;
	private transient long cssLastModified = 0;

	private transient ViewRegistrarFxImpl viewRegistrarFx = new ViewRegistrarFxImpl();

	@Override
	public void registerFrontend(Class<? extends Frontend<ViewRegistrarFx>> frontendClass) {
		registerFrontendImpl(frontendClass);
	}


	@SuppressWarnings("unchecked")
	@Override
	protected void registerViews(Frontend<?> frontendObject) {
		((Frontend<ViewRegistrarFx>) frontendObject).registerViews(this);
	}


	@Override
	protected void addViewLoaderTasks(FrontendTaskFactory frontendTaskFactory) {
		viewRegistrarFx.registerDefaultLocale(getDefaultLocale());
		
		if(cssPath != null) {
			viewRegistrarFx.registerStylesheet(cssPath);
		}


		forEachTranslation((lang, resourcePath) -> {
			viewRegistrarFx.registerTranslations(Ntro.buildLocale(lang), resourcePath);
		});
		
		forEachView(viewSpec -> {
			if(viewSpec instanceof ViewSpecFx) {
				((ViewSpecFx) viewSpec).registerView(viewRegistrarFx);
			}
		});
		
		viewRegistrarFx.addViewLoaderTasks(frontendTaskFactory);
	}

	@Override
	protected void callExectuableCodeAndFillSpec(APP app) {
		super.callExectuableCodeAndFillSpec(app);

		app.registerFrontend(this);
	}

	@Override
	public <V extends View<?>> void registerView(Class<V> viewClass, String fxmlPath) {
		String viewName = viewClass.getSimpleName();
		
		addViewSpec(viewName, new ViewSpecFx(viewClass, fxmlPath));
	}

	@Override
	public <V extends View<?>> void registerFragment(Class<V> fragmentClass, String fxmlPath) {
		String fragmentName = fragmentClass.getSimpleName();
		
		addViewSpec(fragmentName, new ViewSpecFx(fragmentClass, fxmlPath));
	}

	@Override
	public void registerStylesheet(String cssPath) {
		this.cssPath = cssPath;
	}
	
	private static final Pattern FX_CONTROLLER_RE = Pattern.compile("fx:controller\\s*=\\s*[\"']([^\"']+)[\"']");

	@Override
	protected void analyzeProjectResourceFile(File resourcesRootDir, File file, String basename, String ext) {
		super.analyzeProjectResourceFile(resourcesRootDir, file, basename, ext);

		if(ext.equalsIgnoreCase("css")) {
			
			if(file.lastModified() > cssLastModified) {

				Path resourcePath = resourcesRootDir.toPath().relativize(file.toPath());
				String resourcePathRaw = StringUtils.windowsPathToUnix(resourcePath.toString());

				registerStylesheet("/" + resourcePathRaw);
				
				cssLastModified = file.lastModified();
			}
			

		} else if(ext.equalsIgnoreCase("fxml")
				|| ext.equalsIgnoreCase("xml")) {
			
			try {

				Scanner fileScanner = new Scanner(new FileInputStream(file));
				int lineNum = 1;
				while(fileScanner.hasNextLine()) {

					String line = fileScanner.nextLine();
					Matcher matcher = FX_CONTROLLER_RE.matcher(line);
					boolean found =  matcher.find();
					if(found) {
						String classFullname = matcher.group(1);
						
						try {

							Class<?> _class = ClientFxSpec.class.forName(classFullname);

							Path resourcePath = resourcesRootDir.toPath().relativize(file.toPath());
							String resourcePathRaw = StringUtils.windowsPathToUnix(resourcePath.toString());
							
							registerView((Class<? extends View>) _class, "/" + resourcePathRaw);

						} catch (ClassNotFoundException e) {
							
							NtroCore.logger().fatal("(" + basename + "." + ext + ":" + lineNum + ") fx:controller class not found: " + classFullname);
						}
						
						break;
					}
					
					lineNum++;
				}

			} catch (FileNotFoundException e) {

				NtroCore.logger().warning("cannot load file: " + file);

			}
			

		} else if(ext.equalsIgnoreCase("properties")) {
			String[] segments = basename.split("\\.");
			if(segments.length > 0) {
				String lang = segments[segments.length-1];
				
				Path resourcePath = resourcesRootDir.toPath().relativize(file.toPath());
				String resourcePathRaw = StringUtils.windowsPathToUnix(resourcePath.toString());
				
				registerTranslations(Ntro.locale().buildLocale(lang), "/" + resourcePathRaw);
			}
		}
	}


}
