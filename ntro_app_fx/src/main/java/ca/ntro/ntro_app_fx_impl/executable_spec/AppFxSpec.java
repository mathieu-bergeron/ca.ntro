package ca.ntro.ntro_app_fx_impl.executable_spec;

import ca.ntro.app.NtroAppFx;
import ca.ntro.app.backend.LocalBackendNtro;
import ca.ntro.ntro_app_fx_impl.backend.Backend;

public class AppFxSpec extends ClientFxSpec<NtroAppFx> {

	@Override
	protected Class<? extends Backend> desiredBackendClass() {
		return LocalBackendNtro.class;
	}

}
