package ca.ntro.ntro_app_fx_impl.executable_spec;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ca.ntro.app.tasks.SimpleTaskCreator;
import ca.ntro.app.tasks.frontend.BlockingFrontendExecutor;
import ca.ntro.app.tasks.frontend.TypedBlockingFrontendExecutor;
import ca.ntro.core.NtroCore;
import ca.ntro.core.tasks.Task;
import ca.ntro.ntro_app_fx_impl.tasks.TaskCreator;
import ca.ntro.ntro_app_fx_impl.tasks.TaskDescriptor;
import ca.ntro.ntro_app_fx_impl.tasks.WaitsFor;
import ca.ntro.ntro_core_impl.NtroCoreImpl;
import ca.ntro.ntro_core_impl.values.CodeLocation;

public class TaskSpec implements SimpleTaskCreator, TaskCreator, Task {

	private           List<String>       waitsFors         = new ArrayList<>();

	private boolean          returnsResult = false;
	private transient Object executor      = null;
	
	private transient Map<String, TaskDescriptor> waitsForDescriptors = new HashMap<>();
	private transient Map<String, Throwable>      deferredExceptions  = new HashMap<>();
	private transient Map<String, CodeLocation>   waitsForLocations   = new HashMap<>();
	

	@Override
	public Task getTask() {
		return this;
	}

	@Override
	public TaskCreator waitsFor(String taskId) {
		CodeLocation waitsForLocation = NtroCoreImpl.stackAnalyzer().callingLocationOf(new Throwable());

		waitsFors.add(taskId);
		waitsForLocations.put(taskId, waitsForLocation);
		deferredExceptions.put(taskId, new Throwable(""));

		return this;
	}

	@Override
	public TaskCreator waitsFor(TaskDescriptor descriptor) {
		CodeLocation waitsForLocation = NtroCoreImpl.stackAnalyzer().callingLocationOf(new Throwable());

		String taskId = descriptor.id();
		
		waitsFors.add(taskId);
		waitsForDescriptors.put(taskId, descriptor);
		waitsForLocations.put(taskId, waitsForLocation);
		deferredExceptions.put(taskId, new Throwable(""));

		return this;
	}

	@Override
	public SimpleTaskCreator executes(BlockingFrontendExecutor executor) {
		returnsResult = false;
		this.executor = executor;

		return this;
	}

	@Override
	public SimpleTaskCreator executesAndReturnsValue(TypedBlockingFrontendExecutor executor) {
		returnsResult = true;
		this.executor = executor;

		return this;
	}

	@Override
	public void cancel() {
	}

	public void addWaitsFors(WaitsFor taskCreator) {
		for(String waitsForId : waitsFors) {
			
			TaskDescriptor descriptor = waitsForDescriptors.get(waitsForId);
			
			if(descriptor != null) {

				try {

					taskCreator.waitsFor(descriptor);

				}catch(RuntimeException e) {

					fatal(waitsForId, e.getMessage());

				}

			}else {
				
				try {
				
				taskCreator.waitsFor(waitsForId);

				}catch(RuntimeException e) {

					fatal(waitsForId, e.getMessage());

				}
			}
		}
	}
	
	private void fatal(String waitsForId, String msg) {
		CodeLocation codeLocation   = waitsForLocations.get(waitsForId);
		Throwable deferredException = deferredExceptions.get(waitsForId);
		
		if(deferredException != null) {

			NtroCore.logger().fatal(msg, deferredException);
			
		}else {

			NtroCore.logger().fatal(msg);
			
		}
		
		/*
		if(codeLocation != null) {

			NtroCore.logger().fatal(msg, codeLocation);

		}else {

			NtroCore.logger().fatal(msg);

		}
		*/
	}

	public void addExecutor(SimpleTaskCreator taskCreator) {
		if(returnsResult && executor != null) {
			
			taskCreator.executesAndReturnsValue((TypedBlockingFrontendExecutor) executor);
			
		}else if(!returnsResult && executor != null) {
			
			taskCreator.executes((BlockingFrontendExecutor) executor);
			
		}
	}

}
