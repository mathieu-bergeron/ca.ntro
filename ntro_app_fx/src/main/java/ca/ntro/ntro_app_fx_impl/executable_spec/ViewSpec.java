package ca.ntro.ntro_app_fx_impl.executable_spec;

import ca.ntro.app.frontend.View;
import ca.ntro.core.NtroCore;

public class ViewSpec {

	private String viewClass = null;

	private transient Class<? extends View<?>> _viewClass = null;

	public ViewSpec(Class<? extends View<?>> viewClass) {
		this._viewClass = viewClass;
		this.viewClass = viewClass.getCanonicalName();
	}
	
	protected Class<? extends View<?>> viewClass(){
		return _viewClass;
	}

	public void registerNamedClass() {
		NtroCore.factory().registerNamedClass(_viewClass);
	}
	

}
