package ca.ntro.ntro_app_fx_impl.fatal_dialog;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import ca.ntro.core.util.StringUtils;

public class FatalDialogSwing {
	
	public static void main(String[] args) {
		String message = "";
		String details = "";
		if(args.length > 0) {
			message = args[0];
		}
		if(args.length > 1) {
			details = args[1];
		}
		
		if(!StringUtils.isNullOrEmpty(details)) {
			message += "\n\n\n" + details;
		}
		
		JOptionPane.showMessageDialog(null, message, "FATAL", JOptionPane.ERROR_MESSAGE);
		
		/*
		JFrame frame = new JFrame("FATAL");
		JDialog dialog = new JDialog(frame, true);
		JOptionPane optionPane = new JOptionPane(message, JOptionPane.ERROR_MESSAGE);
		dialog.setContentPane(optionPane);
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		dialog.pack();
		dialog.setVisible(true);
		frame.setVisible(true);
		*/
		
		
	}

}