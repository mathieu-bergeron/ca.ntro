package ca.ntro.ntro_app_fx_impl;

import java.nio.file.Path;
import java.nio.file.Paths;

import ca.ntro.app.events.Event;
import ca.ntro.app.services.EventService;
import ca.ntro.app.services.MessageService;
import ca.ntro.app.services.ModelStore;
import ca.ntro.app.services.Window;
import ca.ntro.app.session.Session;
import ca.ntro.app.session.SessionAccessor;
import ca.ntro.core.services.JsonParseError;
import ca.ntro.ntro_app_fx_impl.options.OptionsImpl;
import ca.ntro.ntro_app_fx_impl.services.EventServiceNtro;
import ca.ntro.ntro_app_fx_impl.services.MessageServiceJdk;
import ca.ntro.ntro_app_fx_impl.services.ModelStoreDefault;
import ca.ntro.ntro_core_impl.NtroCoreImpl;
import ca.ntro.ntro_core_impl.json.JsonObject;

public class NtroImpl {
	
	
	/* <Events> */

	private static EventService eventService = new EventServiceNtro();

	static void registerEventService(EventService eventService){
		NtroImpl.eventService = eventService;
	}
	
	public static EventService eventService() {
		return NtroImpl.eventService;
	}

	public static <E extends Event> E newEvent(Class<E> eventClass) {
		return eventService.newEvent(eventClass);
	}

	/* </Events> */
	
	
	
	
	/* <Messages> */

	private static MessageService messageService = new MessageServiceJdk();

	public static void registerMessageService(MessageService messageService){
		NtroImpl.messageService = messageService;
	}

	public static MessageService messageService() {
		return NtroImpl.messageService;
	}

	/* </Messages> */
	

	/* <ModelStore> */

	private static ModelStore modelStore = new ModelStoreDefault();

	static void registerModelStore(ModelStore modelStore){
		NtroImpl.modelStore = modelStore;
	}

	public static ModelStore models() {
		return NtroImpl.modelStore;
	}


	/* </ModelStore> */


	/* <StackAnalyzer> */

	public static void assertNotNull(String message, Object value) {
		NtroCoreImpl.stackAnalyzer.incrementCallDistanceForCurrentThread();

		NtroCoreImpl.asserter().assertNotNull(message, value);

		NtroCoreImpl.stackAnalyzer.decrementCallDistanceForCurrentThread();
	}

	public static void assertNotNull(Object value) {
		NtroCoreImpl.stackAnalyzer.incrementCallDistanceForCurrentThread();

		NtroCoreImpl.asserter().assertNotNull(value);

		NtroCoreImpl.stackAnalyzer.decrementCallDistanceForCurrentThread();
	}

	/* </StackAnalyzer> */

	/* <Window> */
	
	private static Window window = null;
	
	public static void registerWindow(Window window) {
		NtroImpl.window = window;
	}

	public static Window window() {
		return window;
	}

	/* </Window> */
	
	
	/* <Session> */
	
	private static Session session = new Session();
	
	public static void registerSession(Session session) {
		NtroImpl.session = session;
	}
	
	public static <S extends Session> S session() {
		return (S) session;
	}

	public static void writeSessionFile() {
		if(SessionAccessor.isNamedSession(session)) {
			String sessionText = NtroCoreImpl.reflection().toJsonObject(session).toJsonString(true);
			String sessionsPath = NtroImpl.options().coreOptions().sessionsPath();
			
			Path sessionFilePath = Paths.get(sessionsPath, session.sessionId() + ".json");
			
			NtroCoreImpl.storage().writeTextFileBlocking(sessionFilePath.toAbsolutePath().toString(), sessionText);
		}
	}

	/* </Session> */

	/* <CoreOptions> */
	
	private static OptionsImpl options;
	
	public static void registerOptions(OptionsImpl options) {
		NtroImpl.options = options;
	}

	public static OptionsImpl options(){
		return NtroImpl.options;
	}

	public static void writeOptionsFile() {
		if(SessionAccessor.isNamedSession(session)) {

			String optionsText = NtroCoreImpl.reflection().toJsonObject(options).toJsonString(true);

			String optionsPath = NtroImpl.options().coreOptions().optionsPath();
			
			Path optionsFilePath = Paths.get(optionsPath, session.sessionId() + ".json");
			
			NtroCoreImpl.storage().writeTextFileBlocking(optionsFilePath.toAbsolutePath().toString(), optionsText);
		}
	}

	public static void loadOptionsFile() {
		if(SessionAccessor.isNamedSession(session)) {
			String sessionId = session.sessionId();

			String optionsPath = NtroImpl.options().coreOptions().optionsPath();
			Path optionsFilepath = Paths.get(optionsPath, sessionId + ".json");
			String rawOptionsFilepath = optionsFilepath.toAbsolutePath().toString();
			
			if(NtroCoreImpl.storage().ifFileExists(rawOptionsFilepath)) {

				String optionsText = NtroCoreImpl.storage().readTextFile(rawOptionsFilepath);
				
				try {

					JsonObject optionsJson = NtroCoreImpl.json().fromJsonString(optionsText);
					options = (OptionsImpl) NtroCoreImpl.reflection().fromJsonObject(optionsJson);
					options.resolvePaths();
					
				} catch(JsonParseError e) {
					
					NtroCoreImpl.logger().warning("JsonParseError for " + rawOptionsFilepath);
					
				}

			}
			
			NtroCoreImpl.logger().info("options: " + sessionId);

		}else {

			NtroCoreImpl.logger().info("default options");
			
		}
	}

	/* </CoreOptions> */


	/* <Mode> */
	
	private static NtroMode ntroMode = NtroMode.APP;
	
	static void registerNtroMode(NtroMode ntroMode) {
		NtroImpl.ntroMode = ntroMode;
	}

	public static boolean isApp() {
		return ntroMode == NtroMode.APP;
	}
	
	public static boolean isClient() {
		return ntroMode == NtroMode.CLIENT;
	}

	public static boolean isServer() {
		return ntroMode == NtroMode.SERVER;
	}

	/* </Mode> */

	/* <OS> */
	
	private static NtroOs ntroOs = NtroOs.WINDOWS;
	
	static {
		
		String osName = System.getProperty("os.name").toLowerCase();
		
		if(osName.contains("linux")) {
			
			ntroOs = NtroOs.LINUX;
			
		}else if(osName.contains("mac")) {

			ntroOs = NtroOs.MAC;

		}
	}

	public static NtroOs ntroOs() {
		return ntroOs;
	}

	public static boolean isWindows() {
		return ntroOs == NtroOs.WINDOWS;
	}
	
	public static boolean isLinux() {
		return ntroOs == NtroOs.LINUX;
	}

	public static boolean isMac() {
		return ntroOs == NtroOs.MAC;
	}

	/* </OS> */
	
	
}
