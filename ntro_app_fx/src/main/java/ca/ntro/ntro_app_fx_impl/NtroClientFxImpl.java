package ca.ntro.ntro_app_fx_impl;

import java.util.Map;

import ca.ntro.app.NtroClientFx;
import ca.ntro.ntro_app_fx_impl.executable_spec.ClientFxSpec;
import ca.ntro.ntro_app_fx_impl.executable_spec.ExecutableSpec;
import ca.ntro.ntro_core_impl.NtroCoreImpl;
import ca.ntro.ntro_core_impl.services.StackAnalyzerImpl;
import javafx.application.Application;

public class NtroClientFxImpl extends NtroExecutableImpl<NtroClientFx, ClientFxSpec> {
	
	private static final NtroClientFxImpl instance = new NtroClientFxImpl();
	
	public static NtroClientFxImpl  instance() {
		return instance;
	}
	
	private String sessionId;

	public void loadSession(String filePath) {
		initializeIfNeeded();
		failIfAlreadyLaunched("loadSession");
	}
	
	@Override
	protected void initializeWithParams(Map<String, String> params) {
		if(params.containsKey("session")) {
			sessionId = params.get("session");
		}
	}


	@Override
	protected void launchImpl(Class<? extends NtroClientFx> clientClass, ClientFxSpec spec) {

		AppWrapperFx.sessionId = sessionId;
        AppWrapperFx.appClass  = clientClass;
        AppWrapperFx.spec      = spec;
        
    	((StackAnalyzerImpl) NtroCoreImpl.stackAnalyzer()).registerAppWrapperClass(AppWrapperFx.class);
        
        try {

			Application.launch(AppWrapperFx.class);
			
        }catch(Throwable t) {
        	
        	NtroCoreImpl.logger().fatal(t);

		}

	}

	@Override
	protected void initializeNtroMode() {
		NtroImpl.registerNtroMode(NtroMode.CLIENT);
	}

	@Override
	protected ExecutableSpec createEmptySpec() {
		return new ClientFxSpec();
	}



}
