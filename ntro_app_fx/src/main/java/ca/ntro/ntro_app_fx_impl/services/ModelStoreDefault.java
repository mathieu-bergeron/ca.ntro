/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.ntro_app_fx_impl.services;

import java.nio.file.Paths;

import ca.ntro.app.Ntro;
import ca.ntro.app.models.Model;
import ca.ntro.app.models.WatchJson;
import ca.ntro.app.models.WriteObjectGraph;
import ca.ntro.app.services.ModelStore;
import ca.ntro.core.NtroCore;
import ca.ntro.core.services.JsonParseError;
import ca.ntro.core.stream.Stream;
import ca.ntro.core.stream.Visitor;
import ca.ntro.core.util.StringUtils;
import ca.ntro.ntro_app_fx_impl.NtroImpl;
import ca.ntro.ntro_app_fx_impl.modified.Observable;
import ca.ntro.ntro_app_fx_impl.modified.ObservationNtro;
import ca.ntro.ntro_app_fx_impl.structures.ClassIdMap;
import ca.ntro.ntro_core_impl.NtroCoreImpl;
import ca.ntro.ntro_core_impl.json.JsonObject;
import ca.ntro.ntro_core_impl.path.Path;
import ca.ntro.ntro_core_impl.reflection.object_graph.ObjectGraph;
import ca.ntro.ntro_core_impl.stream.StreamNtro;

public class ModelStoreDefault implements ModelStore {
	
	private boolean areDiskOperationsEnabled = true;
	
	private ClassIdMap<Object, Object> previousModels = new ClassIdMap<>();
	private ClassIdMap<Object, Object> currentModels = new ClassIdMap<>();

	private ClassIdMap<Object, Long> modelsLastModified = new ClassIdMap<>();

	@Override
	public <M extends Model> M load(Class<?> modelClass, String modelId) {

		M model = (M) currentModels.get(modelClass, modelId);
		
		if(model == null) {

			model = (M) loadFromFile(modelClass, modelId);

		}
		
		if(model == null) {
			
			model = (M) NtroCore.factory().newInstance(modelClass);

		}
		
		try {

			//previousModels.put(modelClass, modelId, NtroCoreImpl.reflection().clone(model));
			previousModels.put(modelClass, modelId, model);

		}catch(Throwable t) {
			
			NtroCore.logger().fatal("cannnot clone model " + modelClass.getSimpleName(), t);

		}

		currentModels.put(modelClass, modelId, model);
		
		return model;
	}
	
	@Override
	public <M extends Model> M load(Class<?> modelClass) {
		return load(modelClass, null);
	}

	private synchronized <M extends Model> M loadFromJsonString(Class<?> modelClass, 
																String modelId, 
			                                                    String jsonString) {
		M model = null;

			
		try {

			JsonObject jsonObject = NtroCore.json().fromJsonString(jsonString);
			model = (M) NtroCoreImpl.reflection().graphFromJsonObject(jsonObject).buildObject();

		}catch(JsonParseError t) {

			NtroCore.logger().warning("JsonParseError for " + filePathFromClass(modelClass, modelId).toString());

		}catch(Throwable t) {
			
			NtroCore.logger().warning("Loading error for: " + filePathFromClass(modelClass, modelId).toString() + ". Using empty model");
			model = (M) NtroCore.factory().newInstance(modelClass);
		}

		return model;
	}


	private synchronized <M extends Model> M loadFromFile(Class<?> modelClass, String modelId) {
		M model = null;

		String filePath = filePathFromClass(modelClass, modelId);

		if(NtroCore.storage().ifFileExists(filePath)) {

			String jsonString = NtroCore.storage().readTextFile(filePath);
			
			model = loadFromJsonString(modelClass, modelId, jsonString);

		}else {

			NtroCore.logger().warning("Json file not found for: " + filePathFromClass(modelClass, modelId).toString() + ". Using empty model");
			model = (M) NtroCore.factory().newInstance(modelClass);

		}

		return model;
	}

	private String filePathFromClass(Class<?> modelClass, String modelId) {
		String path = null;
		
		if(StringUtils.isNullOrEmpty(modelId)) {
			
			path = Paths.get(NtroCoreImpl.options().modelsPath(), NtroCoreImpl.reflection().simpleName(modelClass) + ".json").toAbsolutePath().toString();
			
		}else {

			path = Paths.get(NtroCoreImpl.options().modelsPath(), NtroCoreImpl.reflection().simpleName(modelClass), modelId + ".json").toAbsolutePath().toString();

		}

		return path;
	}

	@Override
	public void save(Class<?> modelClass, Object model) {
		save(modelClass, null, model);
	}

	@Override
	public void save(Class<?> modelClass, String modelId, Object model) {

		Object previousModel = previousModels.get(modelClass, modelId);
		
		if(model instanceof WatchJson
				&& areDiskOperationsEnabled) {

			writeModelFile(filePathFromClass(modelClass, modelId), modelClass, modelId, model);

		}
			
		pushObservation(modelClass, modelId, previousModel, model);
	}

	private synchronized void writeModelFile(String filePath, Class<?> modelClass, String modelId, Object model) {
		ObjectGraph graph = NtroCoreImpl.reflection().graphFromObject(model);
		
		JsonObject jsonObject = graph.buildJsonObject();
		String jsonString = jsonObject.toJsonString(true);

		long lastModified = NtroCore.storage().writeTextFileBlocking(filePath, jsonString);

		modelsLastModified.put(modelClass, modelId, lastModified);
	}

	private synchronized void writeModelFileBlocking(String filePath, Class<?> modelClass, String modelId, Object model) {
		ObjectGraph graph = NtroCoreImpl.reflection().graphFromObject(model);
		
		JsonObject jsonObject = graph.buildJsonObject();
		String jsonString = jsonObject.toJsonString(true);

		long lastModified = NtroCore.storage().writeTextFileBlocking(filePath, jsonString);

		modelsLastModified.put(modelClass, modelId, lastModified);
	}

	@Override
	public void writeModelFiles() {
		currentModels.entries().forEach(entry -> {
			Class<?> modelClass = entry.entryClass();
			String modelId = entry.entryId();
			Object model = entry.value();
			String filePath = filePathFromClass(modelClass, modelId);

			writeModelFileBlocking(filePath, modelClass, modelId, model);
		});
	}


	@Override
	public void writeGraphs() {
		currentModels.entries().forEach(entry -> {
			Object model = entry.value();

			if(model instanceof WriteObjectGraph) {
				writeModelGraph(entry.entryClass(), entry.entryId(), model);
			}
		});
	}

	private void writeModelGraph(Class<?> modelClass, String modelId, Object model) {
		ObjectGraph graph = NtroCoreImpl.reflection().graphFromObject(model, graphName(modelClass, modelId));
		graph.write(NtroCoreImpl.graphWriter());
	}
	
	private String graphName(Class<?> modelClass, String modelId) {

		String graphName = NtroCoreImpl.reflection().simpleName(modelClass);
		
		if(!StringUtils.isNullOrEmpty(modelId)) {

			graphName += "_" + modelId;

		}
		
		return graphName;
	}
	
	

	@Override
	public void watch(Class<? extends Observable> modelClass, String modelId) {

		String filePath = filePathFromClass(modelClass, modelId);

		Object model = load(modelClass, modelId);

		createModelFileIfNeeded(filePath, modelClass, modelId, model);
		
		pushFirstObservation(modelClass, modelId);

		if(NtroCoreImpl.reflection().ifClassImplements(modelClass, WatchJson.class)) {
			
			NtroCore.storage().watchFileContent(filePath, (lastModified, jsonString) -> {

				onModelFileChanged(modelClass, modelId, lastModified, jsonString);

			});
		}
	}

	@Override
	public void watch(Class<? extends Observable> modelClass) {
		watch(modelClass, null);
	}

	private void createModelFileIfNeeded(String filePath, Class<?> modelClass, String modelId, Object model) {
		if(!NtroCore.storage().ifFileExists(filePath)) {
			writeModelFile(filePath, modelClass, modelId, model);
		}
	}

	private void onModelFileChanged(Class<? extends Observable> modelClass, String modelId, long lastModified, String jsonString) {
		Long previousLastModified = modelsLastModified.get(modelClass, modelId);

		if(previousLastModified == null || lastModified > previousLastModified) {

			modelsLastModified.put(modelClass, modelId, lastModified);

			Object previousModel = currentModels.get(modelClass, modelId);
			Object currentModel = loadFromJsonString(modelClass, modelId, jsonString);
			
			if(currentModel !=  null) {

				currentModels.put(modelClass, modelId, currentModel);
				pushObservation(modelClass, modelId, previousModel, currentModel);
			}
		}
	}
	
	private void pushFirstObservation(Class<? extends Observable> modelClass, String modelId) {
		Object previousModel = NtroCore.factory().newInstance(modelClass);
		Object currentModel = load(modelClass, modelId);

		pushObservation(modelClass, modelId, previousModel, currentModel);
	}

	@Override
	public ObservationNtro getCurrentModelObservation(Class<? extends Observable> modelClass, String modelId) {
		Object previousModel = previousModels.get(modelClass, modelId);
		Object currentModel = load(modelClass, modelId);
		
		return createObservation(modelId, previousModel, currentModel);
	}

	private ObservationNtro createObservation(String modelId, Object previousModel, Object currentModel) {
		ObservationNtro observation = new ObservationNtro<>();

		observation.setId(modelId);
		observation.setPreviousValue((Observable) previousModel);
		observation.setCurrentValue((Observable) currentModel);
		return observation;
	}


	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void pushObservation(Class<?> modelClass, 
			                     String modelId, 
			                     Object previousModel, 
			                     Object currentModel) {

		if(previousModel != null) {
			
			ObservationNtro observation = createObservation(modelId, previousModel, currentModel);

			//Revisions revisions = Ntro.reflection().revisionsFromTo(initialModel, currentModel);
			
			NtroImpl.messageService().receiveObservationFromServer((Class<? extends Observable>) modelClass, modelId, observation);
			NtroImpl.messageService().pushObservationToClients((Class<? extends Observable>) modelClass, modelId, observation);
		}
	}



	@Override
	public Stream<Model> modelStream() {
		return new StreamNtro<Model>() {
			@Override
			public void forEach_(Visitor<Model> visitor) throws Throwable {
				currentModels.values().forEach(model -> {
					visitor.visit((Model) model);
				});
			}
		};
	}

	@Override
	public void suspendDiskOperations() {
		this.areDiskOperationsEnabled = false;
	}

	@Override
	public void resumeDiskOperations() {
		this.areDiskOperationsEnabled = true;
	}

	@Override
	public <M extends Model> void delete(Class<M> modelClass, String modelId) {
		deleteModelFile(modelClass, modelId);
	}

	@Override
	public <M extends Model> void delete(Class<M> modelClass) {
		deleteModelFile(modelClass);
	}

	private synchronized void deleteModelFile(Class<?> modelClass, String modelId) {
		String filePath = filePathFromClass(modelClass, modelId);
		if(NtroCore.storage().ifFileExists(filePath)) {
			NtroCore.storage().deleteTextFile(filePath);
		}
	}

	private synchronized void deleteModelFile(Class<?> modelClass) {
		deleteModelFile(modelClass, null);
	}

}
