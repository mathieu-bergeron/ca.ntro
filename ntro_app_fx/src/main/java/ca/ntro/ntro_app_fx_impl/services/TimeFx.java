package ca.ntro.ntro_app_fx_impl.services;

import java.util.Timer;
import java.util.TimerTask;

import ca.ntro.ntro_core_impl.services.TimeJdk;
import ca.ntro.ntro_core_impl.task_graphs.base.AtomicTaskMutator;
import ca.ntro.ntro_core_impl.task_graphs.generic_task_graph.SimpleTaskOptions;
import ca.ntro.ntro_core_impl.task_graphs.handlers.CancelHandler;
import ca.ntro.ntro_core_impl.task_graphs.task_graph.ExecutableTaskNtro;
import ca.ntro.ntro_core_impl.task_graphs.task_graph.SimpleTask;
import ca.ntro.ntro_core_impl.task_graphs.task_graph.TaskContainer;
import ca.ntro.ntro_core_impl.task_graphs.task_graph_trace.TaskResultsEventHandler;
import ca.ntro.ntro_core_impl.task_graphs.task_graph_trace.TaskTraceNtro;
import ca.ntro.ntro_core_impl.values.ObjectMap;
import javafx.animation.AnimationTimer;
import javafx.application.Platform;

public abstract class TimeFx extends TimeJdk {

	private SimpleTask tickHandler = null;

	public SimpleTask getTickHandler() {
		return tickHandler;
	}

	public void setTickHandler(SimpleTask tickHandler) {
		this.tickHandler = tickHandler;
	}

	@Override
	public void runAfterDelay(long milliseconds, Runnable runnable) {
		new Timer().schedule(new TimerTask() {

			@Override
			public void run() {
				Platform.runLater(runnable);
			}

		}, milliseconds);
	}

	@Override
	public void runRepeatedly(long milliseconds, Runnable runnable) {
		if(milliseconds <= 1) {
			
			new AnimationTimer() {

				@Override
				public void handle(long now) {
					runnable.run();
				}
			}.start();
			
		}else {
			new Timer().scheduleAtFixedRate(new TimerTask() {

				@Override
				public void run() {
					Platform.runLater(runnable);
				}
				
			}, 0, milliseconds);
		}
	}

	@Override
	public SimpleTask newTickHandlerTask(TaskContainer graph) {
		
		if(tickHandler == null) {
			tickHandler = graph.newTask("clock[nextTick]", 
										SimpleTaskOptions.taskClass(ExecutableTaskNtro.class) 
														 .traceClass(TaskTraceNtro.class)
														 .resultsClass(TaskResultsEventHandler.class));
			
			tickHandler.asExecutableTask().onCancel((currentResults, notifyer) -> {
				cancelTickHandler();
			});

			startTickTimer();
		}
		
		return tickHandler;
	}

	protected abstract void startTickTimer();
	protected abstract void stopTickTimer();

	public void cancelTickHandler() {
		tickHandler = null;
		stopTickTimer();
	}

}
