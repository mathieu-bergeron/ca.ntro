package ca.ntro.ntro_app_fx_impl.services;

import ca.ntro.core.services.ThreadService;
import javafx.application.Platform;

public class ThreadServiceFx implements ThreadService {

	@Override
	public void runOnMainThread(Runnable runnable) {
		Platform.runLater(runnable);
	}

	@Override
	public void runInWorkerThread(Runnable runnable) {
		// FIXME: use a thread pool
		new Thread() {

			@Override
			public void run() {
				runnable.run();
			}
			
		}.start();
	}

	@Override
	public String currentThreadId() {
		return String.valueOf(Thread.currentThread().getId());
	}

}
