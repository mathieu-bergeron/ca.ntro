package ca.ntro.ntro_app_fx_impl.services;

import java.io.IOException;

import ca.ntro.app.Ntro;
import ca.ntro.core.NtroCore;
import ca.ntro.ntro_app_fx_impl.NtroImpl;
import ca.ntro.ntro_core_impl.NtroCoreImpl;
import ca.ntro.ntro_core_impl.services.LoggerTasks;
import javafx.fxml.LoadException;

public class LoggerFx extends LoggerTasks {

	@Override
	protected void fatalDialog(String message) {
		//swingErrorDialog(message);

		NtroCoreImpl.threads().runOnMainThread(() -> {
			NtroImpl.window().show();    
			NtroImpl.window().displayFatalErrorMessage(message);
		});
	}

	private void swingErrorDialog(String message) {
		String thisVmClasspath = System.getProperty("java.class.path");
		String thisVmModulepath = System.getProperty("jdk.module.path");
		
		Runtime runtime = Runtime.getRuntime();
		Process process;
		try {
			
			String[] args = new String[] {"java", 
					                      "-Dsun.java2d.opengl=true", 
					                      "-Dfile.encoding=UTF-8", 
					                      "-Duser.country=CA", 
					                      "-Duser.language=fr", 
					                      "-Duser.variant", 
					                      "-cp", thisVmClasspath, 
					                      "--module-path", thisVmModulepath, 
					                      "--module", "ca.ntro.fx/ca.ntro.app.fatal_dialog.FatalDialogSwing", 
					                      message};
			
			//printArgs(args);
			
			process = runtime.exec(args);
			
			// process.waitFor();

		} catch (Throwable t) {

			t.printStackTrace();

		}
	}

	private void printArgs(String[] args) {
		StringBuilder builder = new StringBuilder();
		for(String arg : args) {
			builder.append(" \"");
			builder.append(arg);
			builder.append("\"");
		}
		System.out.println(builder.toString());
	}

	@Override
	public void fatal(Throwable t) {
		if(t instanceof LoadException) {

			memorizeFatalHasNowOccured();
			fatalLoadException((LoadException) t);

		}else {

			super.fatal(t);
		}
	}

	private void fatalLoadException(LoadException e) {
		logException(e);

		String message = "";

		if(e.getMessage() != null) {
			
			try {
				// XXX: must also support
				//      Windows paths, e.g.
				//     /C:/Users/mbergeron/4f5_mathieu_bergeron/pong/bin/main/racine.xml:6
				
				String filepathAndLineNumber = e.getMessage().trim();
				String[] segments = filepathAndLineNumber.split(":");
				String filepath = filepathAndLineNumber;
				int lineNumber = 0;

				if(segments.length >= 2) {
					filepath = segments[segments.length - 2];

					// XXX: LoadException might report the lineNumber
					//      for the last line of a tag, even if the error
					//      occurs when reading an attribute
					lineNumber = Integer.parseInt(segments[segments.length - 1]);
				}

				String filename = filepath;
				String messagePrefix = "";
				if(filepath.contains("/")) {
					segments = filepath.split("/");
				}else {
					segments = filepath.split("\\");
				}

				if(segments.length > 0) {
					filename = segments[segments.length - 1];
				}

				if(segments.length > 1) {
					messagePrefix = segments[0];
					messagePrefix = messagePrefix.replaceAll(System.lineSeparator(), "");
				}
				
				if(lineNumber == 0) {

					message += "(" + filename + ")";

				}else {
					
					message += "(" + filename + ":" + lineNumber + ")";
					
				}
				
				message += " " + messagePrefix;

			}catch(Throwable t) {
				message = e.getMessage();
			}
		}

		if(e.getCause() != null) {
			
			Throwable cause = e.getCause();
			
			if(cause instanceof ClassNotFoundException) {
				
				message += " class not found: " + cause.getMessage();
				
			}else {
				
				message += "\n\t" + cause.getClass().getSimpleName() + " " + cause.getMessage();

			}

		}

		finalizeFatal(message);

	}



}
