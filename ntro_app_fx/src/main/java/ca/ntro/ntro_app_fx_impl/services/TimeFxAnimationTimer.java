/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.ntro_app_fx_impl.services;

import ca.ntro.ntro_app_fx_impl.frontend.TickNtro;
import javafx.animation.AnimationTimer;
import javafx.application.Platform;

public class TimeFxAnimationTimer extends TimeFx {
    
    private final double elapsedMinSeconds = 16d / 1E3;

    private AnimationTimer tickTimer = null;
    
    @Override
    protected void startTickTimer() {
        stopTickTimer();
        
        tickTimer = new AnimationTimer() {
            
            private long lastTick = nowNanoseconds();

            @Override
            public void handle(long now) {
                long tick = nowNanoseconds();
                
                double elapsedSeconds = (tick - lastTick) / 1E9;
                
                if(elapsedSeconds >= elapsedMinSeconds) {

                    lastTick = tick;
                    getTickHandler().addResult(new TickNtro(elapsedSeconds));
                }

            }
        };

        tickTimer.start();
    }

    @Override
    protected void stopTickTimer() {
        if(tickTimer != null) {
            tickTimer.stop();
        }
    }

}
