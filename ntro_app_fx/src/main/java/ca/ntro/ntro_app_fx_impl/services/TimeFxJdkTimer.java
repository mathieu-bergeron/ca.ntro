/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.ntro_app_fx_impl.services;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;

import ca.ntro.ntro_app_fx_impl.frontend.TickNtro;
import javafx.application.Platform;

public class TimeFxJdkTimer extends TimeFx {
	
	private Timer tickTimer = null;
	
	private AtomicBoolean fxReadyForNextTick = new AtomicBoolean(true);
	private long lastTick;
	
	@Override
	protected void startTickTimer() {
		stopTickTimer();
		
		tickTimer = new Timer();

		lastTick = nowNanoseconds();

		tickTimer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				
				if(fxReadyForNextTick.get()) {

					fxReadyForNextTick.set(false);

					Platform.runLater(() -> {
						long tick = nowNanoseconds();
						double elapsed = (tick - lastTick) / 1E9;
						lastTick = tick;

						if(getTickHandler() != null) {
							getTickHandler().addResult(new TickNtro(elapsed));
						}

						fxReadyForNextTick.set(true);
					});
				}
			}

		}, 0, 2);
	}

	@Override
	protected void stopTickTimer() {
		if(tickTimer != null) {
			tickTimer.cancel();
		}
	}
}
