/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.ntro_app_fx_impl.services;

import ca.ntro.core.services.LastModifiedLambda;
import ca.ntro.ntro_core_impl.path.Path;
import ca.ntro.ntro_core_impl.services.StorageServiceJdk;
import ca.ntro.ntro_core_impl.storage.FileWatcher;
import javafx.application.Platform;

public class StorageServiceFx extends StorageServiceJdk {
	
	// FIXME: this should be done in a ThreadService

	@Override
	public void writeTextFile(String filePath, String content, LastModifiedLambda lambda) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				long lastModified = writeTextFileBlocking(filePath, content);
				lambda.onLastModified(lastModified);
			}
		});
	}

	@Override
	protected void callWatcher(FileWatcher watcher, long lastModified, String fileContent) {
		Platform.runLater(new Runnable() {

			@Override
			public void run() {
				watcher.fileChanged(lastModified, fileContent);
			}
		});
	}
	

}
