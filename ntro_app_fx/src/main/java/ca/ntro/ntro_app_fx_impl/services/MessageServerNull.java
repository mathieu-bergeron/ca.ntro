package ca.ntro.ntro_app_fx_impl.services;

import ca.ntro.ntro_app_fx_abstr.messages.MessageAbstr;
import ca.ntro.ntro_app_fx_impl.messages.MessageFromServerHandler;
import ca.ntro.ntro_app_fx_impl.messages.MessageServer;
import ca.ntro.ntro_app_fx_impl.messages.ObservationFromServerHandler;
import ca.ntro.ntro_app_fx_impl.modified.Observable;
import ca.ntro.ntro_app_fx_impl.modified.Observation;

public class MessageServerNull implements MessageServer {

	@Override
	public void sendMessageToServer(MessageAbstr message) {
		// TODO Auto-generated method stub

	}

	@Override
	public <O extends Observable> void requestFirstObservationFromServer(Class<O> observableClass,
			String observableId) {
		// TODO Auto-generated method stub

	}

	@Override
	public void broadcastMessageToOtherClients(MessageAbstr message) {
		// TODO Auto-generated method stub

	}

	@Override
	public <O extends Observable> void pushObservationToClients(Class<O> observableClass, String observableId,
			Observation<?> observation) {
		// TODO Auto-generated method stub

	}

	@Override
	public <O extends Observable> void pushObservationToClients(Class<O> observableClass, Observation<?> observation) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onMessageFromServer(MessageFromServerHandler handler) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onObservationFromServer(ObservationFromServerHandler handler) {
		// TODO Auto-generated method stub

	}

}
