/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.ntro_app_fx_impl.frontend.window;

import ca.ntro.app.frontend.View;
import ca.ntro.core.NtroCore;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

public class AppView extends StackPane {
	
	private RootViewContainer  rootViewContainer = new RootViewContainer();
	private FatalErrorView     fatalErrorView    = new FatalErrorView();
	
	public AppView() {
		super();
		
		this.setBackground(new Background(new BackgroundFill(Color.TRANSPARENT, null, null)));
		
		getChildren().add(fatalErrorView);
		getChildren().add(rootViewContainer);
		
		fatalErrorView.setVisible(false);
	}

	public void installRootView(View<?> rootView) {
		double width = this.getWidth();
		double height = this.getHeight();

    	rootViewContainer.installRootView(rootView);
    	
    	this.setWidth(width);
    	this.setHeight(height);
	}

	public void displayFatalErrorMessage(String message) {
		fatalErrorView.displayMessage(message);
		fatalErrorView.setVisible(true);
		rootViewContainer.setVisible(false);
	}

}
