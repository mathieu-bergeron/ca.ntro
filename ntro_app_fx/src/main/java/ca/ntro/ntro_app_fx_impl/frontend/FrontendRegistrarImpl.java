/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.ntro_app_fx_impl.frontend;

import static ca.ntro.app.tasks.frontend.FrontendTasks.*;

import ca.ntro.app.Ntro;
import ca.ntro.app.frontend.Frontend;
import ca.ntro.app.services.Window;
import ca.ntro.app.session.Session;
import ca.ntro.app.session.SessionAccessor;
import ca.ntro.ntro_app_fx_impl.messages.MessageRegistrarNtro;
import ca.ntro.ntro_app_fx_impl.models.ModelRegistrarImpl;
import ca.ntro.ntro_app_fx_impl.session.SessionRegistrarImpl;
import ca.ntro.ntro_app_fx_impl.tasks.frontend.FrontendTaskFactory;
import ca.ntro.ntro_core_impl.NtroCoreImpl;
import ca.ntro.ntro_core_impl.task_graphs.generic_task_graph.SimpleTaskOptions;
import ca.ntro.ntro_core_impl.task_graphs.task_graph.ExecutableTaskNtro;
import ca.ntro.ntro_core_impl.task_graphs.task_graph_trace.TaskResultsCondition;
import ca.ntro.ntro_core_impl.task_graphs.task_graph_trace.TaskTraceNtro;

public abstract class FrontendRegistrarImpl<VR extends ViewRegistrar> 

       implements     FrontendRegistrar<VR>,
                      FrontendExecutor {

	
	private EventRegistrarNtro eventRegistrar = new EventRegistrarNtro();
	private SessionRegistrarImpl sessionRegistrar = new SessionRegistrarImpl();
	private FrontendTaskFactory taskFactory = new FrontendTaskFactory();
	private VR viewRegistrar = newViewRegistrarInstance();

	private Window window = new WindowNull();

	private Frontend<VR> frontend = null;
	
	private MessageRegistrarNtro messageRegistrar;
	private ModelRegistrarImpl modelRegistrar;
	
	private String sessionId;

	public EventRegistrarNtro getEventRegistrar() {
		return eventRegistrar;
	}

	public void setEventRegistrar(EventRegistrarNtro eventRegistrar) {
		this.eventRegistrar = eventRegistrar;
	}

	public Window getWindow() {
		return window;
	}

	public void setWindow(Window window) {
		this.window = window;
	}

	public SessionRegistrarImpl getSessionRegistrar() {
		return sessionRegistrar;
	}

	public void setSessionRegistrar(SessionRegistrarImpl sessionRegistrar) {
		this.sessionRegistrar = sessionRegistrar;
	}

	public FrontendTaskFactory getTaskFactory() {
		return taskFactory;
	}
	public void setTaskFactory(FrontendTaskFactory taskCreator) {
		this.taskFactory = taskCreator;
	}
	public VR getViewRegistrar() {
		return viewRegistrar;
	}

	public void setViewRegistrar(VR viewRegistrar) {
		this.viewRegistrar = viewRegistrar;
	}

	public Frontend<VR> getFrontend() {
		return frontend;
	}

	public void setFrontend(Frontend<VR> frontend) {
		this.frontend = frontend;
	}

	public MessageRegistrarNtro getMessageRegistrar() {
		return messageRegistrar;
	}

	public void setMessageRegistrar(MessageRegistrarNtro messageRegistrar) {
		this.messageRegistrar = messageRegistrar;
	}

	public ModelRegistrarImpl getModelRegistrar() {
		return modelRegistrar;
	}

	public void setModelRegistrar(ModelRegistrarImpl modelRegistrar) {
		this.modelRegistrar = modelRegistrar;
	}

	protected abstract VR newViewRegistrarInstance();
	

	@Override
	public void registerFrontend(Class<? extends Frontend<VR>> frontendClass) {
		Frontend<VR> frontend = NtroCoreImpl.factory().newInstance(frontendClass);
		
		registerFrontendObject(frontend);
	}
	
	public void registerFrontendObject(Frontend<VR> frontend) {
		this.frontend = frontend;

		frontend.registerEvents(getEventRegistrar());
		frontend.registerViews(getViewRegistrar());
		
		addWindowTask();
		
		//getViewRegistrar().addViewLoaderTasks(getTaskFactory());
		getEventRegistrar().addEventHandlerTasks(getTaskFactory());

		createRegisteredSession(sessionId);

		frontend.createTasks(getTaskFactory().asTasks());
	}
	
	private void addWindowTask() {
		getTaskFactory().orphanTask(window(),
					                SimpleTaskOptions.taskClass(ExecutableTaskNtro.class)
					                                 .traceClass(TaskTraceNtro.class)
					                                 .resultsClass(TaskResultsCondition.class))

		                .executesAndReturnsValue(inputs -> {

		                	return getWindow();
		                });
	}

	
	public void prepareToExecuteTasks() {

		getTaskFactory().prepareToExecuteTasks();
		
		getTaskFactory().registerTaskTrace();
	}

	public void createRegisteredSession(String sessionId) {
		
		if(frontend != null) {
			frontend.registerSessionClass(getSessionRegistrar());
		}

		getSessionRegistrar().createRegisteredSession(sessionId);

		getSessionRegistrar().addSessionTask(getTaskFactory());

		Session<?> session = Ntro.session();
		if(SessionAccessor.isNamedSession(session)) {

			Ntro.logger().info("session: " + session.sessionId());
			
		}else {

			Ntro.logger().info("anonymous session (" + session.sessionId() + ")");

		}
	}

	public void writeGraph() {
		if(getTaskFactory() != null) {
			getTaskFactory().writeGraph();
		}
	}

	@Override
	public void executeTasks() {

		getTaskFactory().executeTasks();

	}

	public boolean isFrontendRegistered() {
		return frontend != null;
	}

	public void registerSessionId(String sessionId) {
		this.sessionId = sessionId;
	}


}
