package ca.ntro.ntro_app_fx_impl.frontend.window;

import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;

public class SpaceHorizontal extends Pane {

	public SpaceHorizontal() {
		HBox.setHgrow(this, Priority.ALWAYS);
	}
	
	public SpaceHorizontal(double minWidth, double maxWidth) {
		HBox.setHgrow(this, Priority.ALWAYS);
		setMinWidth(minWidth);
		setMaxWidth(maxWidth);
	}
}
