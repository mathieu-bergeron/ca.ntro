package ca.ntro.ntro_app_fx_impl.frontend.window;

import ca.ntro.app.frontend.View;
import ca.ntro.app.frontend.ViewFx;
import javafx.animation.Interpolator;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;

public class FadeOutRootViewContainer extends ViewFx {
	
	private static final double FADE_DURATION = 200;
	
	private StackPane stackPane = new StackPane();
	
	private boolean useTransition = false;

	public FadeOutRootViewContainer(boolean useTransition) {
		this.useTransition = useTransition;
		initialize();
	}
	
	public FadeOutRootViewContainer() {
		initialize();
	}

	@Override
	public void initialize() {
		stackPane.setBackground(Background.EMPTY);
		stackPane.setBackground(new Background(new BackgroundFill(Color.TRANSPARENT, null, null)));
	}

	@Override
	public Pane rootNode() {
		return stackPane;
	}

	public void installRootView(View<?> rootView) {
		Pane newRootView = (Pane) rootView.rootNode();
		
		if(useTransition) {

			installRootViewWithFade(newRootView);

		}else {

			installRootViewWithoutFade(newRootView);

		}

	}

	private void installRootViewWithoutFade(Pane newRootView) {

		stackPane.getChildren().clear();
		stackPane.getChildren().add(newRootView);
		stackPane.setBackground(newRootView.getBackground());

	}

	private void installRootViewWithFade(Pane newRootView) {
		if(stackPane.getChildren().isEmpty()) {

			stackPane.getChildren().add(newRootView);
			stackPane.setBackground(newRootView.getBackground());

		}else {

			Pane oldRootView = (Pane) stackPane.getChildren().get(0);
			stackPane.getChildren().add(newRootView);
			stackPane.setBackground(newRootView.getBackground());
			
			oldRootView.toFront();
			newRootView.toBack();
			
			Timeline fadeOut = new Timeline();
			fadeOut.getKeyFrames().add(new KeyFrame(new Duration(0), new KeyValue(oldRootView.opacityProperty(), 1.0)));
			fadeOut.getKeyFrames().add(new KeyFrame(new Duration(FADE_DURATION), new KeyValue(oldRootView.opacityProperty(), 0.0, Interpolator.EASE_BOTH)));
			
			fadeOut.setOnFinished(onFadeOutFinished -> {

				stackPane.getChildren().remove(oldRootView);

			});


			fadeOut.playFromStart();
		}
	}

	public void clear() {
		stackPane = new StackPane();
	}

}
