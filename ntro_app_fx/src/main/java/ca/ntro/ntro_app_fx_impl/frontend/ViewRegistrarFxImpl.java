/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.ntro_app_fx_impl.frontend;

import static ca.ntro.app.tasks.frontend.FrontendTasks.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.Map;

import ca.ntro.app.Ntro;
import ca.ntro.app.frontend.View;
import ca.ntro.app.frontend.ViewData;
import ca.ntro.app.frontend.ViewLoaderFx;
import ca.ntro.app.frontend.ViewRegistrarFx;
import ca.ntro.app.tasks.frontend.FrontendTasks;
import ca.ntro.core.NtroCore;
import ca.ntro.core.locale.Locale;
import ca.ntro.core.tasks.Task;
import ca.ntro.ntro_app_fx_impl.NtroImpl;
import ca.ntro.ntro_app_fx_impl.frontend.window.WindowFx;
import ca.ntro.ntro_app_fx_impl.tasks.frontend.FrontendTaskFactory;
import ca.ntro.ntro_core_abstr.tasks.TaskAbstr;
import ca.ntro.ntro_core_impl.NtroCoreImpl;
import ca.ntro.ntro_core_impl.tasks.TaskWrapper;

public class ViewRegistrarFxImpl implements ViewRegistrarFx, ViewRegistrarAccessor {
	
	private String userCssPath;
	private String antiCacheCssPath;

	private Map<Locale, String> resourcesPaths = new HashMap<>();
	private Locale defaultLocale = Ntro.currentLocale();

	private Map<Class<? extends View<?>>, String> fxmlPaths = new HashMap<>();
	private Map<Class<? extends View<?>>, View<?>> views = new HashMap<>();

	public <V extends View<?>> void registerFragment(Class<V> viewClass, String fxmlPath) {
		NtroCoreImpl.factory().registerNamedClass(viewClass);
		fxmlPaths.put(viewClass, fullFxmlPath(fxmlPath));
	}
	
	private String fullFxmlPath(String fxmlPath) {
		String fullFxmlPath = fxmlPath;
		
		if(!Ntro.options().useJarResources()) {
			fullFxmlPath = Paths.get(NtroCore.options().resourcesPath(), fxmlPath).toString();
		}

		return fullFxmlPath;
	}

	public <V extends View<?>> void registerView(Class<V> viewClass, String fxmlPath) {
		NtroCoreImpl.factory().registerNamedClass(viewClass);
		fxmlPaths.put(viewClass, fullFxmlPath(fxmlPath));
	}

	@Override
	public void registerDefaultLocale(Locale locale) {
		this.defaultLocale = locale;
	}

	@Override
	public void registerTranslations(Locale locale, String resourcesPath) {
		resourcesPaths.put(locale, resourcesPath);
	}
	
	private String userCssPath(String cssPath) {
		String userCssPath = cssPath;
		
		if(!Ntro.options().useJarResources()) {
			userCssPath = Paths.get(NtroCore.options().resourcesPath(), cssPath).toString();
		}
		
		return userCssPath;
	}
	
	@Override
	public void registerStylesheet(String cssPath) {
		this.userCssPath = userCssPath(cssPath);
		this.antiCacheCssPath = userCssPath;
	}

	public void registerViewData(Class<? extends ViewData> viewDataClass) {
		NtroCore.factory().registerNamedClass(viewDataClass);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <V extends View<?>> V view(Class<V> viewClass) {
		return (V) views.get(viewClass);
	}
	
	public void addViewLoaderTasks(FrontendTaskFactory tasks) {
		if(fxmlPaths.isEmpty()) return;

		tasks.taskGroup(viewLoaders().id())

		     .contains(subTasks -> {
		    	 
		    	Map<Class<? extends View<?>>, TaskAbstr> taskByViewClass = createTasks(subTasks);
		    	
		    	if(!Ntro.options().useJarResources()) {
					watchCssFileAndReloadTasks(taskByViewClass);
					watchFxmlFilesAndReloadTasks(taskByViewClass);
		    	}

		    	watchLocaleAndReloadTasks(taskByViewClass);

		    	watchFontSizeAndReloadTasks(taskByViewClass);

			});
	}

	private Map<Class<? extends View<?>>, TaskAbstr> createTasks(FrontendTasks subTasks) {

		Map<Class<? extends View<?>>, TaskAbstr> taskByViewClass = new HashMap<>();

		for(Map.Entry<Class<? extends View<?>>, String> entry : fxmlPaths.entrySet()) {

			Class<? extends View<?>> viewClass = entry.getKey();
			String fxmlPath = entry.getValue();
			
			Task task = subTasks.task(viewLoader(viewClass).id())

			        .executesAndReturnsValue(inputs -> {

						return buildViewLoader(viewClass, fxmlPath, userCssPath);

			        }).getTask();
			
			TaskWrapper taskWrapper = (TaskWrapper) task;
			
			TaskAbstr actualTask = taskWrapper.getTaskImpl();
			
			taskByViewClass.put(viewClass, actualTask);
		}
		
		return taskByViewClass;
	}

	

	private void watchCssFileAndReloadTasks(Map<Class<? extends View<?>>, TaskAbstr> taskByViewClass) {
		if(userCssPath == null) return;

		NtroCore.storage().watchFile(userCssPath, () -> {

			Path original = Paths.get(userCssPath);
			
			antiCacheCssPath = nextAntiCacheCssPath(userCssPath);

			java.nio.file.Path copy = Paths.get(antiCacheCssPath);

			try {
				Files.copy(original, copy, StandardCopyOption.REPLACE_EXISTING);

			} catch (IOException e) {
				
				NtroCore.logger().warning("cannot copy " + userCssPath + " to " + antiCacheCssPath);
				e.printStackTrace();

			}

			for(Map.Entry<Class<? extends View<?>>, TaskAbstr> entry : taskByViewClass.entrySet()) {
				
				Class<? extends View<?>> viewClass = entry.getKey();
				TaskAbstr task = entry.getValue();

				String fxmlPath = fxmlPaths.get(viewClass);

				ViewLoaderFx<? extends View<?>> loader = buildViewLoader(viewClass, fxmlPath, antiCacheCssPath);

				NtroCore.threads().runOnMainThread(() -> {

					task.cancel();
					task.asSimpleTask().addResult(loader);

				});
			}
		});
	}

	private String nextAntiCacheCssPath(String userCssPath) {
		java.nio.file.Path path = Paths.get(userCssPath);
		String filename = path.getFileName().toString();

		return Paths.get(NtroCore.options().tmpPath(), filename + "_" +  Ntro.random().nextId(8)).toString();
		
	}

	private void watchFxmlFilesAndReloadTasks(Map<Class<? extends View<?>>, TaskAbstr> taskByViewClass) {
		for(Map.Entry<Class<? extends View<?>>, TaskAbstr> entry : taskByViewClass.entrySet()) {
			
			Class<? extends View<?>> viewClass = entry.getKey();
			TaskAbstr task = entry.getValue();

			String fxmlPath = fxmlPaths.get(viewClass);
			
			NtroCore.storage().watchFile(fxmlPath, () -> {
				
				ViewLoaderFx<? extends View<?>> loader = buildViewLoader(viewClass, fxmlPath, antiCacheCssPath);

				NtroCore.threads().runOnMainThread(() -> {
						
					task.cancel();
					task.asSimpleTask().addResult(loader);

				});
			});
		}
	}

	private void watchLocaleAndReloadTasks(Map<Class<? extends View<?>>, TaskAbstr> taskByViewClass) {
		NtroCoreImpl.locale().onLocaleChanged(() -> {

			for(Map.Entry<Class<? extends View<?>>, TaskAbstr> entry : taskByViewClass.entrySet()) {
				
				Class<? extends View<?>> viewClass = entry.getKey();
				TaskAbstr task = entry.getValue();

				String fxmlPath = fxmlPaths.get(viewClass);
				
				ViewLoaderFx<? extends View<?>> loader = buildViewLoader(viewClass, fxmlPath, antiCacheCssPath);

				NtroCore.threads().runOnMainThread(() -> {
						
					task.cancel();
					task.asSimpleTask().addResult(loader);

				});
			}
		});
	}

	private void watchFontSizeAndReloadTasks(Map<Class<? extends View<?>>, TaskAbstr> taskByViewClass) {
		((WindowFx) NtroImpl.window()).onFontSizeChanged(() -> {

			for(Map.Entry<Class<? extends View<?>>, TaskAbstr> entry : taskByViewClass.entrySet()) {
				
				Class<? extends View<?>> viewClass = entry.getKey();
				TaskAbstr task = entry.getValue();

				String fxmlPath = fxmlPaths.get(viewClass);
				
				ViewLoaderFx<? extends View<?>> loader = buildViewLoader(viewClass, fxmlPath, antiCacheCssPath);

				NtroCore.threads().runOnMainThread(() -> {
						
					task.cancel();
					task.asSimpleTask().addResult(loader);

				});
			}
		});
	}


	private ViewLoaderFx<? extends View<?>> buildViewLoader(Class<? extends View<?>> viewClass, 
			                                                String fxmlPath,
			                                                String cssPath) {

		ViewLoaderFx<? extends View<?>> viewLoader = new ViewLoaderFx<>();
		
		viewLoader.setViewClass(viewClass);

		viewLoader.setFxmlPath(fxmlPath);
		if(cssPath !=null) {
			viewLoader.setCssPath(cssPath);
		}
		
		String resourcesPath = resourcesPaths.get(Ntro.currentLocale());
		if(resourcesPath == null) {
			resourcesPath = resourcesPaths.get(defaultLocale);
		}

		if(resourcesPath != null) {
			viewLoader.setResourcesPath(resourcesPath);
		}

		return viewLoader;
	}
}
