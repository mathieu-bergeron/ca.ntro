package ca.ntro.ntro_app_fx_impl.frontend.window;

import ca.ntro.core.NtroCore;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.text.TextFlow;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

public class FatalErrorView extends StackPane {
	
	private VBox mainVBox      = new VBox();

	private HBox animationHBox = new HBox();
	private VBox animationVBox = new VBox();

	private Label             fpsLabel = new Label();
	private FatalErrorCanvas  canvas   = new FatalErrorCanvas();
	private FatalErrorWorld2d world2d  = new FatalErrorWorld2d();

	private HBox     messageHBox         = new HBox();
	private VBox     messageTextFlowVBox = new VBox();
	private HBox     messageTextFlowHBox = new HBox();
	private TextFlow messageTextFlow     = new TextFlow();
	
	private long previousNanotime = System.nanoTime();

	private long   imagesDrawnSinceLastFpsMeasure   = 0;
	private double elapsedTimeSinceLastFpsMeasure   = 0;
	private double secondsBetweenFpsMeasures        = 0.2;
	
	public FatalErrorView() {
		/*
		 * 
		 *  	#f61f56 	(246,31,86)
				#be133f 	(190,19,63)
				#2b2828 	(43,40,40)
				#1a1616 	(26,22,22)
				#060000 	(6,0,0)
		 * 
		 * 
		 */
		setStyle("-fx-background-color:#be133f;");

		getChildren().add(mainVBox);
		
		canvas.initialize();
		world2d.initialize();
		
		canvas.setMinWidth(FatalErrorWorld2d.WIDTH / 4);
		canvas.setMinHeight(FatalErrorWorld2d.HEIGHT / 4);
		canvas.setMaxWidth(FatalErrorWorld2d.WIDTH * 2);
		canvas.setMaxHeight(FatalErrorWorld2d.HEIGHT * 2);
		
		mainVBox.getChildren().add(animationHBox);
		animationHBox.setAlignment(Pos.CENTER);
		VBox.setVgrow(animationHBox, Priority.ALWAYS);

		animationHBox.getChildren().add(new SpaceHorizontal(20,100));
		animationHBox.getChildren().add(animationVBox);

		HBox.setHgrow(animationVBox, Priority.ALWAYS);
		animationVBox.setAlignment(Pos.CENTER);

		animationHBox.getChildren().add(new SpaceHorizontal(20,100));
		
		animationVBox.getChildren().add(new SpaceVertical(10, 20));
		animationVBox.getChildren().add(fpsLabel);
		animationVBox.getChildren().add(canvas);
		VBox.setVgrow(canvas, Priority.ALWAYS);
		animationVBox.getChildren().add(new SpaceVertical(10, 20));

		fpsLabel.setStyle("-fx-text-fill:#2b2828;");

		mainVBox.getChildren().add(new SpaceVertical(20, 40));
		mainVBox.getChildren().add(messageHBox);
		VBox.setVgrow(messageHBox, Priority.ALWAYS);
		mainVBox.getChildren().add(new SpaceVertical(20, 40));

		messageHBox.setAlignment(Pos.TOP_CENTER);

		messageHBox.getChildren().add(new SpaceHorizontal(20, 40));
		messageHBox.getChildren().add(messageTextFlowHBox);
		messageHBox.getChildren().add(new SpaceHorizontal(20, 40));

		HBox.setHgrow(messageTextFlowHBox, Priority.ALWAYS);
		messageTextFlowHBox.setStyle("-fx-background-color:#f61f56;-fx-background-radius:12;");
		
		messageTextFlowHBox.getChildren().add(new SpaceHorizontal());
		messageTextFlowHBox.getChildren().add(messageTextFlowVBox);
		messageTextFlowHBox.getChildren().add(new SpaceHorizontal());

		HBox.setHgrow(messageTextFlowVBox, Priority.ALWAYS);
		
		messageTextFlowVBox.getChildren().add(new SpaceVertical());
		messageTextFlowVBox.getChildren().add(messageTextFlow);
		messageTextFlowVBox.getChildren().add(new SpaceVertical());
		messageTextFlowVBox.setAlignment(Pos.CENTER);
		
		messageTextFlowVBox.setMinHeight(200);
		messageTextFlow.setStyle("-fx-font-size:2em;-fx-text-fill:#2b2828;-fx-font-family:" + FatalErrorWorld2d.FONT_SANS +  ";");
		
	}

	public void displayMessage(String message) {
		Text messageText = new Text(message);
		messageText.setFill(Color.web("#2b2828"));

		messageTextFlow.getChildren().add(messageText);
		world2d.drawOn(canvas);
		
		previousNanotime = System.nanoTime();
		NtroCore.time().runRepeatedly(1, () -> {

			canvas.clearCanvas();

			long currentNanotime = System.nanoTime();
			double elapsedSeconds = (currentNanotime - previousNanotime) / 1E9;
			world2d.onTimePasses(elapsedSeconds);
			
			previousNanotime = currentNanotime;

			world2d.drawOn(canvas);
			imagesDrawnSinceLastFpsMeasure++;
			
			elapsedTimeSinceLastFpsMeasure += elapsedSeconds;
			
			if(elapsedTimeSinceLastFpsMeasure >= secondsBetweenFpsMeasures) {

				fpsLabel.setText("FPS " + Math.round(imagesDrawnSinceLastFpsMeasure / secondsBetweenFpsMeasures));

				elapsedTimeSinceLastFpsMeasure = 0;
				imagesDrawnSinceLastFpsMeasure = 0;
			}
		});
	}

}
