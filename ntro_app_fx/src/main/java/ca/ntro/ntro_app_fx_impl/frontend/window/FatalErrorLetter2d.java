package ca.ntro.ntro_app_fx_impl.frontend.window;

import ca.ntro.app.fx.controls.World2dMouseEventFx;
import ca.ntro.app.world2d.Object2dFx;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.effect.Bloom;
import javafx.scene.effect.Glow;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;
import javafx.scene.text.Font;

public class FatalErrorLetter2d extends Object2dFx<FatalErrorWorld2d> {
	
	public static final double EPSILON = 1.0;
	
	private String letter = "";
	private int pos = 0;
	private double fontSize = 20.0;
	private double accelerationY = 1.08;
	private boolean firstAnimationIsDone = false;
	private Glow glowEffect = new Glow(0.0);
	private double secondsToWaitUntilSecondAnimation = 10;
	private double secondAnimationElapsedSeconds = 0;
	private boolean secondAnimationStarted = false;

	@Override
	public String id() {
		return letter + pos;
	}

	public FatalErrorLetter2d() {
	}
	
	public FatalErrorLetter2d(String letter, int pos) {
		this.letter = letter;
		this.pos = pos;
	}

	@Override
	public void initialize() {
		fontSize = getWorld2d().getWidth() / FatalErrorWorld2d.NUMBER_OF_LETTERS;

		setWidth(fontSize);
		setHeight(fontSize);

		setTopLeftX(20.0 + getWidth()  * 0.95 * pos);
		setTopLeftY(-getHeight());
		
		setSpeedX(0);
		setSpeedY(30);
	}

	@Override
	public void drawOnWorld(GraphicsContext gc) {
		
		if(secondAnimationStarted) {
			gc.save();
			glowEffect.setLevel(Math.sin((secondAnimationElapsedSeconds-secondsToWaitUntilSecondAnimation)*2));
			gc.setEffect(glowEffect);
		}
		
		gc.setFont(FatalErrorWorld2d.FONT_MONO);
		gc.setStroke(Color.web("#2b2828"));
		gc.setFill(Color.web("#2b2828"));
		gc.fillText(letter, getTopLeftX(), getTopLeftY());
		gc.strokeText(letter, getTopLeftX(), getTopLeftY());

		if(secondAnimationStarted) {
			gc.restore();
		}
	}
	
	@Override
	public void onTimePasses(double secondsElapsed) {
		super.onTimePasses(secondsElapsed);
		
		if(!firstAnimationIsDone) {
			setSpeedY(getSpeedY() * accelerationY);

			if((getTopLeftY() + getHeight()) >= getWorld2d().getHeight()) {
				setTopLeftY(getWorld2d().getHeight() - getHeight() - EPSILON);
				setSpeedY(0);
				firstAnimationIsDone = true;
			}

		} else {
			secondAnimationElapsedSeconds += secondsElapsed;
			if(secondAnimationElapsedSeconds > secondsToWaitUntilSecondAnimation) {
				secondAnimationStarted = true;
			}
		}
	}

	@Override
	protected boolean onMouseEvent(World2dMouseEventFx mouseEvent) {

		return false;
	}

}
