package ca.ntro.ntro_app_fx_impl.frontend.window;

import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

public class SpaceVertical extends Pane {

	public SpaceVertical() {
		VBox.setVgrow(this, Priority.ALWAYS);
	}
	
	public SpaceVertical(double minHeight, double maxHeight) {
		VBox.setVgrow(this, Priority.ALWAYS);
		setMinHeight(minHeight);
		setMaxHeight(maxHeight);
	}
}
