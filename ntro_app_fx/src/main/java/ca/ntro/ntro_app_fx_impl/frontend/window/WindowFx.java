/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.ntro_app_fx_impl.frontend.window;

import ca.ntro.app.Ntro;
import ca.ntro.app.frontend.View;
import ca.ntro.app.services.Window;
import ca.ntro.core.NtroCore;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class WindowFx implements Window {
    
    private Stage primaryStage;
    private AppView appView = new AppView();
    private Scene rootScene;
    
    private double width = 1600;
    private double height = 900;
    
    private double SIZE_EPSILON = 2.0;
    
    private double fontSize = 13; 
    
    private Runnable fontSizeListener = null;

	public Stage getPrimaryStage() {
		return primaryStage;
	}

    public WindowFx(Stage primaryStage) {
        this.primaryStage = primaryStage;
        
		rootScene = new Scene(appView, width, height);
		rootScene.setFill(Color.TRANSPARENT);
		primaryStage.setScene(rootScene);
		
		primaryStage.widthProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				width = newValue.doubleValue();
			}
		});

		primaryStage.heightProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
				height = newValue.doubleValue();
			}
		});
		
    }

    @Override
    public void resize(double width, double height) {
    	if(Math.abs(primaryStage.getWidth() - width) > SIZE_EPSILON) {
			primaryStage.setWidth(width);
    	}

    	if(Math.abs(primaryStage.getHeight() - height) > SIZE_EPSILON) {
			primaryStage.setHeight(height);
    	}
    }
    

    @Override
    public void installRootView(View<?> rootView) {
    	if(rootView.rootNode() == null) {
            NtroCore.logger().fatal("[WindowFx.installRootView] rootView.rootNode() is null");
    	}

    	width = primaryStage.getWidth();
    	height = primaryStage.getHeight();

    	setFontSizeImpl(fontSize);

    	appView.installRootView(rootView);
    	
    	resize(width, height);
    	
    	/*
    	
    	Parent newParent = (Parent) rootView.rootNode();
    	
    	if(newParent != getParent()) {

    		setParent(newParent);
			
			double sceneWidth = width;
			double sceneHeight = height;
			
			Scene existingScene = getPrimaryStage().getScene();
			if(existingScene != null) {
				this.width = getPrimaryStage().getWidth();
				this.height = getPrimaryStage().getHeight();
				
				sceneWidth = existingScene.getWidth();
				sceneHeight = existingScene.getHeight();
			}

			setRootScene(sceneWidth, sceneHeight);

    	}
    	*/
    }

    @Override
    public void show() {

        /* TMP: for screenshots
        getPrimaryStage().setMinWidth(600);
        getPrimaryStage().setMaxWidth(600);
        getPrimaryStage().setMinHeight(400);
        getPrimaryStage().setMaxHeight(400);
        */

    	if(!primaryStage.isShowing()) {
			primaryStage.setIconified(false);
			primaryStage.show();
    	}

    }

    @Override
    public void fullscreen(boolean isFullscreen) {
        primaryStage.setFullScreen(isFullscreen);
    }

    @Override
    public void decorations(boolean hasDecorations) {
    	if(primaryStage.isShowing()) {
    		NtroCore.logger().warning("[WARNING] window.decorations() must be called before window.show()");
    		return;
    	}

        if(hasDecorations) {

            primaryStage.initStyle(StageStyle.DECORATED);

        }else {

            primaryStage.initStyle(StageStyle.UNDECORATED);

        }
    }

	@Override
	public void hide() {
		ObservableList<javafx.stage.Window> windows = primaryStage.getWindows();
		
		javafx.stage.Window fxWindow = windows.get(0);
		
		fxWindow.hide();
	}

	@Override
	public void displayFatalErrorMessage(String message) {
		appView.displayFatalErrorMessage(message);
	}

	@Override
	public void setTitle(String title) {
		primaryStage.setTitle(title);
	}

	@Override
	public void setFontSize(int size) {
		setFontSizeImpl(size);
    	
    	if(fontSizeListener != null) {
    		fontSizeListener.run();
    	}
	}

	public void setFontSizeImpl(double size) {
    	appView.setStyle("-fx-font-size: " + size);
	}

	@Override
	public void multiplyFontSizeBy(double factor) {
		fontSize = fontSize * factor;
		
		setFontSizeImpl(fontSize);

    	if(fontSizeListener != null) {
    		fontSizeListener.run();
    	}
	}

	public void onFontSizeChanged(Runnable listener) {
		this.fontSizeListener = listener;
	}

	@Override
	public void maximized(boolean isMaximized) {
		this.primaryStage.setMaximized(isMaximized);
	}



}
