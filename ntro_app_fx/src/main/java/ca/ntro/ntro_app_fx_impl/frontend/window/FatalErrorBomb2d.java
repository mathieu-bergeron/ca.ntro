package ca.ntro.ntro_app_fx_impl.frontend.window;

import ca.ntro.app.fx.controls.World2dMouseEventFx;
import ca.ntro.app.world2d.Object2dFx;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.shape.ArcType;

public class FatalErrorBomb2d extends Object2dFx<FatalErrorWorld2d> {
	
	public static final double EPSILON = 1.0;

	@Override
	public String id() {
		return "bombe";
	}

	@Override
	public void initialize() {
		setWidth(50);
		setHeight(50);

		setTopLeftX(0);
		setTopLeftY(getWorld2d().getHeight() - getHeight());
		
		setSpeedX(100);
		setSpeedY(150);

	}

	@Override
	public void drawOnWorld(GraphicsContext gc) {
		gc.setStroke(Color.BLUEVIOLET);
		gc.fillArc(getTopLeftX(), getTopLeftY(), getWidth(), getHeight(), 0, 360, ArcType.CHORD);
		
		gc.beginPath();
		gc.moveTo(0, 0);
		gc.bezierCurveTo(getTopLeftX(), getTopLeftY(), getTopLeftX() + 50, getTopLeftY() - 50, getTopLeftX() + 100, getTopLeftY() + 100);
		gc.stroke();
	}
	
	@Override
	public void onTimePasses(double secondsElapsed) {
		super.onTimePasses(secondsElapsed);
		
		if((getTopLeftX() + getWidth()) >= getWorld2d().getWidth()) {
			setTopLeftX(getWorld2d().getWidth() - getWidth() - EPSILON);
			setSpeedX(-1 * getSpeedX());
		}

		if((getTopLeftY() + getHeight()) >= getWorld2d().getHeight()) {
			setTopLeftY(getWorld2d().getHeight() - getHeight() - EPSILON);
			setSpeedY(-1 * getSpeedY());
		}

		if(getTopLeftX() <= 0) {
			setTopLeftX(0 + EPSILON);
			setSpeedX(-1 * getSpeedX());
		}

		if(getTopLeftY() <= 0) {
			setTopLeftY(0 + EPSILON);
			setSpeedY(-1 * getSpeedY());
		}
	}

	@Override
	protected boolean onMouseEvent(World2dMouseEventFx mouseEvent) {

		return false;
	}

}
