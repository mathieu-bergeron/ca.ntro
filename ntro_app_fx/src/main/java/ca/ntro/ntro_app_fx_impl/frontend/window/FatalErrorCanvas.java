package ca.ntro.ntro_app_fx_impl.frontend.window;

import ca.ntro.app.fx.controls.ResizableWorld2dCanvasFx;

public class FatalErrorCanvas extends ResizableWorld2dCanvasFx {
	

	@Override
	protected void initialize() {
		setWidth(FatalErrorWorld2d.WIDTH);
		setHeight(FatalErrorWorld2d.HEIGHT);
		
		setViewportTopLeftX(0);
		setViewportTopLeftY(0);
		setViewportWidth(FatalErrorWorld2d.WIDTH);
		setViewportHeight(FatalErrorWorld2d.HEIGHT);

		setWorldWidth(FatalErrorWorld2d.WIDTH);
		setWorldHeight(FatalErrorWorld2d.HEIGHT);

	}

}
