package ca.ntro.ntro_app_fx_impl.frontend.window;

import ca.ntro.app.frontend.View;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.util.Duration;

public class RootViewContainer extends SplitPane {
	
	private FadeOutRootViewContainer fadeOutRootViewContainer = new FadeOutRootViewContainer();
	private WarningMessagesView      warningMessagesView      = new WarningMessagesView();
	
	public RootViewContainer() {
		this.setBackground(Background.EMPTY);
		this.setBackground(new Background(new BackgroundFill(Color.TRANSPARENT, null, null)));

		setOrientation(Orientation.VERTICAL);

		getItems().add(fadeOutRootViewContainer.rootNode());
		
		// TODO: add / remove warningMessagesView dynamically
		//getItems().add(warningMessagesView);

		//getDividers().get(0).setPosition(1.00);
		
	}

	public void installRootView(View<?> rootView) {
		//double dividerPosition = getDividers().get(0).getPosition();
		
		double width = this.getWidth();
		double height = this.getHeight();
		
		fadeOutRootViewContainer.installRootView(rootView);
		
		
		this.setWidth(width);
		this.setHeight(height);

		//getDividers().get(0).setPosition(dividerPosition);

	}

}
