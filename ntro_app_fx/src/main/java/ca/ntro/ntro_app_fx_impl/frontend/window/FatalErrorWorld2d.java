package ca.ntro.ntro_app_fx_impl.frontend.window;

import java.util.List;

import ca.ntro.app.fx.controls.ResizableWorld2dCanvasFx;
import ca.ntro.app.fx.controls.World2dMouseEventFx;
import ca.ntro.app.world2d.World2dFx;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

public class FatalErrorWorld2d extends World2dFx {

	public static final double WIDTH = 400;
	public static final double HEIGHT = 200;
	
	public static final int NUMBER_OF_LETTERS = 5;
	public static Font FONT_MONO = new Font("Liberation Mono", WIDTH / NUMBER_OF_LETTERS);
	public static String FONT_SANS = "Liberation Sans";
	/*
	static {
		for(String fontFamily : Font.getFamilies()) {
			System.out.println(fontFamily);
		}
	}
	*/
	
	private final double newLetterAtEvery = 0.15;
	private final List<FatalErrorLetter2d> letters = List.of(new FatalErrorLetter2d("F", 0), 
			                                                 new FatalErrorLetter2d("A", 1), 
			                                                 new FatalErrorLetter2d("T", 2), 
			                                                 new FatalErrorLetter2d("A", 3), 
			                                                 new FatalErrorLetter2d("L", 4));
	private int indexNextLetter = 0;
	private double secondsSinceLastLetter = 0.0;

	@Override
	protected void initialize() {
		setWidth(WIDTH);
		setHeight(HEIGHT);
		
	}
	
	@Override
	public void drawOn(ResizableWorld2dCanvasFx canvas) {
		//drawBackground(canvas);

		super.drawOn(canvas);
	}

	private void drawBackground(ResizableWorld2dCanvasFx canvas) {
		canvas.drawOnWorld(gc -> {
			
			gc.setFill(Color.web("#be134f"));
			gc.fillRect(0,0,getWidth(),getHeight());
			
		});
	}
	
	@Override
	public void onTimePasses(double secondsElapsed) {
		super.onTimePasses(secondsElapsed);
		
		secondsSinceLastLetter += secondsElapsed;
		
		if(secondsSinceLastLetter >= newLetterAtEvery) {
			if(indexNextLetter < letters.size()) {
				addObject2d(letters.get(indexNextLetter));
				indexNextLetter++;
				secondsSinceLastLetter = 0;
			}
		}

	}

	@Override
	protected void onMouseEventNotConsumed(World2dMouseEventFx mouseEvent) {
		
	}

}
