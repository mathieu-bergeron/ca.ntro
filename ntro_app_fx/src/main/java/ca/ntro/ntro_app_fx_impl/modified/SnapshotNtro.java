package ca.ntro.ntro_app_fx_impl.modified;

import ca.ntro.app.modified.Snapshot;

public class SnapshotNtro<O extends Observable> implements Snapshot<O> {

	private O currentValue;
	private String id;

	public O getCurrentValue() {
		return currentValue;
	}

	public void setCurrentValue(O currentValue) {
		this.currentValue = currentValue;
	}

	@Override
	public O currentValue() {
		return getCurrentValue();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String id() {
		return id;
	}
}
