package ca.ntro.ntro_app_fx_impl;

import ca.ntro.app.Ntro;
import ca.ntro.app.session.Session;
import ca.ntro.ntro_app_fx_impl.models.SelectionsByModel;
import ca.ntro.ntro_app_fx_impl.models.SelectionsOneModel;
import ca.ntro.ntro_app_fx_impl.options.OptionsImpl;
import ca.ntro.ntro_core_impl.InitializerFinalizerImpl;
import ca.ntro.ntro_core_impl.NtroCoreImpl;
import ca.ntro.ntro_core_impl.options.CoreOptionsImpl;
import ca.ntro.ntro_core_impl.services.LoggerImpl;

public class InitializerFinalizerApp extends InitializerFinalizerImpl {

	@Override
	public void initialize() {
		super.initialize();

		NtroCoreImpl.factory().registerNamedClass(Session.class);
		NtroCoreImpl.factory().registerNamedClass(OptionsImpl.class);
		NtroCoreImpl.factory().registerNamedClass(CoreOptionsImpl.class);
		NtroCoreImpl.factory().registerNamedClass(SelectionsByModel.class);
		NtroCoreImpl.factory().registerNamedClass(SelectionsOneModel.class);
		
		((LoggerImpl) NtroCoreImpl.logger()).initialize(Ntro.session().sessionId());
	}

}
