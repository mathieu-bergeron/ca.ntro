package ca.ntro.ntro_app_fx_impl;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

import ca.ntro.app.Ntro;
import ca.ntro.app.NtroServerFx;
import ca.ntro.app.common.ServerRegistrarJdkImpl;
import ca.ntro.app.common.WebSocketServerNtro;
import ca.ntro.core.NtroCore;
import ca.ntro.core.services.ExitHandler;
import ca.ntro.ntro_app_fx_impl.backend.BackendRegistrarNtro;
import ca.ntro.ntro_app_fx_impl.executable_spec.ServerSpec;
import ca.ntro.ntro_app_fx_impl.messages.MessageRegistrarNtro;
import ca.ntro.ntro_app_fx_impl.models.ModelRegistrarImpl;
import ca.ntro.ntro_core_impl.NtroCoreImpl;
import javafx.application.Application;
import javafx.stage.Stage;

public class ServerWrapperFx extends Application implements ExitHandler {
    
    public static Class<? extends NtroServerFx> serverClass = null;
    public static ServerSpec                    spec        = null;;
    
    private NtroServerFx        serverExecutable = null;
    private WebSocketServerNtro webSocketServer  = null;

    public WebSocketServerNtro getWebSocketServer() {
        return webSocketServer;
    }

    public void setWebSocketServer(WebSocketServerNtro webSocketServer) {
        this.webSocketServer = webSocketServer;
    }

    public ServerWrapperFx() {
    }

    public ServerWrapperFx(NtroServerFx serverExecutable) {
    	this.serverExecutable = serverExecutable;
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
		NtroCoreImpl.registerAppExitHandler(this);

        Runtime.getRuntime().addShutdownHook(new Thread() {
        	@Override
        	public void run() {
        		Ntro.exit();
			}
        });

        NtroCore.logger().run();

        new Thread() {
            
            @Override
            public void run() {

                NtroCore.logger().info("App running. Press Enter here to close App");

                try {

                    System.in.read();
                    NtroCore.exit();

                } catch (IOException e) {

                    e.printStackTrace();
                }
            }
        }.start();
        
        try {

			System.out.println("\n\n\n");
			NtroCore.logger().info("Ntro version 0.2");
			NtroCore.logger().info("java version " + System.getProperty("java.version"));

			File javaRootDir      = Paths.get(NtroCore.options().javaPath()).toFile();
			File resourcesRootDir = Paths.get(NtroCore.options().resourcesPath()).toFile();

			spec.analyzeSources(resourcesRootDir, javaRootDir);

			spec.registerAppClass(serverClass);
			
			spec.callExecutableCodeAndFillSpec();

			NtroImpl.loadOptionsFile();

			NtroCore.logger().info("locale: '" + Ntro.currentLocale() + "'");
			
			spec.writeSpec();

			spec.createExecutableFromSpec();
			
			NtroCore.threads().runOnMainThread(() -> {
				spec.runExecutable();
			});
			
			spec.startWebSocketServer();

			
        } catch(Throwable t) {
        	
        	NtroCoreImpl.logger().fatal(t);
        	
        }
        
    }

    @Override
    public void onExit() {

        NtroCore.logger().info("Writing JSON files");
        NtroImpl.models().writeModelFiles();

        NtroCore.logger().info("Generating graphs");
        NtroImpl.models().writeGraphs();

        if(!spec.isRemoteBackend()) {
			spec.writeBackendGraph();
        }

		spec.stopWebSocketServer();
    }

}
