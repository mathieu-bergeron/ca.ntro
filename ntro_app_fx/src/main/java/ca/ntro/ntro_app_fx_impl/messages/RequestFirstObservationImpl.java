package ca.ntro.ntro_app_fx_impl.messages;

public class RequestFirstObservationImpl {
	
	private String className;
	private String modelId;

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getModelId() {
		return modelId;
	}

	public void setModelId(String modelId) {
		this.modelId = modelId;
	}
	
	public RequestFirstObservationImpl() {
	}

	public RequestFirstObservationImpl(String className, String modelId) {
		setClassName(className);
		setModelId(modelId);
	}
}
