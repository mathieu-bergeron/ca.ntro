package ca.ntro.ntro_app_fx_impl.messages;


public interface RegisterSession {

	String       sessionId();

}
