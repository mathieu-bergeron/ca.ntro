/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.ntro_app_fx_impl.messages;

import java.util.HashMap;
import java.util.Map;

import ca.ntro.app.Ntro;
import ca.ntro.app.messages.Message;
import ca.ntro.app.messages.MessageAccessor;
import ca.ntro.app.models.Model;
import ca.ntro.app.modified.Modified;
import ca.ntro.app.modified.Snapshot;
import ca.ntro.app.services.MessageService;
import ca.ntro.app.session.Session;
import ca.ntro.core.NtroCore;
import ca.ntro.ntro_app_fx_abstr.messages.MessageAbstr;
import ca.ntro.ntro_core_impl.NtroCoreImpl;
import ca.ntro.ntro_app_fx_impl.NtroImpl;
import ca.ntro.ntro_app_fx_impl.modified.Observable;
import ca.ntro.ntro_app_fx_impl.modified.Observation;
import ca.ntro.ntro_app_fx_impl.services.MessageServerNull;
import ca.ntro.ntro_app_fx_impl.structures.ValuesByClassId;
import ca.ntro.ntro_core_impl.task_graphs.task_graph.SimpleTask;

public class MessageServiceImpl implements MessageService {
	
	private ValuesByClassId<MessageAbstr, SimpleTask> messageHandlers = new ValuesByClassId<>();
	private ValuesByClassId<Observable, SimpleTask> observationHandlers = new ValuesByClassId<>();
	private ValuesByClassId<Observable, SimpleTask> snapshotHandlers = new ValuesByClassId<>();

	private MessageServer messageServer = new MessageServerNull();
	private DeliveryMode deliveryMode = DeliveryMode.LOCAL;

	@Override
	public <MSG extends Message> MSG newMessage(Class<MSG> messageClass) {
		MSG message = NtroCore.factory().newInstance(messageClass);
		
		Session session = Ntro.session();
		session.copyModelSelectionsInto(message);
		
		MessageAccessor.registerMessageService(message, this);

		return message;
	}

	@Override
	public <MSG extends MessageAbstr> void addMessageHandler(Class<MSG> messageClass, SimpleTask messageHandlerTask) {
		messageHandlers.add(messageClass, null, messageHandlerTask);
	}

	@Override
	public <MSG extends MessageAbstr> void removeMessageHandler(Class<MSG> messageClass, SimpleTask messageHandlerTask) {
		messageHandlers.remove(messageClass, null, messageHandlerTask);
	}

	@Override
	public void addObserverTask(Class<? extends Observable> observableClass, SimpleTask observationHandlerTask) {
		observationHandlers.add(observableClass, null, observationHandlerTask);
		
		observe(observableClass, null);
	}


	@Override
	public void observe(Class<? extends Observable> observableClass, String observableId) {
		if(deliveryMode == DeliveryMode.LOCAL) {
			
			try {
			
				NtroImpl.models().watch((Class<? extends Model>) observableClass, observableId);

			}catch(ClassCastException e) {

				NtroCore.logger().fatal(e);
			}

		}else {
			
			messageServer.requestFirstObservationFromServer(observableClass, observableId);

		}
	}


	@Override
	public void removeObserverTask(Class<? extends Observable> observableClass, SimpleTask observationHandlerTask) {
		observationHandlers.remove(observableClass, null, observationHandlerTask);
	}

	@Override
	public void addSnapshotTask(Class<? extends Observable> observableClass, SimpleTask observationHandlerTask) {
		snapshotHandlers.add(observableClass, null, observationHandlerTask);
		messageServer.requestFirstObservationFromServer(observableClass, null);
	}

	@Override
	public void removeSnapshotTask(Class<? extends Observable> observableClass, SimpleTask observationHandlerTask) {
		snapshotHandlers.remove(observableClass, null, observationHandlerTask);
	}


	@Override
	public void sendMessageToServer(MessageAbstr message) {
		messageServer.sendMessageToServer(message);
	}

	@Override
	public void broadcastMessageToOtherClients(MessageAbstr message) {
		switch(deliveryMode) {
			case LOCAL:
			default:
				deliverMessageLocally((Message) message);
				break;

			case CLIENT_MODE:
			case SERVER_MODE:
				messageServer.broadcastMessageToOtherClients(message);
				break;
		}
	}

	@Override
	public synchronized void receiveMessageFromServer(MessageAbstr message) {
		messageHandlers.values(message.getClass(), null).forEach(handler -> {

			addMessageToMessageHandlerTask(handler, message);

		});
	}

	protected void addMessageToMessageHandlerTask(SimpleTask handler, MessageAbstr message) {
		handler.addResult(message);
	}

	@Override
	public <O extends Observable> void receiveObservationFromServer(Class<O> observableClass, Observation<?> observation) {
		receiveObservationFromServer(observableClass, null, observation);
	}

	@Override
	public <O extends Observable> void receiveObservationFromServer(Class<O> observableClass, String observableId, Observation<?> observation) {
		
		observationHandlers.values(observableClass, null).forEach(handler -> {
			
			Session session = NtroImpl.session();
			
			if(session.isModelSelected((Class<? extends Model>)observableClass, observableId)) {

				addObservationToObservationHandlerTask(handler, observation);

			}else if(observableId == null 
					&& !session.hasModelSelections((Class<? extends Model>)observableClass)) {

				addObservationToObservationHandlerTask(handler, observation);
				
			}
		});
		
		snapshotHandlers.values(observableClass, observableId).forEach(handler -> {

			addSnapshotToSnapshotHandlerTask(handler, (Snapshot) observation);
		});

		snapshotHandlers.clear(observableClass, observableId);
	}

	protected void addObservationToObservationHandlerTask(SimpleTask handler, Observation<?> observation) {
		handler.addResult((Modified<?>) observation);
	}

	protected void addSnapshotToSnapshotHandlerTask(SimpleTask handler, Snapshot<?> snapshot) {
		handler.addResult(snapshot);
	}

	@Override
	public <O extends Observable> void pushObservationToClients(Class<O> observableClass, String observationId, Observation<O> observation) {
		messageServer.pushObservationToClients(observableClass, observationId, observation);
	}

	@Override
	public <O extends Observable> void pushObservationToClients(Class<O> observationClass, Observation<O> observation) {
		pushObservationToClients(observationClass, null, observation);
	}

	public void deliverMessage(Message message) {
		switch(deliveryMode) {
			case LOCAL:
			case SERVER_MODE:
			default:
				deliverMessageLocally(message);
				break;

			case CLIENT_MODE:
				sendMessageToServer(message);
				break;
		}
	}

	public void deliverMessageTo(Message<?> message, String sessionId) {
		switch(deliveryMode) {
			case LOCAL:
				if(Ntro.session().sessionId().equals(sessionId)) {
					deliverMessageLocally(message);
				}
			break;
			case SERVER_MODE:
			default:
				deliverMessageLocally(message);
				break;

			case CLIENT_MODE:
				SendMessageTo sendMessageTo = new SendMessageToImpl(message, sessionId);
				sendMessageToServer(sendMessageTo);
				break;
		}
		
	}

	private void deliverMessageLocally(Message message) {
		// XXX: copy message to simulate that the
		//      message is sent through a channel

		//Message copyOfMessage = (Message) NtroCoreImpl.reflection().clone(message);
		Message copyOfMessage = message;

		//MessageAccessor.registerMessageService(copyOfMessage, this);
		receiveMessageFromServer(copyOfMessage);
	}

	@Override
	public void registerMessageServer(MessageServer messageServer, DeliveryMode deliveryMode) {
		this.messageServer = messageServer;
		this.deliveryMode = deliveryMode;
		
		this.messageServer.onMessageFromServer(message -> {
			receiveMessageFromServer(message);
		});
		
		this.messageServer.onObservationFromServer((observableClass, observableId, observation) -> {
			receiveObservationFromServer((Class<? extends Observable>) observableClass, observableId, observation);
		});
	}



}
