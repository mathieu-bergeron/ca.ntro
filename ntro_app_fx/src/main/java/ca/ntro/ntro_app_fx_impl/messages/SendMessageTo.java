package ca.ntro.ntro_app_fx_impl.messages;

import ca.ntro.ntro_app_fx_abstr.messages.MessageAbstr;

public interface SendMessageTo extends MessageAbstr {

	String       targetSessionId();
	MessageAbstr message();

}
