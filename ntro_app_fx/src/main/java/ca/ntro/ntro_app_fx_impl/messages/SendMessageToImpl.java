package ca.ntro.ntro_app_fx_impl.messages;

import ca.ntro.ntro_app_fx_abstr.messages.MessageAbstr;
import ca.ntro.ntro_app_fx_abstr.models.ModelSelectionsWriter;

public class SendMessageToImpl implements SendMessageTo {

	private MessageAbstr message;
	private String       targetSessionId;

	public SendMessageToImpl() {
		
	}

	public SendMessageToImpl(MessageAbstr message, String targetSessionId) {
		this.message = message;
		this.targetSessionId = targetSessionId;
	}

	@Override
	public String targetSessionId() {
		return targetSessionId;
	}

	@Override
	public MessageAbstr message() {
		return message;
	}

	@Override
	public ModelSelectionsWriter setModelSelection(Class modelClass, String modelId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ModelSelectionsWriter addModelSelection(Class modelClass, String modelId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ModelSelectionsWriter clearModelSelections(Class modelClass) {
		// TODO Auto-generated method stub
		return null;
	}

}
