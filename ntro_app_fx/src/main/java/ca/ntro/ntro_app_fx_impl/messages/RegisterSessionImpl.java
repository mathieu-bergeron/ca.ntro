package ca.ntro.ntro_app_fx_impl.messages;

public class RegisterSessionImpl implements RegisterSession {
	
	private String sessionId;
	
	public RegisterSessionImpl() {
		
	}

	public RegisterSessionImpl(String sessionId) {
		this.sessionId = sessionId;
	}

	@Override
	public String sessionId() {
		return sessionId;
	}

}
