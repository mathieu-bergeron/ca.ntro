package ca.ntro.ntro_app_fx_impl.structures;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import ca.ntro.core.stream.Stream;
import ca.ntro.core.stream.Visitor;
import ca.ntro.core.util.StringUtils;
import ca.ntro.ntro_core_impl.stream.StreamNtro;

public class IdMap<V> {
	
	private Map<String, V> valueById = new ConcurrentHashMap<>();
	private V singletonValue = null;

	public Map<String, V> getValueById() {
		return valueById;
	}

	public void setValueById(Map<String, V> valueById) {
		this.valueById = valueById;
	}

	public synchronized void put(String id, V value) {
		if(isSingletonId(id)) {

			singletonValue = value;

		}else {

			valueById.put(id, value);

		}
	}

	private boolean isSingletonId(String id) {
		return StringUtils.isNullOrEmpty(id);
	}

	public synchronized V get(String id) {
		V value = null;
		
		if(isSingletonId(id)) {
			
			value = singletonValue;
			
		}else {
			
			value = valueById.get(id);
		}

		return value;
	}

	public Stream<V> values(){
		return entries().map(entry -> entry.value());
	}

	public Stream<IdMapEntry<V>> entries(){
		return new StreamNtro<IdMapEntry<V>>() {

			@Override
			public void forEach_(Visitor<IdMapEntry<V>> visitor) throws Throwable {

				synchronized(IdMap.this) {
					
					if(singletonValue != null) {
						visitor.visit(new IdMapEntry<V>() {

							@Override
							public String entryId() {
								return null;
							}

							@Override
							public V value() {
								return singletonValue;
							}
						});
					}

					for(Map.Entry<String, V> entry : valueById.entrySet()) {

						visitor.visit(new IdMapEntry<V>() {

							@Override
							public String entryId() {
								return entry.getKey();
							}

							@Override
							public V value() {
								return entry.getValue();
							}
						});
					}
				}
			}
		};
	}
	
	public String id(V value) {
		String id = null;

		if(singletonValue == value) {

			id = "";

		}else {
			
			for(Map.Entry<String, V> entry : valueById.entrySet()) {
				if(entry.getValue() == value) {
					id = entry.getKey();
					break;
				}
			}
		}
		
		return id;
	}

}
