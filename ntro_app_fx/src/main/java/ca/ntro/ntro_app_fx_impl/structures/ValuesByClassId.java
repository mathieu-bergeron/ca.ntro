package ca.ntro.ntro_app_fx_impl.structures;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import ca.ntro.core.stream.Stream;
import ca.ntro.core.stream.Visitor;
import ca.ntro.ntro_core_impl.stream.StreamNtro;

public class ValuesByClassId<C, V> {
	
	private ClassIdMap<C, Set<V>> valuesByClassId = new ClassIdMap<>();

	public ClassIdMap<C, Set<V>> getValuesByClassId() {
		return valuesByClassId;
	}

	public void setValuesByClassId(ClassIdMap<C, Set<V>> valuesByClassId) {
		this.valuesByClassId = valuesByClassId;
	}

	public synchronized void add(Class<? extends C> _class, String id, V value) {
		Set<V> values = valuesByClassId.get(_class, id);
		
		if(values == null) {
			values = Collections.synchronizedSet(new HashSet<>());
			valuesByClassId.put(_class, id, values);
		}

		values.add(value);
	}

	public synchronized void remove(Class<? extends C> _class, String id, V value) {
		Set<V> values = valuesByClassId.get(_class, id);

		if(values != null) {
			values.remove(value);
		}
	}

	
	public synchronized  void clear(Class<? extends C> _class, String id){
		Set<V> values = valuesByClassId.get(_class, id);

		if(values != null) {
			values.clear();
		}
	}
	
	public Stream<V> values(Class<? extends C> _class, String id){
		return new StreamNtro<V>() {

			@Override
			public void forEach_(Visitor<V> visitor) throws Throwable {

				synchronized(ValuesByClassId.this) {

					Set<V> values = valuesByClassId.get(_class, id);

					if(values != null) {
						for(V task : values) {
							visitor.visit(task);
						}
					}
				}
			}

		};
	}
}
