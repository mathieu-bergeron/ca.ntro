package ca.ntro.ntro_app_fx_impl.structures;

public interface IdMapEntry<V> {
	
	String entryId();
	V value();

}
