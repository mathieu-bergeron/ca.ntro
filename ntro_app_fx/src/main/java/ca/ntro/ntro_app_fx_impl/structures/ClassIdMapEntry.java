package ca.ntro.ntro_app_fx_impl.structures;

public interface ClassIdMapEntry<C,V> extends IdMapEntry<V> {
	
	Class<? extends C> entryClass();
	
	
	

}
