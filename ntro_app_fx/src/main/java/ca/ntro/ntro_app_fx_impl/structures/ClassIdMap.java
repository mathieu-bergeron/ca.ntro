package ca.ntro.ntro_app_fx_impl.structures;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import ca.ntro.core.stream.Stream;
import ca.ntro.core.stream.Visitor;
import ca.ntro.ntro_core_impl.stream.StreamNtro;

public class ClassIdMap<C, V> {
	
	private Map<Class<? extends C>, IdMap<V>> idMapByClass = new ConcurrentHashMap<>();

	public Map<Class<? extends C>, IdMap<V>> getIdMapByClass() {
		return idMapByClass;
	}

	public void setIdMapByClass(Map<Class<? extends C>, IdMap<V>> idMapByClass) {
		this.idMapByClass = idMapByClass;
	}

	public synchronized void put(Class<? extends C> _class, String id, V value) {
		IdMap<V> idMap = idMapByClass.get(_class);
		
		if(idMap == null) {
			idMap = new IdMap<>();
			idMapByClass.put(_class, idMap);
		}
		
		idMap.put(id, value);
	}

	public synchronized V get(Class<? extends C> _class, String id) {
		V value = null;
		IdMap<V> idMap = idMapByClass.get(_class);
		
		if(idMap != null) {
			value = idMap.get(id);
		}

		return value;
	}

	public Stream<V> values(){
		return entries().map(entry -> entry.value());
	}

	public Stream<ClassIdMapEntry<C,V>> entries(){

		return new StreamNtro<ClassIdMapEntry<C,V>>() {

			@Override
			public void forEach_(Visitor<ClassIdMapEntry<C, V>> visitor) throws Throwable {

				synchronized(ClassIdMap.this) {

					for(Map.Entry<Class<? extends C>, IdMap<V>> idMapByClassEntry : idMapByClass.entrySet()) {
						
						IdMap<V> idMap = idMapByClassEntry.getValue();
						
						idMap.entries().forEach(idMapEntry -> {
							
							visitor.visit(new ClassIdMapEntry<C,V>(){

								@Override
								public String entryId() {
									return idMapEntry.entryId();
								}

								@Override
								public V value() {
									return idMapEntry.value();
								}

								@Override
								public Class<? extends C> entryClass() {
									return idMapByClassEntry.getKey();
								}
							});
						});
					}
				}
			}
		};
	}
	
	public String id(Class<?> _class, V value) {
		
		IdMap<V> idMap = idMapByClass.get(_class);
		
		return idMap.id(value);
	}

}
