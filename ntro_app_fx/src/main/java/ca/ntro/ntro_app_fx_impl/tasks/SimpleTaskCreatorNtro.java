/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.ntro_app_fx_impl.tasks;

import java.util.Map;

import ca.ntro.app.tasks.SimpleTaskCreator;
import ca.ntro.app.tasks.frontend.BlockingFrontendExecutor;
import ca.ntro.app.tasks.frontend.TypedBlockingFrontendExecutor;
import ca.ntro.ntro_core_abstr.tasks.TaskAbstr;
import ca.ntro.ntro_core_impl.task_graphs.generic_task_graph.GenericTaskImpl;
import ca.ntro.ntro_core_impl.task_graphs.task_graph.SimpleTask;
import ca.ntro.ntro_core_impl.task_graphs.task_graph.TaskContainer;
import ca.ntro.ntro_core_impl.task_graphs.task_graph.TaskGraph;
import ca.ntro.ntro_core_impl.values.ObjectMap;

public class   SimpleTaskCreatorNtro<O extends Object>

       extends TaskCreatorNtro<O, 
                               SimpleTaskDescriptor<?>, 
                               SimpleTaskCreator<O>,
                               TaskAbstr>

       implements SimpleTaskCreator<O> {
	

	public SimpleTaskCreatorNtro(Map<String, TaskAbstr> orphanTasks,
			               TaskGraph graph, 
			               TaskContainer parent, 
			               TaskAbstr task) {

		super(orphanTasks, graph, parent, task);
	}

	@Override
	public SimpleTaskCreator<O> executes(BlockingFrontendExecutor executor) {
		if(getTaskImpl().isSimpleTask()
				&& getTaskImpl().asSimpleTask().isExecutableTask()) {
			
			
			registerExecutionHandler(executor);
			addExecutionTrace(getTaskImpl().asSimpleTask());
		}

		return this;
	}

	private void addExecutionTrace(SimpleTask newTask) {
		GenericTaskImpl<?,?,?,?,?> newTaskNtro = (GenericTaskImpl<?,?,?,?,?>) newTask;
		
		if(newTaskNtro.getGraph().hasExecutionTraces()) {

			newTaskNtro.getGraph().addTaskToExistingTraces(newTaskNtro);
		}
	}

	private void registerExecutionHandler(BlockingFrontendExecutor executor) {
		getTaskImpl().asSimpleTask().asExecutableTask().execute((results, notifyer) -> {

			executeTask(executor, results);

			notifyer.addResult(true);
		});
	}

	protected void executeTask(BlockingFrontendExecutor executor, ObjectMap results) {
		executor.execute(new TaskInputsNtro(getGraph(), getTaskImpl(), results));
	}

	@Override
	public SimpleTaskCreator<O> executesAndReturnsValue(TypedBlockingFrontendExecutor<?> executor) {

		if(getTaskImpl().isSimpleTask()
				&& getTaskImpl().asSimpleTask().isExecutableTask()) {
			
			registerExecutionForValueHandler(executor);
			addExecutionTrace(getTaskImpl().asSimpleTask());
		}

		return this;
	}

	private void registerExecutionForValueHandler(TypedBlockingFrontendExecutor<?> executor) {
		getTaskImpl().asSimpleTask().asExecutableTask().execute((results, notifyer) -> {
			
			Object value = executeTaskForValue(executor, results);

			notifyer.addResult(value);
		});
	}

	protected Object executeTaskForValue(TypedBlockingFrontendExecutor<?> executor, ObjectMap results) {
		return executor.execute(new TaskInputsNtro(getGraph(), getTaskImpl(), results));
	}

	@Override
	protected SimpleTaskDescriptor<?> newTaskDescriptor(String id) {
		return new SimpleTaskDescriptorNtro<>(id);
	}

}
