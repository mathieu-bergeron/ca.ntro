package ca.ntro.ntro_app_fx_impl.tasks.frontend;

import ca.ntro.app.Ntro;
import ca.ntro.app.models.Model;
import ca.ntro.ntro_app_fx_impl.NtroImpl;
import ca.ntro.ntro_app_fx_impl.tasks.ModelTaskDescriptorNtro;
import ca.ntro.ntro_core_abstr.tasks.TaskAbstr;
import ca.ntro.ntro_core_impl.task_graphs.generic_task_graph.SimpleTaskOptions;
import ca.ntro.ntro_core_impl.task_graphs.task_graph.SimpleTaskNtro;
import ca.ntro.ntro_core_impl.task_graphs.task_graph.TaskContainer;
import ca.ntro.ntro_core_impl.task_graphs.task_graph_trace.TaskResultsCondition;
import ca.ntro.ntro_core_impl.task_graphs.task_graph_trace.TaskResultsEventHandler;
import ca.ntro.ntro_core_impl.task_graphs.task_graph_trace.TaskResultsMessageHandler;
import ca.ntro.ntro_core_impl.task_graphs.task_graph_trace.TaskTraceNtro;

public class FrontendModifiedTaskDescriptorNtro<O>

       extends    ModelTaskDescriptorNtro<O>

       implements FrontendSimpleTaskDescriptor<O> {
    
    public FrontendModifiedTaskDescriptorNtro(Class<?> entryClass) {
        super(entryClass);
    }

    public FrontendModifiedTaskDescriptorNtro(Class<?> entryClass, String id) {
        super(entryClass, id);
    }
    

    @Override
    public TaskAbstr newTask(TaskContainer parent) {
        String taskId = id();

        TaskAbstr modifiedHandler = parent.newTask(taskId,
                                              SimpleTaskOptions.taskClass(SimpleTaskNtro.class)
                                                                      .traceClass(TaskTraceNtro.class)
                                                                      .resultsClass(TaskResultsCondition.class)); 

        NtroImpl.messageService().addObserverTask((Class<? extends Model>) typeClass(), modifiedHandler.asSimpleTask());
        
        modifiedHandler.onRemovedFromGraph(() -> {
            
            NtroImpl.messageService().removeObserverTask((Class<? extends Model>) typeClass(), modifiedHandler.asSimpleTask());

        });
        
        return modifiedHandler;
    }

    @Override
    protected String taskLabel() {
        return "modified";
    }
}
