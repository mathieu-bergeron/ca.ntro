/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.ntro_app_fx_impl.tasks.backend;

import java.util.List;
import java.util.Map;

import ca.ntro.app.messages.Message;
import ca.ntro.app.messages.MessageAccessor;
import ca.ntro.app.models.Model;
import ca.ntro.app.tasks.frontend.BlockingFrontendExecutor;
import ca.ntro.app.tasks.frontend.TypedBlockingFrontendExecutor;
import ca.ntro.core.NtroCore;
import ca.ntro.core.stream.Stream;
import ca.ntro.ntro_app_fx_impl.NtroImpl;
import ca.ntro.ntro_app_fx_impl.models.SelectionsByModel;
import ca.ntro.ntro_core_impl.NtroCoreImpl;
import ca.ntro.ntro_app_fx_impl.modified.Observable;
import ca.ntro.ntro_app_fx_impl.structures.ClassIdMap;
import ca.ntro.ntro_app_fx_impl.tasks.SimpleTaskCreatorNtro;
import ca.ntro.ntro_app_fx_impl.tasks.TaskInputsNtro;
import ca.ntro.ntro_core_abstr.tasks.TaskAbstr;
import ca.ntro.ntro_core_impl.task_graphs.task_graph.TaskContainer;
import ca.ntro.ntro_core_impl.task_graphs.task_graph.TaskGraph;
import ca.ntro.ntro_core_impl.values.ObjectMap;
import ca.ntro.ntro_core_impl.values.ObjectMapNtro;

public class   BackendSimpleTaskCreatorNtro<O> 

       extends     SimpleTaskCreatorNtro<O> 

       implements  BackendSimpleTaskCreator<O> {


	public BackendSimpleTaskCreatorNtro(Map<String, TaskAbstr> orphanTasks, 
			                            TaskGraph graph, TaskContainer parent, 
			                            TaskAbstr task) {

		super(orphanTasks, graph, parent, task);
	}
	
	@Override
	protected void executeTask(BlockingFrontendExecutor executor, ObjectMap results) {
		createContextAndExecute(executor, results);
	}

	private class ExecutionContext {
		
		
		private ObjectMapNtro newResults = new ObjectMapNtro();
		private ClassIdMap<Object, Object> models = new ClassIdMap<>();
		private SelectionsByModel modelSelections = new SelectionsByModel();

		ObjectMap newResults() {
			return newResults;
		}
		
		void aquireLock() {
		}
		
		
		void loadModels(ObjectMap results) {
			
			collectModelSelections(results);

			results.ids().forEach(id -> {
				
				if(id.startsWith("model[")) {
					
					String classNameAndId = id.replace("model[", "");
					classNameAndId = classNameAndId.replace("]", "");
					
					String className = null;
					String modelId = null;
					String[] segments = classNameAndId.split("/");
					
					if(segments.length == 1) {

						className = segments[0];

					}else if(segments.length == 2) {

						className = segments[0];
						modelId = segments[1];

					}else {
						
						NtroCoreImpl.logger().fatal("could not parse model task " + id);
						
					}
					
					Class<?> modelClass = NtroCore.factory().namedClass(className);
					
					if(modelId == null) {
						
						// FIXME: in the case of multiple selections, should run the task
						//        once for each selection
						modelId = modelSelections.selections((Class<? extends Model>)modelClass).findFirst(x -> true);
						
					}
					
					Observable model = NtroImpl.models().load(modelClass, modelId);
					
					models.put(modelClass, modelId, model);
					
					newResults.registerObject(id, model);

				}else {

					newResults.registerObject(id, results.get(id));
				}
			});
		}

		private void collectModelSelections(ObjectMap results) {
			Stream<String> messageIds = results.ids().findAll(id -> id.startsWith("message["));
			
			List<Message> messages = messageIds.map(messageId -> (Message) results.get(messageId)).collect();
			
			if(messages.size() > 0) {
				modelSelections = MessageAccessor.getModelSelections(messages.get(0));
			}
			
			for(int i = 1; i < messages.size(); i++) {
				MessageAccessor.mergeCompatibleModelSelectionsInto(messages.get(i), modelSelections);
			}
				
			//NtroCoreImpl.logger().info("modelSelection: "  + NtroCoreImpl.reflection().toJsonObject(modelSelections).toJsonString());
		}
		

		void saveModels() {
			models.entries().forEach(entry -> {
				NtroImpl.models().save(entry.entryClass(), entry.entryId(), entry.value());
			});
		}

		void releaseLock() {
		}
	}
	
	private synchronized void createContextAndExecute(BlockingFrontendExecutor executor, ObjectMap results) {
		ExecutionContext context = new ExecutionContext();
		
		context.aquireLock();

		context.loadModels(results);

		executor.execute(new TaskInputsNtro(getGraph(), getTaskImpl(), context.newResults()));
		
		context.saveModels();
		
		context.releaseLock();
	}

	@Override
	protected Object executeTaskForValue(TypedBlockingFrontendExecutor<?> executor, ObjectMap results) {
		return createContextAndExecuteForValue(executor, results);
	}

	private synchronized Object createContextAndExecuteForValue(TypedBlockingFrontendExecutor<?> executor, ObjectMap results) {
		ExecutionContext context = new ExecutionContext();
		
		context.aquireLock();

		context.loadModels(results);

		Object result = executor.execute(new TaskInputsNtro(getGraph(), getTaskImpl(), context.newResults()));
		
		context.saveModels();
		
		context.releaseLock();
		
		return result;
	}

}
