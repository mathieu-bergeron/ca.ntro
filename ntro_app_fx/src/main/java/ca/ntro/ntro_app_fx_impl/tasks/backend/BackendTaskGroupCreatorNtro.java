/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.ntro_app_fx_impl.tasks.backend;

import java.util.Map;

import ca.ntro.app.tasks.backend.BackendTasks;
import ca.ntro.ntro_app_fx_impl.tasks.ContainerTasks;
import ca.ntro.ntro_app_fx_impl.tasks.TaskGroupCreatorNtro;
import ca.ntro.ntro_app_fx_impl.tasks.TaskGroupDescriptor;
import ca.ntro.ntro_core_abstr.tasks.TaskAbstr;
import ca.ntro.ntro_core_abstr.tasks.TaskGroupAbstr;
import ca.ntro.ntro_core_impl.task_graphs.task_graph.TaskContainer;
import ca.ntro.ntro_core_impl.task_graphs.task_graph.TaskGraph;

public class BackendTaskGroupCreatorNtro<O>

       extends TaskGroupCreatorNtro<O, BackendTasks> {

	public BackendTaskGroupCreatorNtro(Map<String, TaskAbstr> orphanTasks, 
			                            TaskGraph graph, TaskContainer parent, 
			                            TaskGroupAbstr task) {

		super(orphanTasks, graph, parent, task);
	}


	@Override
	protected TaskGroupDescriptor<?> newTaskDescriptor(String id) {
		return new BackendTaskGroupDescriptorNtro<>(id);
	}


	@Override
	protected ContainerTasks<BackendTasks> newContainerTasks(Map<String, TaskAbstr> orphanTasks, 
			                                                  TaskGraph graph, 
			                                                  TaskGroupAbstr task) {

		return new BackendContainerTasksNtro(orphanTasks, graph, task);
	}

}
