package ca.ntro.ntro_app_fx_impl.tasks.frontend;

import ca.ntro.ntro_app_fx_impl.tasks.MessageHandlerTaskDescriptorNtro;

public class FrontendMessageTaskDescriptorNtro<O>

       extends    MessageHandlerTaskDescriptorNtro<O>

       implements FrontendSimpleTaskDescriptor<O> {
	
	
	public FrontendMessageTaskDescriptorNtro(Class<?> entryClass) {
		super(entryClass);
	}

	public FrontendMessageTaskDescriptorNtro(Class<?> entryClass, String id) {
		super(entryClass, id);
	}
}
