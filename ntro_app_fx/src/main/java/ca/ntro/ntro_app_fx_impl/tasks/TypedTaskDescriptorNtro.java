package ca.ntro.ntro_app_fx_impl.tasks;

import ca.ntro.core.util.StringUtils;
import ca.ntro.ntro_core_impl.NtroCoreImpl;

public abstract class TypedTaskDescriptorNtro<O>

       extends        SimpleTaskDescriptorNtro<O> {
	
	
	private Class<?> typeClass;

	public Class<?> typeClass() {
		return typeClass;
	}

	public void setTypeClass(Class<?> entryClass) {
		this.typeClass = entryClass;
	}

	public TypedTaskDescriptorNtro(Class<?> entryClass) {
		super(null);
		setTypeClass(entryClass);
	}

	public TypedTaskDescriptorNtro(Class<?> entryClass, String id) {
		super(id);
		setTypeClass(entryClass);
	}
	
	protected abstract String taskLabel();
	
	@Override
	public String id() {
		String modelId = getId();
		String id = taskLabel();
		id += "[" + NtroCoreImpl.reflection().simpleName(typeClass());
		
		if(!StringUtils.isNullOrEmpty(modelId)) {

			id += "/" + modelId;
		}
		
		id += "]";

		return id;
	}
}
