package ca.ntro.ntro_app_fx_impl.tasks;


public abstract class ModelTaskDescriptorNtro<O>

       extends        TypedTaskDescriptorNtro<O> {
	
	
	public ModelTaskDescriptorNtro(Class<?> entryClass) {
		super(entryClass);
	}

	public ModelTaskDescriptorNtro(Class<?> entryClass, String id) {
		super(entryClass, id);
	}
}
