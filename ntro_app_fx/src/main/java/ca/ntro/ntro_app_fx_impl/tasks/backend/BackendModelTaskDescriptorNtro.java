package ca.ntro.ntro_app_fx_impl.tasks.backend;

import ca.ntro.ntro_app_fx_impl.tasks.ModelTaskDescriptorNtro;
import ca.ntro.ntro_core_abstr.tasks.TaskAbstr;
import ca.ntro.ntro_core_impl.task_graphs.generic_task_graph.SimpleTaskOptions;
import ca.ntro.ntro_core_impl.task_graphs.task_graph.SimpleTaskNtro;
import ca.ntro.ntro_core_impl.task_graphs.task_graph.TaskContainer;
import ca.ntro.ntro_core_impl.task_graphs.task_graph_trace.TaskResultsLock;
import ca.ntro.ntro_core_impl.task_graphs.task_graph_trace.TaskTraceNtro;

public class BackendModelTaskDescriptorNtro<O>

       extends    ModelTaskDescriptorNtro<O>

       implements BackendSimpleTaskDescriptor<O> {
	
	
	public BackendModelTaskDescriptorNtro(Class<?> entryClass) {
		super(entryClass);
	}

	public BackendModelTaskDescriptorNtro(Class<?> entryClass, String id) {
		super(entryClass, id);
	}

	@Override
	public TaskAbstr newTask(TaskContainer parent) {
		String taskId = id();
		
		TaskAbstr task = parent.newTask(taskId, SimpleTaskOptions.taskClass(SimpleTaskNtro.class)
				                                            .traceClass(TaskTraceNtro.class)
				                                            .resultsClass(TaskResultsLock.class));

		return task;

	}

	@Override
	protected String taskLabel() {
		return "model";
	}
}
