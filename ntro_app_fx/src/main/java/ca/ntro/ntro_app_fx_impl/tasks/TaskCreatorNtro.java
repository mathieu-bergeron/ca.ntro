/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.ntro_app_fx_impl.tasks;

import java.util.Map;

import ca.ntro.app.Ntro;
import ca.ntro.core.NtroCore;
import ca.ntro.core.tasks.Task;
import ca.ntro.ntro_core_abstr.tasks.TaskAbstr;
import ca.ntro.ntro_core_impl.task_graphs.task_graph.TaskContainer;
import ca.ntro.ntro_core_impl.task_graphs.task_graph.TaskGraph;
import ca.ntro.ntro_core_impl.tasks.TaskWrapper;

public abstract class TaskCreatorNtro<O extends Object,
								      TD extends TaskDescriptor<?>,
								      TC extends TaskCreator<O,T>,
                                      T extends TaskAbstr>


       implements     TaskCreator<O,T>,
                      WaitsFor<O,TD,TC,T> {
	
	private Map<String, TaskAbstr> orphanTasks;
	private TaskGraph graph;
	private TaskContainer parent;
	private T task;

	public TaskContainer getParent() {
		return parent;
	}

	public void setParent(TaskContainer parent) {
		this.parent = parent;
	}

	public Task getTask() {
		return new TaskWrapper(task);
	}

	public T getTaskImpl() {
		return task;
	}

	public void setTask(T task) {
		this.task = task;
	}

	public TaskGraph getGraph() {
		return graph;
	}

	public void setGraph(TaskGraph graph) {
		this.graph = graph;
	}

	public Map<String, TaskAbstr> getOrphanTasks() {
		return orphanTasks;
	}

	public void setOrphanTasks(Map<String, TaskAbstr> orphanTasks) {
		this.orphanTasks = orphanTasks;
	}


	public TaskCreatorNtro(Map<String, TaskAbstr> orphanTasks,
			                      TaskGraph graph,
			                      TaskContainer parent, 
			                      T task) {
		setOrphanTasks(orphanTasks);
		setGraph(graph);
		setParent(parent);
		setTask(task);
	}
	

	protected abstract TD newTaskDescriptor(String id);

	@Override
	public TC waitsFor(String id) {
		return waitsFor(newTaskDescriptor(id));
	}

	@Override
	public TC waitsFor(TD descriptor) {
		TaskAbstr previousTask = null;
		
		previousTask = getGraph().findTask(descriptor.id());

		if(previousTask == null) {

			previousTask = getOrphanTasks().get(descriptor.id());
		}
		
		if(previousTask == null) {

			previousTask = descriptor.newTask(getParent());
		}


		if(previousTask == null) {
			String msg = "waitsFor must be called with a task that is already created (" + descriptor.id() + " not found)";
			msg += "\n\n\tplease correct " + getTaskImpl().id() + ".waitsFor";
			
			throw new RuntimeException(msg);
		}
		
		getTaskImpl().addWaitsFor(previousTask);

		if(!isAlreadyAPreconditionOf(previousTask, (TaskAbstr) getTaskImpl())) {
			
			addAsDirectPrecondition(previousTask);
		}

		return (TC) this;
	}

	private void addAsDirectPrecondition(TaskAbstr previousTask) {
		
		if(!isAlreadyInGraph(previousTask)) {

			addSibling(previousTask);

		}else if((previousTask.isEvent() || previousTask.id().startsWith("session["))
				&& !isAlreadyASibling(previousTask)) {

			addSibling(previousTask);
		}

		getTaskImpl().addPreviousTask(previousTask);
		
	}
	
	private boolean isAlreadyInGraph(TaskAbstr previousTask) {
		return getGraph().findTask(previousTask.id()) != null;
	}

	private boolean isAlreadyASibling(TaskAbstr previousTask) {
		boolean ifAlreadyExistsAsSibling = false;

		if(getParent() != null) {
			
			ifAlreadyExistsAsSibling = getParent().findTask(previousTask.id()) != null;
		}

		return ifAlreadyExistsAsSibling;
	}
	

	private void addSibling(TaskAbstr newSiblingTask) {

		if(newSiblingTask.isTaskGroup()) {
			
			getParent().addGroup(newSiblingTask.asTaskGroup());
			
		}else if(newSiblingTask.isSimpleTask()){
			
			getParent().addTask(newSiblingTask.asSimpleTask());
		}
	}

	private boolean isAlreadyAPreconditionOf(TaskAbstr precondition, TaskAbstr currentTask) {
		return currentTask.preConditions().ifSome(preTask -> preTask.id().equals(precondition.id()));
	}
}
