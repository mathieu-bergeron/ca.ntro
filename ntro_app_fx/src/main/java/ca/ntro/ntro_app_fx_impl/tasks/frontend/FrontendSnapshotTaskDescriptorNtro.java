package ca.ntro.ntro_app_fx_impl.tasks.frontend;

import ca.ntro.app.Ntro;
import ca.ntro.ntro_app_fx_impl.NtroImpl;
import ca.ntro.ntro_app_fx_impl.modified.Observable;
import ca.ntro.ntro_app_fx_impl.tasks.ModelTaskDescriptorNtro;
import ca.ntro.ntro_core_abstr.tasks.TaskAbstr;
import ca.ntro.ntro_core_impl.task_graphs.generic_task_graph.SimpleTaskOptions;
import ca.ntro.ntro_core_impl.task_graphs.task_graph.SimpleTaskNtro;
import ca.ntro.ntro_core_impl.task_graphs.task_graph.TaskContainer;
import ca.ntro.ntro_core_impl.task_graphs.task_graph_trace.TaskResultsCondition;
import ca.ntro.ntro_core_impl.task_graphs.task_graph_trace.TaskTraceNtro;

public class FrontendSnapshotTaskDescriptorNtro<O>

       extends    ModelTaskDescriptorNtro<O>

       implements FrontendSimpleTaskDescriptor<O> {
	
	
	public FrontendSnapshotTaskDescriptorNtro(Class<?> entryClass) {
		super(entryClass);
	}

	public FrontendSnapshotTaskDescriptorNtro(Class<?> entryClass, String id) {
		super(entryClass, id);
	}

	@Override
	public TaskAbstr newTask(TaskContainer parent) {
		String taskId = id();
		
		TaskAbstr snapshotTask = parent.newTask(taskId,
										   SimpleTaskOptions.taskClass(SimpleTaskNtro.class)
															.traceClass(TaskTraceNtro.class)
														    .resultsClass(TaskResultsCondition.class));

		NtroImpl.messageService().addSnapshotTask((Class<? extends Observable>) typeClass(), snapshotTask.asSimpleTask());
		
		snapshotTask.onRemovedFromGraph(() -> {

			NtroImpl.messageService().removeSnapshotTask((Class<? extends Observable>) typeClass(), snapshotTask.asSimpleTask());

		});
		
		NtroImpl.models().watch((Class<? extends Observable>) typeClass());
		
		return snapshotTask;
	}

	@Override
	protected String taskLabel() {
		return "snapshot";
	}
}
