/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.ntro_app_fx_impl.tasks;

import java.util.Map;

import ca.ntro.app.tasks.SimpleTaskCreator;
import ca.ntro.app.tasks.TaskGroupCreator;
import ca.ntro.core.NtroCore;
import ca.ntro.ntro_core_abstr.tasks.TaskAbstr;
import ca.ntro.ntro_core_abstr.tasks.TaskGroupAbstr;
import ca.ntro.ntro_core_impl.NtroCoreImpl;
import ca.ntro.ntro_core_impl.identifyers.Key;
import ca.ntro.ntro_core_impl.task_graphs.generic_task_graph.SimpleTaskOptions;
import ca.ntro.ntro_core_impl.task_graphs.generic_task_graph.TaskAlreadyExistsException;
import ca.ntro.ntro_core_impl.task_graphs.generic_task_graph.TaskGroupOptions;
import ca.ntro.ntro_core_impl.task_graphs.task_graph.ExecutableTask;
import ca.ntro.ntro_core_impl.task_graphs.task_graph.ExecutableTaskNtro;
import ca.ntro.ntro_core_impl.task_graphs.task_graph.TaskContainer;
import ca.ntro.ntro_core_impl.task_graphs.task_graph.TaskGraph;
import ca.ntro.ntro_core_impl.task_graphs.task_graph.TaskGroupNtro;
import ca.ntro.ntro_core_impl.task_graphs.task_graph_trace.TaskResultsCondition;
import ca.ntro.ntro_core_impl.task_graphs.task_graph_trace.TaskTraceNtro;

public abstract class ContainerTasksNtro<TASKS extends Tasks>


       implements     ContainerTasks<TASKS> {

	private Map<String, TaskAbstr> orphanTasks;
	private TaskGraph graph;
	private TaskContainer parent;


	public Map<String, TaskAbstr> getOrphanTasks() {
		return orphanTasks;
	}

	public void setOrphanTasks(Map<String, TaskAbstr> orphanTasks) {
		this.orphanTasks = orphanTasks;
	}

	public TaskGraph getGraph() {
		return graph;
	}

	public void setGraph(TaskGraph graph) {
		this.graph = graph;
	}
	
	public TaskContainer getParent() {
		return parent;
	}

	public void setParent(TaskContainer parent) {
		this.parent = parent;
	}
	
	
	public ContainerTasksNtro() {
	}
	
	public ContainerTasksNtro(Map<String, TaskAbstr> orphanTasks, TaskGraph graph, TaskContainer parent) {
		setOrphanTasks(orphanTasks);
		setGraph(graph);
		setParent(parent);
	}


	protected abstract <O> TaskGroupCreator<O, TASKS> newTaskGroupCreator(Map<String, TaskAbstr> orphanTasks, 
			                                                          TaskGraph graph, 
			                                                          TaskContainer parent, 
			                                                          TaskGroupAbstr group); 

	protected <O> SimpleTaskCreator<O> newSimpleTaskCreator(Map<String, TaskAbstr> orphanTasks, 
			                                                TaskGraph graph, 
			                                                TaskContainer parent, 
			                                                ExecutableTask task) {

		return new SimpleTaskCreatorNtro<>(orphanTasks, graph, parent, task);
	}

	@Override
	public SimpleTaskCreator<?> task(String taskId) {
		try {

			new Key(taskId);

		}catch(Throwable t) {
			
			NtroCoreImpl.logger().fatal("taskId must be a valid Java identifyer\n\t\t" + taskId + " is not valid");

		}
		
		return task(new SimpleTaskDescriptorNtro<>(taskId));
	}

	@Override
	public <O> SimpleTaskCreator<O> task(SimpleTaskDescriptor<O> descriptor) {
		
		TaskAbstr task = getParent().findTask(descriptor.id());
		
		if(task != null){

			System.out.println("[WARNING] task " + descriptor.id() + " already exists and will not be added again" );
			
		}else {

			task = getParent().newTask(descriptor.id(), 
									   SimpleTaskOptions.taskClass(ExecutableTaskNtro.class) 
														.traceClass(TaskTraceNtro.class)
														.resultsClass(TaskResultsCondition.class));
		}

		return newSimpleTaskCreator(getOrphanTasks(), getGraph(), getParent(), (ExecutableTask) task);
	}



	@Override
	public TaskGroupCreator<?, TASKS> taskGroup(String taskGroupId) {
		try {

			new Key(taskGroupId);

		}catch(Throwable t) {

			NtroCoreImpl.logger().fatal("taskId must be a valid Java identifyer\n\t\t" + taskGroupId + " is not valid");

		}
		
		return taskGroup(new TaskGroupDescriptorNtro(taskGroupId));
	}

	@Override
	public <O> TaskGroupCreator<O, TASKS> taskGroup(TaskGroupDescriptor<O> descriptor) {

		TaskAbstr taskGroup = getParent().findTask(descriptor.id());

		if(taskGroup != null){

			System.out.println("[WARNING] task " + descriptor.id() + " already exists and will not be added again" );
			
		}else {

			taskGroup = getParent().newGroup(descriptor.id(), TaskGroupOptions.taskGroupClass(TaskGroupNtro.class));
		}
		
		return newTaskGroupCreator(getOrphanTasks(), getGraph(), getParent(), (TaskGroupAbstr) taskGroup);
	}

}
