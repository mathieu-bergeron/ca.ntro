package ca.ntro.ntro_app_fx_impl.tasks.backend;

import ca.ntro.ntro_app_fx_impl.tasks.MessageHandlerTaskDescriptorNtro;

public class BackendMessageTaskDescriptorNtro<O>

       extends    MessageHandlerTaskDescriptorNtro<O>

       implements BackendSimpleTaskDescriptor<O> {
	
	
	public BackendMessageTaskDescriptorNtro(Class<?> entryClass) {
		super(entryClass);
	}

	public BackendMessageTaskDescriptorNtro(Class<?> entryClass, String id) {
		super(entryClass, id);
	}
}
