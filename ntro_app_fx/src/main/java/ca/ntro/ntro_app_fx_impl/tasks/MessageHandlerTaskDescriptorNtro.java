package ca.ntro.ntro_app_fx_impl.tasks;

import ca.ntro.app.Ntro;
import ca.ntro.ntro_app_fx_abstr.messages.MessageAbstr;
import ca.ntro.ntro_app_fx_impl.NtroImpl;
import ca.ntro.ntro_core_abstr.tasks.TaskAbstr;
import ca.ntro.ntro_core_impl.task_graphs.generic_task_graph.SimpleTaskOptions;
import ca.ntro.ntro_core_impl.task_graphs.task_graph.SimpleTaskNtro;
import ca.ntro.ntro_core_impl.task_graphs.task_graph.TaskContainer;
import ca.ntro.ntro_core_impl.task_graphs.task_graph_trace.TaskResultsMessageHandler;
import ca.ntro.ntro_core_impl.task_graphs.task_graph_trace.TaskTraceNtro;

public class MessageHandlerTaskDescriptorNtro<O>

       extends    TypedTaskDescriptorNtro<O> {

	
	public MessageHandlerTaskDescriptorNtro(Class<?> entryClass) {
		super(entryClass);
	}

	public MessageHandlerTaskDescriptorNtro(Class<?> entryClass, String id) {
		super(entryClass, id);
	}

	@Override
	public TaskAbstr newTask(TaskContainer parent) {
		String taskId = id();
		
		TaskAbstr messageHandler = parent.newTask(taskId, SimpleTaskOptions.taskClass(SimpleTaskNtro.class)
				                                                      .traceClass(TaskTraceNtro.class)
				                                                      .resultsClass(TaskResultsMessageHandler.class));

		NtroImpl.messageService().addMessageHandler((Class<? extends MessageAbstr>) typeClass(), messageHandler.asSimpleTask());
		
		messageHandler.onRemovedFromGraph(() -> {
			NtroImpl.messageService().removeMessageHandler((Class<? extends MessageAbstr>) typeClass(), messageHandler.asSimpleTask());
		});

		return messageHandler;

	}

	@Override
	protected String taskLabel() {
		return "message";
	}
}
