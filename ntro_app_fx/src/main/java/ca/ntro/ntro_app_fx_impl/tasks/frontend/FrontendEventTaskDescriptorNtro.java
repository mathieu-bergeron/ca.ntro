package ca.ntro.ntro_app_fx_impl.tasks.frontend;

import ca.ntro.app.Ntro;
import ca.ntro.app.models.Model;
import ca.ntro.ntro_app_fx_impl.NtroImpl;
import ca.ntro.ntro_app_fx_impl.tasks.ModelTaskDescriptorNtro;
import ca.ntro.ntro_app_fx_impl.tasks.TypedTaskDescriptorNtro;
import ca.ntro.ntro_core_abstr.tasks.TaskAbstr;
import ca.ntro.ntro_core_impl.task_graphs.generic_task_graph.SimpleTaskOptions;
import ca.ntro.ntro_core_impl.task_graphs.task_graph.SimpleTaskNtro;
import ca.ntro.ntro_core_impl.task_graphs.task_graph.TaskContainer;
import ca.ntro.ntro_core_impl.task_graphs.task_graph_trace.TaskResultsCondition;
import ca.ntro.ntro_core_impl.task_graphs.task_graph_trace.TaskResultsEventHandler;
import ca.ntro.ntro_core_impl.task_graphs.task_graph_trace.TaskResultsMessageHandler;
import ca.ntro.ntro_core_impl.task_graphs.task_graph_trace.TaskTraceNtro;

public class FrontendEventTaskDescriptorNtro<O>

       extends    TypedTaskDescriptorNtro<O>

       implements FrontendSimpleTaskDescriptor<O> {
    
    public FrontendEventTaskDescriptorNtro(Class<?> entryClass) {
        super(entryClass);
    }

    public FrontendEventTaskDescriptorNtro(Class<?> entryClass, String id) {
        super(entryClass, id);
    }
    

    @Override
    public TaskAbstr newTask(TaskContainer parent) {
        String taskId = id();

        TaskAbstr eventHandler = parent.newTask(taskId,
                                           SimpleTaskOptions.taskClass(SimpleTaskNtro.class)
                                                            .traceClass(TaskTraceNtro.class)
                                                            .resultsClass(TaskResultsEventHandler.class)); 

        
        return eventHandler;
    }

    @Override
    protected String taskLabel() {
        return "event";
    }
}
