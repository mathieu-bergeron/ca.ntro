/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.ntro_app_fx_impl.tasks;

import ca.ntro.app.Ntro;
import ca.ntro.app.tasks.TaskInputs;
import ca.ntro.core.NtroCore;
import ca.ntro.ntro_core_abstr.tasks.TaskAbstr;
import ca.ntro.ntro_core_impl.NtroCoreImpl;
import ca.ntro.ntro_core_impl.services.StackAnalyzerImpl;
import ca.ntro.ntro_core_impl.task_graphs.task_graph.TaskGraph;
import ca.ntro.ntro_core_impl.values.ObjectMap;

public class TaskInputsNtro implements TaskInputs {
	
	private TaskGraph graph;
	private TaskAbstr task;
	private ObjectMap inputs;

	public ObjectMap getInputs() {
		return inputs;
	}

	public void setInputs(ObjectMap inputs) {
		this.inputs = inputs;
	}

	public TaskGraph getGraph() {
		return graph;
	}

	public void setGraph(TaskGraph graph) {
		this.graph = graph;
	}

	public TaskAbstr getTask() {
		return task;
	}

	public void setTask(TaskAbstr task) {
		this.task = task;
	}

	public TaskInputsNtro(TaskGraph graph, TaskAbstr task, ObjectMap inputs) {
		setGraph(graph);
		setTask(task);
		setInputs(inputs);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <O> O get(TaskDescriptor<O> task) {
		return (O) get(task.id());
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object get(String id) {
		Object result = getInputs().get(id);
		
		if(result == null) {
			getGraph().write(NtroCoreImpl.graphWriter());
			StringBuilder msg = new StringBuilder();
			msg.append("inputs.get(" + id + ") not found in task " + getTask().id());
			msg.append("\n\nplease add the required .waitsFor and/or correct the call to inputs.get()");
			NtroCore.logger().fatal(msg.toString());
		}

		return result;
	}

}
