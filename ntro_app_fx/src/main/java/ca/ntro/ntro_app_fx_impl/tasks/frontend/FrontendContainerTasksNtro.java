/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.ntro_app_fx_impl.tasks.frontend;

import java.util.Map;

import ca.ntro.app.tasks.TaskGroupCreator;
import ca.ntro.app.tasks.frontend.FrontendTasks;
import ca.ntro.ntro_app_fx_impl.tasks.ContainerTasksNtro;
import ca.ntro.ntro_core_abstr.tasks.TaskAbstr;
import ca.ntro.ntro_core_abstr.tasks.TaskGroupAbstr;
import ca.ntro.ntro_core_impl.task_graphs.task_graph.TaskContainer;
import ca.ntro.ntro_core_impl.task_graphs.task_graph.TaskGraph;

public class FrontendContainerTasksNtro 

       extends ContainerTasksNtro<FrontendTasks>

       implements FrontendContainerTasks {
	
	private FrontendTasksNtro frontendTasks = new FrontendTasksNtro(this);


	public FrontendTasksNtro getFrontendTasks() {
		return frontendTasks;
	}

	public void setFrontendTasks(FrontendTasksNtro frontendTasks) {
		this.frontendTasks = frontendTasks;
	}
	
	public FrontendContainerTasksNtro() {
		super();
	}
	
	public FrontendContainerTasksNtro(Map<String, TaskAbstr> orphanTasks, 
			                          TaskGraph graph, 
			                          TaskGroupAbstr task) {

		super(orphanTasks, graph, task);
	}
	


	@SuppressWarnings("rawtypes")
	@Override
	protected <O> TaskGroupCreator<O, FrontendTasks> newTaskGroupCreator(Map<String, TaskAbstr> orphanTasks, 
			                                                             TaskGraph graph, 
			                                                             TaskContainer parent, 
			                                                             TaskGroupAbstr group) {

		return new FrontendTaskGroupCreatorNtro(orphanTasks, graph, parent, group);
	}

	@Override
	public FrontendTasks asTasks() {
		return getFrontendTasks();
	}
}
