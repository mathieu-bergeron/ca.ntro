/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.ntro_app_fx_impl.tasks.backend;

import java.util.Map;

import ca.ntro.app.tasks.SimpleTaskCreator;
import ca.ntro.app.tasks.TaskGroupCreator;
import ca.ntro.app.tasks.backend.BackendTasks;
import ca.ntro.ntro_app_fx_impl.tasks.ContainerTasksNtro;
import ca.ntro.ntro_core_abstr.tasks.TaskAbstr;
import ca.ntro.ntro_core_abstr.tasks.TaskGroupAbstr;
import ca.ntro.ntro_core_impl.task_graphs.task_graph.ExecutableTask;
import ca.ntro.ntro_core_impl.task_graphs.task_graph.TaskContainer;
import ca.ntro.ntro_core_impl.task_graphs.task_graph.TaskGraph;

public class BackendContainerTasksNtro 

       extends ContainerTasksNtro<BackendTasks>

       implements BackendContainerTasks {
	
	private BackendTasksNtro backendTasks = new BackendTasksNtro(this);

	public BackendTasksNtro getBackendTasks() {
		return backendTasks;
	}

	public void setBackendTasks(BackendTasksNtro backendTasks) {
		this.backendTasks = backendTasks;
	}

	public BackendContainerTasksNtro() {
		super();
	}
	
	public BackendContainerTasksNtro(Map<String, TaskAbstr> orphanTasks, 
			                          TaskGraph graph, 
			                          TaskGroupAbstr task) {

		super(orphanTasks, graph, task);
	}
	


	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	protected <O> TaskGroupCreator<O, BackendTasks> newTaskGroupCreator(Map<String, TaskAbstr> orphanTasks, 
			                                                             TaskGraph graph, 
			                                                             TaskContainer parent, 
			                                                             TaskGroupAbstr group) {

		return new BackendTaskGroupCreatorNtro(orphanTasks, graph, parent, group);
	}
	
	@Override
	protected <O> SimpleTaskCreator<O> newSimpleTaskCreator(Map<String, TaskAbstr> orphanTasks, 
			                                                TaskGraph graph, 
			                                                TaskContainer parent, 
			                                                ExecutableTask task) {

		return new BackendSimpleTaskCreatorNtro<>(orphanTasks, graph, parent, task);
	}

	@Override
	public BackendTasks asTasks() {
		return getBackendTasks();
	}
}
