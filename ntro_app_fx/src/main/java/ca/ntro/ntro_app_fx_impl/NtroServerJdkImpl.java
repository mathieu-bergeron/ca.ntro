package ca.ntro.ntro_app_fx_impl;

import java.util.Map;

import ca.ntro.app.headless.NtroServerJdk;
import ca.ntro.core.NtroCore;
import ca.ntro.core.options.CoreOptions;
import ca.ntro.ntro_app_fx_impl.executable_spec.ExecutableSpec;
import ca.ntro.ntro_app_fx_impl.executable_spec.ServerSpec;
import ca.ntro.ntro_core_impl.NtroCoreImpl;
import ca.ntro.ntro_core_impl.services.StackAnalyzerImpl;

public class NtroServerJdkImpl extends NtroExecutableImpl<NtroServerJdk, ServerSpec> {
	
	private static final NtroServerJdkImpl instance = new NtroServerJdkImpl();
	
	public static NtroServerJdkImpl  instance() {
		return instance;
	}
	

	@Override
	protected void launchImpl(Class<? extends NtroServerJdk> serverClass, ServerSpec projectDescription) {

    	// Jdk headless
    	ServerWrapperJdk serverWrapperJdk = new ServerWrapperJdk();

    	((StackAnalyzerImpl) NtroCoreImpl.stackAnalyzer()).registerAppWrapperClass(ServerWrapperJdk.class);

        NtroCore.logger().setErrorDialogEnabled(false);
    	
    	NtroCore.threads().runOnMainThread(() -> {
			serverWrapperJdk.start(serverClass, projectDescription);
    	});

	}

	@Override
	protected void initializeNtroMode() {
		NtroImpl.registerNtroMode(NtroMode.SERVER);
	}


	@Override
	protected void initializeWithParams(Map<String, String> params) {

	}

	// FIXME: we keep this public
	//        to use it in 3c6 w/o really initializing Ntro
	public CoreOptions options() {
		return options.coreOptions();
	}


	@Override
	protected ExecutableSpec createEmptySpec() {
		return new ServerSpec();
	}

}
