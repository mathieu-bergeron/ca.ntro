/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.ntro_app_fx_impl.models;

import java.util.HashSet;
import java.util.Set;

import ca.ntro.app.models.Model;
import ca.ntro.app.models.ModelOrValue;
import ca.ntro.app.models.ModelRegistrar;
import ca.ntro.core.NtroCore;

public class ModelRegistrarImpl implements ModelRegistrar {
	
	private Set<Class<? extends Model>> modelClasses = new HashSet<>();
	private Set<Class<? extends ModelOrValue>> valueClasses = new HashSet<>();

	public Set<Class<? extends Model>> getModelClasses() {
		return modelClasses;
	}

	public void setModelClasses(Set<Class<? extends Model>> modelClasses) {
		this.modelClasses = modelClasses;
	}

	public Set<Class<? extends ModelOrValue>> getValueClasses() {
		return valueClasses;
	}

	public void setValueClasses(Set<Class<? extends ModelOrValue>> valueClasses) {
		this.valueClasses = valueClasses;
	}
	

	@Override
	public <M extends Model> void registerModel(Class<M> modelClass) {
		getModelClasses().add(modelClass);
		NtroCore.factory().registerNamedClass(modelClass);
	}

	@Override
	public <V extends ModelOrValue> void registerValue(Class<V> valueClass) {
		getValueClasses().add(valueClass);
		NtroCore.factory().registerNamedClass(valueClass);
	}

}
