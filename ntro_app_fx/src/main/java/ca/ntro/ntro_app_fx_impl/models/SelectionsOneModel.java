package ca.ntro.ntro_app_fx_impl.models;

import java.util.ArrayList;
import java.util.List;

import ca.ntro.core.stream.Stream;

public class SelectionsOneModel {
	
	private List<String> selections = new ArrayList<>();

	public void setSelection(String modelId) {
		selections.clear();
		selections.add(modelId);
	}

	public void addSelection(String modelId) {
		if(!selections.contains(modelId)) {
			selections.add(modelId);
		}
	}

	public void clearSelections() {
		selections.clear();
	}

	public Stream<String> selections() {
		return Stream.forList(selections);
	}

	public void restricTo(SelectionsOneModel theirSelections) {
		selections.retainAll(theirSelections.selections);
	}

	public boolean isModelSelected(String modelId) {
		return selections.contains(modelId);
	}

}
