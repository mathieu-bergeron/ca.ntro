package ca.ntro.ntro_app_fx_impl.models;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ca.ntro.app.models.Model;
import ca.ntro.core.stream.Stream;
import ca.ntro.ntro_app_fx_abstr.models.ModelSelectionsReader;
import ca.ntro.ntro_app_fx_abstr.models.ModelSelectionsStreamer;
import ca.ntro.ntro_app_fx_abstr.models.ModelSelectionsWriter;
import ca.ntro.ntro_core_impl.NtroCoreImpl;

public class SelectionsByModel implements ModelSelectionsWriter<SelectionsByModel>, ModelSelectionsReader, ModelSelectionsStreamer {
	
	private Map<String, SelectionsOneModel> selections = new HashMap<>();

	@Override
	public SelectionsByModel setModelSelection(Class<? extends Model> modelClass, String modelId) {

		SelectionsOneModel modelSelections = getOrAddModelSelections(modelClass);

		modelSelections.setSelection(modelId);
		
		return this;
	}

	private SelectionsOneModel getOrAddModelSelections(Class<? extends Model> modelClass) {
		String modelClassName = NtroCoreImpl.reflection().simpleName(modelClass);

		SelectionsOneModel modelSelections = selections.get(modelClassName);
		
		if(modelSelections == null) {
			modelSelections = new SelectionsOneModel();
			selections.put(modelClassName, modelSelections);
		}

		return modelSelections;
	}

	@Override
	public SelectionsByModel addModelSelection(Class<? extends Model> modelClass, String modelId) {
		SelectionsOneModel modelSelections = getOrAddModelSelections(modelClass);

		modelSelections.addSelection(modelId);
		
		return this;
	}

	@Override
	public SelectionsByModel clearModelSelections(Class<? extends Model> modelClass) {
		SelectionsOneModel modelSelections = getOrAddModelSelections(modelClass);

		modelSelections.clearSelections();
		
		return this;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Stream<Class<? extends Model>> modelClasses() {

		Stream<String> modelClassNames = Stream.forMapKeys(selections);

		Stream<Class<? extends Model>> modelClasses = modelClassNames.map(className -> (Class<? extends Model>) NtroCoreImpl.factory().namedClass(className));
		
		return modelClasses;
	}

	@Override
	public Stream<String> selections(Class<? extends Model> modelClass) {
		String modelClassName = NtroCoreImpl.reflection().simpleName(modelClass);

		SelectionsOneModel modelSelections = selections.get(modelClassName);
		
		if(modelSelections == null) {
			modelSelections = new SelectionsOneModel();
		}
		
		return modelSelections.selections();
	}

	public void restricTo(SelectionsByModel otherSelections) {
		Set<String> ourClasses = selections.keySet();
		Set<String> theirClasses = otherSelections.selections.keySet();
		
		ourClasses.retainAll(theirClasses);

		Map<String, SelectionsOneModel> newSelections = new HashMap<>();
		
		for(String className : ourClasses) {
			
			SelectionsOneModel ourSelections = selections.get(className);
			SelectionsOneModel theirSelections = otherSelections.selections.get(className);
			
			ourSelections.restricTo(theirSelections);
			
			newSelections.put(className, ourSelections);
		}
		
		
		selections = newSelections;
	}

	public boolean isModelSelected(Class<? extends Model> modelClass, String modelId) {
		boolean isSelected = false;

		String modelClassName = NtroCoreImpl.reflection().simpleName(modelClass);

		SelectionsOneModel modelSelections = selections.get(modelClassName);
		
		if(modelSelections != null) {
			
			isSelected = modelSelections.isModelSelected(modelId);
			
		}

		return isSelected;
	}


	@Override
	public boolean hasModelSelections(Class<? extends Model> modelClass) {
		String modelClassName = NtroCoreImpl.reflection().simpleName(modelClass);
		
		return selections.containsKey(modelClassName);
	}

	@Override
	public String getModelSelection(Class<? extends Model> modelClass) {
		return selections(modelClass).findFirst(x -> true);
	}

	@Override
	public List<String> getModelSelections(Class<? extends Model> modelClass) {
		return selections(modelClass).collect();
	}
	
}
