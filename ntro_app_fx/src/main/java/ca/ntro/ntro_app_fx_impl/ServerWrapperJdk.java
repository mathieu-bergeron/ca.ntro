package ca.ntro.ntro_app_fx_impl;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

import ca.ntro.app.Ntro;
import ca.ntro.app.common.ServerRegistrarJdkImpl;
import ca.ntro.app.common.WebSocketServerNtro;
import ca.ntro.app.headless.NtroServerJdk;
import ca.ntro.core.NtroCore;
import ca.ntro.core.services.ExitHandler;
import ca.ntro.ntro_app_fx_impl.backend.BackendRegistrarNtro;
import ca.ntro.ntro_app_fx_impl.executable_spec.ServerSpec;
import ca.ntro.ntro_app_fx_impl.messages.MessageRegistrarNtro;
import ca.ntro.ntro_app_fx_impl.models.ModelRegistrarImpl;
import ca.ntro.ntro_core_impl.NtroCoreImpl;

public class ServerWrapperJdk implements ExitHandler {
    
	private ServerSpec         spec;
    private WebSocketServerNtro webSocketServer;

    private MessageRegistrarNtro   messageRegistrar  = new MessageRegistrarNtro();
    private ModelRegistrarImpl     modelRegistrar    = new ModelRegistrarImpl();
    private BackendRegistrarNtro   backendRegistrar  = new BackendRegistrarNtro(modelRegistrar, messageRegistrar);
    private ServerRegistrarJdkImpl serverRegistrar   = new ServerRegistrarJdkImpl();

    public void start(Class<? extends NtroServerJdk> serverClass, ServerSpec spec) {
		NtroCoreImpl.registerAppExitHandler(this);
		
		this.spec = spec;

        new Thread() {
            
            @Override
            public void run() {

                NtroCore.logger().info("App running. Press Enter here to close App");

                try {

                    System.in.read();
                    NtroCore.exit();

                } catch (IOException e) {

                    e.printStackTrace();
                }
            }
        }.start();

        Runtime.getRuntime().addShutdownHook(new Thread() {
        	@Override
        	public void run() {
        		NtroCore.exit();
			}
        });

        NtroCore.logger().run();

		System.out.println("\n\n\n");
		NtroCore.logger().info("Ntro version 0.2");
		NtroCore.logger().info("java version " + System.getProperty("java.version"));

		File javaRootDir      = Paths.get(NtroCore.options().javaPath()).toFile();
		File resourcesRootDir = Paths.get(NtroCore.options().resourcesPath()).toFile();

		spec.analyzeSources(resourcesRootDir, javaRootDir);

		spec.registerAppClass(serverClass);
		
		spec.callExecutableCodeAndFillSpec();

		NtroImpl.loadOptionsFile();

		NtroCore.logger().info("locale: '" + Ntro.currentLocale() + "'");
		
		spec.writeSpec();

		spec.createExecutableFromSpec();
		
		NtroCore.threads().runOnMainThread(() -> {
			spec.runExecutable();
		});
		
		spec.startWebSocketServer();
    }

    @Override
    public void onExit() {
        NtroCore.logger().info("Writing JSON files");
        NtroImpl.models().writeModelFiles();

        NtroCore.logger().info("Generating graphs");
        NtroImpl.models().writeGraphs();

        if(!spec.isRemoteBackend()) {
			spec.writeBackendGraph();
        }

		spec.stopWebSocketServer();
    }

}
