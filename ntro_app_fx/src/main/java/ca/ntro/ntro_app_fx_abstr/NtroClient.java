package ca.ntro.ntro_app_fx_abstr;

import ca.ntro.ntro_app_fx_impl.frontend.FrontendRegistrar;

public interface NtroClient<FR extends FrontendRegistrar<?>> extends NtroExecutable {

	void registerFrontend(FR registrar);

}
