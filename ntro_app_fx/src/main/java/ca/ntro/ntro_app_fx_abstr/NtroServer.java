package ca.ntro.ntro_app_fx_abstr;

import ca.ntro.app.common.ServerRegistrar;

public interface NtroServer extends NtroExecutable {

	void registerServer(ServerRegistrar registrar);

}
