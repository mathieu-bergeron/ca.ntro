package ca.ntro.ntro_app_fx_abstr;

import ca.ntro.app.backend.BackendRegistrar;
import ca.ntro.app.messages.MessageRegistrar;
import ca.ntro.app.models.ModelRegistrar;

public interface NtroExecutable {

	void registerModels(ModelRegistrar registrar);
	void registerMessages(MessageRegistrar registrar);

	void registerBackend(BackendRegistrar registrar);


}
