package ca.ntro.ntro_app_fx_abstr.session;


import ca.ntro.core.locale.Locale;
import ca.ntro.ntro_app_fx_abstr.models.ModelSelectionsReader;
import ca.ntro.ntro_app_fx_abstr.models.ModelSelectionsWriter;

public interface SessionAbstr<THIS extends SessionAbstr<?>> extends ModelSelectionsWriter<THIS>, ModelSelectionsReader {
	
	String sessionId();
	
	Locale locale();
	




}
