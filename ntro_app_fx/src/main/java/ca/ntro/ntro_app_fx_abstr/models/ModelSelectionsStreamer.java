package ca.ntro.ntro_app_fx_abstr.models;

import ca.ntro.app.models.Model;
import ca.ntro.core.stream.Stream;

public interface ModelSelectionsStreamer {


	Stream<Class<? extends Model>> modelClasses();
	Stream<String> selections(Class<? extends Model> modelClass);


}
