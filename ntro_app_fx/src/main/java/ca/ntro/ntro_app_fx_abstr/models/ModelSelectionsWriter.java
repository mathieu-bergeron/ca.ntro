package ca.ntro.ntro_app_fx_abstr.models;

import ca.ntro.app.models.Model;

public interface ModelSelectionsWriter<THIS extends ModelSelectionsWriter<?>> {

	THIS setModelSelection(Class<? extends Model> modelClass, String modelId);
	THIS addModelSelection(Class<? extends Model> modelClass, String modelId);
    THIS clearModelSelections(Class<? extends Model> modelClass);

}
