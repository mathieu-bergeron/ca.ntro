package ca.ntro.ntro_app_fx_abstr.models;

import java.util.List;

import ca.ntro.app.models.Model;

public interface ModelSelectionsReader {

	boolean isModelSelected(Class<? extends Model> modelClass, String modelId);
	boolean hasModelSelections(Class<? extends Model> modelClass);

	String getModelSelection(Class<? extends Model> modelClass);
	List<String> getModelSelections(Class<? extends Model> modelClass);





}
