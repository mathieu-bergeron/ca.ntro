# TODO

## Prochaines «grosses» améliorations

1. Ajout de la session
    * à la fois un genre de «modèle» spécifique au Frontend
      et l'identifiant de l'instance client en mode client/serveur

1. Affichage des messages FATAL et vérifier l'utilisation de NtroCore.logger().fatal() plutôt que throwException ou autre

1. Javadoc minimale + ménage dans l'API



## Tutoriels

1. FAIT) dans tut04 (navigation), ajouter des raccourcis clavier pour illustrer l'importance des EvtAfficherPartie

## Matérie de cours

1. Réviser les tutoriels, etc.

1. Ajuster le matériel pour parler de la session

1. Ajuster le matériel de cours pour le IdMatcher (si on l'implante)

## TODO

1. Ntro via des .jar
1. Modules 1-7: màj des tutoriels
    * retirer ce qui n'est pas utilisé dans les tutoriels (p.ex. snapshot)
1. Modules 1-7: Javadoc
1. Module 3: 
    * plus de vérifications dynamiques et de messages d'erreur sur le genre d'erreur que font les étudiants
    * watchFile sur le `.css`
1. Module 8: 
    * Javadoc
    * s'assurer que `clock()` fonctionne avec plusieurs instances
1. Module 9: client/serveur: 
    * ajouter une notion de id de connexion
    * comme ça on peut envoyer un message à un client en particulier
1. Module 10:
    * redéfinir/remplacer une tâche quand le targetId change (via le IdMatcher??)
        * i.e. on retire Task.removeFromGraph, mais on implante un retrait
               automatique quand le Id.valueOf() a changé
               suivi d'une redéfinition automatique avec la nouvelle valeur de valueOf()

    * utiliser de `IdMatcher` pour ne pas avoir à mettre le Id partout

## Plus tard

1. Passer en mode librairie??:
    * pas besoin d'implanter NtroAppFx
    * NtroAppFx.registerFrontend() plutôt qu'une méthode obligatoire registerFrontend
