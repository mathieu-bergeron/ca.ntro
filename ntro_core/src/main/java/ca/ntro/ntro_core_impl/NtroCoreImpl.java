package ca.ntro.ntro_core_impl;

import ca.ntro.core.NtroCore;
import ca.ntro.core.options.CoreOptions;
import ca.ntro.core.services.Collections;
import ca.ntro.core.services.LocaleService;
import ca.ntro.core.services.Asserter;
import ca.ntro.core.services.RandomService;
import ca.ntro.core.services.LocaleServiceJdk;
import ca.ntro.core.services.ExitHandler;
import ca.ntro.core.services.ExitService;
import ca.ntro.core.services.FactoryService;
import ca.ntro.core.services.JsonService;
import ca.ntro.core.services.Logger;
import ca.ntro.core.services.ReflectionService;
import ca.ntro.core.services.StackAnalyzer;
import ca.ntro.core.services.StorageService;
import ca.ntro.core.services.ThreadService;
import ca.ntro.core.services.Time;
import ca.ntro.ntro_core_impl.graph_writer.GraphWriter;
import ca.ntro.ntro_core_impl.options.CoreOptionsImpl;
import ca.ntro.ntro_core_impl.services.AsserterJdk;
import ca.ntro.ntro_core_impl.services.CollectionsJdk;
import ca.ntro.ntro_core_impl.services.ExitServiceImpl;
import ca.ntro.ntro_core_impl.services.ExitServiceJdk;
import ca.ntro.ntro_core_impl.services.FactoryServiceDefault;
import ca.ntro.ntro_core_impl.services.GraphWriterJdk;
import ca.ntro.ntro_core_impl.services.JsonServiceJdk;
import ca.ntro.ntro_core_impl.services.LoggerTasks;
import ca.ntro.ntro_core_impl.services.RandomServiceJdk;
import ca.ntro.ntro_core_impl.services.ReflectionServiceJdk;
import ca.ntro.ntro_core_impl.services.StackAnalyzerJdk;
import ca.ntro.ntro_core_impl.services.StackAnalyzerImpl;
import ca.ntro.ntro_core_impl.services.StorageServiceJdk;
import ca.ntro.ntro_core_impl.services.ThreadServiceJdk;
import ca.ntro.ntro_core_impl.services.TimeJdk;
import ca.ntro.ntro_core_impl.task_graphs.task_graph_trace.TaskGraphTrace;

public class NtroCoreImpl {

	/* <Factory> */
	
	private static FactoryService factoryService = new FactoryServiceDefault();
	
	public static void registerFactoryService(FactoryService factoryService){
		NtroCoreImpl.factoryService = factoryService;
	}

	public static FactoryService factory(){
		return NtroCoreImpl.factoryService;
	}

	/* </Factory> */
	

	/* <Collections> */
	
	private static Collections collections = new CollectionsJdk();

	public static void registerCollections(Collections collections){
		NtroCoreImpl.collections = collections;
	}

	public static Collections collections(){
		return NtroCoreImpl.collections;
	}

	/* </Collections> */
	
	
	/* <JsonService> */
	
	private static JsonService jsonService = new JsonServiceJdk();

	public static void registerJsonService(JsonService jsonService){
		NtroCoreImpl.jsonService = jsonService;
	}

	public static JsonService json(){
		return NtroCoreImpl.jsonService;
	}

	/* </JsonService> */


	/* <Storage> */
	
	private static StorageService storageService = new StorageServiceJdk();

	public static void registerStorageService(StorageService storageService){
		NtroCoreImpl.storageService = storageService;
	}

	public static StorageService storage(){
		return NtroCoreImpl.storageService;
	}

	/* </Storage> */


	/* <Threads> */
	
	private static ThreadService threadService = new ThreadServiceJdk();

	public static void registerThreadService(ThreadService threadService){
		NtroCoreImpl.threadService = threadService;
	}

	public static ThreadService threads(){
		return NtroCoreImpl.threadService;
	}

	/* </Threads> */


	/* <Logger> */
	
	private static Logger logger = new LoggerTasks();
	
	public static void registerLogger(Logger logger){
		NtroCoreImpl.logger = logger;
	}

	public static Logger logger(){
		return NtroCoreImpl.logger;
	}

	/* </Logger> */


	/* <ExitService> */
	
	private static ExitHandler appExitHandler = null;

	private static ExitService exitService = new ExitServiceJdk();
	
	public static void registerAppExitHandler(ExitHandler appExitHandler) {
		NtroCoreImpl.appExitHandler = appExitHandler;
		if(exitService != null) {
			((ExitServiceImpl) exitService).registerAppExitHandler(NtroCoreImpl.appExitHandler);
		}
	}

	public static void registerExitService(ExitService exitService){
		NtroCoreImpl.exitService = exitService;
		if(appExitHandler != null) {
			((ExitServiceImpl) NtroCoreImpl.exitService).registerAppExitHandler(appExitHandler);
		}
	}

	public static ExitService exitService() {
		return exitService;
		
	}

	/* </ExitService> */
	
	
	/* <Time> */
	
	private static Time time = new TimeJdk();

	public static void registerTime(Time time){
		NtroCoreImpl.time = time;
	}

	public static Time time(){
		return NtroCoreImpl.time;
	}

	/* </Time> */


	

	/* <ReflectionService> */
	
	private static ReflectionService reflectionService = new ReflectionServiceJdk();
	
	public static void registerReflectionService(ReflectionService reflectionService){
		NtroCoreImpl.reflectionService = reflectionService;
	}

	public static ReflectionService reflection(){
		return NtroCoreImpl.reflectionService;
	}

	/* </ReflectionService> */
	
	
	
	
	
	/* <StackAnalyzer> */
	
	public static StackAnalyzerImpl stackAnalyzer = new StackAnalyzerJdk();
	
	public static void registerStackAnalyzer(StackAnalyzerImpl stackAnalyzer){
		NtroCoreImpl.stackAnalyzer = stackAnalyzer;
	}

	public static StackAnalyzer stackAnalyzer(){
		return NtroCoreImpl.stackAnalyzer;
	}

	/* </StackAnalyzer> */
	
	
	

	/* <GraphWriter> */
	
	private static Class<? extends GraphWriter> graphWriterClass = GraphWriterJdk.class;

	public static void registerGraphWriter(Class<? extends GraphWriter> graphWriterClass){
		NtroCoreImpl.graphWriterClass = graphWriterClass;
	}

	public static GraphWriter graphWriter(){
		return NtroCore.factory().newInstance(graphWriterClass);
	}

	/* </GraphWriter> */
	
	
	/* <CoreOptions> */
	
	private static CoreOptionsImpl coreOptions;
	
	public static void registerOptions(CoreOptionsImpl coreOptions) {
		NtroCoreImpl.coreOptions = coreOptions;
	}

	public static CoreOptions options(){
		return NtroCoreImpl.coreOptions;
	}

	/* </CoreOptions> */

	
	/* <Locale> */

	private static LocaleService localeService = new LocaleServiceJdk();

	public static void registerLocaleService(LocaleService localeService){
		NtroCoreImpl.localeService = localeService;
	}

	public static LocaleService locale() {
		return NtroCoreImpl.localeService;
		
	}
	
	/* </Locale> */
	
	
	
	
	/* <Asserter> */
	
	private static Asserter asserter = new AsserterJdk();
	
	static void registerAsserter(Asserter asserter){
		NtroCoreImpl.asserter = asserter;
	}

	public static Asserter asserter(){
		return NtroCoreImpl.asserter;
	}
	

	/* </Asserter> */
	
	
	
	

	/* <Random> */
	
	private static RandomService random = new RandomServiceJdk();

	static void registerRandom(RandomService random){
		NtroCoreImpl.random = random;
	}

	public static RandomService random(){
		return NtroCoreImpl.random;
	}


	/* </Random> */


	/* <TaskGraphTrace> */
	
	private static TaskGraphTrace frontendTaskGraphTrace;

	public static void registerFrontendTaskGraphTrace(TaskGraphTrace frontendTaskGraphTrace) {
		NtroCoreImpl.frontendTaskGraphTrace = frontendTaskGraphTrace;
	}
	
	public static void stopFrontendTaskGraph() {
		if(frontendTaskGraphTrace != null) {
			frontendTaskGraphTrace.stop();
		}
	}

	/* </TaskGraphTrace> */

}
