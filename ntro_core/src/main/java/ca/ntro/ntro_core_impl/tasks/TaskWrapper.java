package ca.ntro.ntro_core_impl.tasks;

import ca.ntro.core.tasks.Task;
import ca.ntro.ntro_core_abstr.tasks.TaskAbstr;

public class TaskWrapper implements Task {
	
	private TaskAbstr task;
	
	public TaskWrapper(TaskAbstr task) {
		this.task = task;
	}

	@Override
	public void cancel() {
		task.cancel();
	}

	public TaskAbstr getTaskImpl() {
		return task;
	}

}
