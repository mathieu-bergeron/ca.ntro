package ca.ntro.ntro_core_impl.values;

public class CodeLocationImpl implements CodeLocation {
	
	private boolean isAppFile = false;
	private boolean isNtroFile = false;
	
	private Class<?> _class;
	private String filename;
	private int lineNumber;

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public int getLineNumber() {
		return lineNumber;
	}

	public void setLineNumber(int lineNumber) {
		this.lineNumber = lineNumber;
	}
	
	public CodeLocationImpl() {
	}

	public CodeLocationImpl(Class<?> _class, 
			                String filename, 
			                int lineNumber) {
		
		this._class = _class;
		
		setFilename(filename);
		setLineNumber(lineNumber);
	}

	@Override
	public String filename() {
		return getFilename();
	}

	@Override
	public int lineNumber() {
		return getLineNumber();
	}

	@Override
	public boolean isAppFile() {
		return isAppFile;
	}

	@Override
	public boolean isNtroFile() {
		return isNtroFile;
	}

	public CodeLocationImpl isAppFile(boolean isAppFile) {
		this.isAppFile = isAppFile;
		return this;
	}

	public CodeLocationImpl isNtroFile(boolean isNtroFile) {
		this.isNtroFile = isNtroFile;
		return this;
	}

	@Override
	public Class<?> _class() {
		return _class;
	}

}
