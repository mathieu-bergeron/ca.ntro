package ca.ntro.ntro_core_impl.values;

public interface CodeLocation {
	
	boolean isAppFile();
	boolean isNtroFile();

	Class<?> _class();
	String filename();
	int lineNumber();

}
