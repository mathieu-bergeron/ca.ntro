package ca.ntro.ntro_core_impl.values;

import java.util.Set;

import ca.ntro.core.stream.Stream;

import java.util.Map;
import java.util.HashMap;
import java.util.HashSet;

public class MapSet<K,V> {
	
	private Map<K,Set<V>> mapSet = new HashMap<>();

	public void add(K key, V value) {
		Set<V> values = mapSet.get(key);
		
		if(values == null) {
			values = new HashSet<>();
			mapSet.put(key, values);
		}
		
		values.add(value);

	}
	
	public Stream<V> stream(K key){
		Set<V> values = mapSet.get(key);
		if(values == null) {
			values = new HashSet<>();
		}
		
		return Stream.forSet(values);
	}
	

}
