/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.ntro_core_impl.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import ca.ntro.core.NtroCore;
import ca.ntro.core.services.LastModifiedLambda;
import ca.ntro.core.stream.Stream;
import ca.ntro.ntro_core_impl.NtroCoreImpl;
import ca.ntro.ntro_core_impl.storage.FileWatcher;
import ca.ntro.ntro_core_impl.values.MapSet;


public class   StorageServiceJdk 

       extends StorageServiceNtro {

	private Set<String> watchedDirectories = new HashSet<>();
	private MapSet<String, FileWatcher> contentWatchersByFilePath = new MapSet<>();
	private MapSet<String, Runnable> fileWatchersByFilePath = new MapSet<>();
	private Map<String, Long> lastModifiedByFilePath = new HashMap<>();
	
	public StorageServiceJdk() {

	}

	private void startEventPollingThread(File parentDirectory, WatchService watchService) {

		new Thread() {

			@Override
			public void run() {

				boolean poll = true;
					
				while(poll) {
					
					WatchKey key = null;

					try {

						key = watchService.take();

					} catch (InterruptedException e) {

						NtroCore.logger().fatal(e);
					}

					for(WatchEvent<?> event : key.pollEvents()) {

						if(event.kind() == StandardWatchEventKinds.ENTRY_MODIFY) {
							
							Path watchedFilename = (Path) event.context();
							
							Path watchedPath = Paths.get(parentDirectory.getAbsolutePath(), watchedFilename.toString());
							
							File watchedFile = watchedPath.toFile();
							
							long lastModified = watchedFile.lastModified();

							String filePath = watchedPath.toAbsolutePath().toString();

							Long previousLastModified = lastModifiedByFilePath.get(filePath);
							lastModifiedByFilePath.put(filePath, lastModified);
							
							if(previousLastModified == null || previousLastModified < lastModified) {

								Stream<FileWatcher> contentWatchers = contentWatchersByFilePath.stream(filePath);
								
								if(contentWatchers.size() > 0) {
									String fileContent = readTextFile(filePath);
									
									contentWatchers.forEach(contentWatcher -> {
										contentWatcher.fileChanged(lastModified, fileContent);
									});
								}

								Stream<Runnable> fileWatchers = fileWatchersByFilePath.stream(filePath);
								fileWatchers.forEach(runnable -> {
									runnable.run();
								});
							}
						}
					}

					poll = key.reset();

				}
			}

		}.start();
	}

	private void addWatchService(File parentDirectory) {
		if(!watchedDirectories.contains(parentDirectory.getAbsolutePath())) {
			watchedDirectories.add(parentDirectory.getAbsolutePath());

			WatchService watchService;
			try {

				watchService = FileSystems.getDefault().newWatchService();

				parentDirectory.toPath().register(watchService, StandardWatchEventKinds.ENTRY_MODIFY);

				startEventPollingThread(parentDirectory, watchService);

			} catch (IOException e) {
				
				NtroCoreImpl.logger().fatal(e);

			}

			
		}
	}

	@Override
	public String readTextFile(String filePath) {
		return readTextFile(toFile(filePath));
	}

	public String readTextFile(File file) {
		StringBuilder builder = new StringBuilder();
		
		try {

			Scanner scanner = new Scanner(new FileInputStream(file));
			while(scanner.hasNextLine()) {

				builder.append(scanner.nextLine());
				builder.append(System.lineSeparator());
			}
			
			scanner.close();

		} catch (FileNotFoundException e) {
			
			NtroCore.logger().fatal(e);
		}
		
		
		return builder.toString();
	}

	@Override
	public void writeTextFile(String filePath, String content, LastModifiedLambda lambda) {
		new Thread() {
			@Override
			public void run() {
				long lastModified = writeTextFileBlocking(filePath, content);
				lambda.onLastModified(lastModified);
			}
		}.start();
	}
	
	@Override
	public long writeTextFileBlocking(String filePath, String content) {
		long lastModified = -1;

		File file = toFile(filePath);
		

		createParentDirectories(file);

		try {

			FileOutputStream out = new FileOutputStream(file);
			
			out.write(content.getBytes());
			out.close();
			
			lastModified = file.lastModified();

		} catch (IOException e) {

			NtroCore.logger().fatal(e);
		}
		
		return lastModified;
	}
	
	
	

	private void createParentDirectories(File file) {
		if(file.getParentFile() != null) {
			file.getParentFile().mkdirs();
		}
	}
	
	

	@Override
	public boolean ifFileExists(String filePath) {
		return toFile(filePath).exists();
	}
	
	private File toFile(String filePath) {
		return Paths.get(filePath).toFile();
	}

	@Override
	public void watchFileContent(String filePath, FileWatcher watcher) {
		contentWatchersByFilePath.add(filePath, watcher);

		File parentDirectory = toFile(filePath).getParentFile();
		if(parentDirectory != null) {
			
			if(parentDirectory.exists()) {

				addWatchService(parentDirectory);

			}else {

				NtroCore.logger().fatal("Cannot watch file, file not found: " + filePath);
				
			}
		}
	}

	protected void callWatcher(FileWatcher watcher, long lastModified, String fileContent) {
		// FIXME: should use a Thread service
		watcher.fileChanged(lastModified, fileContent);
	}

	@Override
	public void deleteTextFile(String filePath) {
		toFile(filePath).delete();
	}	

	@Override
	public void watchFile(String filePath, Runnable onFileChanged) {
		fileWatchersByFilePath.add(filePath, onFileChanged);

		java.nio.file.Path watchedPath = Paths.get(filePath);
		File watchedFile = watchedPath.toFile();
		File parentDirectory = watchedFile.getParentFile();
		

		if(parentDirectory != null) {
			
			if(!parentDirectory.exists()) {
				parentDirectory.mkdirs();
			}
			
			addWatchService(parentDirectory);
		}
	}

}
