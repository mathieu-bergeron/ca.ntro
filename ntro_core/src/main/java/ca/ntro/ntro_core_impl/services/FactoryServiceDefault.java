/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.ntro_core_impl.services;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import ca.ntro.core.NtroCore;
import ca.ntro.core.services.FactoryService;
import ca.ntro.ntro_core_impl.NtroCoreImpl;
import sun.misc.Unsafe;

public class FactoryServiceDefault implements FactoryService {
    
    private Map<String, Class<?>> namedClasses = new HashMap<>();
    
    private static Unsafe unsafe;
    static {
    	Field f;
		try {

			f = Unsafe.class.getDeclaredField("theUnsafe");
			f.setAccessible(true);
			unsafe = (Unsafe) f.get(null);

		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) { }
    }

    @SuppressWarnings("unchecked")
	@Override
    public <O> O newInstance(Class<O> _class) {
        O result = null;

        try {
        	
            result = (O) _class.getDeclaredConstructor().newInstance();

        } catch (NoSuchMethodException e) {
        	
        	if(unsafe != null) {
        		try {

					result = (O) unsafe.allocateInstance(_class);

				} catch (InstantiationException e1) {

					NtroCore.logger().fatal("cannot instantiate " + NtroCoreImpl.reflection().simpleName(_class) + ". Please add a public constructor with no parameters.", e);
				}
        	}

        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
                | InvocationTargetException | SecurityException e) {
        	
        	NtroCore.logger().fatal("cannot instantiate " + NtroCoreImpl.reflection().simpleName(_class) + ". Please add a public constructor with no parameters.", e);

        }

        return result;
    }

    @Override
    public Object newInstance(String className) {
        Object result = null;
        
        Class<?> _class = namedClass(className);
        
        if(_class != null) {
            result = newInstance(_class);
        }

        return result;
    }

    @Override
    public Class<?> namedClass(String className) {
        Class<?> namedClass = namedClasses.get(className);

        if(namedClass == null) {
            NtroCoreImpl.logger().fatal("class not found '" + className + "'. Please register every user-defined class (View, Model, Session, Value, Message and Event)");
        }
        
        return namedClass;
    }

    @Override
    public void registerNamedClass(String className, Class<?> _class) {
        namedClasses.put(className, _class);
    }

    @Override
    public void registerNamedClass(Class<?> _class) {
        registerNamedClass(NtroCoreImpl.reflection().simpleName(_class), _class);
    }

}
