/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.ntro_core_impl.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ca.ntro.core.NtroCore;

public class LoggerTasks extends LoggerImpl {
	
	private List<String> tasks = new ArrayList<>();
	private Map<Integer, Integer> repeats = new HashMap<>();

	public LoggerTasks() {
		super();
	}
	
	@Override
	public synchronized void flushTasks() {
		for(int i = 0; i < tasks.size(); i++) {
			String message = "[TASK] " + tasks.get(i);
			Integer repeatCount = repeats.get(i);
			if(repeatCount != null) {
				int totalCount = repeatCount + 1;
				message += " (" + (totalCount) + ")";
			}

			if(shouldDisplayTasks()) {
				System.out.println(message);
			}
		}
		
		tasks.clear();
		repeats.clear();
	}

	protected boolean shouldDisplayTasks() {
		return NtroCore.options().displayTasks();
	}
	
	@Override
	public synchronized void taskExecutes(String taskId) {
		int repeatsIndex = tasks.size() - 1;

		if(!tasks.isEmpty()
				&& tasks.get(repeatsIndex).equals(taskId)) {
			
			Integer repeatCount = repeats.get(repeatsIndex);

			if(repeatCount == null) {

				repeatCount = 1;

			}else {

				repeatCount++;
			}

			repeats.put(repeatsIndex, repeatCount);
			

		}else {
			
			tasks.add(taskId);

		}
	}

	@Override
	public void run() {
		NtroCore.time().runRepeatedly(1000, () -> {
			flushTasks();
		});
	}
}
