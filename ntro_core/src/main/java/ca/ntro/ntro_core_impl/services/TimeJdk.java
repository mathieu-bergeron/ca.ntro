/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.ntro_core_impl.services;

import java.util.Timer;
import java.util.TimerTask;

import ca.ntro.core.NtroCore;
import ca.ntro.core.services.Time;
import ca.ntro.ntro_core_impl.NtroCoreImpl;
import ca.ntro.ntro_core_impl.task_graphs.task_graph.SimpleTask;
import ca.ntro.ntro_core_impl.task_graphs.task_graph.TaskContainer;

public class TimeJdk implements Time {

	@Override
	public void sleep(long milliseconds) {
		try {

			Thread.sleep(milliseconds);

		} catch (InterruptedException e) {

			NtroCoreImpl.logger().fatal(e);
		}
	}

	@Override
	public long nowMilliseconds() {
		return System.currentTimeMillis();
	}

	@Override
	public long nowNanoseconds() {
		return System.nanoTime();
	}

	@Override
	public void runAfterDelay(long milliseconds, Runnable runnable) {
		new Timer().schedule(new TimerTask() {
			@Override
			public void run() {
				NtroCore.threads().runOnMainThread(runnable);
			}

		}, milliseconds);
	}

	@Override
	public void runRepeatedly(long milliseconds, Runnable runnable) {
		new Timer().scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				NtroCore.threads().runOnMainThread(runnable);
			}
		}, 0, milliseconds);
	}


	@Override
	public SimpleTask newTickHandlerTask(TaskContainer graph) {
		throw new RuntimeException("[FATAL] not implemented");
	}

}
