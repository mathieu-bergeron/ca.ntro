package ca.ntro.ntro_core_impl.services;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import ca.ntro.core.services.ThreadService;

public class ThreadServiceJdk implements ThreadService {
	
	private BlockingQueue<Runnable> mainThreadQueue = new LinkedBlockingQueue<>();
	
	public ThreadServiceJdk() {
		new Thread() {
			@Override 
			public void run() {
				while(true) {
					
					try {

						Runnable current = mainThreadQueue.take();
						current.run();

					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}.start();
	}

	@Override
	public String currentThreadId() {
		return String.valueOf(Thread.currentThread().getId());
	}

	@Override
	public void runOnMainThread(Runnable runnable) {
		mainThreadQueue.add(runnable);
	}

	@Override
	public void runInWorkerThread(Runnable runnable) {
		// FIXME: use thread pool
		new Thread() {
			@Override 
			public void run() {
				runnable.run();
			}
		}.start();
	}

}
