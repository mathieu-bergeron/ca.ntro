/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.ntro_core_impl.services;


import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ca.ntro.core.NtroCore;
import ca.ntro.core.stream.Stream;
import ca.ntro.core.stream.Visitor;
import ca.ntro.ntro_core_impl.NtroCoreImpl;
import ca.ntro.ntro_core_impl.json.JsonObject;
import ca.ntro.ntro_core_impl.reflection.JsonObjectGraphJdk;
import ca.ntro.ntro_core_impl.reflection.ObjectGraphJdk;
import ca.ntro.ntro_core_impl.reflection.object_graph.ObjectGraph;
import ca.ntro.ntro_core_impl.stream.StreamNtro;

public class ReflectionServiceJdk extends ReflectionServiceNtro { 

	@Override
	public ObjectGraph graphFromObject(Object o) {
		return new ObjectGraphJdk(o);
	}

	@Override
	public ObjectGraph graphFromObject(Object o, String graphName) {
		return new ObjectGraphJdk(o, graphName);
	}

	@Override
	public ObjectGraph graphFromJsonObject(JsonObject jsonObject) {
		return new JsonObjectGraphJdk(jsonObject);
	}

	@Override
	public ObjectGraph graphFromJsonObject(JsonObject jsonObject, String graphName) {
		return new JsonObjectGraphJdk(jsonObject, graphName);
	}

	@Override
	public boolean isUserDefinedObject(Object object) {
		return super.isUserDefinedObject(object) && !isEnum(object) && !isClass(object);
	}

	@Override
	public boolean isSimpleValue(Object object) {
		return super.isSimpleValue(object) || isEnum(object) || isClass(object);
	}
	
	public boolean isEnum(Object object) {
		return object.getClass().isEnum();
	}

	@Override
	public char asChar(Object object) {
		if(object == null) return 0;
		
		double real = asDouble(object);
		long rounded = Math.round(real);
		
		return (char) rounded;
	}

	@Override
	public int asInt(Object object) {
		if(object == null) return 0;

		double real = asDouble(object);
		long rounded = Math.round(real);
		
		return (int) rounded;
	}

	@Override
	public long asLong(Object object) {
		if(object == null) return 0;

		double real = asDouble(object);
		long rounded = Math.round(real);
		
		return rounded;
	}

	@Override
	public float asFloat(Object object) {
		if(object == null) return 0;

		return Float.parseFloat(object.toString());
	}

	@Override
	public double asDouble(Object object) {
		if(object == null) return 0;

		return Double.parseDouble(object.toString());
	}

	@Override
	public boolean isList(Object object) {
		return object instanceof List 
				|| (object != null && object.getClass().isArray());
	}

	@Override
	public List<Object> asList(Object object) {
		List<Object> list;
		
		if(object != null 
				&& object.getClass().isArray()) {

			list = new ArrayList<>();
			
			for(int i = 0; i < Array.getLength(object); i++) {
				list.add(Array.get(object, i));
			}
			
		}else {
			
			list = (List<Object>) object;
		}
		
		
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <I> List<I> asList(Object object, Class<I> itemClass) {
		List<I> list;
		
		if(object != null
				&& object.getClass().isArray()) {

			list = new ArrayList<>();
			
			for(int i = 0; i < Array.getLength(object); i++) {
				list.add((I) Array.get(object, i));
			}
			
		}else {
			
			list = (List<I>) object;
		}
		
		return list;
	}

	@Override
	public String simpleName(Class<?> _class) {		int i = 0;
		return _class.getSimpleName();
	}

	@Override
	public Method getMethodByName(Class<?> _class, String methodName) {
		Method method = null;
		
		for(Method candidate : _class.getMethods()) {
			if(candidate.getName().equals(methodName)) {
				method = candidate;
				break;
			}
		}
		
		return method;
	}

	@Override
	public boolean ifClassExtends(Class<?> _class, Class<?> superClass) {
		boolean ifExtends = false;
		
		if(_class.equals(superClass)) {

			ifExtends = true;
			
		}else {
			
			Class<?> parentClass = _class.getSuperclass();
			
			if(parentClass != null) {
				
				ifExtends = ifClassExtends(parentClass, superClass);
				
			}
		}
		
		return ifExtends;
	}

	@Override
	public boolean ifClassIsSubTypeOf(Class<?> _class, Class<?> type) {
		boolean ifSubTypeOf = false;
		
		if(type.isInterface()) {
			
			ifSubTypeOf = ifClassImplements(_class, type);
			
		}else {
			
			ifSubTypeOf = ifClassExtends(_class, type);
			
		}

		return ifSubTypeOf;
	}

	@Override
	public boolean ifClassImplements(Class<?> _class, Class<?> _interface) {
		boolean ifImplements = false;
		
		for(Class<?> _candidate : _class.getInterfaces()) {
			if(_candidate.equals(_interface)) {

				ifImplements = true;
				break;

			}else if(ifClassImplements(_candidate, _interface)) {

				ifImplements = true;
				break;

			}
		}
		
		if(!ifImplements) {
			Class<?> superClass = _class.getSuperclass();
			if(superClass != null) {
				ifImplements = ifClassImplements(superClass, _interface);
			}
		}

		return ifImplements;
	}

	@Override
	public boolean isArray(Object object) {
		boolean isArray = false;

		if(object != null) {
			isArray = object.getClass().isArray();
		}
		
		return isArray;
	}

	@Override
	public Stream<String> methodNames(Object object) {
		return new StreamNtro<String>() {

			@Override
			public void forEach_(Visitor<String> visitor) throws Throwable {
				for(Method method : object.getClass().getMethods()) {

					visitor.visit(method.getName());
				}
			}
		};
	}

	@Override
	public Stream<String> fieldNames(Object object) {
		return new StreamNtro<String>() {
			@Override
			public void forEach_(Visitor<String> visitor) throws Throwable {
				Class<?> _class = object.getClass();
				Set<String> fieldNames = new HashSet<>();

				addFieldNamesForClassAndSuperClasses(_class, fieldNames);

				for(String fieldName : fieldNames) {
					visitor.visit(fieldName);
				}
			}
		};
	}

	private void addFieldNamesForClassAndSuperClasses(Class<?> _class, Set<String> fieldNames) {
		for(Field field : _class.getDeclaredFields()) {
			if(shouldSerializeField(field)) {
				fieldNames.add(field.getName());
			}
		}

		for(Field field : _class.getFields()) {
			if(shouldSerializeField(field)) {
				fieldNames.add(field.getName());
			}
		}
		
		Class<?> superClass = _class.getSuperclass();
		if(!superClass.equals(Object.class)) {
			addFieldNamesForClassAndSuperClasses(superClass, fieldNames);
		}
	}


	private boolean shouldSerializeField(Field field) {
		int modifiers = field.getModifiers();
		return !Modifier.isStatic(modifiers)
				&& !Modifier.isTransient(modifiers)
				&& field.getName() != null
				&& !field.getName().equals("componentType")
				&& !field.getName().contains("$");
	}

	@Override
	public void setFieldValue(Object object, String fieldName, Object fieldValue) {
		Field field = null;

		try {
			
			field = object.getClass().getDeclaredField(fieldName);

		} catch(Throwable t) {
			
			field = findFieldInClassOrSuperClasses(object.getClass(), fieldName);

		}
		
		if(field == null) {
			NtroCore.logger().fatal("field not found: " + NtroCoreImpl.reflection().simpleName(object.getClass()) + "." + fieldName);
		}

		setAttribute(object, field, fieldValue);
	}

	private static void setAttribute(Object object, Field field, Object fieldValue) {
		fieldValue = convertValueAccordingToFieldType(field, fieldValue);
		
		field.setAccessible(true);

		try {

			field.set(object, fieldValue);

		} catch (IllegalArgumentException | IllegalAccessException e) {

			NtroCore.logger().fatal("cannot set value for field " + NtroCoreImpl.reflection().simpleName(object.getClass()) + "." + field.getName() + " = " + fieldValue, e);
		}
	}

	@Override
	public Object getFieldValue(Object object, String fieldName) {
		Field field = null;

		try {
			
			field = object.getClass().getDeclaredField(fieldName);

		} catch(Throwable t) {
			
			field = findFieldInClassOrSuperClasses(object.getClass(), fieldName);

		}
		
		Object value = null;

		try {

			field.setAccessible(true);
			value = field.get(object);

		} catch (IllegalArgumentException | IllegalAccessException e) {

			NtroCore.logger().fatal("field not found: " + NtroCoreImpl.reflection().simpleName(object.getClass()) + "." + fieldName, e);

		}

		return value;
	}

	public static Field findFieldInClassOrSuperClasses(Class<? extends Object> _class, String attributeName) {
		Field field = null;

		for(Field candidate : _class.getDeclaredFields()) {
			if(candidate.getName().equals(attributeName)) {
				field = candidate;
				break;
			}
		}

		for(Field candidate : _class.getFields()) {
			if(candidate.getName().equals(attributeName)) {
				field = candidate;
				break;
			}
		}
		
		if(field == null) {
			Class<?> superClass = _class.getSuperclass();
			if(!superClass.equals(Object.class)) {
				field = findFieldInClassOrSuperClasses(superClass, attributeName);
			}
		}

		return field;
	}

	@Override
	public Object invokeGetter(Object object, String getterName) {
		Method method;
		Object value = null;
		try {

			method = object.getClass().getMethod(getterName);
			method.setAccessible(true);

			value = method.invoke(object);

		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			
			NtroCore.logger().fatal("cannot invoke getter " + simpleName(object.getClass()) + "." + getterName, e);

		}

		return value;
	}

	@Override
	public void invokeSetter(Object object, String setterName, Object fieldValue) {
		try {

			Method method = NtroCoreImpl.reflection().getMethodByName(object.getClass(), setterName);
			
			if(method == null) {
				NtroCoreImpl.logger().fatal("setter not found: " + setterName(object, setterName));
			}
			
			fieldValue = convertValueAccordingToSetterType(method, fieldValue);

			method.invoke(object, fieldValue);

		} catch (IllegalArgumentException | SecurityException | IllegalAccessException | InvocationTargetException e) {
			
			NtroCore.logger().fatal("cannot invoke setter " + setterName(object, setterName) + " with parameter of type " + fieldValue.getClass());

		}

	}

	private static Object convertValueAccordingToFieldType(Field field, Object attributeValue) {
		Object result = attributeValue;

		Class<?> fieldType = field.getType();
		Type genericFieldType = field.getGenericType();

		result = convertValueToTargetType(attributeValue, fieldType, genericFieldType);
		
		return result;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static Object convertValueAccordingToSetterType(Method method, Object attributeValue) {
		Object result = attributeValue;

		Class<?> paramType = null;
		Type genericParamType = null;

		if(method.getParameterTypes().length > 0) {
			
			paramType = method.getParameterTypes()[0];
			genericParamType = method.getGenericParameterTypes()[0];

		}else {
			
			NtroCoreImpl.logger().fatal("setter must have a least one input parameter " + method.getName());

		}

		result = convertValueToTargetType(attributeValue, paramType, genericParamType);

		return result;
	}

	private static Object convertValueToTargetType(Object value, Class<?> targetType, Type genericTargetType) {
		Object result = value;

		if(targetType != null
				&& targetType.isArray()
				&& value != null
				&& value instanceof List) {
			
			result = convertListToArray(targetType, (List<?>) value);

		}else if(targetType != null
				&& genericTargetType != null
				&& genericTargetType instanceof ParameterizedType
				&& value != null
				&& value instanceof Map) {
			
			result = convertMapKeys(targetType, (ParameterizedType) genericTargetType, (Map<Object, Object>) value);

		}else if(targetType != null
				&& genericTargetType != null
				&& genericTargetType instanceof ParameterizedType
				&& value != null
				&& value instanceof List) {
			
			result = convertListValues(targetType, (ParameterizedType) genericTargetType, (List<Object>) value);

		}else if(targetType != null
				&& targetType.isEnum()
				&& value != null
				&& value instanceof String) {

			result = convertStringToEnum((Class<? extends Enum>) targetType, (String) value);

		}else if(targetType != null
				&& targetType.equals(Long.class)
				&& value != null
				&& value instanceof Integer) {

			result = convertIntegerToLong((Class<? extends Long>) targetType, (Integer) value);

		}else if(targetType != null
				&& targetType.equals(Class.class)
				&& value != null
				&& value instanceof String) {
			
			Class<?> namedClass = NtroCore.factory().namedClass((String)value);
			
			result = namedClass;

		}

		return result;
	}

	private static Long convertIntegerToLong(Class<? extends Long> paramType, Integer attributeValue) {
		return (long)((int) attributeValue);
	}

	private static Map<Object,Object> convertMapKeys(Class<?> mapType, ParameterizedType genericMapType, Map<Object, Object> map) {
		Map<Object, Object> result = map;
		
		Class<?> keyType = (Class<?>) genericMapType.getActualTypeArguments()[0];
		
		if(keyType.isEnum()) {
			
			result = new HashMap<>();
			
			for(Map.Entry<Object, Object> entry : map.entrySet()) {
				
				String keyString = String.valueOf(entry.getKey());
				Object value = entry.getValue();
				
				Enum<?> keyEnum = Enum.valueOf((Class<? extends Enum>) keyType, keyString);
				
				result.put(keyEnum, value);
			}
		}
		
		
		return result;
	}

	private static List<Object> convertListValues(Class<?> listType, ParameterizedType genericListType, List<Object> list) {
		List<Object> result = list;
		
		Object itemTypeInfo = genericListType.getActualTypeArguments()[0];
		
		if(itemTypeInfo instanceof Class) {

			Class<?> itemType = (Class<?>) genericListType.getActualTypeArguments()[0];
			
			if(itemType.isEnum()) {
				
				result = new ArrayList<>();
				
				for(Object item : list) {
					
					String itemString = String.valueOf(item);
					
					Enum<?> itemEnum = Enum.valueOf((Class<? extends Enum>) itemType, itemString);
					
					result.add(itemEnum);
				}
			}
		}
		
		return result;
	}

	private static Object convertListToArray(Class<?> arrayType, List<?> list) {

		Class<?>  componentType = arrayType.getComponentType();
		
		Object array = Array.newInstance(componentType, list.size());
		
		for(int i = 0; i < list.size(); i++) {
			Object value = list.get(i);

			if(componentType.isArray()
					&& value != null
					&& value instanceof List) {
				
				value = convertListToArray(componentType, (List<?>) value);
			}

			Array.set(array, i, value);
		}

		return array;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static <T extends Enum> T convertStringToEnum(Class<T> enumType, String name) {
		return (T) Enum.valueOf(enumType, name.toUpperCase());
	}

	private static String setterName(Object object, String setterName) {
		return NtroCoreImpl.reflection().simpleName(object.getClass()) + "." + setterName;
	}

	@Override
	public boolean isClass(Object object) {
		boolean isClass = false;
		if(object != null) {
			isClass = object.getClass().equals(Class.class);
		}
		return isClass;
	}


}
