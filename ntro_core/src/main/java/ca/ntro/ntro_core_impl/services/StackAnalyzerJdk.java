/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.ntro_core_impl.services;

import ca.ntro.core.NtroCore;
import ca.ntro.ntro_core_impl.NtroCoreImpl;
import ca.ntro.ntro_core_impl.values.CodeLocation;
import ca.ntro.ntro_core_impl.values.CodeLocationImpl;

public class StackAnalyzerJdk extends StackAnalyzerImpl {

	@Override
	public void analyzeCall(Object calledClassOrObject) {

		
	}

	@Override
	public Class<?> callerClass() {
		
		return callerLocation().getClass();

	}

	@Override
	public CodeLocation callerLocation() {
		StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
		
		StackTraceElement call = stackTrace[stackTrace.length - 1];
		
		return locationFromCall(call);
	}
	
	private Class<?> classForName(String className){
		
		Class<?> _class = null;

		try {

			_class = Class.forName(className);

		} catch (ClassNotFoundException e) {
			
			NtroCoreImpl.logger().fatal(e);
		}
		
		return _class;

	}

	@Override
	public CodeLocation callingLocationOf(Throwable t) {
		
		CodeLocation location = null;
		
		int callDistance = callDistance();

		StackTraceElement[] currentStack = t.getStackTrace();

		StackTraceElement call = null;
		
		for(int i = callDistance; i < currentStack.length; i++) {

			call = currentStack[i];
			
			location = locationFromCall(call);
			
			if(location.isAppFile()) {
				break;
			}
			
			if(location._class().equals(appWrapperClass())) {
				location = appLocation();
				break;
			}
		}

		return location;
	}
	
	private CodeLocation locationFromCall(StackTraceElement call) {

		if(call == null) return new CodeLocationImpl(Class.class, "Unknown", 0);

		CodeLocationImpl location = null;
		
		String className = call.getClassName();
		
		if(isUserDefinedClassName(className)) {

			location = new CodeLocationImpl(classForName(className), call.getFileName(), call.getLineNumber())
										  .isAppFile(true)
										  .isNtroFile(false);
		}
		
		else if(isNtroClassName(className)) {

			location = new CodeLocationImpl(classForName(className), call.getFileName(), call.getLineNumber())
										   .isAppFile(false)
										   .isNtroFile(true);

		}else {

			location = new CodeLocationImpl(classForName(className), call.getFileName(), call.getLineNumber());
			
		}
			
		return location;
		
	}

	private boolean isNtroClassName(String className) {
		if(className == null) return false;
		
		return className.startsWith("ca.ntro");
	}

	private boolean isUserDefinedClassName(String className) {
		if(className == null) return true;
		
		if(isNtroClassName(className)) return false;
		
		return !className.startsWith("java")
				&& !className.startsWith("javafx")
				&& !className.startsWith("com.sun");
	}


}
