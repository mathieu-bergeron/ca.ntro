/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.ntro_core_impl.services;

import ca.ntro.core.services.RandomService;

public class    RandomServiceJdk 

	   extends  RandomServiceNtro

       implements RandomService {
	
	private static final Character[] ID_CHARACTERS = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
				                                      'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
				                                      '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};
	
	private java.util.Random random = new java.util.Random(System.currentTimeMillis());

	@Override
	public boolean nextBoolean() {
		return random.nextBoolean();
	}

	@Override
	public int nextInt() {
		return random.nextInt();
	}

	@Override
	public int nextInt(int bound) {
		return random.nextInt(bound);
	}

	@Override
	public double nextDouble() {
		return random.nextDouble();
	}

	@Override
	public double nextDouble(double bound) {
		return random.nextDouble() * bound;
	}

	@Override
	public String nextId() {
		return nextId(DEFAULT_ID_SIZE);
	}

	@Override
	public String nextId(int size) {
		StringBuilder builder = new StringBuilder();
		
		for(int i = 0; i < size; i++) {
			builder.append(choice(ID_CHARACTERS));
		}
		
		return builder.toString();
	}

}
