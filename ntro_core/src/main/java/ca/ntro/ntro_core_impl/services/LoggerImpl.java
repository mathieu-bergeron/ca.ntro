/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.ntro_core_impl.services;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;

import ca.ntro.core.NtroCore;
import ca.ntro.core.services.Logger;
import ca.ntro.core.util.StringUtils;
import ca.ntro.ntro_core_impl.NtroCoreImpl;
import ca.ntro.ntro_core_impl.values.CodeLocation;

public class LoggerImpl implements Logger {
	
	private String logFileShortPath = null;
	private PrintWriter logFileWriter;
	private PrintWriter afterFatalWriter;
	
	private boolean dialogEnabled = true;
	private boolean fatalAlreadyOccured = false;
	

	public void initialize(String sessionId) {

		String rawProjectPath = NtroCore.options().projectPath();
		Path projectPath = Paths.get(rawProjectPath);

		String rawTmpPath = NtroCore.options().tmpPath();
		Path tmpPath = Paths.get(rawTmpPath);
		File tmpDir = tmpPath.toFile();
				
		if(!tmpDir.exists()) {
			tmpDir.mkdirs();
		}

		Path afterFatalPath = Paths.get(tmpPath.toAbsolutePath().toString(), "after_fatal.log");
		File afterFatalFile = afterFatalPath.toFile();
		
		Path exceptionsPath = Paths.get(tmpPath.toAbsolutePath().toString(), "exception_" + sessionId + ".log");
		File exceptionsFile = exceptionsPath.toFile();
		
		Path logFileRelativePath = projectPath.getParent().relativize(exceptionsPath);
		logFileShortPath = logFileRelativePath.toString();

		try {

			logFileWriter    = new PrintWriter(exceptionsFile);
			afterFatalWriter = new PrintWriter(afterFatalFile);

		} catch (FileNotFoundException e) {

			NtroCoreImpl.logger().fatal("cannot write log file: " + exceptionsPath.toAbsolutePath().toString());

		}

	}

	public synchronized void flushTasks() {
	}
	
	@Override
	public synchronized void taskExecutes(String taskId) {
	}

	@Override
	public void run() {
	}

	@Override
	public void info(String message) {
		flushTasks();

		System.out.println("[INFO] " + message);
	}

	@Override
	public void value(Object value) {
		flushTasks();

		System.out.println("[VALUE] " + value);
	}

	@Override
	public void error(String message) {
		flushTasks();
		System.out.println("[ERROR] " + message);
	}

	@Override
	public void warning(String message) {
		flushTasks();
		if(shouldDisplayWarnings()) {
			System.out.println("[WARNING] " + message);
		}
	}

	protected boolean shouldDisplayWarnings() {
		return NtroCore.options().displayWarnings();
	}

	@Override
	public void exception(Throwable t) {
		flushTasks();

		CodeLocation location = NtroCoreImpl.stackAnalyzer().callingLocationOf(t);

		System.out.println("[EXCEPTION] (" + location.filename() + ":" + location.lineNumber() + ") " + t.getMessage());
	}
	

	@Override
	public void fatal(Throwable t) {
		if(!fatalAlreadyOccured) {
			memorizeFatalHasNowOccured();

			fatalImpl("", t);

		}else {
			
			if(!StringUtils.isNullOrEmpty(t.getMessage())) {
				afterFatalWriter.println("[FATAL] " + t.getMessage());
			}
			
			afterFatalWriter.println("");
			afterFatalWriter.println("");

			t.printStackTrace(afterFatalWriter);
		}
	}

	protected void memorizeFatalHasNowOccured() {
		fatalAlreadyOccured = true;
	}

	@Override
	public void fatal(String customMessage, Throwable t) {
		if(!fatalAlreadyOccured) {
			memorizeFatalHasNowOccured();

			fatalImpl(customMessage, t);

		}else {

			afterFatalWriter.println("[FATAL]" + customMessage);

			afterFatalWriter.println("");
			afterFatalWriter.println("");

			t.printStackTrace(afterFatalWriter);

		}
	}

	@Override
	public void fatal(String message) {
		if(!fatalAlreadyOccured) {
			memorizeFatalHasNowOccured();
			
			try {
				
				if(StringUtils.isNullOrEmpty(message)) {

					throw new RuntimeException();
					
				}else {

					throw new RuntimeException(message);

				}
				
				
			}catch(Throwable t) {
				
				((StackAnalyzerImpl)NtroCoreImpl.stackAnalyzer()).addToCallDistanceForCurrentThread(1);
				
				fatalImpl("", t);

			}

		} else {

			afterFatalWriter.println("");
			afterFatalWriter.println("");

			afterFatalWriter.println("[FATAL]" + message);
			
		}
	}

	private void fatalImpl(String customMessage, Throwable t) {
		logException(t);

		Throwable cause = t;
		while(cause.getCause() != null) {
			cause = cause.getCause();
		}
		
		CodeLocation location = NtroCoreImpl.stackAnalyzer().callingLocationOf(cause);
		
		String message = "";
		
		if(location.isAppFile()) {

			message = "(" + location.filename() + ":" + location.lineNumber() + ") ";

		}else if(location.isNtroFile()) {

			message = "[NtroError]\n\n(" + location.filename() + ":" + location.lineNumber() + ") ";
			
		}else {

			message = "[UnknownError]\n\n";
			
			if(!location.filename().equals("Unknown")) {

				message += "(" +location.filename() + ":" + location.lineNumber() + ") ";
			}
		}

		if(!StringUtils.isNullOrEmpty(customMessage)) {
			message += customMessage;
		}

		if(!(cause.getClass().getSimpleName().equals("RuntimeException")
			  || cause.getClass().getSimpleName().equals("Exception")
			  || cause.getClass().getSimpleName().equals("Error")
			  || cause.getClass().getSimpleName().equals("Throwable"))) {

			message += "\n\n" + cause.getClass().getSimpleName();
			
		}

		if(!StringUtils.isNullOrEmpty(cause.getMessage())) {
			message += "\n\n" + cause.getMessage();
		}
		
		finalizeFatal(message);
	}
	
	protected void logException(Throwable t) {
		t.printStackTrace(logFileWriter);
		logFileWriter.close();
	}
	
	protected void finalizeFatal(String message) {
		flushTasks();
		System.out.flush();
		
		afterFatalWriter.close();
		
		String seeLog = "details: " + logFileShortPath + "\n";

		System.err.println("\n[FATAL] " + message + 
							"\n\n        " + seeLog);
		
		fatalDialogIfEnabled(message + "\n\n\n" + seeLog);
	}


	private void fatalDialogIfEnabled(String message) {
		if(dialogEnabled) {

			NtroCoreImpl.stopFrontendTaskGraph();
			fatalDialog(message);

		} else {
			
			NtroCore.exit();
			
		}
	}

	protected void fatalDialog(String message) {
	}

	@Override
	public void setErrorDialogEnabled(boolean isEnabled) {
		this.dialogEnabled = isEnabled;
	}

}
