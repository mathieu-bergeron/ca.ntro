package ca.ntro.ntro_core_impl.options;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

import ca.ntro.core.NtroCore;
import ca.ntro.core.options.CoreOptions;
import ca.ntro.core.options.CoreOptionsWriter;
import ca.ntro.core.util.StringUtils;

public class CoreOptionsImpl implements CoreOptions, CoreOptionsWriter {
    
    private boolean displayWarnings = true;
    private boolean displayTasks    = true;

    private String projectPath   = "${userDir}";
    private String storagePath   = "${projectPath}/_storage";
    private String modelsPath    = "${storagePath}/models";
    private String tmpPath       = "${storagePath}/tmp";
    private String sessionsPath  = "${storagePath}/sessions";
    private String optionsPath   = "${storagePath}/options";
    private String graphsPath    = "${storagePath}/graphs";
    private String srcPath       = "${projectPath}/src";
    private String javaPath      = "${srcPath}/main/java";
    private String resourcesPath = "${srcPath}/main/resources";
    
    private transient String userDirImpl       = null;
    private transient String projectPathImpl   = null;
    private transient String storagePathImpl   = null;
    private transient String modelsPathImpl    = null;
    private transient String tmpPathImpl       = null;
    private transient String sessionsPathImpl  = null;
    private transient String optionsPathImpl   = null;
    private transient String graphsPathImpl    = null;
    private transient String srcPathImpl       = null;
    private transient String javaPathImpl      = null;
    private transient String resourcesPathImpl = null;

    @Override
    public void projectPath(String projectPath) {
        this.projectPath = projectPath;
        resolvePaths();
    }

    @Override
    public void resourcesPath(String resourcesPath) {
        this.resourcesPath = resourcesPath;
        resolvePaths();
    }

    @Override
    public void displayWarnings(boolean displayWarnings) {
        this.displayWarnings = displayWarnings;
    }

    @Override
    public void displayTasks(boolean displayTasks) {
        this.displayTasks = displayTasks;
        
    }

    @Override
    public boolean displayWarnings() {
        return displayWarnings;
    }

    @Override
    public boolean displayTasks() {
        return displayTasks;
    }

    @Override
    public String projectPath() {
        return projectPathImpl;
    }

    @Override
    public String resourcesPath() {
        return resourcesPathImpl;
    }

    @Override
    public String storagePath() {
        return storagePathImpl;
    }

    @Override
    public String modelsPath() {
        return modelsPathImpl;
    }

    @Override
    public String tmpPath() {
        return tmpPathImpl;
    }

    @Override
    public String sessionsPath() {
        return sessionsPathImpl;
    }

    @Override
    public String graphsPath() {
        return graphsPathImpl;
    }

    @Override
    public String optionsPath() {
        return optionsPathImpl;
    }

	@Override
	public String srcPath() {
		return srcPathImpl;
	}

	@Override
	public String javaPath() {
		return javaPathImpl;
	}
    
    /**
     * userDir
     * projectPath
     * storagePath
    **/
    private String resolvePath(String path) {
    
        String resolved = path;

        if(userDirImpl != null){
            resolved = resolved.replace("${userDir}", userDirImpl);
        }

        if(projectPathImpl != null){
            resolved = resolved.replace("${projectPath}", projectPathImpl);
        }

        if(srcPathImpl != null){
            resolved = resolved.replace("${srcPath}", srcPathImpl);
        }

        if(storagePathImpl != null){
            resolved = resolved.replace("${storagePath}", storagePathImpl);
        }
        
        resolved = Paths.get(resolved).toAbsolutePath().toString();

        return resolved;        
    }

    
    
    public void resolvePaths() {
        userDirImpl       = StringUtils.windowsPathToUnix(System.getProperty("user.dir"));
        resolvePathsImpl();
    }

    private void resolvePathsImpl() {
        projectPathImpl   = resolvePath(projectPath);
        storagePathImpl   = resolvePath(storagePath);
        modelsPathImpl    = resolvePath(modelsPath);
        tmpPathImpl       = resolvePath(tmpPath);
        sessionsPathImpl  = resolvePath(sessionsPath);
        optionsPathImpl   = resolvePath(optionsPath);
        graphsPathImpl    = resolvePath(graphsPath);
        srcPathImpl       = resolvePath(srcPath);
        javaPathImpl      = resolvePath(javaPath);
        resourcesPathImpl = resolvePath(resourcesPath);
    }

    public void resolveProjectPathIfNeeded(String appFilename, String projectDirname) {
        userDirImpl = StringUtils.windowsPathToUnix(System.getProperty("user.dir"));

        if(ifCwdIsTheRootProjectDir(userDirImpl)) {
            userDirImpl = findProjectDirpath(userDirImpl, appFilename, projectDirname);
        }

        resolvePathsImpl();

		NtroCore.logger().info("project path resolved to: " + projectPath());
    }
    
    
    private boolean ifCwdIsTheRootProjectDir(String cwdPathRaw) {
        File cwdDir = Paths.get(cwdPathRaw).toFile();

        String[] filesAtRootProjectDir =  cwdDir.list(new FilenameFilter() {

                        @Override
                        public boolean accept(File file, String filename) {
                            boolean isRootDirFile = false;

                            if(filename.equals(".git")
                                //|| filename.equals(".gitignore")
                                || filename.equals(".gradle")
                                || filename.equals("buildSrc")
                                || filename.equals("gradle")
                                || filename.equals("gradlew")
                                || filename.equals("gradlew.bat")
                                || filename.equals("settings.gradle")
                                || filename.equals("gradle.properties")) {

                                //NtroCore.logger().info("root file: " + filename);
                                isRootDirFile = true;
                            }

                            return isRootDirFile;
                        }
                    });

        return filesAtRootProjectDir.length > 0;
    }

    private boolean ifPathIsSomeAppRoot(String pathRaw) {
    	if(pathRaw.equals("buildSrc")) {
    		return false;
    	}

        File appRootDir = Paths.get(pathRaw).toFile();
        
        String[] filesAtAppRootDir = appRootDir.list(new FilenameFilter() {
			
			@Override
			public boolean accept(File dir, String filename) {
				boolean isAppRootDir = false;

				if(filename.equalsIgnoreCase("build.gradle")) {

					isAppRootDir = true;
				}

				return isAppRootDir;
			}
		});
        
        return filesAtAppRootDir.length > 0;
    }
    
    private boolean ifPathIsThisAppRoot(String pathRaw, String appFilename, String projectDirname) {
    	boolean isThisAppRoot = false;
        
    	
    	if(ifPathIsSomeAppRoot(pathRaw)){

			if(ifSubPathMatchesApp(pathRaw, appFilename, projectDirname)) {

				isThisAppRoot = true;
			}
    	}
    	
    	return isThisAppRoot;
    }

    private boolean ifSubPathMatchesApp(String currentPathRaw, String appFilename, String projectDirname) {
    	boolean ifMatchesApp = false;

		Path currentPath = Paths.get(currentPathRaw);
		File currentFile = currentPath.toFile();
		
		if(currentFile.isFile() && currentFile.getName().equalsIgnoreCase(appFilename)) {
			
			ifMatchesApp = true;
			
		}else if(currentFile.isDirectory() && currentFile.getName().equalsIgnoreCase(projectDirname)) {

			ifMatchesApp = true;
			
		}else if(currentFile.isDirectory()){
			
			for(File subFile : currentFile.listFiles()) {
                String subFilePathRaw = subFile.getAbsolutePath();
                
                if(ifSubPathMatchesApp(subFilePathRaw, appFilename, projectDirname)) {
                	
                	ifMatchesApp = true;
                	break;
                }
			}
		}
    	
    	return ifMatchesApp;
    }


    private String findProjectDirpath(String currentDirpathRaw, String appFilename, String projectDirname) {
        String projectDirpathRaw = null;
        
        if(ifPathIsThisAppRoot(currentDirpathRaw, appFilename, projectDirname)) {

        	projectDirpathRaw = currentDirpathRaw;

        }else {
        	
            Path currentDirpath = Paths.get(currentDirpathRaw);
            File currentDir = currentDirpath.toFile();

            File[] subDirs = currentDir.listFiles(new FileFilter() {
                @Override
                public boolean accept(File subDir) {
                    return subDir.isDirectory();
                }
            });

            for(File subDir : subDirs) {
                String subDirpathRaw = subDir.getAbsolutePath();

                String candidateProjectDirpathRaw = findProjectDirpath(subDirpathRaw, appFilename, projectDirname);
                
                if(candidateProjectDirpathRaw != null) {
                	projectDirpathRaw = candidateProjectDirpathRaw;
					break;
                }
            }
        }

        return projectDirpathRaw;
    }

}
