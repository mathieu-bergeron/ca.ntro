/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.ntro_core_impl.task_graphs.generic_task_graph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ca.ntro.core.NtroCore;
import ca.ntro.core.stream.Stream;
import ca.ntro.core.stream.Visitor;
import ca.ntro.ntro_core_impl.graphs.common.Direction;
import ca.ntro.ntro_core_impl.graphs.generics.graph.SearchStrategy;
import ca.ntro.ntro_core_impl.graphs.hierarchical_dag.HierarchicalDagNodeBuilder;
import ca.ntro.ntro_core_impl.stream.StreamNtro;
import ca.ntro.ntro_core_impl.task_graphs.handlers.RemovedFromGraphHandler;

public abstract class GenericTaskImpl<T  extends GenericTask<T,ST,ET,TG,G>,
                                      ST extends GenericSimpleTask<T,ST,ET,TG,G>,
                                      ET extends GenericExecutableTask<T,ST,ET,TG,G>,
                                      TG extends GenericTaskGroup<T,ST,ET,TG,G>,
                                      G  extends GenericTaskGraph<T,ST,ET,TG,G>> 

       implements GenericTask<T,ST,ET,TG,G> {
	
	private String id;
	private GenericTaskGraphNtro<T,ST,ET,TG,G> graph;
	private Set<GenericTaskGraphNodeNtro<T,ST,ET,TG,G>> nodes = new HashSet<>();
	private RemovedFromGraphHandler removeHandler;
	private Set<T> waitsFor = new HashSet<>();

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public GenericTaskGraphNtro<T, ST, ET, TG, G> getGraph() {
		return graph;
	}

	public void setGraph(GenericTaskGraphNtro<T, ST, ET, TG, G> graph) {
		this.graph = graph;
	}

	public Set<GenericTaskGraphNodeNtro<T,ST,ET,TG,G>> getNodes() {
		return nodes;
	}

	public void setNodes(Set<GenericTaskGraphNodeNtro<T, ST, ET, TG, G>> nodes) {
		this.nodes = nodes;
	}
	
	public void addNode(GenericTaskGraphNodeNtro<T,ST,ET,TG,G> node) {
		getNodes().add(node);
	}

	public RemovedFromGraphHandler getRemoveHandler() {
		return removeHandler;
	}

	public void setRemoveHandler(RemovedFromGraphHandler removeHandler) {
		this.removeHandler = removeHandler;
	}

	public Stream<GenericTaskGraphNodeNtro<T,ST,ET,TG,G>> nodes() {
		return Stream.forSet(getNodes());
	}

	public Set<T> getWaitsFor() {
		return waitsFor;
	}

	public void setWaitsFor(Set<T> waitsFor) {
		this.waitsFor = waitsFor;
	}

	@Override
	public String id() {
		return getId();
	}

	@Override
	public G parentGraph() {
		return (G) getGraph();
	}

	@Override
	public boolean isTaskGroup() {
		return false;
	}

	@Override
	public boolean isSimpleTask() {
		return false;
	}

	@Override
	public TG asTaskGroup() {
		return (TG) this;
	}

	@Override
	public ST asSimpleTask() {
		return (ST) this;
	}

	@Override
	public void addWaitsFor(T waitsFor) {
		getWaitsFor().add(waitsFor);
	}

	@Override
	public void addPreviousTask(T previousTask) {

		nodes().forEach(toNode -> {


			String siblingFromNodeId = previousTask.id();
			
			if(toNode.hasParent()) {
				siblingFromNodeId = toNode.parent().id().toKey().toString() + "_" + siblingFromNodeId;
			}

			HierarchicalDagNodeBuilder<GenericTaskGraphNode<T, ST, ET, TG, G>, GenericTaskGraphEdge<T, ST, ET, TG, G>> siblingFromNodeBuilder = getGraph().getHdagBuilder().findNode(siblingFromNodeId);
			HierarchicalDagNodeBuilder<GenericTaskGraphNode<T, ST, ET, TG, G>, GenericTaskGraphEdge<T, ST, ET, TG, G>> toNodeBuilder = (HierarchicalDagNodeBuilder<GenericTaskGraphNode<T, ST, ET, TG, G>, GenericTaskGraphEdge<T, ST, ET, TG, G>>) toNode.getNodeStructure();
			
			if(siblingFromNodeBuilder != null) {
				
				siblingFromNodeBuilder.addEdge("", toNodeBuilder);

			}else {

				GenericTaskImpl<?,?,?,?,?> previousTaskNtro = (GenericTaskImpl<?,?,?,?,?>) previousTask;

				String firstFromNodeId = previousTaskNtro.nodes().findFirst(n -> true).id().toKey().toString();
			    HierarchicalDagNodeBuilder<GenericTaskGraphNode<T, ST, ET, TG, G>, GenericTaskGraphEdge<T, ST, ET, TG, G>> firstFromNodeBuilder = getGraph().getHdagBuilder().findNode(firstFromNodeId);
			    
			    firstFromNodeBuilder.addEdge("", toNodeBuilder);
			}
		});
	}

	@Override
	public void addNextTask(T nextTask) {
		nodes().forEach(fromNode -> {

			String siblingToNodeId = nextTask.id();

			if(fromNode.hasParent()) {
				siblingToNodeId = fromNode.parent().id().toKey().toString() + "_" + siblingToNodeId;
			}

			HierarchicalDagNodeBuilder<GenericTaskGraphNode<T, ST, ET, TG, G>, GenericTaskGraphEdge<T, ST, ET, TG, G>> fromNodeBuilder = (HierarchicalDagNodeBuilder<GenericTaskGraphNode<T, ST, ET, TG, G>, GenericTaskGraphEdge<T, ST, ET, TG, G>>) fromNode.getNodeStructure();
			HierarchicalDagNodeBuilder<GenericTaskGraphNode<T, ST, ET, TG, G>, GenericTaskGraphEdge<T, ST, ET, TG, G>> siblingToNodeBuilder = getGraph().getHdagBuilder().findNode(siblingToNodeId);
			
			if(siblingToNodeBuilder != null) {

				fromNodeBuilder.addEdge("", siblingToNodeBuilder);

			}else {

				GenericTaskImpl<?,?,?,?,?> nextTaskNtro = (GenericTaskImpl<?,?,?,?,?>) nextTask;

				String firstToNodeId = nextTaskNtro.nodes().findFirst(n -> true).id().toKey().toString();
			    HierarchicalDagNodeBuilder<GenericTaskGraphNode<T, ST, ET, TG, G>, GenericTaskGraphEdge<T, ST, ET, TG, G>> firstToNodeBuilder = getGraph().getHdagBuilder().findNode(firstToNodeId);
			    
			    fromNodeBuilder.addEdge("", firstToNodeBuilder);
			}
		});
	}

	protected TaskGraphSearchOptionsNtro neighborSearchOptions(Direction direction) {

		TaskGraphSearchOptionsNtro neighborOptions = new TaskGraphSearchOptionsNtro();

		neighborOptions.internal().setSearchStrategy(SearchStrategy.DEPTH_FIRST_SEARCH);
		neighborOptions.internal().setDirections(new Direction[] {direction});
		neighborOptions.internal().setMaxDistance(1);
		neighborOptions.internal().setSortEdgesByName(false);

		return neighborOptions;
	}

	@Override
	public Stream<T> previousTasks() {
		return reachableTasks(neighborSearchOptions(Direction.BACKWARD));
	}

	@Override
	public Stream<T> nextTasks() {
		return reachableTasks(neighborSearchOptions(Direction.FORWARD));
	}

	@Override
	public Stream<T> parentTasks() {
		return reachableTasks(neighborSearchOptions(Direction.UP));
	}

	@Override
	public Stream<T> reachableTasks() {
		return reachableTasks(new TaskGraphSearchOptionsNtro());
	}

	@Override
	public Stream<T> reachableTasks(TaskGraphSearchOptions options) {
		Map<String, T> tasks = new HashMap<>();

		nodes().forEach(node -> {
			
			node.reachableNodes(options).forEach(visitedNode -> {
				
				T task = visitedNode.node().task();
				
				tasks.put(task.id(), task);
			});
		});
		
		return Stream.forMapValues(tasks);
	}

	public Stream<T> waitsFor() {
		return Stream.forSet(getWaitsFor());
	}

	public Stream<T> preConditions() {
		return new StreamNtro<T>() {

			@Override
			public void forEach_(Visitor<T> visitor) throws Throwable {

				Set<String> visitedTasks = new HashSet<>();
				visitedTasks.add(id());

				visitPreconditionsUpwards((T) GenericTaskImpl.this, 0, visitedTasks, visitor);
				visitPreconditionsBackwards((T) GenericTaskImpl.this, 0, visitedTasks, visitor);
			}
			
			private void visitPreconditionsBackwards(T currentTask, 
					                                 int distance, 
					                                 Set<String> visitedTasks,
					                                 Visitor<T> visitor) throws Throwable {
				
				currentTask.previousTasks().forEach(previousTask -> {

					if(visitedTasks.contains(previousTask.id())) {
						return;
					}

					visitedTasks.add(previousTask.id());
					
					visitPreconditions(previousTask, distance+1, visitedTasks, visitor);
				});
			}

			private void visitPreconditionsUpwards(T currentTask, 
					                               int distance, 
					                               Set<String> visitedTasks,
					                               Visitor<T> visitor) throws Throwable {
				
				currentTask.parentTasks().forEach(parentTask -> {
					
					if(visitedTasks.contains(parentTask.id())) {
						return;
					}

					visitedTasks.add(parentTask.id());
					
					visitPreconditionsBackwards(parentTask, distance, visitedTasks, visitor);
				    visitPreconditionsUpwards(parentTask, distance, visitedTasks, visitor);
				});
				
			}

			private void visitPreconditionsDownwards(TG currentGroup, 
					                                 int distance, 
					                                 Set<String> visitedTasks,
					                                 Visitor<T> visitor) throws Throwable {

				currentGroup.tasks().forEach(subTask -> {

					if(visitedTasks.contains(subTask.id())) {
						return;
					}

					visitedTasks.add(subTask.id());
					
					visitPreconditions(subTask, distance, visitedTasks, visitor);
				});
			}

			private void visitPreconditions(T currentTask, 
					                        int distance, 
					                        Set<String> visitedTasks,
					                        Visitor<T> visitor) throws Throwable {
				
				if(shouldVisitAsPrecondition(currentTask, distance)) {
					visitor.visit(currentTask);
				}

				visitPreconditionsUpwards(currentTask, distance, visitedTasks, visitor);
				visitPreconditionsBackwards(currentTask, distance, visitedTasks, visitor);
				
				if(currentTask.isTaskGroup()) {
					visitPreconditionsDownwards(currentTask.asTaskGroup(), 
							                    distance,
							                    visitedTasks,
							                    visitor);
				}
				
			}
			
			private boolean shouldVisitAsPrecondition(T currentTask, int distance) {
				boolean shouldVisit = true;

				if(!currentTask.isSimpleTask()) {
					
					shouldVisit = false;

				}else if(currentTask.isEvent() && distance > 1) {

					shouldVisit = false;
				}
				
				return shouldVisit;
			}
		};
	}

	public Stream<T> postConditions() {
		return new StreamNtro<T>() {

			@Override
			public void forEach_(Visitor<T> visitor) throws Throwable {

				Set<String> visitedTasks = new HashSet<>();
				visitedTasks.add(id());
				
				T currentTask = (T) GenericTaskImpl.this;

				visitPostconditionsUpwards(currentTask, visitedTasks, visitor);
				visitPostconditionsForwards(currentTask, visitedTasks, visitor);

				if(currentTask.isTaskGroup()) {
					visitPostconditionsDownwards(currentTask.asTaskGroup(), visitedTasks, visitor);
				}
			}

			private void visitPostconditionsUpwards(T currentTask, 
					                                Set<String> visitedTasks,
					                                Visitor<T> visitor) throws Throwable {
				
				currentTask.parentTasks().forEach(parentTask -> {

					if(visitedTasks.contains(parentTask.id())) {
						return;
					}
					
					visitedTasks.add(parentTask.id());
					
					visitor.visit(parentTask);

					visitPostconditionsUpwards(parentTask, visitedTasks, visitor);
					visitPostconditionsForwards(parentTask, visitedTasks, visitor);
				});
			}

			private void visitPostconditionsForwards(T currentTask, 
					                                 Set<String> visitedTasks,
					                                 Visitor<T> visitor) throws Throwable {
				
				currentTask.nextTasks().forEach(nextTask -> {
					
					if(visitedTasks.contains(nextTask.id())) {
						return;
					}

					visitedTasks.add(nextTask.id());

					visitor.visit(nextTask);

					visitPostconditionsUpwards(nextTask, visitedTasks, visitor);
					visitPostconditionsForwards(nextTask, visitedTasks, visitor);

					if(nextTask.isTaskGroup()) {

						visitPostconditionsDownwards(nextTask.asTaskGroup(), visitedTasks, visitor);
					}
				});
			}

			private void visitPostconditionsDownwards(TG currentTaskGroup, 
					                                  Set<String> visitedTasks,
					                                  Visitor<T> visitor) throws Throwable {
				
				currentTaskGroup.tasks().forEach(subTask -> {

					if(visitedTasks.contains(subTask.id())) {
						return;
					}


					visitedTasks.add(subTask.id());

					visitor.visit(subTask);

					visitPostconditionsForwards(subTask, visitedTasks, visitor);
					
					if(subTask.isTaskGroup()) {
						visitPostconditionsDownwards(subTask.asTaskGroup(), visitedTasks, visitor);
					}
				});
			}

		};
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public void removeFromGraph() {

		removePostConditionsFromGraph();
		
		removeThisTaskFromGraph();

		NtroCore.threads().runOnMainThread(() -> {

			getGraph().removeUnusedEventTasks();

		});
	}

	private void removePostConditionsFromGraph() {

		List<GenericTaskImpl> postConditions = postConditions().map(postCondition -> (GenericTaskImpl) postCondition).collect();

		for(GenericTaskImpl postCondition : postConditions) {
			postCondition.removeThisTaskFromGraph();
		}
	}
	
	private void removeThisTaskFromGraph() {
		//System.out.println("removeThisTaskFromGraph: " + id());

		if(isSimpleTask()
				&& asSimpleTask().isExecutableTask()) {
			
			asSimpleTask().asExecutableTask().cancel();
		}
		
		Set<GenericTaskGraphNodeNtro<T,ST,ET,TG,G>> nodesToRemove = new HashSet<>(getNodes());

		for(GenericTaskGraphNodeNtro<T,ST,ET,TG,G> node: nodesToRemove) {

			getGraph().getHdagBuilder().removeFromGraph(node.id());

			getNodes().remove(node);
		}
		
		NtroCore.threads().runOnMainThread(() -> {

			getGraph().removeTask(id());

			if(removeHandler != null) {
				removeHandler.onRemovedFromGraph();
			}
		});
	}

	public void onRemovedFromGraph(RemovedFromGraphHandler removeHandler) {
		setRemoveHandler(removeHandler);
	}

	@Override
	public void cancel() {
		// TODO Auto-generated method stub
		
	}

}
	
