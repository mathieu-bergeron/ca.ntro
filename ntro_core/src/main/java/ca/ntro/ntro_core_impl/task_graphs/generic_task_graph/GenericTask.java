/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.ntro_core_impl.task_graphs.generic_task_graph;

import ca.ntro.core.stream.Stream;
import ca.ntro.ntro_core_impl.task_graphs.handlers.RemovedFromGraphHandler;

public interface GenericTask<T  extends GenericTask<T,ST,ET,TG,G>,
                             ST extends GenericSimpleTask<T,ST,ET,TG,G>,
                             ET extends GenericExecutableTask<T,ST,ET,TG,G>,
                             TG extends GenericTaskGroup<T,ST,ET,TG,G>,
                             G  extends GenericTaskGraph<T,ST,ET,TG,G>> {

	String id();
	G parentGraph();
	
	boolean isTaskGroup();
	boolean isSimpleTask();
	TG asTaskGroup();
	ST asSimpleTask();

	boolean isEvent();
	boolean isCondition();

	void addPreviousTask(T previousTask);
	void addWaitsFor(T waitsFor);

	void addNextTask(T nextTask);

	Stream<T> previousTasks();
	Stream<T> waitsFor();

	Stream<T>  nextTasks();

	Stream<T> parentTasks();

	Stream<T> reachableTasks();
	Stream<T> reachableTasks(TaskGraphSearchOptions options);

	Stream<T> preConditions();
	Stream<T> postConditions();

	void removeFromGraph();
	void onRemovedFromGraph(RemovedFromGraphHandler removeHandler);
	
	void cancel();
	

}