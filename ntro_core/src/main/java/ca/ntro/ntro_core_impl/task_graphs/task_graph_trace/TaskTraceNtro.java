/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.ntro_core_impl.task_graphs.task_graph_trace;

import java.util.HashMap;
import java.util.Map;

import ca.ntro.core.NtroCore;
import ca.ntro.core.stream.Stream;
import ca.ntro.ntro_core_impl.NtroCoreImpl;
import ca.ntro.ntro_core_impl.identifyers.Id;
import ca.ntro.ntro_core_impl.services.StackAnalyzerImpl;
import ca.ntro.ntro_core_impl.task_graphs.base.AtomicTaskMutator;
import ca.ntro.ntro_core_impl.task_graphs.generic_task_graph.GenericExecutableTaskNtro;
import ca.ntro.ntro_core_impl.task_graphs.generic_task_graph.GenericSimpleTaskNtro;
import ca.ntro.ntro_core_impl.task_graphs.generic_task_graph.GenericTaskImpl;
import ca.ntro.ntro_core_impl.task_graphs.handlers.ExecuteHandler;
import ca.ntro.ntro_core_impl.values.ObjectMap;

public  class TaskTraceNtro 

       implements TaskTrace,
                  ObjectMap {
	
	private GenericSimpleTaskNtro<?,?,?,?,?> task;
	private TaskGraphTraceNtro graphTrace;
	private Map<String, TaskResults> preconditions = new HashMap<>();
	
	// FIXME
	boolean hasExecutedOnce = false;
	
	
	public GenericSimpleTaskNtro<?, ?, ?, ?, ?> getTask() {
		return task;
	}

	public void setTask(GenericSimpleTaskNtro<?, ?, ?, ?, ?> task) {
		this.task = task;
	}

	public TaskGraphTraceNtro getGraphTrace() {
		return graphTrace;
	}

	public void setGraphTrace(TaskGraphTraceNtro graphTrace) {
		this.graphTrace = graphTrace;
	}

	public Map<String, TaskResults> getPreconditions() {
		return preconditions;
	}

	public void setPreconditions(Map<String, TaskResults> preconditions) {
		this.preconditions = preconditions;
	}
	
	public void initialize(boolean copyExistingResults) {
		
		// XXX: in the execution trace, we use only
		//      explicit .waitsFor as preconditions
		
		getTask().waitsFor().forEach(waitsFor -> {
			
			if(waitsFor.isSimpleTask()) {

				TaskResults results = waitsFor.asSimpleTask().newResults(getGraphTrace(), copyExistingResults);

				getPreconditions().put(waitsFor.id(), results);
				
				getGraphTrace().registerNewTaskResults(waitsFor.id(), results);
			}
		});
		
		// XXX: we still detect cycles

		getTask().preConditions().forEach(preCondition -> {

			detectCycle((GenericTaskImpl<?, ?, ?, ?, ?>) preCondition);

		});

	}

	private void detectCycle(GenericTaskImpl<?, ?, ?, ?, ?> currentTask) {
		if(currentTask == getTask()) {
			getGraphTrace().getGraph().write(NtroCoreImpl.graphWriter());
			NtroCore.logger().fatal("cyclic dependancy for task " + getTask().id() + ". Please correct graph.\n\n\n");
		}
	}

	Stream<TaskResults> preconditions(){
		return Stream.forMapValues(getPreconditions());
	}
	

	@Override
	public boolean canExecuteOneStep() {
		//System.out.println("canExecuteOneStep: " + getTask().getId());
		
		/*
		if(getTask().id().equals("snapshot_ModelePartie")) {

			System.out.println("snapshot");

		} else if(getTask().id().equals("afficherVuePartie")) {

			System.out.println("afficher");
		}
		*/

		// FIXME: use a needsUpdate method instead
		//        at the beginning, needsUpdate is always true
		return (preconditions().isEmpty() && !hasExecutedOnce)
				|| preconditions().ifSome(pre -> pre.hasNext());
	}

	@Override
	public void executeOneStep() {

		preconditions().forEach(pre -> pre.advanceToNext());

		if(preconditions().ifAll(pre -> pre.hasResult())) {

			if(getTask().isExecutableTask()) {
				executeTask((GenericExecutableTaskNtro<?,?,?,?,?>) getTask().asExecutableTask());
			}
			
			hasExecutedOnce = true;

			//System.out.println("resultWasUsed: " + getTask().getId());
			
			preconditions().forEach(pre -> pre.notifyResultWasUsed());
			
		}else {

			//System.out.println("resultCouldNotBeUsed: " + getTask().getId());
			
			preconditions().forEach(pre -> pre.notifyResultCouldNotBeUsed());
		}
	}

	private void executeTask(GenericExecutableTaskNtro<?,?,?,?,?> executableTask) {
		ExecuteHandler executeHandler = executableTask.getExecuteHandler();
		
		if(executeHandler != null) {
			
			try {
				
				NtroCore.logger().taskExecutes(executableTask.id());
			
				executeHandler.execute((ObjectMap) this, new AtomicTaskMutator() {

					@Override
					public void addResult(Object value) {
						getGraphTrace().registerNewResult(getTask(), value);
					}

					@Override
					public void clearResults() {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void notifyWaitingForResult() {
						// TODO Auto-generated method stub
						
					}
				});
			}
				
			catch(Throwable t) {
				
				NtroCoreImpl.logger().fatal(t);

			}
		}
	}

	@Override
	public boolean contains(Id id) {
		return contains(id.toKey().toString());
	}

	@Override
	public boolean contains(String id) {
		return get(id) != null;
	}

	@Override
	public <O> O get(Class<O> _class, Id id) {
		return get(_class, id.toKey().toString());
	}

	@Override
	public <O> O get(Class<O> _class, String id) {
		return (O) get(id);
	}

	@Override
	public Object get(Id id) {
		return get(id.toKey().toString());
	}

	@Override
	public Object get(String id) {
		Object result = null;
		
		TaskResults results = getPreconditions().get(id);
		
		if(results != null
				&& results.hasResult()) {
			result = results.result();
		}
		
		return result;
	}

	@Override
	public Stream<String> ids() {
		return Stream.forMapKeys(getPreconditions());
	}

	@Override
	public Stream<Object> objects() {
		return Stream.forMapValues(getPreconditions()).reduceToStream((results, visitor) -> {
			if(results.hasResult()) {
				visitor.visit(results.result());
			}
		}) ;
	}

	public void removeTask(String taskId) {
		getPreconditions().remove(taskId);
	}

}
