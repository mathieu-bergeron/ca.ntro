package ca.ntro.ntro_core_impl.task_graphs.handlers;

public interface RemovedFromGraphHandler {
	
	void onRemovedFromGraph();

}
