/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.ntro_core_impl.task_graphs.task_graph_trace;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import ca.ntro.core.NtroCore;
import ca.ntro.core.stream.Stream;
import ca.ntro.ntro_core_impl.graph_writer.GraphWriter;
import ca.ntro.ntro_core_impl.task_graphs.generic_task_graph.GenericSimpleTask;
import ca.ntro.ntro_core_impl.task_graphs.generic_task_graph.GenericTaskGraphNtro;
import ca.ntro.ntro_core_impl.task_graphs.generic_task_graph.GenericTaskImpl;
import ca.ntro.ntro_core_impl.task_graphs.task_graph.SimpleTask;

public class TaskGraphTraceNtro 

       implements TaskGraphTrace {
	
	private GenericTaskGraphNtro<?,?,?,?,?> graph;

	private TaskGraphTraceState state = TaskGraphTraceState.INACTIVE;

	private Map<String, TaskTraceNtro> traces = new HashMap<>();
	private Map<String, Set<TaskResults>> results = new HashMap<>();
	private Map<String, TaskResults> storedConditionResults = new HashMap<>();
	
	public synchronized TaskGraphTraceState getState() {
		return state;
	}

	public synchronized void setState(TaskGraphTraceState state) {
		this.state = state;
	}

	public Map<String, TaskTraceNtro> getTraces() {
		return traces;
	}

	public void setTraces(Map<String, TaskTraceNtro> traces) {
		this.traces = traces;
	}
	
	private Stream<TaskTraceNtro> traces(){
		return Stream.forMapValues(getTraces());
	}

	public GenericTaskGraphNtro<?, ?, ?, ?, ?> getGraph() {
		return graph;
	}

	public void setGraph(GenericTaskGraphNtro<?, ?, ?, ?, ?> graph) {
		this.graph = graph;
	}

	public Map<String, Set<TaskResults>> getResults() {
		return results;
	}

	public void setResults(Map<String, Set<TaskResults>> results) {
		this.results = results;
	}

	public Map<String, TaskResults> getStoredConditionResults() {
		return storedConditionResults;
	}

	public void setStoredConditionResults(Map<String, TaskResults> storedConditionResults) {
		this.storedConditionResults = storedConditionResults;
	}

	public void initialize() {
		getGraph().tasks().forEach(task -> {
			
			if(task.isSimpleTask()) {
				
				boolean initializeWithStoredConditionResults = false;

				addTrace((SimpleTask) task.asSimpleTask(), initializeWithStoredConditionResults);
			}
		});
	}

	private void addTrace(SimpleTask task, boolean initializeWithStoredConditionResults) {

		TaskTraceNtro trace = (TaskTraceNtro) task.asSimpleTask().newTrace(this, initializeWithStoredConditionResults);
		
		getTraces().put(task.id(), trace);
	}

	public void addTaskToExistingTrace(GenericTaskImpl<?, ?, ?, ?, ?> newTaskNtro) {
		
		if(newTaskNtro.isSimpleTask()) {
			
			NtroCore.threads().runOnMainThread(new Runnable() {
				@Override
				public void run() {

					boolean initializeWithStoredConditionResults = true;

					addTrace((SimpleTask) newTaskNtro.asSimpleTask(), initializeWithStoredConditionResults);

					executeAfterResult();
				}
			});
		}
	}

	public TaskResults getStoredConditionResults(String taskId) {

		TaskResults orphanResult = getStoredConditionResults().get(taskId);
		
		return orphanResult;
	}
	
	public void registerNewTaskResults(String taskId, TaskResults taskResults) {

		Set<TaskResults> resultsForTask = getResults().get(taskId);
		if(resultsForTask == null) {
			resultsForTask = new HashSet<>();
			getResults().put(taskId, resultsForTask);
		}

		resultsForTask.add(taskResults);
	}


	public void execute() {
		switch(getState()) {
		
			case INACTIVE:
				setState(TaskGraphTraceState.ACTIVE_AND_EXECUTING);
				executeLoop();
				break;

			case ACTIVE_AND_EXECUTING:
			case ACTIVE_BUT_DONE_FOR_NOW:
				break;
		}
	}

	public void stop() {
		setState(TaskGraphTraceState.INACTIVE);

	}
	
	private void executeLoop() {
		/* FIXME: we do not need to check every task every time
		 *        we should maintain a set of candidate tasks
		 *        
		 *        we know when to add tasks to the candidate set
		 *        (on registerNewResult and tasks that have that result as a
		 *         precondition)
		 * 
		 */
		
		
		while(getState() == TaskGraphTraceState.ACTIVE_AND_EXECUTING) {
			
			boolean stillExecuting = traces().reduceToResult(false, (accumulator, trace) -> {

				if(trace.canExecuteOneStep()) {
					
					trace.executeOneStep();

					accumulator = true;
				}

				return accumulator;

			}).value();
			
			if(!stillExecuting && getState() == TaskGraphTraceState.ACTIVE_AND_EXECUTING) {
				setState(TaskGraphTraceState.ACTIVE_BUT_DONE_FOR_NOW);
			}
		}
	}
	
	public void registerNewResult(GenericSimpleTask<?,?,?,?,?> task, Object result) {
		if(getState() != TaskGraphTraceState.INACTIVE) {
			registerNewResultImpl(task, result);
			executeAfterResult();
		}
	}


	private void registerNewResultImpl(GenericSimpleTask<?,?,?,?,?> task, Object result) {
		
		String taskId = task.id();

		Set<TaskResults> resultsForTask = getResults().get(taskId);
		
		if(resultsForTask != null) {
			for(TaskResults results : resultsForTask) {
				results.registerNewResult(result);
			}
		}
		
		TaskResults thisConditionResults = getStoredConditionResults().get(taskId);

		if(thisConditionResults == null) {
			boolean initializeWithStoredConditionResults = false;
			thisConditionResults = task.newResults(this, initializeWithStoredConditionResults);
		}
		
		// FIXME: for now we keep result
		//        but do not copy it on initialization
		//        see GenericSimpleTaskNtro.newResults
		if(thisConditionResults instanceof TaskResultsCondition) {
			getStoredConditionResults().put(taskId, thisConditionResults);
			thisConditionResults.registerNewResult(result);
		}
	}

	public void clearResults(GenericSimpleTask<?,?,?,?,?> task) {
		clearResultsImpl(task.id());
	}

	public void clearAllResults() {
		for(String taskId : getResults().keySet()) {
			clearResultsImpl(taskId);
		}
	}

	private void clearResultsImpl(String taskId) {
		
		Set<TaskResults> resultsForTask = getResults().get(taskId);
		
		if(resultsForTask != null) {
			for(TaskResults results : resultsForTask) {
				results.clearResults();
			}
		}
		
		TaskResults orphanTaskResults = getStoredConditionResults().get(taskId);
		if(orphanTaskResults != null) {
			orphanTaskResults.clearResults();
		}

	}

	private void executeAfterResult() {
		switch(getState()) {
		
			case ACTIVE_BUT_DONE_FOR_NOW:
				setState(TaskGraphTraceState.ACTIVE_AND_EXECUTING);
				executeLoop();
				break;

			case INACTIVE:
			case ACTIVE_AND_EXECUTING:
				break;
		}
	}


	@Override
	public boolean isWaitingForExternalResults() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void writeCurrentState(GraphWriter writer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void writeTrace(GraphWriter writer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean isActive() {
		TaskGraphTraceState currentState = getState();
		
		return currentState == TaskGraphTraceState.ACTIVE_AND_EXECUTING
				|| currentState == TaskGraphTraceState.ACTIVE_BUT_DONE_FOR_NOW;
	}

	public void removeTask(String taskId) {
		//System.out.println("REMOVE: " + taskId);
		
		for(TaskTraceNtro trace : getTraces().values()) {
			trace.removeTask(taskId);
		}

		getTraces().remove(taskId);
		getResults().remove(taskId);
		getStoredConditionResults().remove(taskId);
	}

}
