package ca.ntro.ntro_core_impl.json;

public interface AfterLoadedFromJson {

	void afterLoadedFromJson();

}
