/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.ntro_core_impl.graphs.generics.graph;

import java.util.HashMap;
import java.util.Map;

import ca.ntro.core.NtroCore;
import ca.ntro.core.stream.Stream;
import ca.ntro.core.stream.Visitor;
import ca.ntro.ntro_core_impl.NtroCoreImpl;
import ca.ntro.ntro_core_impl.graphs.common.Direction;
import ca.ntro.ntro_core_impl.graphs.common.EdgeType;
import ca.ntro.ntro_core_impl.graphs.common.EdgeTypeNtro;
import ca.ntro.ntro_core_impl.graphs.common.NodeAlreadyAddedException;
import ca.ntro.ntro_core_impl.graphs.common.NodeId;
import ca.ntro.ntro_core_impl.graphs.common.NodeIdNtro;
import ca.ntro.ntro_core_impl.stream.StreamNtro;

public abstract class GenericGraphBuilderNtro<N extends GenericNode<N,E,SO>,
                                              E extends GenericEdge<N,E,SO>,
                                              SO extends SearchOptions,
                                              NB extends GenericNodeBuilder<N,E,SO,NB>,
                                              GO extends GraphWriterOptions,
                                              G extends GenericGraph<N,E,SO,GO>> 

       implements     GenericGraphBuilder<N,E,SO,NB,GO,G> {
	
	private GenericNodeFactory<N,E,SO> nodeFactory;
	private GenericEdgeFactory<N,E,SO> edgeFactory;
	
	private G graph;

	
	
	
	
	
	
	
	private Map<String, Object> startNodes = new HashMap<>();
	// JSweet type error: property 'equals' does not exist on type 'N'
	//private Map<String, N> startNodes = new HashMap<>();
	
	protected abstract G newGraphInstance();
	protected abstract NB newNodeBuilderInstance();
	
	public GenericNodeFactory<N, E, SO> getNodeFactory() {
		return nodeFactory;
	}

	public void setNodeFactory(GenericNodeFactory<N, E, SO> nodeFactory) {
		this.nodeFactory = nodeFactory;
	}

	public GenericEdgeFactory<N, E, SO> getEdgeFactory() {
		return edgeFactory;
	}

	public void setEdgeFactory(GenericEdgeFactory<N, E, SO> edgeFactory) {
		this.edgeFactory = edgeFactory;
	}

	public Map<String, Object> getStartNodes() {
		return startNodes;
	}

	public void setStartNodes(Map<String, Object> nodes) {
		this.startNodes = nodes;
	}

	public G getGraph() {
		return graph;
	}
	public void setGraph(G graph) {
		this.graph = graph;
	}

	public void initialize() {
		setGraph(newGraphInstance());
		graphNtro(getGraph()).setGraphStructure(this);
	}

	@Override
	public void removeFromGraph(String nodeId) {
		NodeIdNtro nodeIdNtro = new NodeIdNtro(nodeId);
		
		removeFromGraph(nodeIdNtro);
	}
	
	protected SO allEdgesOptions() {

		InternalSearchOptionsNtro options = new InternalSearchOptionsNtro();
		options.setDirections(Direction.values());
		options.setSearchStrategy(SearchStrategy.DEPTH_FIRST_SEARCH);
		options.setMaxDistance(1);
		options.setSortEdgesByName(false);

		SO allEdgesOptions = (SO) new SearchOptionsNtro();
		allEdgesOptions.copyOptions(options);
		
		return allEdgesOptions;
	}

	@Override
	public void removeFromGraph(NodeId nodeId) {
		NB nodeBuilder = findNode(nodeId);
		
		if(nodeBuilder != null) {

			removeExistingNodeFromGraph(nodeId, nodeBuilder);
		}
	}

	private void removeExistingNodeFromGraph(NodeId nodeId, NB nodeBuilder) {
		Map<String, E> edgesToRemove = new HashMap<>();
		
		nodeBuilder.node().edges(allEdgesOptions()).forEach(edge -> {
			edgesToRemove.put(edge.id().toKey().toString(), edge);
		});
		
		for(E edge : edgesToRemove.values()) {
			NB fromBuilder = findNode(edge.from().id());
			NB toBuilder = findNode(edge.to().id());
			
			if(fromBuilder != null) {
				nodeBuilderNtro(fromBuilder).removeEdgesTo(edge.to());
			}

			if(toBuilder != null) {
				nodeBuilderNtro(toBuilder).removeEdgesTo(edge.from());
			}
		}
			
		if(getStartNodes().containsKey(nodeId.toKey().toString())) {
			getStartNodes().remove(nodeId.toKey().toString());
		}
	}

	@Override
	public NB addNode(String nodeId) {
		NodeIdNtro nodeIdNtro = new NodeIdNtro(nodeId);

		return addNode(nodeIdNtro);
	}

	@Override
	public NB addNode(NodeId nodeId) {
		N node = getNodeFactory().newInstance();
		
		nodeNtro(node).setGraph(graph());
		nodeNtro(node).setNodeId(nodeId);
		
		return addNode(node);
	}
	
	@SuppressWarnings("unchecked")
	private GenericNodeNtro<N,E,SO> nodeNtro(N node){
		return (GenericNodeNtro<N,E,SO>) node;
	}

	@SuppressWarnings("unchecked")
	private GenericNodeBuilderNtro<N,E,SO,NB> nodeBuilderNtro(NB nodeBuilder){
		return (GenericNodeBuilderNtro<N,E,SO,NB>) nodeBuilder;
	}

	@Override
	public NB addNode(N node) {
		NB builder = newNodeBuilderInstance();
		nodeBuilderNtro(builder).setGraphBuilder(this);
		nodeBuilderNtro(builder).setNode(node);
		
		nodeNtro(node).setNodeStructure(builder);
		nodeNtro(node).setGraph(getGraph());

		memorizeNode(node);

		return builder;
	}

	@Override
	public NB findNode(String nodeId) {
		NodeIdNtro nodeIdNtro = new NodeIdNtro(nodeId);

		return findNode(nodeIdNtro);
	}

	@SuppressWarnings("unchecked")
	@Override
	public NB findNode(NodeId nodeId) {
		NB nodeBuilder = null;

		N node = graph().findNode(nodeId);
		
		if(node != null) {
			nodeBuilder = (NB) nodeNtro(node).nodeStructure();
		}
		
		return nodeBuilder;
	}

	@Override
	public E addEdge(NB fromNode, String edgeName, NB toNode) {
		EdgeType edgeTypeForward = new EdgeTypeNtro(Direction.FORWARD, edgeName);
		EdgeType edgeTypeBackward = new EdgeTypeNtro(Direction.BACKWARD, edgeName);

		E forwardEdge = addEdge(fromNode,edgeTypeForward,toNode);

		addEdge(toNode, edgeTypeBackward, fromNode);

		if(!toNode.node().isPartOfCycle()) {

		    nodeBuilderNtro(toNode).setIsStartNode(false);
		    removeStartNode(toNode.node());

		}else {
			
			onCycleDetected();

		}

		return forwardEdge;
	}

	protected void onCycleDetected() {
	}

	public void removeStartNode(N node) {
		getStartNodes().remove(node.id().toKey().toString());
	}

	protected E addEdge(NB fromNode, EdgeType edgeType, NB toNode) {

		E edge = createEdge(fromNode, edgeType, toNode);

		nodeBuilderNtro(fromNode).addEdge(edge);
		
		return edge;
	}

    @SuppressWarnings("unchecked")
	private GenericEdgeNtro<N,E,SO> edgeNtro(E edge){
		return (GenericEdgeNtro<N,E,SO>) edge;
    }

	protected E createEdge(NB fromNode, EdgeType edgeType, NB toNode) {
		
		E edge = edgeFactory.newInstance();

		edgeNtro(edge).setFrom(fromNode.node());
		edgeNtro(edge).setTo(toNode.node());
		edgeNtro(edge).setEdgeType(edgeType);
		
		return edge;
	}

	protected void memorizeNode(N node) {
		if(ifNodeAlreadyExists(node)) {

			NtroCoreImpl.logger().fatal(new NodeAlreadyAddedException("Node already added: " + node.id().toKey()));

		}else {
			
			getStartNodes().put(node.id().toKey().toString(), node);

		}
	}

	protected boolean ifNodeAlreadyExists(N node) {
		// JSWeet: line was wrong!
		// JSWeet: equals does not exists in N
		return graph().nodes().ifSome(n -> ((Object)n).equals(node));
	}

	@Override
	public G graph() {
		return getGraph();
	}

	@Override
	public Stream<N> startNodes(){
		return new StreamNtro<N>() {

			@Override
			public void forEach_(Visitor<N> visitor) throws Throwable {
				for(Object node : getStartNodes().values()) {

					visitor.visit((N) node);
				}
			}
		};
	}
	
	@SuppressWarnings("unchecked")
	private GenericGraphNtro<N,E,SO,GO> graphNtro(G graph){
		return (GenericGraphNtro<N,E,SO,GO>) graph;
	}

	@Override
	public void setGraphName(String graphName) {
		graphNtro(graph()).setId(GraphId.fromGraphName(graphName));
	}

}
