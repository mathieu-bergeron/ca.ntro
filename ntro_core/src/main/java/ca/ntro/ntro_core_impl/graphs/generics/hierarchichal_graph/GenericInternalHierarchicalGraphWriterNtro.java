/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.ntro_core_impl.graphs.generics.hierarchichal_graph;

import ca.ntro.core.NtroCore;
import ca.ntro.ntro_core_impl.NtroCoreImpl;
import ca.ntro.ntro_core_impl.graph_writer.EdgeSpecNtro;
import ca.ntro.ntro_core_impl.graph_writer.GraphWriter;
import ca.ntro.ntro_core_impl.graph_writer.GraphWriterException;
import ca.ntro.ntro_core_impl.graphs.common.Direction;
import ca.ntro.ntro_core_impl.graphs.generics.graph.GenericEdge;
import ca.ntro.ntro_core_impl.graphs.generics.graph.GenericGraph;
import ca.ntro.ntro_core_impl.graphs.generics.graph.GenericInternalGraphWriterNtro;
import ca.ntro.ntro_core_impl.graphs.generics.graph.InternalSearchOptionsNtro;
import ca.ntro.ntro_core_impl.graphs.generics.graph.VisitedNode;

public class      GenericInternalHierarchicalGraphWriterNtro<N extends GenericHierarchicalNode<N,E,SO>,
                                                             E extends GenericEdge<N,E,SO>,
													         SO extends HierarchicalSearchOptions,
													         GO extends HierarchicalGraphWriterOptions>

       extends    GenericInternalGraphWriterNtro<N,E,SO,GO>

       implements InternalHierarchicalGraphWriter<N,E,SO,GO> {

	@Override
	protected void writeNodes(GenericGraph<N,E,SO,GO> graph, GO options, GraphWriter writer) {
		SO downOptions = graph.defaultSearchOptions();
		InternalSearchOptionsNtro downOptionsNtro = new InternalSearchOptionsNtro();
		downOptionsNtro.setDirections(new Direction[] {Direction.DOWN});
		downOptions.copyOptions(downOptionsNtro);

		SO backwardOptions = graph.defaultSearchOptions();
		InternalSearchOptionsNtro backwardOptionsNtro = new InternalSearchOptionsNtro();
		backwardOptionsNtro.setDirections(new Direction[] {Direction.BACKWARD});
		backwardOptions.copyOptions(backwardOptionsNtro);
		
		graph.nodes().forEach(n -> {

			try {

				if(n.hasSubNodes() && !n.hasParent()) {

					writer.addCluster(nodeSpec(n, options));
					
					// FIXME: we'd like the SubGraph to have a notion of firstNodes()
					VisitedNode<N,E,SO> firstStartSubNode = n.subNodes().findFirst(visitedSubNode -> {

						if(visitedSubNode.node().edges(backwardOptions).isEmpty()) {
							return true;
						}

						return false;

					});
					
					if(firstStartSubNode != null) {
						writeSubNode(options, writer, firstStartSubNode.node());
					}
					
					n.subNodes().forEach(visitedSubNode -> {
						
						N subNode = visitedSubNode.node();
						
						if(firstStartSubNode == null
								|| firstStartSubNode.node() != subNode) {

							writeSubNode(options, writer, subNode);
						}
					});

				}else if(!n.hasSubNodes() && !n.hasParent()){

					writer.addNode(nodeSpec(n, options));
					
				}

			}catch(GraphWriterException e) {
				
				NtroCoreImpl.logger().fatal(e);

			}
		});
	}

	private void writeSubNode(GO options, GraphWriter writer, N subNode) throws GraphWriterException {
		if(subNode.hasSubNodes() && subNode.hasParent()) {

			writer.addSubCluster(nodeSpec(subNode.parent(), options), nodeSpec(subNode, options));

		} else if(!subNode.hasSubNodes() && subNode.hasParent()) {

			writer.addSubNode(nodeSpec(subNode.parent(), options), nodeSpec(subNode, options));
		}
	}

	@Override
	protected EdgeSpecNtro edgeSpec(GenericEdge<N,E,SO> edge, GO options) {
		return new EdgeSpecNtro(nodeSpec(edge.from(), options), edge, nodeSpec(edge.to(), options));
	}

	@Override
	protected void writeEdge(GraphWriter writer, GO options, E edge) {
		if(edge.type().direction() == Direction.DOWN
				|| edge.type().direction() == Direction.UP) {
			return;
		}

		try {
			
			//System.out.println("addEdge: " + edge.from().id().toKey().toString() + " --> " + edge.to().id().toKey().toString());
			
			writer.addEdge(edgeSpec(edge, options));

		} catch (GraphWriterException e) {
			
			NtroCoreImpl.logger().fatal(e);

		}
	}

}
