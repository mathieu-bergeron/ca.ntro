/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.ntro_core_impl.graphs.generics.graph;

import ca.ntro.ntro_core_impl.graphs.common.EdgeId;
import ca.ntro.ntro_core_impl.graphs.common.EdgeIdNtro;
import ca.ntro.ntro_core_impl.graphs.common.EdgeType;
import ca.ntro.ntro_core_impl.identifyers.Key;
import ca.ntro.ntro_core_impl.identifyers.Name;

public class     GenericEdgeNtro<N extends GenericNode<N,E,SO>, 
                          E extends GenericEdge<N,E,SO>,
                          SO extends SearchOptions> 

      implements GenericEdge<N,E,SO> {

	private N from;
	private EdgeType edgeType;
	private N to;

	public N getFrom() {
		return from;
	}

	public void setFrom(N from) {
		this.from = from;
	}

	public EdgeType getEdgeType() {
		return edgeType;
	}

	public void setEdgeType(EdgeType edgeType) {
		this.edgeType = edgeType;
	}

	public N getTo() {
		return to;
	}

	public void setTo(N to) {
		this.to = to;
	}

	public GenericEdgeNtro() {
	}

	public GenericEdgeNtro(N from, EdgeType edgeType, N to) {
		setFrom(from);
		setEdgeType(edgeType);
		setTo(to);
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object o) {
		if(o == this) return true;
		if(o == null) return false;
		if(o instanceof GenericEdge) {
			GenericEdge<N,E,SO> e = (GenericEdge<N,E,SO>) o;

			if(!equals(e.from(), from())) {
				return false;
			}

			if(!equals(e.type(), type())) {
				return false;
			}

			if(!equals(e.to(), to())) {
				return false;
			}
		
			return true;
		}
		
		return false;
	}

	protected boolean equals(Object a, Object b) {
		if(a == null && b != null) {
			return false;
		}
		
		if(a != null && !a.equals(b)) {
			return false;
		}
		
		return true;
	}

	protected boolean equalsUndirected(EdgeType typeA, EdgeType typeB) {
		if(typeA == null && typeB != null) {
			return false;
		}
		
		if(typeA != null && !typeA.equalsUndirected(typeB)) {
			return false;
		}
		
		return true;
	}
	

	@Override
	public EdgeType type() {
		return getEdgeType();
	}

	@Override
	public N from() {
		return getFrom();
	}

	@Override
	public N to() {
		return getTo();
	}

	@Override
	public EdgeId id() {
		String edgeId = "";
		
		edgeId += from().id().toKey().toString();
		edgeId += "_";
		edgeId += type().direction().name();
		edgeId += "_";
		edgeId += type().name().toString();
		edgeId += "_";
		edgeId += to().id().toKey().toString();
		
		return new EdgeIdNtro(new Key(edgeId));
	}

	@Override
	public Name name() {
		return getEdgeType().name();
	}

	@Override
	public boolean equalsUndirected(E edge) {
		return equals(from(), edge.from())
				&& equals(to(), edge.to())
				&& equalsUndirected(type(), edge.type());
	}
}
