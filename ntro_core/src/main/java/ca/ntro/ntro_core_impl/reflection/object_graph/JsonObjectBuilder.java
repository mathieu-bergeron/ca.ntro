/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.ntro_core_impl.reflection.object_graph;

import ca.ntro.core.NtroCore;
import ca.ntro.ntro_core_impl.NtroCoreImpl;
import ca.ntro.ntro_core_impl.json.JsonObject;

public class JsonObjectBuilder extends GenericObjectBuilderNtro<JsonObject> {
	
	
	public JsonObjectBuilder() {
		super();
	}

	public JsonObjectBuilder(ObjectGraphNtro graph) {
		super(graph);
	}

	public JsonObject emptyObjectFromNode(ObjectNode node) {
		JsonObject jsonObject = NtroCore.json().newJsonObject();
		
		jsonObject.put(JsonObject.TYPE_KEY, NtroCoreImpl.reflection().simpleName(node.type()));

		return jsonObject;
	}

	@Override
	protected void setFieldValue(JsonObject object, String fieldName, Object fieldValue) {
		object.put(fieldName, fieldValue);
	}

	@Override
	protected Object newObjectReference(String referencedObjetId, Object referencedObject) {
		JsonObject objectReference = NtroCore.json().newJsonObject();
		
		objectReference.put(JsonObject.REFERENCE_KEY, referencedObjetId);

		return objectReference;
	}
}
