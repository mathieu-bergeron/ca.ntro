/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.ntro_core_impl.reflection.object_graph;

import java.lang.reflect.TypeVariable;
import java.util.List;
import java.util.Map;

import ca.ntro.core.stream.Stream;
import ca.ntro.core.stream.Visitor;
import ca.ntro.ntro_core_impl.NtroCoreImpl;
import ca.ntro.ntro_core_impl.graphs.common.Direction;
import ca.ntro.ntro_core_impl.graphs.common.EdgeType;
import ca.ntro.ntro_core_impl.graphs.common.EdgeTypeNtro;
import ca.ntro.ntro_core_impl.path.Path;
import ca.ntro.ntro_core_impl.stream.StreamNtro;

public abstract class GenericObjectNodeStructureNtro<ON extends ObjectNodeNtro> 

       implements     ObjectNodeStructure {

	private ON node;
	private ObjectGraphNtro graph;
	private boolean isStartNode;

	public boolean getIsStartNode() {
		return isStartNode;
	}

	public void setIsStartNode(boolean isStartNode) {
		this.isStartNode = isStartNode;
	}

	public GenericObjectNodeStructureNtro(ON node, ObjectGraphNtro graph, boolean isStartNode) {
		setNode(node);
		setGraph(graph);
		setIsStartNode(isStartNode);
	}

	public ON getNode() {
		return node;
	}

	public void setNode(ON node) {
		this.node = node;
	}

	public ObjectGraphNtro getGraph() {
		return graph;
	}

	public void setGraph(ObjectGraphNtro graph) {
		this.graph = graph;
	}

	@Override
	public Stream<EdgeType> edgeTypes(Direction direction) {
		return new StreamNtro<EdgeType>() {
			@Override
			public void forEach_(Visitor<EdgeType> visitor) throws Throwable {
				if(direction != Direction.FORWARD) {
					return;
				}

				if(node().isUserDefinedObject()){
					
					_visitEdgeTypesForUserDefinedObject(visitor, node().asUserDefinedObject());

				} else if(node().isList()) {
					
					_visitEdgeTypesForList(visitor, node().asList());
					
				} else if(node().isMap()) {

					_visitEdgeTypesForMap(visitor, (Map<?,?>) node().asMap());

				}
			}
		};
	}

	protected void _visitEdgeTypesForList(Visitor<EdgeType> visitor, List<?> list) throws Throwable {
		for(int i = 0; i < list.size(); i++) {
			visitor.visit(new EdgeTypeNtro(Direction.FORWARD, String.valueOf(i)));
		}
	}

	protected void _visitEdgeTypesForMap(Visitor<EdgeType> visitor, Map<?, ?> map) throws Throwable {
		for(Object key : map.keySet()) {
			String keyString = null;

			if(key instanceof String) {

				keyString = (String) key;

			} else if(key.getClass().isEnum()) {

				keyString = ((Enum<?>)key).name();

			}else {
				
				keyString = key.toString();
				
			}

			visitor.visit(new EdgeTypeNtro(Direction.FORWARD, keyString));
		}
		
	}

	protected abstract void _visitEdgeTypesForUserDefinedObject(Visitor<EdgeType> visitor, Object object) throws Throwable;

	@Override
	public Stream<ReferenceEdge> edges(EdgeType edgeType) {
		return new StreamNtro<ReferenceEdge>(){

			@Override
			public void forEach_(Visitor<ReferenceEdge> visitor) throws Throwable {
				if(edgeType.direction() != Direction.FORWARD) {
					return;
				}

				if(node().isList()) {
					
					_visitEdgesByTypeForList(edgeType, node().asList(), visitor);

					
				} else if(node().isMap()) {

					_visitEdgesByTypeForMap(edgeType, node().asMap(), visitor);

					
				}else if(node().isUserDefinedObject()){

					_visitEdgesByTypeForUserDefinedObject(edgeType, node().asUserDefinedObject(), visitor);
				}
				
			}
		};
	}

	protected void _visitEdgesByTypeForList(EdgeType edgeType, 
			                              List<?> list, 
			                              Visitor<ReferenceEdge> visitor) throws Throwable {

		String attributeName = edgeType.name().toString();
		
		Integer index = Integer.parseInt(attributeName);

		Object attributeValue = list.get(index);
		
		_visitAttributeEdge(attributeName, attributeValue, visitor);
	}

	protected void _visitAttributeEdge(String attributeName, Object attributeValue, Visitor<ReferenceEdge> visitor) throws Throwable {

		Path attributePath = Path.fromRawPath(this.node().id().toKey().toString());
		attributePath.addName(attributeName);
		
		ObjectGraphStructureNtro graphStructure = (ObjectGraphStructureNtro) getGraph().graphStructure();
		
		ObjectNode toNode = graphStructure.getLocalHeap().findOrCreateNode(getGraph(), attributePath, attributeValue, false);
		ReferenceEdge edge = new ReferenceEdgeNtro(this.node(), attributeName, toNode);
		
		visitor.visit(edge);
	}

	protected void _visitEdgesByTypeForMap(EdgeType edgeType, 
			                             Map<?, ?> map, 
			                             Visitor<ReferenceEdge> visitor) throws Throwable {

		String keyString = edgeType.name().toString();
		Object key = keyString;
		Class<?> keyType = String.class;
		
		Object keySample = null;
		for(Map.Entry<?, ?> entry : map.entrySet()) {
			keySample = entry.getKey();
			break;
		}
		
		if(keySample != null) {
			keyType = keySample.getClass();
		}
		
		if(keyType.isEnum()) {
			key = Enum.valueOf((Class<? extends Enum>)keyType, keyString);
		}
		
		Object attributeValue = map.get(key);
		
		_visitAttributeEdge(keyString, attributeValue, visitor);
		
	}

	protected abstract void _visitEdgesByTypeForUserDefinedObject(EdgeType edgeType, 
			                                                      Object object, 
			                                                      Visitor<ReferenceEdge> visitor) throws Throwable;

	@Override
	public boolean isStartNode() {
		return getIsStartNode();
	}


	@Override
	public ObjectNode node() {
		return getNode();
	}

	@Override
	public ObjectGraph parentGraph() {
		return getGraph();
	}

}
