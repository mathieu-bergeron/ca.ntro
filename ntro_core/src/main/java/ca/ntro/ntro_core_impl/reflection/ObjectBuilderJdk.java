/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.ntro_core_impl.reflection;

import ca.ntro.core.NtroCore;
import ca.ntro.ntro_core_impl.NtroCoreImpl;
import ca.ntro.ntro_core_impl.reflection.object_graph.ObjectBuilderNtro;
import ca.ntro.ntro_core_impl.reflection.object_graph.ObjectGraphNtro;

public class ObjectBuilderJdk extends ObjectBuilderNtro {

	public ObjectBuilderJdk(ObjectGraphNtro graph) {
		super(graph);
	}

	protected void setAttributeUsingSetter(Object object, String attributeName, Object attributeValue) {
		String setterName = ReflectionUtils.setterNameFromAttributeName(attributeName);
		
		NtroCoreImpl.reflection().invokeSetter(object, setterName, attributeValue);
	}

	@Override
	protected void setFieldValue(Object object, String fieldName, Object fieldValue) {
		NtroCoreImpl.reflection().setFieldValue(object, fieldName, fieldValue);
	}



}
