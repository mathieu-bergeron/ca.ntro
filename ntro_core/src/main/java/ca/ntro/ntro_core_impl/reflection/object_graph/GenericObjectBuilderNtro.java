/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.ntro_core_impl.reflection.object_graph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ca.ntro.core.NtroCore;
import ca.ntro.ntro_core_impl.NtroCoreImpl;
import ca.ntro.ntro_core_impl.json.JsonObject;

public abstract class GenericObjectBuilderNtro<O> {

	private ObjectGraphNtro graph;
	private Map<String, Object> localHeap = new HashMap<>();


	public ObjectGraphNtro getGraph() {
		return graph;
	}

	public void setGraph(ObjectGraphNtro graph) {
		this.graph = graph;
	}

	public Map<String, Object> getLocalHeap() {
		return localHeap;
	}

	public void setLocalHeap(Map<String, Object> localHeap) {
		this.localHeap = localHeap;
	}

	public GenericObjectBuilderNtro() {
	}

	public GenericObjectBuilderNtro(ObjectGraphNtro graph) {
		setGraph(graph);
	}
	
	public void initialize() {
	}
	
	public O build() {
		ObjectNode startNode = getGraph().startNodes().findFirst(n -> true);

		return objectFromNode(startNode);
	}
	
	protected O objectFromNode(ObjectNode node) {
		
		String objectId = node.id().toKey().toString();

		if(getLocalHeap().containsKey(objectId)) {
			return (O) newObjectReference(objectId, getLocalHeap().get(objectId));
		}

		O object = emptyObjectFromNode(node);
		getLocalHeap().put(objectId, object);
		
		Set<String> initializedAttributes = new HashSet<>();
		
		node.edges().forEach(edge -> {
			
			String attributeName = edge.type().name().toString();
			
			
			if(!attributeName.equals(JsonObject.REFERENCE_KEY)
					&& !attributeName.equals(JsonObject.TYPE_KEY)) {

				initializedAttributes.add(attributeName);

				Object attributeValue = valueFromNode(edge.to());
				setFieldValue(object, attributeName, attributeValue);
			}
		});
		
		NtroCoreImpl.reflection().graphFromObject(object).startNode().edges().forEach(attributeEdge -> {
			String attributeName = attributeEdge.name().toString();

			if(!attributeName.equals(JsonObject.REFERENCE_KEY)
					&& !attributeName.equals(JsonObject.TYPE_KEY)
					&& !initializedAttributes.contains(attributeName)) {

				NtroCoreImpl.logger().fatal("could not find initial value for attribute " + attributeEdge.name().toString());
			}
		});

		if(object instanceof Initialize) {
			((Initialize) object).initialize();
		}
		
		return object;
	}

	protected Object valueFromNode(ObjectNode node) {
		String objectId = node.id().toKey().toString();
		if(getLocalHeap().containsKey(objectId)) {
			return newObjectReference(objectId, getLocalHeap().get(objectId));
		}
		
		if(isReferenceNode(node)) {
			return valueFromNode(getReferencedNode(node));
		}
		
		Object value = null;
		
		if(node.isUserDefinedObject()) {

			value = objectFromNode(node);
			
		} else if(node.isList()){
			
			value = buildList(node);

		}else if(node.isMap()) {

			value = buildMap(node);

		}else if(node.isSimpleValue()) {

			value = nodeValue(node.asSimpleValue());
			
		}else {

			value = node.object();

		}

		getLocalHeap().put(node.id().toKey().toString(), value);
		
		return value;
	}

	private Object nodeValue(ObjectNodeSimpleValue node) {
		Object result = node.object();

		if(node.isClass()) {
			
			result = NtroCore.reflection().simpleName((Class<?>)node.object());
			
		}
		
		return result;
	}

	protected boolean isReferenceNode(ObjectNode node) {
		boolean isReferenceNode = false;
		
		if(node.isMap()
				&& node.asMap().containsKey(JsonObject.REFERENCE_KEY)) {
			
			isReferenceNode = true;
		}
		
		return isReferenceNode;
	}

	protected ObjectNode getReferencedNode(ObjectNode objectReferenceNode) {

		String referencedObjectId = (String) objectReferenceNode.asMap().get(JsonObject.REFERENCE_KEY);
		
		ObjectNode referencedNode = getGraph().findNode(referencedObjectId);

		return referencedNode;
	}
	
	
	protected abstract Object newObjectReference(String referencedObjetId, Object referencedObject);

	private List<Object> buildList(ObjectNode listNode){
		List<Object> list = new ArrayList<>();

		String listId = listNode.id().toKey().toString();
		getLocalHeap().put(listId, list);

		listNode.edges().forEach(edge -> {
			
			String indexName = edge.type().name().toString();
			int index = Integer.valueOf(indexName);
			Object item = valueFromNode(edge.to());
			
			list.add(index, item);
		});
		
		return list;
	}

	private Map<String,Object> buildMap(ObjectNode mapNode){
		Map<String,Object> map = new HashMap<>();

		String mapId = mapNode.id().toKey().toString();
		getLocalHeap().put(mapId, map);
		
		mapNode.edges().forEach(edge -> {
			
			String key = edge.type().name().toString();
			Object value = valueFromNode(edge.to());
			
			map.put(key, value);
		});
		
		return map;
	}
	

	protected abstract O emptyObjectFromNode(ObjectNode node);
	protected abstract void setFieldValue(O object, String fieldName, Object fieldValue);


}
