/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)


This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.core;

import ca.ntro.core.locale.Locale;
import ca.ntro.core.options.CoreOptions;
import ca.ntro.core.services.Asserter;
import ca.ntro.core.services.Collections;
import ca.ntro.core.services.ExitHandler;
import ca.ntro.core.services.FactoryService;
import ca.ntro.core.services.JsonService;
import ca.ntro.core.services.LocaleService;
import ca.ntro.core.services.Logger;
import ca.ntro.core.services.RandomService;
import ca.ntro.core.services.ReflectionService;
import ca.ntro.core.services.StorageService;
import ca.ntro.core.services.ThreadService;
import ca.ntro.core.services.Time;
import ca.ntro.ntro_core_impl.NtroCoreImpl;

public class NtroCore {

	public static FactoryService factory(){
		return NtroCoreImpl.factory();
	}

	public static Collections collections(){
		return NtroCoreImpl.collections();
	}

	public static JsonService json(){
		return NtroCoreImpl.json();
	}

	public static StorageService storage(){
		return NtroCoreImpl.storage();
	}

	public static ThreadService threads(){
		return NtroCoreImpl.threads();
	}

	public static Logger logger(){
		return NtroCoreImpl.logger();
	}

	public static void exit(ExitHandler exitHandler) {
		NtroCoreImpl.exitService().exit(exitHandler);
	}

	public static void exit() {
		NtroCoreImpl.exitService().exit(() -> {});
	}

	public static Time time(){
		return NtroCoreImpl.time();
	}

	public static LocaleService locale() {
		return NtroCoreImpl.locale();
	}

	public static Asserter asserter(){
		return NtroCoreImpl.asserter();
	}

	public static RandomService random(){
		return NtroCoreImpl.random();
	}

	public static CoreOptions options(){
		return NtroCoreImpl.options();
	}

	public static ReflectionService reflection(){
		return NtroCoreImpl.reflection();
	}

}
