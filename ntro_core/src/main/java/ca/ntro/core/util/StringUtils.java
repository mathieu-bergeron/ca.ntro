/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.core.util;

import java.util.List;

public class StringUtils {
	
	public static boolean isNullOrEmpty(String string) {
		return string == null || string.isEmpty() || string.isBlank();
	}
	

	public static String fromArray(Object[] array, String delimiter){
		StringBuilder builder = new StringBuilder();
		
		if(array.length > 0) {
			builder.append(array[0]);
		}
		
		for(int i = 1; i < array.length; i++) {
			builder.append(delimiter);
			builder.append(array[i]);
			
		}

		return builder.toString();
	}

	public static String fromArray(Object[] array){
		return fromArray(array, "");
	}

	public static String fromList(List<Object> list, String delimiter){
		Object[] array = ArrayUtils.fromList(list);

		return fromArray(array, delimiter);
	}

	public static String fromList(List<Object> list){
		return fromList(list, "");
	}
	
	public static String backslashesToForwardSlashes(String string) {
        return string.replaceAll("\\\\","/");
	}

	public static String removeDoubledForwardSlashes(String string) {
		for(int i = 0; i < 100; i++) {
			if(string.contains("/")) {
				
				string = string.replaceAll("//", "/");
				
			}else {
				break;
			}
		}
		
		return string;
	}

	public static String windowsPathToUnix(String string) {
		return removeDoubledForwardSlashes(backslashesToForwardSlashes(string));
	}

}
