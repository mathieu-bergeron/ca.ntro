/*
Copyright (C) (2020) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)

This file is part of Ntro, an application framework designed with teaching in mind.

This is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/
package ca.ntro.core.services;

import ca.ntro.ntro_core_impl.values.CodeLocation;

public interface Logger {
	
	void taskExecutes(String taskId);
	void info(String message);
	void value(Object value);
	void run();

	void error(String message);
	void exception(Throwable t);

	void fatal(Throwable t);
	void fatal(String message);
	void fatal(String message, Throwable t);
	
	void setErrorDialogEnabled(boolean isEnabled);

	void warning(String string);

}
