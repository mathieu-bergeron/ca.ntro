package ca.ntro.core.services;

public interface LastModifiedLambda {
	
	void onLastModified(long lastModified);

}
