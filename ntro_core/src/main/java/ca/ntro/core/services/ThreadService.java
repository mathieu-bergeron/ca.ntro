package ca.ntro.core.services;

public interface ThreadService {

	String currentThreadId();
	
	void runOnMainThread(Runnable runnable);
	void runInWorkerThread(Runnable runnable);

}
