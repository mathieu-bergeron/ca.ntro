package ca.ntro.core.options;

public interface CoreOptions {

	boolean displayWarnings();
	boolean displayTasks();

	String projectPath();

	String srcPath();
		String javaPath();
		String resourcesPath();

	String storagePath();
		String modelsPath();
		String tmpPath();
		String sessionsPath();
		String optionsPath();
		String graphsPath();

}
