package ca.ntro.core.options;

public interface CoreOptionsWriter {

	void projectPath(String projectPath);

	void resourcesPath(String resourcesPath);

	void displayWarnings(boolean displayWarnings);
	void displayTasks(boolean displayTasks);

}
