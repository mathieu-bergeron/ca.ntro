package ca.ntro.ntro_core_abstr;

import ca.ntro.core.options.CoreOptionsWriter;

public interface SetCoreOptionsLambda {
	
	void setCoreOptions(CoreOptionsWriter coreOptions);

}
