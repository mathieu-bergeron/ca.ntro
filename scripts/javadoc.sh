# Copyright (C) (2022) (Mathieu Bergeron) (mathieu.bergeron@cmontmorency.qc.ca)
#
# This file is part of Ntro
#
# Ntro is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Ntro is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with aquiletour.  If not, see <https://www.gnu.org/licenses/>

##### INCLUDE #####
this_dir=$(readlink -f "$0")
scripts_dir=$(dirname "$this_dir")
. "$scripts_dir/include.sh"
###################

exclude_from_javadoc(){
    pattern=$1

    cat javadoc.options | grep -v "$1" > tmp.options
    mv tmp.options javadoc.options
}

save_dir

cd "$root_dir"

if [ ! -e "$javadoc_dir" ]; then
    mkdir "$javadoc_dir"
fi

sh gradlew ntro_app_fx:javadoc

cp ntro_app_fx/build/tmp/javadoc/javadoc.options "$javadoc_dir"/

cd "$javadoc_dir"

exclude_from_javadoc "ntro_core_impl"
exclude_from_javadoc "ntro_core_abstr"
exclude_from_javadoc "ntro_app_fx_impl"
exclude_from_javadoc "ntro_app_fx_abstr"
exclude_from_javadoc "SessionAccessor.java"
exclude_from_javadoc "MessageAccessor.java"
exclude_from_javadoc "EventAccessor.java"

sed "s&-d '/home/mbergeron/ca.ntro/4f5/ntro_4f5/ntro_app_fx/build/docs/javadoc'&-d 'result'&" -i javadoc.options

javadoc @javadoc.options


restore_dir

