# Version avec SQL?


## Dorsal

1. Tâches pour charger le modèle

```java
.receives(selector(SelecteurFileAttente.class))
.loads(model(ModelFileAttente.class))
.executesAndReturns(inputs -> {

    SelecteurFileAttente selecteur = inputs.get(selector(SelecteurFileAttente.class);
    ModeleFileAttente    modele    = new ModeleFileAttente();

    // charger les données dans le modèle
    // à partir de la DB avec une requête SQL

    return modele;

})
```

1. Tâches pour modifier le modèle

```java
.receives(msg(MsgAjouterRendezVous.class))          // ou: MsgAjouterRendezVous est un sélecteur
.executes(inputs -> {

    MsgAjouterRendezVous msgAjouterRendezVous  = inputs.get(msg(MsgAjouterRendezVous.class));
    
    // requête SQL qui modifie les données
    // le MsgAjouterRendezVous agit comme le sélecteur
    msgAjouterRendezVous.executerSql();

    // autre option:
    // charger le modèle (MsgAjouterRendezVous est un SelecteurFileAttente)
    // et exécuter du code pour modifier le modèle
    // ensuite il faut resauvegarder (ce n'est pas automatique)
    msgAjouterRendezVous.executer();


    // doit automatiquement re-exécuter les tâches qui recharge les modèles
    // (à commencer par celle qui correspond au sélecteur)

    // option plus performante: décider quels modèles recharger

})
```

## Frontal

1. Dynamiquement changer le sélecteur dans la session

```java
session.setModelSelector(ModeleFileAttente.class, new SelecteurFileAttente().setId("asdf"));
```

1. Tâche pour observer selon un sélecteur

```java
.waitsFor(modified(ModeleFileAttente.class))
.waitsFor(created(VueFileAttente.class))
.executes(inputs -> {

    VueFileAttente    vueFileAttente = inputs.get(created(VueFileAttente.class));
    ModeleFileAttente fileAttente    = inputs.get(modified(ModeleFileAttente.class));

    fileAttente.afficherSur(vueFileAttente);
})
```
